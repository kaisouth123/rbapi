﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.Core.Framework.Interfaces;


namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ChargeContext : BaseContext<ChargeContext>, IChargeContext, IContext
    {
        public IDbSet<ChargeTransactionsAux> ChargeTransactionsAuxes { get; set; }

        public ChargeContext()
            : base("BridgePayDatabase")
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        public ChargeContext(string connstring)
            : base(connstring)
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add<ChargeTransactionsAux>(new ChargeTransactionsAuxMap());

            base.OnModelCreating(modelBuilder);
        }

        public void SetAdded(IObjectWithState entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(IObjectWithState entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void SetAdded(object entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void ApplyStateChanges()
        {
            this.ApplyStateChanges();
        }

        public void SetCredentials(string user, string password)
        {
            return;
        }

        public void SetState(IObjectWithState entity)
        {
            Entry(entity).State = StateHelpers.ConvertState(entity.ObjectState);
        }
    }
}
