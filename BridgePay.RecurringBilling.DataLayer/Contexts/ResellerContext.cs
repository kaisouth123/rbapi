﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.RecurringBilling.DataLayer.Helpers;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public partial class ResellerContext : BaseContext<ResellerContext>,IContext, IResellerContext
    {
        public DbSet<Reseller> Resellers { get; set; }
        public DbSet<MerchantAccount> MerchantAccounts { get; set; }
        public DbSet<UnipayMerchant> Merchants { get; set; }
        static ResellerContext()
        {
            Database.SetInitializer<ResellerContext>(null);
        }

        public ResellerContext()
            : base("BridgePayDatabase")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add<Reseller>(new ResellerMap());
            modelBuilder.Configurations.Add<MerchantAccount>(new MerchantAccountMap());
            modelBuilder.Configurations.Add<UnipayMerchant>(new UnipayMerchantMap());
            base.OnModelCreating(modelBuilder);
        }

        public void SetState(IObjectWithState entity)
        {
            throw new NotImplementedException();
        }

        public void SetAdded(object entity)
        {
            throw new NotImplementedException();
        }

        public void ApplyStateChanges()
        {
            foreach (var entry in this.ChangeTracker.Entries<IObjectWithState>())
            {
                IObjectWithState stateInfo = entry.Entity;
                entry.State = StateHelpers.ConvertState(stateInfo.ObjectState);

            }

        }

        public void SetCredentials(string Reseller, string password)
        {
            return;
        }

        public void SetServiceCredentials(string Reseller)
        {
            throw new NotImplementedException();
        }
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
