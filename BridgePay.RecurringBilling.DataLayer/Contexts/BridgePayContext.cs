﻿using System;
using System.Data.Entity;
using Bridgepay.Core.DataLayer;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Contexts
{
    public class BridgePayContext : BaseDBContext<BridgePayContext>, IBridgePayContext
    {
        public IDbSet<OrganizationMap> OrganizationMaps { get; set; }

        static BridgePayContext()
        {
            Database.SetInitializer<BridgePayContext>(null);
        }

        public BridgePayContext() : base("BridgePayDatabase")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new OrganizationMapMapper());
            base.OnModelCreating(modelBuilder);
        }

        public void SetState(IObjectWithState entity)
        {
            Entry(entity).State = Bridgepay.Core.DataLayer.Helpers.StateHelpers.ConvertState(entity.ObjectState);
        }

        public void SetAdded(object entity)
        {
            throw new NotImplementedException();
        }

        public void ApplyStateChanges()
        {
            foreach (var entry in this.ChangeTracker.Entries<IObjectWithState>())
            {
                IObjectWithState stateInfo = entry.Entity;
                entry.State = StateHelpers.ConvertState(stateInfo.ObjectState);
            }

        }

        public void SetCredentials(string user, string password)
        {
            return;
        }

        public void SetServiceCredentials(string user)
        {
            throw new NotImplementedException();
        }

    }
}
