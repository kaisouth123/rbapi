﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.Core.Framework;

using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.RecurringBilling.DataLayer.Helpers;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class VendorContext : BaseContext<VendorContext>, IContext, IVendorContext
   {
        public IDbSet<Account> Accounts { get; set; }
        public IDbSet<Address> Addresses { get; set; }
        public IDbSet<BillingFrequency> BillingFrequencies { get; set; }
        public IDbSet<Contact> Contacts { get; set; }
        public IDbSet<Contract> Contracts { get; set; }
        public IDbSet<ContactType> ContactTypes { get; set; }
        public IDbSet<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        public IDbSet<Interval> Intervals { get; set; }
        public IDbSet<Product> Products { get; set; }
        public IDbSet<ScheduledPayment> ScheduledPayments { get; set; }
        public IDbSet<ContractCustomField> ContractCustomFields { get; set; }


        public VendorContext()
        {
#if DEBUG
            Database.Log = l => System.Diagnostics.Debug.WriteLine(l);
#endif
        }

        public VendorContext(string connString)
            : base(connString)
        {
#if DEBUG
            Database.Log = l => System.Diagnostics.Debug.WriteLine(l);
#endif
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add<Account>(new AccountMap());
            modelBuilder.Configurations.Add<Address>(new AddressMap());
            modelBuilder.Configurations.Add<BillingFrequency>(new BillingFrequencyMap());
            modelBuilder.Configurations.Add<Contact>(new ContactMap());
            modelBuilder.Configurations.Add<ContactType>(new ContactTypeMap());
            modelBuilder.Configurations.Add<Contract>(new ContractMap());
            modelBuilder.Configurations.Add<FinancialResponsibleParty>(new FinancialResponsiblePartyMap());
            modelBuilder.Configurations.Add<Interval>(new IntervalMap());
            modelBuilder.Configurations.Add<Product>(new ProductMap());
            modelBuilder.Configurations.Add<ScheduledPayment>(new ScheduledPaymentMap());
            modelBuilder.Configurations.Add<ContractCustomField>(new ContractCustomFieldMap());
            modelBuilder.Configurations.Add<PaymentMethod>(new PaymentMethodMap());
            modelBuilder.Configurations.Add<Wallet>(new WalletMap());
            modelBuilder.Ignore<ContractBalance>();
            modelBuilder.Entity<Contract>().Ignore(x => x.ContractCustomFields);
            base.OnModelCreating(modelBuilder);
        }

        public void SetAdded(IObjectWithState entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(IObjectWithState entity)
        {
            Entry(entity).State = EntityState.Modified;
        }


        public void SetAdded(object entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void SetCredentials(string user, string password)
        {
            return;//not used atm
        }

        public void SetState(IObjectWithState entity)
        {
            Entry(entity).State = Bridgepay.Core.DataLayer.Helpers.StateHelpers.ConvertState(entity.ObjectState);
        }

        public void ApplyStateChanges()
        {
            base.ApplyStateChanges();
        }
   }
}

