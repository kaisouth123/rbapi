﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.Core.Framework.Interfaces;


namespace Bridgepay.RecurringBilling.DataLayer
{
    public class AdminContext : BaseContext<AdminContext>, IContext
    {
        public IDbSet<Account> Accounts { get; set; }
        public IDbSet<Address> Addresses { get; set; }
        public IDbSet<BillingFrequency> BillingFrequencies { get; set; }
        public IDbSet<ChargeTransactionsAux> ChargeTransactionAuxs { get; set; }
        public IDbSet<Contact> Contacts { get; set; }
        public IDbSet<Contract> Contracts { get; set; }
        public IDbSet<ContactType> ContractTypes { get; set; }
        public IDbSet<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        public IDbSet<Interval> Intervals { get; set; }
        public IDbSet<Product> Products { get; set; }
        public IDbSet<ScheduledPayment> ScheduledPayments { get; set; }
        public IDbSet<Merchant> Merchants { get; set; }

        static AdminContext()
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add<Account>(new AccountMap());
            modelBuilder.Configurations.Add<Address>(new AddressMap());
            modelBuilder.Configurations.Add<BillingFrequency>(new BillingFrequencyMap());
            modelBuilder.Configurations.Add<ChargeTransactionsAux>(new ChargeTransactionsAuxMap());
            modelBuilder.Configurations.Add<Contact>(new ContactMap());
            modelBuilder.Configurations.Add<ContactType>(new ContactTypeMap());
            modelBuilder.Configurations.Add<Contract>(new ContractMap());
            modelBuilder.Configurations.Add<FinancialResponsibleParty>(new FinancialResponsiblePartyMap());
            modelBuilder.Configurations.Add<Interval>(new IntervalMap());
            modelBuilder.Configurations.Add<Merchant>(new MerchantMap());
            modelBuilder.Configurations.Add<Product>(new ProductMap());
            modelBuilder.Configurations.Add<ScheduledPayment>(new ScheduledPaymentMap());
            modelBuilder.Configurations.Add<PaymentMethod>(new PaymentMethodMap());
            modelBuilder.Configurations.Add<Wallet>(new WalletMap());
            modelBuilder.Ignore<ContractBalance>();

            base.OnModelCreating(modelBuilder);
        }

    }
}
