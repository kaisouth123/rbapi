﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.Core.Framework.Interfaces;


namespace Bridgepay.RecurringBilling.DataLayer
{
    public class MerchantContext : BaseContext<MerchantContext>, IMerchantContext, IContext
    {
        public IDbSet<Merchant> Merchants { get; set; }

        public MerchantContext()
            : base("BridgePayDatabase")
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        public MerchantContext(string connString)
            : base(connString)
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add<Merchant>(new MerchantMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
