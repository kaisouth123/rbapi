﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.RecurringBilling.DataLayer.Helpers;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public partial class UserContext : BaseContext<UserContext>,IContext, IUserContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserEmail> UserEmails { get; set; }
        public DbSet<UserName> UserNames { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserAddress> UserAddresses { get; set; }
        public DbSet<Address> Address { get; set; }
        static UserContext()
        {
            Database.SetInitializer<UserContext>(null);
        }

        public UserContext()
            : base("MyBridgePay")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserNameMap());
            modelBuilder.Configurations.Add(new UserEmailMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserAddressMap());
            modelBuilder.Configurations.Add(new AddressMap());
            base.OnModelCreating(modelBuilder);
        }

        public void SetState(IObjectWithState entity)
        {
            throw new NotImplementedException();
        }

        public void SetAdded(object entity)
        {
            throw new NotImplementedException();
        }

        public void ApplyStateChanges()
        {
            foreach (var entry in this.ChangeTracker.Entries<IObjectWithState>())
            {
                IObjectWithState stateInfo = entry.Entity;
                entry.State = StateHelpers.ConvertState(stateInfo.ObjectState);

            }

        }

        public void SetCredentials(string user, string password)
        {
            return;
        }

        public void SetServiceCredentials(string user)
        {
            throw new NotImplementedException();
        }
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
