﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.Core.DataLayer.Helpers;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Mappers;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public partial class CustomFieldContext : BaseDBContext<CustomFieldContext>, ICustomFieldContext
    {
        public DbSet<CustomField> CustomFields { get; set; }
        public DbSet<CustomFieldType> CustomFieldTypes { get; set; }
        public DbSet<CustomFieldActivation> CustomFieldActivations { get; set; }
        public DbSet<CustomizableEntity> CustomizableEntities { get; set; }
        public DbSet<ContractCustomField> ContractCustomFields { get; set; }
        public DbSet<CustomFieldData> CustomFieldDatas { get; set; }
        static CustomFieldContext()
        {
            Database.SetInitializer<CustomFieldContext>(null);
        }

        public CustomFieldContext()
            : base("BridgePayDatabase")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CustomFieldMap());
            modelBuilder.Configurations.Add(new CustomFieldTypeMap());
            modelBuilder.Configurations.Add(new CustomFieldActivationMap());
            modelBuilder.Configurations.Add(new CustomizableEntityMap());
            modelBuilder.Configurations.Add(new CustomFieldDataMap());
            modelBuilder.Configurations.Add(new ContractCustomFieldMap());
            modelBuilder.Entity<CustomField>().Ignore(x => x.EntityName);
            //modelBuilder.Entity<ContractCustomField>().Ignore(x => x.CustomFieldConfigId);
            base.OnModelCreating(modelBuilder);
        }

        public void SetState(IObjectWithState entity)
        {
            Entry(entity).State = Bridgepay.Core.DataLayer.Helpers.StateHelpers.ConvertState(entity.ObjectState);
        }

        public void SetAdded(object entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }
        public void ApplyStateChanges()
        {
            foreach (var entry in this.ChangeTracker.Entries<IObjectWithState>())
            {
                IObjectWithState stateInfo = entry.Entity;
                entry.State = StateHelpers.ConvertState(stateInfo.ObjectState);
            }
        }

        public void SetCredentials(string user, string password)
        {
            return;
        }

        public void SetServiceCredentials(string user)
        {
            throw new NotImplementedException();
        }
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
