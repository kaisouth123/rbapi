﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using Bridgepay.Core.Framework.Interfaces;


namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomerContext : BaseContext<CustomerContext>, IContext, ICustomerContext
    {
        public IDbSet<Account> Accounts { get; set; }
        public IDbSet<Product> Products { get; set; }
        public IDbSet<CustomerAddress> Addresses { get; set; }
        public IDbSet<BillingFrequency> BillingFrequencies { get; set; }
        public IDbSet<ChargeTransactionsAux> ChargeTransactionAuxs { get; set; }
        public IDbSet<Customer> Customers { get; set; }
        public IDbSet<ContactType> ContactTypes { get; set; }
        public IDbSet<Contract> Contracts { get; set; }
        public IDbSet<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        public IDbSet<Interval> Intervals { get; set; }
        public IDbSet<ScheduledPayment> ScheduledPayments { get; set; }

        public CustomerContext()
        {

        }

        public CustomerContext(string connString) : base(connString)
        {

        }

        public void SetAdded(IObjectWithState entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(IObjectWithState entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add<Account>(new AccountMap());
            modelBuilder.Configurations.Add<Product>(new ProductCustMap());
            modelBuilder.Configurations.Add<CustomerAddress>(new CustomerAddressMap());
            modelBuilder.Configurations.Add<BillingFrequency>(new BillingFrequencyMap());
            modelBuilder.Configurations.Add<ChargeTransactionsAux>(new ChargeTransactionsAuxMap());
            modelBuilder.Configurations.Add<Customer>(new CustomerMap());
            modelBuilder.Configurations.Add<ContactType>(new ContactTypeMap());
            modelBuilder.Configurations.Add<Contract>(new ContractMap());
            modelBuilder.Configurations.Add<FinancialResponsibleParty>(new FinancialResponsibleCustomerMap());
            modelBuilder.Configurations.Add<Interval>(new IntervalMap());
            modelBuilder.Configurations.Add<ScheduledPayment>(new ScheduledPaymentMap());
            modelBuilder.Ignore<ContractBalance>();

            base.OnModelCreating(modelBuilder);
        }

        public void SetAdded(object entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void SetCredentials(string user, string password)
        {
            return;
        }

        public void SetState(IObjectWithState entity)
        {
            Entry(entity).State = StateHelpers.ConvertState(entity.ObjectState);
        }
    }
}
