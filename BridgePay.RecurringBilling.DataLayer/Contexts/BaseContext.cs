﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Linq.Expressions;
using Bridgepay.Core.Framework;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.Core.Cryptography;
using System.Configuration;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class BaseContext<TContext> : DbContext where TContext : DbContext
    {
        static BaseContext()
        {
            Database.SetInitializer<TContext>(null);
        }

        protected BaseContext(string ConnectionString = "RecurringBillingEncrypt")
            : base(DecryptedConnectionString(ConnectionString))
        {
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        private static string DecryptedConnectionString(string connectionName)
        {
            SimpleAES crypto = new SimpleAES();
            return crypto.DecryptString(ConfigurationManager.ConnectionStrings[connectionName].ConnectionString);
        }

        public void SetAdded(IObjectWithState entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(IObjectWithState entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void SetAdded(object entity)
        {
            Entry(entity).State = EntityState.Added;
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void ApplyStateChanges()
        {
            this.ApplyStateChange();
        }

        public void SetCredentials(string user, string password)
        {
            return;
        }

        public void SetState(IObjectWithState entity)
        {
            Entry(entity).State = StateHelpers.ConvertState(entity.ObjectState);
        }

        protected override void Dispose(bool disposing)
        {
            //Configuration.LazyLoadingEnabled = false;
            base.Dispose(disposing);
        }
    }
}
