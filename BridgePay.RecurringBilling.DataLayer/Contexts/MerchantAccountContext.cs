﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Mappers;
using Bridgepay.RecurringBilling.DataLayer.Helpers;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public partial class MerchantAccountContext : BaseContext<MerchantAccountContext>,IContext, IMerchantAccountContext
    {
        public DbSet<MerchantAccount> MerchantAccounts { get; set; }
        static MerchantAccountContext()
        {
            Database.SetInitializer<MerchantAccountContext>(null);
        }

        public MerchantAccountContext()
            : base("BridgePayDatabase")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new MerchantAccountMap());     
            base.OnModelCreating(modelBuilder);
        }

        public void SetState(IObjectWithState entity)
        {
            throw new NotImplementedException();
        }

        public void SetAdded(object entity)
        {
            throw new NotImplementedException();
        }

        public void ApplyStateChanges()
        {
            foreach (var entry in this.ChangeTracker.Entries<IObjectWithState>())
            {
                IObjectWithState stateInfo = entry.Entity;
                entry.State = StateHelpers.ConvertState(stateInfo.ObjectState);

            }

        }

        public void SetCredentials(string MerchantAccount, string password)
        {
            return;
        }

        public void SetServiceCredentials(string MerchantAccount)
        {
            throw new NotImplementedException();
        }
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
