﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public enum SQLReportType
    {
        GetBatchList,
        GetBatchReportResults,
        GetBatchSummaryReportResults,
        GetDeclinesReportResults,
        GetFilteredTransactionList,
        GetMyTransactionList,
        GetSalesReportResults
    }
}
