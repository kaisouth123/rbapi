﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class UnitOfWork<TContext> : IUnitOfWork, IDisposable where TContext : IContext, new()
    {
        private readonly IContext _context;

        public UnitOfWork()
        {
            _context = new TContext();
        }

        public UnitOfWork(IContext context)
        {
            _context = context;
        }

        public IContext Context 
        { 
            get { return (TContext) _context;}
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        public int Save()
        {
            return _context.SaveChanges();
        }
    }
}
