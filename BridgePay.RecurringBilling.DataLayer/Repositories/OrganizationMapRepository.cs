﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Bridgepay.Core.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using IUnitOfWork = Bridgepay.Core.DataLayer.Interfaces.IUnitOfWork;

namespace Bridgepay.RecurringBilling.DataLayer.Repositories
{
    public class OrganizationMapRepository : Core.DataLayer.Interfaces.IRepository<OrganizationMap>
    {
        private readonly IBridgePayContext context;

        public OrganizationMapRepository(IUnitOfWork uow)
        {
            context = uow.Context as IBridgePayContext;
        }

        public IQueryable<OrganizationMap> All
        {
            get { return context.OrganizationMaps; }
        }

        public IQueryable<OrganizationMap> AllIncluding(
            params Expression<Func<OrganizationMap, object>>[] includeProperties)
        {
            IQueryable<OrganizationMap> query = context.OrganizationMaps;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }


        public void FilterRequest(Core.Interfaces.IFilter<OrganizationMap> filter)
        {
            //var query = from wallet in AllIncluding(t => t.PaymentMethods)

        }

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public void InsertOrUpdate(OrganizationMap entity)
        {
            if (entity.ObjectState == Core.Framework.ObjectState.Added)
            {
                context.OrganizationMaps.Add(entity);
            }
            else
            {
                context.SetState(entity);
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public OrganizationMap Find(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<OrganizationMap> FindByFilter(Core.Interfaces.IFilter<OrganizationMap> filter)
        {
            throw new NotImplementedException();
        }

        public OrganizationMap FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public void SetSaveParams(Type type, ISaveParams saveParams)
        {
            throw new NotImplementedException();
        }

        public OrganizationMap GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
    }
}
