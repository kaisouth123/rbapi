﻿//using Bridgepay.Core.DataLayer.Interfaces;
//using Bridgepay.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomFieldActivationRepository<T> where T : class, IBaseEntity, new()
    {
        private readonly Interfaces.ICustomFieldContext context;
        public CustomFieldActivationRepository(IUnitOfWork _uow)
        {
            context = _uow.Context as Interfaces.ICustomFieldContext;
        }
        public IQueryable<CustomFieldActivation> All { get { return context.CustomFieldActivations; } }

        public IQueryable<CustomFieldActivation> AllIncluding(params Expression<Func<CustomFieldActivation, object>>[] includeProperties)
        {
            IQueryable<CustomFieldActivation> query = context.CustomFieldActivations;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            var deleteEntity= context.CustomFieldActivations.Where(x=>x.Id==id).FirstOrDefault();
            deleteEntity.ObjectState = Core.Framework.ObjectState.Deleted;
            context.CustomFieldActivations.Remove(deleteEntity);
            context.SaveChanges();
        }
        public void Dispose()
        {
            context.Dispose();
        }

        public CustomFieldActivation Find(int id)
        {
            return context.CustomFieldActivations.Find(id);
        }

        public IQueryable<CustomFieldActivation> FindByFilter(IFilter<CustomFieldActivation> filter)
        {
            throw new NotImplementedException();
            //return context.CustomFieldActivations.Where(filter.GetFilterExpression());
        }

        public CustomFieldActivation FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public CustomFieldActivation GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }
        public void InsertOrUpdateGraph(CustomFieldActivation entityGraph)
        {
            if (entityGraph.ObjectState == Core.Framework.ObjectState.Added)
            {
                context.CustomFieldActivations.Add(entityGraph);
            }
            else
            {
                context.CustomFieldActivations.Add(entityGraph);
                context.ApplyStateChanges();
            }
        }
        public void InsertOrUpdate(CustomFieldActivation entity)
        {
            if (entity.Id == default(Int64)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }
        //CustomFieldActivation IRepository<CustomFieldActivation>.InsertOrUpdate(CustomFieldActivation entity)
        //{
        //    throw new NotImplementedException();
        //}
        public IQueryable<CustomFieldActivation> GetAll(int skip = 0, int take = -1)
        {
            throw new NotImplementedException();
        }
        public IQueryable<CustomFieldActivation> FindByFilterIncluding(IFilter<CustomFieldActivation> filter, params Expression<Func<CustomFieldActivation, object>>[] includeProperties)
        {
            throw new NotImplementedException();
        }
        public void Delete(CustomFieldActivation entity)
        {
            throw new NotImplementedException();
        }
    }
}
