﻿using System;
using System.Linq;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Linq.Expressions;
using Bridgepay.Core.Framework;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class MerchantAccountRepository<T> where T : class, IBaseEntity, new()
    {

        private readonly IMerchantAccountContext context;
        protected IDbSet<T> DbSet { get; set; }
        #region ctor

        public MerchantAccountRepository(IUnitOfWork uow)
        {
            context = uow.Context as IMerchantAccountContext;
        }

        #endregion

        #region Get Methods

        public IQueryable<MerchantAccount> All
        {
            get { return context.MerchantAccounts; }
        }
        public IQueryable<T> GetAll(int skip = 0, int take = -1)
        {
            return take == -1 ? DbSet.OrderBy(e => e.Id).Skip(skip) : DbSet.OrderBy(e => e.Id).Skip(skip).Take(take).SetUnchanged();
        }
        public IQueryable<MerchantAccount> AllIncluding(params Expression<Func<MerchantAccount, object>>[] includeProperties)
        {
            IQueryable<MerchantAccount> query = context.MerchantAccounts;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public MerchantAccount Find(int id)
        {
            return context.MerchantAccounts.Find(id);
        }
        public MerchantAccount FindByMerchantAccountID(int MerchantAccountID)
        {
            try
            {
                return context.MerchantAccounts.Where(x => x.MerchantAccountID == MerchantAccountID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return new MerchantAccount();
            }
        }
        public IQueryable<T> FindByFilter(IFilter<T> filter)
        {
            Expression<Func<T, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse, filter.Operators);
            IQueryable<T> query = DbSet;
            filter.OrderBy = string.IsNullOrEmpty(filter.OrderBy) ? "MerchantAccountID" : filter.OrderBy;
            if (expression == null)
                return filter.Take <= 0 ? query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else
                return filter.Take <= 0 ? query.Where(filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse)).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) :
                    query.Where(filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse)).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
        }
        public MerchantAccount FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public MerchantAccount GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        #endregion

        public void InsertOrUpdateGraph(MerchantAccount MerchantAccount)
        {
            throw new NotImplementedException();
        }

        public void InsertOrUpdate(MerchantAccount MerchantAccount)
        {
            if (MerchantAccount.ObjectState == ObjectState.Added)
            {
                context.MerchantAccounts.Add(MerchantAccount);
            }
            else
            {
                context.MerchantAccounts.Add(MerchantAccount);
                context.ApplyStateChanges();
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public void SetSaveParams(Type type, Core.DataLayer.Interfaces.ISaveParams saveParams)
        {
            throw new NotImplementedException();
        }

    }
}
