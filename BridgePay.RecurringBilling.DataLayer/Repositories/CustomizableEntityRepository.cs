﻿//using Bridgepay.Core.DataLayer.Interfaces;
//using Bridgepay.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomizableEntityRepository<T> where T : class, IBaseEntity, new()
    {
        private readonly Interfaces.ICustomFieldContext context;
        public CustomizableEntityRepository(IUnitOfWork _uow)
        {
            context = _uow.Context as Interfaces.ICustomFieldContext;
        }
        public IQueryable<CustomizableEntity> All { get { return context.CustomizableEntities; } }

        public IQueryable<CustomizableEntity> AllIncluding(params Expression<Func<CustomizableEntity, object>>[] includeProperties)
        {
            IQueryable<CustomizableEntity> query = context.CustomizableEntities;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public CustomizableEntity Find(int id)
        {
            return context.CustomizableEntities.Find(id);
        }

        public IQueryable<CustomizableEntity> FindByFilter(IFilter<CustomizableEntity> filter)
        {
            throw new NotImplementedException();
        }

        public CustomizableEntity FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public CustomizableEntity GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }
        public void InsertOrUpdateGraph(CustomizableEntity entityGraph)
        {
            if (entityGraph.ObjectState == Core.Framework.ObjectState.Added)
            {
                context.CustomizableEntities.Add(entityGraph);
            }
            else
            {
                context.CustomizableEntities.Add(entityGraph);
                context.ApplyStateChanges();
            }
        }
        public void InsertOrUpdate(CustomizableEntity entity)
        {
            if (entity.Id == default(Int64)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }
        public IQueryable<CustomizableEntity> GetAll(int skip = 0, int take = -1)
        {
            throw new NotImplementedException();
        }
        public IQueryable<CustomizableEntity> FindByFilterIncluding(IFilter<CustomizableEntity> filter, params Expression<Func<CustomizableEntity, object>>[] includeProperties)
        {
            throw new NotImplementedException();
        }
        public void Delete(CustomizableEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
