﻿//using Bridgepay.Core.DataLayer.Interfaces;
//using Bridgepay.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ContractCustomFieldRepository<T> where T : class, IBaseEntity, new()
    {
        private readonly Interfaces.ICustomFieldContext context;
        public ContractCustomFieldRepository(IUnitOfWork _uow)
        {
            context = _uow.Context as Interfaces.ICustomFieldContext;
        }
        public IQueryable<ContractCustomField> All { get { return context.ContractCustomFields; } }

        public IQueryable<ContractCustomField> AllIncluding(params Expression<Func<ContractCustomField, object>>[] includeProperties)
        {
            IQueryable<ContractCustomField> query = context.ContractCustomFields;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(ContractCustomField entity)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public ContractCustomField Find(int id)
        {
            return context.ContractCustomFields.Find(id);
        }

        public IQueryable<ContractCustomField> FindByFilter(IFilter<ContractCustomField> filter)
        {
            throw new NotImplementedException();
        }

        public IQueryable<ContractCustomField> FindByFilterIncluding(IFilter<ContractCustomField> filter, params Expression<Func<ContractCustomField, object>>[] includeProperties)
        {
            throw new NotImplementedException();
        }

        public ContractCustomField FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public IQueryable<ContractCustomField> GetAll(int skip = 0, int take = -1)
        {
            throw new NotImplementedException();
        }

        public ContractCustomField GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public void InsertOrUpdate(ContractCustomField entity)
        {
            if (entity.Id == default(Int64)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }
        public void InsertOrUpdateGraph(ContractCustomField entityGraph)
        {
            if (entityGraph.ObjectState ==  Core.Framework.ObjectState.Added)
            {
                context.ContractCustomFields.Add(entityGraph);
            }
            else
            {
                context.ContractCustomFields.Add(entityGraph);
                context.ApplyStateChanges();
            }
        }
        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }
   }
}
