﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Linq.Expressions;
using Bridgepay.Core.Framework;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ChargeRepository : IChargeRepository
    {
        private IChargeContext context;

        public ChargeRepository(IUnitOfWork uow)
        {
            context = uow.Context as IChargeContext;
        }

        public ChargeRepository(IUnitOfWork uow, IDbSet<Account> dbSet)
        {
            context = uow.Context as IChargeContext;
        }

        public IQueryable<ChargeTransactionsAux> GetAll(int skip = 0, int take = -1)
        {
            return take == -1 ? context.ChargeTransactionsAuxes.OrderBy(e => e.ID).Skip(skip) : context.ChargeTransactionsAuxes.OrderBy(e => e.ID).Skip(skip).Take(take);
        }

        public ChargeTransactionsAux Find(int id)
        {
            return context.ChargeTransactionsAuxes.Find(id);
        }

        public void Delete(ChargeTransactionsAux entity)
        {
            context.ChargeTransactionsAuxes.Remove(entity);
        }

        public void Delete(int id)
        {
            var entity = context.ChargeTransactionsAuxes.Find(id);
            context.ChargeTransactionsAuxes.Remove(entity);
        }

        public void InsertOrUpdate(ChargeTransactionsAux entity)
        {
            if (entity.ID == default(Int64)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }

        public void InsertOrUpdateGraph(ChargeTransactionsAux entityGraph)
        {
            if (entityGraph.ObjectState == ObjectState.Added)
            {
                context.ChargeTransactionsAuxes.Add(entityGraph);
            }
            else
            {
                context.ChargeTransactionsAuxes.Add(entityGraph);
                context.ApplyStateChanges();
            }
        }

        public IQueryable<ChargeTransactionsAux> AllIncluding(params Expression<Func<ChargeTransactionsAux, object>>[] includeProperties)
        {
            IQueryable<ChargeTransactionsAux> query = context.ChargeTransactionsAuxes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public IQueryable<ChargeTransactionsAux> FindByFilter(IFilter<ChargeTransactionsAux> filter)
        {
            IQueryable<ChargeTransactionsAux> query = context.ChargeTransactionsAuxes.Where(filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse));
            return query;
        }

        public ChargeTransactionsAux FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public ChargeTransactionsAux GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            context.SaveChanges();
        }


      
    }
}
