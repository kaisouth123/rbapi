﻿using System;
using System.Linq;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Linq.Expressions;
using Bridgepay.Core.Framework;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class UnipayMerchantRepository<T> where T : class, IBaseEntity, new()
    {

        private readonly IUnipayMerchantContext context;
        protected IDbSet<T> DbSet { get; set; }
        #region ctor

        public UnipayMerchantRepository(IUnitOfWork uow)
        {
            context = uow.Context as IUnipayMerchantContext;
        }

        #endregion

        #region Get Methods

        public IQueryable<UnipayMerchant> All
        {
            get { return context.Merchants; }
        }
        public IQueryable<T> GetAll(int skip = 0, int take = -1)
        {
            return take == -1 ? DbSet.OrderBy(e => e.Id).Skip(skip) : DbSet.OrderBy(e => e.Id).Skip(skip).Take(take).SetUnchanged();
        }
        public IQueryable<UnipayMerchant> AllIncluding(params Expression<Func<UnipayMerchant, object>>[] includeProperties)
        {
            IQueryable<UnipayMerchant> query = context.Merchants;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public UnipayMerchant Find(int id)
        {
            return context.Merchants.Find(id);
        }
        public UnipayMerchant FindByUnipayMerchantName(string UnipayMerchantName)
        {
            try
            {
                return context.Merchants.Where(x => x.Name == UnipayMerchantName).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return new UnipayMerchant();
            }
        }
        public UnipayMerchant FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public UnipayMerchant GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        #endregion

        public void InsertOrUpdateGraph(UnipayMerchant UnipayMerchant)
        {
            throw new NotImplementedException();
        }

        public void InsertOrUpdate(UnipayMerchant UnipayMerchant)
        {
            if (UnipayMerchant.ObjectState == ObjectState.Added)
            {
                context.Merchants.Add(UnipayMerchant);
            }
            else
            {
                context.Merchants.Add(UnipayMerchant);
                context.ApplyStateChanges();
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public void SetSaveParams(Type type, Core.DataLayer.Interfaces.ISaveParams saveParams)
        {
            throw new NotImplementedException();
        }

    }
}
