﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Linq.Expressions;
using Bridgepay.Core.Framework;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class MerchantRepository : IMerchantRepository
    {
        private IMerchantContext context;

        public MerchantRepository(IUnitOfWork uow)
        {
            context = uow.Context as IMerchantContext;
        }

        public MerchantRepository(IUnitOfWork uow, IDbSet<Merchant> dbSet)
        {
            context = uow.Context as IMerchantContext;
        }

        public IQueryable<Merchant> GetAll(int skip = 0, int take = -1)
        {
            return take == -1 ? context.Merchants.OrderBy(e => e.Id).Skip(skip) : context.Merchants.OrderBy(e => e.Id).Skip(skip).Take(take);
        }

        public Merchant Find(int id)
        {
            return context.Merchants.Find(id);
        }

        public void Delete(Merchant entity)
        {
            context.Merchants.Remove(entity);
        }

        public void Delete(int id)
        {
            var entity = context.Merchants.Find(id);
            context.Merchants.Remove(entity);
        }

        public void InsertOrUpdate(Merchant entity)
        {
            if (entity.Id == default(int)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }

        public void InsertOrUpdateGraph(Merchant entityGraph)
        {
            if (entityGraph.ObjectState == ObjectState.Added)
            {
                context.Merchants.Add(entityGraph);
            }
            else
            {
                context.Merchants.Add(entityGraph);
                context.ApplyStateChanges();
            }
        }

        public IQueryable<Merchant> AllIncluding(params Expression<Func<Merchant, object>>[] includeProperties)
        {
            IQueryable<Merchant> query = context.Merchants;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public IQueryable<Merchant> FindByFilter(IFilter<Merchant> filter)
        {
            Expression<Func<Merchant, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            IQueryable<Merchant> query = context.Merchants;
            filter.OrderBy = string.IsNullOrEmpty(filter.OrderBy) ? "Id" : filter.OrderBy;
            if (expression == null)
                return filter.Take <= 0 ? query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else
                return filter.Take <= 0 ? query.Where(filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse)).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) :
                    query.Where(filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse)).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
        }

        public IQueryable<Merchant> FindByFilterIncluding(IFilter<Merchant> filter, params Expression<Func<Merchant, object>>[] includeProperties)
        {
            Expression<Func<Merchant, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            IQueryable<Merchant> query = context.Merchants;
            filter.OrderBy = string.IsNullOrEmpty(filter.OrderBy) ? "Id" : filter.OrderBy;
            //non-graph 
            if (expression == null && filter.IncludeGraph == false)
                return filter.Take <= 0 ? query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else if (expression != null && filter.IncludeGraph == false)
                return query.Where<Merchant>(expression).SetUnchanged();

            //addg raph
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            //with graph
            if (expression == null && filter.IncludeGraph)
                return filter.Take <= 0 ? query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else if (filter.IncludeGraph && expression != null)
                return filter.Take <= 0 ? query.Where<Merchant>(expression).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.Where<Merchant>(expression).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else
                return query.SetUnchanged();
        }

        public Merchant FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public Merchant GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
