﻿//using Bridgepay.Core.DataLayer.Interfaces;
//using Bridgepay.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomFieldDataRepository<T> where T : class, IBaseEntity, new()
    {
        private readonly Interfaces.ICustomFieldContext context;
        public CustomFieldDataRepository(IUnitOfWork _uow)
        {
            context = _uow.Context as Interfaces.ICustomFieldContext;
        }
        public IQueryable<CustomFieldData> All { get { return context.CustomFieldDatas; } }

        public IQueryable<CustomFieldData> AllIncluding(params Expression<Func<CustomFieldData, object>>[] includeProperties)
        {
            IQueryable<CustomFieldData> query = context.CustomFieldDatas;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            var deleteEntity= context.CustomFieldDatas.Where(x=>x.Id==id).FirstOrDefault();
            deleteEntity.ObjectState = Core.Framework.ObjectState.Deleted;
            context.CustomFieldDatas.Remove(deleteEntity);
            context.SaveChanges();
        }
        public void Dispose()
        {
            context.Dispose();
        }

        public CustomFieldData Find(int id)
        {
            return context.CustomFieldDatas.Find(id);
        }

        public IQueryable<CustomFieldData> FindByFilter(IFilter<CustomFieldData> filter)
        {
            throw new NotImplementedException();
        }

        public CustomFieldData FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public CustomFieldData GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }
        public void InsertOrUpdateGraph(CustomFieldData entityGraph)
        {
            if (entityGraph.ObjectState == Core.Framework.ObjectState.Added)
            {
                context.CustomFieldDatas.Add(entityGraph);
            }
            else
            {
                context.CustomFieldDatas.Add(entityGraph);
                context.ApplyStateChanges();
            }
        }
        public void InsertOrUpdate(CustomFieldData entity)
        {
            if (entity.Id == default(Int64)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }

        public IQueryable<CustomFieldData> GetAll(int skip = 0, int take = -1)
        {
            throw new NotImplementedException();
        }
        public IQueryable<CustomFieldData> FindByFilterIncluding(IFilter<CustomFieldData> filter, params Expression<Func<CustomFieldData, object>>[] includeProperties)
        {
            throw new NotImplementedException();
        }
        public void Delete(CustomFieldData entity)
        {
            throw new NotImplementedException();
        }
    }
}
