﻿//using Bridgepay.Core.DataLayer.Interfaces;
//using Bridgepay.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomFieldRepository<T> where T : class, IBaseEntity, new()
    {
        private readonly Interfaces.ICustomFieldContext context;
        public CustomFieldRepository(IUnitOfWork _uow)
        {
            context = _uow.Context as Interfaces.ICustomFieldContext;
        }
        public IQueryable<CustomField> All { get { return context.CustomFields; } }

        public IQueryable<CustomField> AllIncluding(params Expression<Func<CustomField, object>>[] includeProperties)
        {
            IQueryable<CustomField> query = context.CustomFields;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(CustomField entity)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public CustomField Find(int id)
        {
            return context.CustomFields.Find(id);
        }

        public IQueryable<CustomField> FindByFilter(IFilter<CustomField> filter)
        {
            throw new NotImplementedException();
        }

        public IQueryable<CustomField> FindByFilterIncluding(IFilter<CustomField> filter, params Expression<Func<CustomField, object>>[] includeProperties)
        {
            throw new NotImplementedException();
        }

        public CustomField FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public IQueryable<CustomField> GetAll(int skip = 0, int take = -1)
        {
            throw new NotImplementedException();
        }

        public CustomField GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public void InsertOrUpdate(CustomField entity)
        {
            if (entity.Id == default(Int64)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }
        public void InsertOrUpdateGraph(CustomField entityGraph)
        {
            if (entityGraph.ObjectState ==  Core.Framework.ObjectState.Added)
            {
                context.CustomFields.Add(entityGraph);
            }
            else
            {
                context.CustomFields.Add(entityGraph);
                context.ApplyStateChanges();
            }
        }
        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }
    }
}
