﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Linq.Expressions;
using Bridgepay.Core.Framework;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class UserRepository<T> where T : class,IBaseEntity, new()
    {
        private readonly IUserContext context;
        protected IDbSet<T> DbSet { get; set; }
        #region ctor

        public UserRepository(IUnitOfWork uow)
        {
            context = uow.Context as IUserContext;
        }

        #endregion

        #region Get Methods

        public IQueryable<User> All
        {
            get { return context.Users; }
        }
        public IQueryable<T> GetAll(int skip = 0, int take = -1)
        {
            return take == -1 ? DbSet.OrderBy(e => e.Id).Skip(skip) : DbSet.OrderBy(e => e.Id).Skip(skip).Take(take).SetUnchanged();
        }
        public IQueryable<User> AllIncluding(params Expression<Func<User, object>>[] includeProperties)
        {
            IQueryable<User> query = context.Users;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public User Find(int id)
        {
            return context.Users.Find(id);
        }
        public User FindByUserName(string userName)
        {
            try
            {
                return context.Users.Where(u => u.UserName == userName).FirstOrDefault();
            }
            catch(Exception ex)
            {
                return new User();
            }
        }
       
        public User FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public IQueryable<Role> GetRoles()
        {
            return context.Roles ;
        }
        public User GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        #endregion

        public void InsertOrUpdateGraph(User user)
        {
            throw new NotImplementedException();
        }

        public void InsertOrUpdate(User user)
        {
            if (user.ObjectState == ObjectState.Added)
            {
                context.Users.Add(user);
            }
            else
            {
                context.Users.Add(user);
                context.ApplyStateChanges();
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public void SetSaveParams(Type type, Core.DataLayer.Interfaces.ISaveParams saveParams)
        {
            throw new NotImplementedException();
        }


    }
}