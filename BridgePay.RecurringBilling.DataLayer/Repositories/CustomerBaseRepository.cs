﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Linq.Expressions;
using Bridgepay.Core.Framework;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomerBaseRepository<T> where T : class, IBaseEntity, new()
    {
        protected ICustomerContext context;
        protected IDbSet<T> DbSet { get; set; }
        protected DbContext dbContext;

        public CustomerBaseRepository(IUnitOfWork uow)
        {
            context = uow.Context as ICustomerContext;
            dbContext = uow.Context as DbContext;
            DbSet = dbContext.Set<T>();
        }

        public CustomerBaseRepository(IUnitOfWork uow, IDbSet<T> dbSet)
        {
            context = uow.Context as ICustomerContext;
            dbContext = uow.Context as DbContext;
            DbSet = dbSet;
        }

        public IQueryable<T> GetAll(int skip = 0, int take = -1)
        {
            return take == -1 ? DbSet.OrderBy(e => e.Id).Skip(skip) : DbSet.OrderBy(e => e.Id).Skip(skip).Take(take).SetUnchanged();
        }

        public T Find(int id)
        {
            T entity = DbSet.Find(id);
            entity.SetUnchanged();
            return entity;
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }

        public void Delete(int id)
        {
            var entity = DbSet.Find(id);
            DbSet.Remove(entity);
        }

        public void InsertOrUpdate(T entity)
        {
            if (entity.Id == default(int)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }

        public void InsertOrUpdateGraph(T entityGraph)
        {
            if (entityGraph.ObjectState == ObjectState.Added)
            {

                if (DbSet is IMockDbSet<T>)
                    context.SetAdded(entityGraph);
                else
                    DbSet.Add(entityGraph);
            }
            else
            {
                if (DbSet is IMockDbSet<T>)
                    context.SetModified(entityGraph);
                else
                    DbSet.Add(entityGraph);

                context.ApplyStateChanges();
            }
        }

        public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = DbSet;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.SetUnchanged();
        }

        public IQueryable<T> FindByFilter(IFilter<T> filter)
        {
            Expression<Func<T, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            IQueryable<T> query = DbSet;
            filter.OrderBy = string.IsNullOrEmpty(filter.OrderBy) ? "Id" : filter.OrderBy;
            if (expression == null)
                return filter.Take <= 0 ? query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else
                return filter.Take <= 0 ? query.Where(filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse)).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) :
                    query.Where(filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse)).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
        }

        public IQueryable<T> FindByFilterIncluding(IFilter<T> filter, params Expression<Func<T, object>>[] includeProperties)
        {
            Expression<Func<T, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            IQueryable<T> query = DbSet;
            filter.OrderBy = string.IsNullOrEmpty(filter.OrderBy) ? "Id" : filter.OrderBy;
            //non-graph 
            if (expression == null && filter.IncludeGraph == false)
                return filter.Take <= 0 ? query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else if (expression != null && filter.IncludeGraph == false)
                return query.Where<T>(expression).SetUnchanged();

            //addg raph
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            //with graph
            if (expression == null && filter.IncludeGraph)
                return filter.Take <= 0 ? query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else if (filter.IncludeGraph && expression != null)
                return filter.Take <= 0 ? query.Where<T>(expression).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip) : query.Where<T>(expression).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).SetUnchanged();
            else
                return query.SetUnchanged();
        }

        public T FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public T GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            dbContext.SaveChanges();
        }
    }
}
