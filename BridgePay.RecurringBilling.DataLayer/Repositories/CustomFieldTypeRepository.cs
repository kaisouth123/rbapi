﻿//using Bridgepay.Core.DataLayer.Interfaces;
//using Bridgepay.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomFieldTypeRepository<T> where T : class, IBaseEntity, new()
    {
        private readonly Interfaces.ICustomFieldContext context;
        public CustomFieldTypeRepository(IUnitOfWork _uow)
        {
            context = _uow.Context as Interfaces.ICustomFieldContext;
        }
        public IQueryable<CustomFieldType> All { get { return context.CustomFieldTypes; } }

        public IQueryable<CustomFieldType> AllIncluding(params Expression<Func<CustomFieldType, object>>[] includeProperties)
        {
            IQueryable<CustomFieldType> query = context.CustomFieldTypes;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public CustomFieldType Find(int id)
        {
            return context.CustomFieldTypes.Find(id);
        }

        public IQueryable<CustomFieldType> FindByFilter(IFilter<CustomFieldType> filter)
        {
            throw new NotImplementedException();
            //return context.CustomFieldTypes.Where(filter.GetFilterExpression());
        }

        public CustomFieldType FindByParameters(params object[] parameters)
        {
            throw new NotImplementedException();
        }

        public CustomFieldType GetEntityFromCollection(params object[] parameters)
        {
            throw new NotImplementedException();
        }
        public void SetSaveParams(Type type, ISaveParameters saveParams)
        {
            throw new NotImplementedException();
        }
        public void InsertOrUpdateGraph(CustomFieldType entityGraph)
        {
            if (entityGraph.ObjectState == Core.Framework.ObjectState.Added)
            {
                context.CustomFieldTypes.Add(entityGraph);
            }
            else
            {
                context.CustomFieldTypes.Add(entityGraph);
                context.ApplyStateChanges();
            }
        }
        public void InsertOrUpdate(CustomFieldType entity)
        {
            if (entity.Id == default(Int64)) // New entity
            {
                context.SetAdded(entity);
            }
            else        // Existing entity
            {
                context.SetModified(entity);
            }
        }
        public IQueryable<CustomFieldType> GetAll(int skip = 0, int take = -1)
        {
            throw new NotImplementedException();
        }
        public IQueryable<CustomFieldType> FindByFilterIncluding(IFilter<CustomFieldType> filter, params Expression<Func<CustomFieldType, object>>[] includeProperties)
        {
            throw new NotImplementedException();
        }
        public void Delete(CustomFieldType entity)
        {
            throw new NotImplementedException();
        }
    }
}
