﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IAddressRepository<T>
    {
        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);
        void Delete(T entity);
        void Delete(int id);
        T Find(int id);
        IQueryable<T> FindByFilter(IFilter<T> filter);
        IQueryable<T> FindByFilterIncluding(IFilter<T> filter, params Expression<Func<T, object>>[] includeProperties);
        T FindByParameters(params object[] parameters);
        IQueryable<T> GetAll(int skip = 0, int take = -1);
        T GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(T entity);
        void InsertOrUpdateGraph(T entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
