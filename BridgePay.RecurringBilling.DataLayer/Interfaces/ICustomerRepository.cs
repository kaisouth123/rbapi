﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface ICustomerRepository
    {
        IQueryable<Customer> AllIncluding(params Expression<Func<Customer, object>>[] includeProperties);
        void Delete(Customer entity);
        void Delete(int id);
        Customer Find(int id);
        IQueryable<Customer> FindByFilter(IFilter<Customer> filter);
        IQueryable<Customer> FindByFilterIncluding(IFilter<Customer> filter, params Expression<Func<Customer, object>>[] includeProperties);
        Customer FindByParameters(params object[] parameters);
        IQueryable<Customer> GetAll(int skip = 0, int take = -1);
        Customer GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(Customer entity);
        void InsertOrUpdateGraph(Customer entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
