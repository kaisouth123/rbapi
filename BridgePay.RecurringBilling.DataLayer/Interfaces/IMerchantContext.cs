﻿using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IMerchantContext : IDisposable
    {
        IDbSet<Merchant> Merchants { get; set; }

        void ApplyStateChanges();
        void SetAdded(IObjectWithState entity);
        void SetModified(IObjectWithState entity);
        void SetCredentials(string user, string password);
        void SetState(IObjectWithState entity);
        int SaveChanges();
    }
}
