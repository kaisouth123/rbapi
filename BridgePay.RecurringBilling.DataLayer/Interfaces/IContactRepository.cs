﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IContactRepository
    {
        IQueryable<Contact> AllIncluding(params Expression<Func<Contact, object>>[] includeProperties);
        void Delete(Contact entity);
        void Delete(int id);
        Contact Find(int id);
        IQueryable<Contact> FindByFilter(IFilter<Contact> filter);
        IQueryable<Contact> FindByFilterIncluding(IFilter<Contact> filter, params Expression<Func<Contact, object>>[] includeProperties);
        Contact FindByParameters(params object[] parameters);
        IQueryable<Contact> GetAll(int skip = 0, int take = -1);
        Contact GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(Contact entity);
        void InsertOrUpdateGraph(Contact entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
