﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface ICustomFieldContext : IContext
    {
        DbSet<CustomField> CustomFields { get; set; }
        DbSet<CustomFieldType> CustomFieldTypes { get; set; }
        DbSet<CustomFieldActivation> CustomFieldActivations { get; set; }
        DbSet<CustomizableEntity> CustomizableEntities { get; set; }
        DbSet<ContractCustomField> ContractCustomFields { get; set; }
        DbSet<CustomFieldData> CustomFieldDatas { get; set; }
    }
}
