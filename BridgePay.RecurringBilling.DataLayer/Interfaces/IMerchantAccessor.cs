﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using System;

namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IMerchantAccessor
    {
        IMerchantRepository MerchantRepository { get; }
        void Save();
    }
}
