﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using System;
namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IChargeAccessor
    {
        IChargeRepository ChargeRepository { get; }
        void Save();
    }
}
