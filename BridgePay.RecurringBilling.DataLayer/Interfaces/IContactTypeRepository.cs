﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IContactTypeRepository
    {
        IQueryable<ContactType> AllIncluding(params Expression<Func<ContactType, object>>[] includeProperties);
        void Delete(ContactType entity);
        void Delete(int id);
        ContactType Find(int id);
        IQueryable<ContactType> FindByFilter(IFilter<ContactType> filter);
        IQueryable<ContactType> FindByFilterIncluding(IFilter<ContactType> filter, params Expression<Func<ContactType, object>>[] includeProperties);
        ContactType FindByParameters(params object[] parameters);
        IQueryable<ContactType> GetAll(int skip = 0, int take = -1);
        ContactType GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(ContactType entity);
        void InsertOrUpdateGraph(ContactType entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
