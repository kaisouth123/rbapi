﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;

namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IResellerAccessor
    {
        ResellerRepository<Reseller> ResellerRepository { get; }
        ResellerRepository<MerchantAccount> MerchantAccountRepository { get; }
        ResellerRepository<UnipayMerchant> UnipayMerchantRepository { get; }
        void Save();
    }
}
