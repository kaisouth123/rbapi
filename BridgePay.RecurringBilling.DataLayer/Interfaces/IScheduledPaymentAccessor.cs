﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;

namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IScheduledPaymentAccessor
    {
        VendorRepository<Contract> ContractRepository { get; }
        VendorRepository<ScheduledPayment> ScheduledPaymentRepository { get; }
        void Save();
    }
}
