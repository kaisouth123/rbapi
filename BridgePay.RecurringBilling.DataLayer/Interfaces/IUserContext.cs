﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IUserContext : IContext
    {
        DbSet<User> Users { get; set; }
        DbSet<UserEmail> UserEmails { get; set; }
        DbSet<UserName> UserNames { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<UserAddress> UserAddresses { get; set; }
    }
}
