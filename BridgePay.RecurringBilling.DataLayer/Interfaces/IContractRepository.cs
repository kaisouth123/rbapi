﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IContractRepository
    {
        IQueryable<Contract> AllIncluding(params Expression<Func<Contract, object>>[] includeProperties);
        void Delete(Contract entity);
        void Delete(int id);
        Contract Find(int id);
        IQueryable<Contract> FindByFilter(IFilter<Contract> filter);
        IQueryable<Contract> FindByFilterIncluding(IFilter<Contract> filter, params Expression<Func<Contract, object>>[] includeProperties);
        Contract FindByParameters(params object[] parameters);
        IQueryable<Contract> GetAll(int skip = 0, int take = -1);
        Contract GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(Contract entity);
        void InsertOrUpdateGraph(Contract entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
