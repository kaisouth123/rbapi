﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IBridgePayContext : Core.DataLayer.Interfaces.IContext
    {
        IDbSet<OrganizationMap> OrganizationMaps { get; }
    }
}
