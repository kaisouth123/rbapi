﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IIntervalRepository
    {
        IQueryable<Interval> AllIncluding(params Expression<Func<Interval, object>>[] includeProperties);
        void Delete(Interval entity);
        void Delete(int id);
        Interval Find(int id);
        IQueryable<Interval> FindByFilter(IFilter<Interval> filter);
        IQueryable<Interval> FindByFilterIncluding(IFilter<Interval> filter, params Expression<Func<Interval, object>>[] includeProperties);
        Interval FindByParameters(params object[] parameters);
        IQueryable<Interval> GetAll(int skip = 0, int take = -1);
        Interval GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(Interval entity);
        void InsertOrUpdateGraph(Interval entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
