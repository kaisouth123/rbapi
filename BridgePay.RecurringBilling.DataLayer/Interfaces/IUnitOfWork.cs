﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IContext Context { get; }
        int Save();
    }
}
