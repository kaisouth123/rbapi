﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IProductRepository
    {
        IQueryable<Product> AllIncluding(params Expression<Func<Product, object>>[] includeProperties);
        void Delete(Product entity);
        void Delete(int id);
        Product Find(int id);
        IQueryable<Product> FindByFilter(IFilter<Product> filter);
        Product FindByParameters(params object[] parameters);
        IQueryable<Product> FindByFilterIncluding(IFilter<Product> filter, params Expression<Func<Product, object>>[] includeProperties);
        IQueryable<Product> GetAll(int skip = 0, int take = -1);
        Product GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(Product entity);
        void InsertOrUpdateGraph(Product entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
