﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IMerchantAccountContext : IContext
    {
        DbSet<MerchantAccount> MerchantAccounts { get; set; }
       
    }
}
