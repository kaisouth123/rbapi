﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IAccountRepository
    {
        IQueryable<Account> AllIncluding(params Expression<Func<Account, object>>[] includeProperties);
        void Delete(Account entity);
        void Delete(int id);
        Account Find(int id);
        IQueryable<Account> FindByFilter(IFilter<Account> filter);
        IQueryable<Account> FindByFilterIncluding(IFilter<Account> filter, params Expression<Func<Account, object>>[] includeProperties);
        Account FindByParameters(params object[] parameters);
        IQueryable<Account> GetAll(int skip = 0, int take = -1);
        Account GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(Account entity);
        void InsertOrUpdateGraph(Account entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
