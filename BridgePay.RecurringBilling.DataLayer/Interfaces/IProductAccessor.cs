﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;

namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IProductAccessor
    {
        VendorRepository<Contact> ContactRepository { get; }
        VendorRepository<Product> ProductRepository { get; }
        void Save();
    }
}
