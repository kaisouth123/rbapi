﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IMerchantRepository
    {
        IQueryable<Merchant> AllIncluding(params Expression<Func<Merchant, object>>[] includeProperties);
        void Delete(Merchant entity);
        void Delete(int id);
        Merchant Find(int id);
        IQueryable<Merchant> FindByFilter(IFilter<Merchant> filter);
        IQueryable<Merchant> FindByFilterIncluding(IFilter<Merchant> filter, params Expression<Func<Merchant, object>>[] includeProperties);
        Merchant FindByParameters(params object[] parameters);
        IQueryable<Merchant> GetAll(int skip = 0, int take = -1);
        Merchant GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(Merchant entity);
        void InsertOrUpdateGraph(Merchant entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
