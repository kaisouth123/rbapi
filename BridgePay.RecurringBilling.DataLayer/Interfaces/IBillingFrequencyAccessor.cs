﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IBillingFrequencyAccessor
    {
        VendorRepository<BillingFrequency> BillingFrequencyRepository { get; }
        VendorRepository<Contract> ContractRepository { get; }
        VendorRepository<Interval> IntervalRepository { get; }
        void Save();
    }
}
