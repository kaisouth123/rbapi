﻿using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IVendorContext: IDisposable
    {
        IDbSet<Account> Accounts { get; set; }
        IDbSet<Address> Addresses { get; set; }
        IDbSet<BillingFrequency> BillingFrequencies { get; set; }
        IDbSet<Contact> Contacts { get; set; }
        IDbSet<ContactType> ContactTypes { get; set; }
        IDbSet<Contract> Contracts { get; set; }
        IDbSet<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        IDbSet<Interval> Intervals { get; set; }
        IDbSet<Product> Products { get; set; }
        IDbSet<ScheduledPayment> ScheduledPayments { get; set; }
        IDbSet<ContractCustomField> ContractCustomFields { get; set; }
        void SetAdded(IObjectWithState entity);
        void SetModified(IObjectWithState entity);
        void SetCredentials(string user, string password);
        void SetState(IObjectWithState entity);
        void ApplyStateChanges();
    }
}
