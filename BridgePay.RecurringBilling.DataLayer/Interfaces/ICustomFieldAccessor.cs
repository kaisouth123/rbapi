﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface ICustomFieldAccessor
    {
        CustomFieldRepository<CustomField> CustomFieldRepository { get; }
        CustomFieldActivationRepository<CustomFieldActivation> CustomFieldActivationRepository { get; }
        CustomFieldTypeRepository<CustomFieldType> CustomFieldTypeRepository { get; }
        CustomizableEntityRepository<CustomizableEntity> CustomizableEntityRepository { get; }
        void Save();
    }
}
