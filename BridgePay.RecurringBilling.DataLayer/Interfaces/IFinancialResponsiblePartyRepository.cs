﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IFinancialResponsiblePartyRepository
    {
        IQueryable<FinancialResponsibleParty> AllIncluding(params Expression<Func<FinancialResponsibleParty, object>>[] includeProperties);
        void Delete(FinancialResponsibleParty entity);
        void Delete(int id);
        FinancialResponsibleParty Find(int id);
        IQueryable<FinancialResponsibleParty> FindByFilter(IFilter<FinancialResponsibleParty> filter);
        IQueryable<FinancialResponsibleParty> FindByFilterIncluding(IFilter<FinancialResponsibleParty> filter, params Expression<Func<FinancialResponsibleParty, object>>[] includeProperties);
        FinancialResponsibleParty FindByParameters(params object[] parameters);
        IQueryable<FinancialResponsibleParty> GetAll(int skip = 0, int take = -1);
        FinancialResponsibleParty GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(FinancialResponsibleParty entity);
        void InsertOrUpdateGraph(FinancialResponsibleParty entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
