﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IMerchantAccountRepository
    {
        IQueryable<MerchantAccount> AllIncluding(params Expression<Func<MerchantAccount, object>>[] includeProperties);
        void Delete(MerchantAccount entity);
        void Delete(int id);
        MerchantAccount Find(int id);
        IQueryable<MerchantAccount> FindByFilter(IFilter<MerchantAccount> filter);
        IQueryable<MerchantAccount> FindByFilterIncluding(IFilter<MerchantAccount> filter, params Expression<Func<MerchantAccount, object>>[] includeProperties);
        MerchantAccount FindByParameters(params object[] parameters);
        IQueryable<MerchantAccount> GetAll(int skip = 0, int take = -1);
        MerchantAccount GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(MerchantAccount entity);
        void InsertOrUpdateGraph(MerchantAccount entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
