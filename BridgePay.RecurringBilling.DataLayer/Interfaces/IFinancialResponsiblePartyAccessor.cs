﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IFinancialResponsiblePartyAccessor
    {
        CustomerBaseRepository<Account> AccountRepository { get; }
        CustomerBaseRepository<Contract> ContractRepository { get; }
        CustomerBaseRepository<Customer> CustomerRepository { get; }
        CustomerBaseRepository<FinancialResponsibleParty> FinancialResponsiblePartyRepository { get; }
        void Save();
    }
}
