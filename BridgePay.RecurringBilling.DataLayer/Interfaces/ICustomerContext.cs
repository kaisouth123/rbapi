﻿using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface ICustomerContext: IDisposable
    {
        IDbSet<Account> Accounts { get; set; }
        IDbSet<CustomerAddress> Addresses { get; set; }
        IDbSet<BillingFrequency> BillingFrequencies { get; set; }
        IDbSet<ContactType> ContactTypes { get; set; }
        IDbSet<Contract> Contracts { get; set; }
        IDbSet<Customer> Customers { get; set; }
        IDbSet<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        IDbSet<Interval> Intervals { get; set; }
        IDbSet<Product> Products { get; set; }
        IDbSet<ScheduledPayment> ScheduledPayments { get; set; }

        void ApplyStateChanges();
        void SetAdded(IObjectWithState entity);
        void SetModified(IObjectWithState entity);
        void SetCredentials(string user, string password);
        void SetState(IObjectWithState entity);
    }
}
