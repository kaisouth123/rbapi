﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IBillingFrequencyRepository
    {
        IQueryable<BillingFrequency> AllIncluding(params Expression<Func<BillingFrequency, object>>[] includeProperties);
        void Delete(BillingFrequency entity);
        void Delete(int id);
        BillingFrequency Find(int id);
        IQueryable<BillingFrequency> FindByFilter(IFilter<BillingFrequency> filter);
        IQueryable<BillingFrequency> FindByFilterIncluding(IFilter<BillingFrequency> filter, params Expression<Func<BillingFrequency, object>>[] includeProperties);
        BillingFrequency FindByParameters(params object[] parameters);
        IQueryable<BillingFrequency> GetAll(int skip = 0, int take = -1);
        BillingFrequency GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(BillingFrequency entity);
        void InsertOrUpdateGraph(BillingFrequency entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
