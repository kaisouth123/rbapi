﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface ISaveParameters
    {
        string AddCommand { get; set; }
        string DeleteCommand { get; set; }
        string Resource { get; set; }
        string UpdateCommand { get; set; }
    }
}
