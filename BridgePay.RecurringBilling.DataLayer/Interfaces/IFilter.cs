﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IFilter<T>
    {
        T entity { get; set; }
        List<String> IgnoreProperties { get; set; }
        List<ExpressionOperator> Operators { get; set; }
        bool SearchFalse { get; set; }
        bool IncludeGraph { get; set; }
        int Skip { get; set; }
        int Take { get; set; }
        string OrderBy { get; set; }
        bool SortDesc { get; set; }


        Expression<System.Func<T, bool>> GetFilterExpression(T account, List<String> ignoreProperties=null, bool searchFalse=true, List<ExpressionOperator> operators =null);
    }
}
