﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.Core.Framework.Interfaces;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IContext : IDisposable
    {
        void ApplyStateChanges();
        int SaveChanges();
        void SetAdded(object entity);
        void SetModified(object entity);
        void SetCredentials(string user, string password);
        void SetState(IObjectWithState entity);
    }
}
