﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface ICustomFieldRepository
    {
        IQueryable<CustomField> AllIncluding(params Expression<Func<CustomField, object>>[] includeProperties);
        void Delete(CustomField entity);
        void Delete(int id);
        CustomField Find(int id);
        IQueryable<CustomField> FindByFilter(IFilter<CustomField> filter);
        CustomField FindByParameters(params object[] parameters);
        IQueryable<CustomField> GetAll(int skip = 0, int take = -1);
        CustomField GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(CustomField entity);
        void InsertOrUpdateGraph(CustomField entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
