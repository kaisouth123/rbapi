﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;

namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IContractAccessor
    {
        VendorRepository<Account> AccountRepository { get; }
        VendorRepository<BillingFrequency> BillingFrequencyRepository { get; }
        VendorRepository<Contract> ContractRepository { get; }
        VendorRepository<Product> ProductRepository { get; }
        VendorRepository<Interval> IntervalRepository { get; }
        VendorRepository<ScheduledPayment> ScheduledPaymentRepository { get; }
        VendorRepository<FinancialResponsibleParty> FinancialResponsiblePartyRepository { get; }
        VendorRepository<Contact> ContactRepository { get; }
        void Save();
    }
}
