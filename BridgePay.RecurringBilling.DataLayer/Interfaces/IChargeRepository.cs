﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IChargeRepository
    {
        IQueryable<ChargeTransactionsAux> AllIncluding(params Expression<Func<ChargeTransactionsAux, object>>[] includeProperties);
        void Delete(ChargeTransactionsAux entity);
        void Delete(int id);
        ChargeTransactionsAux Find(int id);
        IQueryable<ChargeTransactionsAux> FindByFilter(IFilter<ChargeTransactionsAux> filter);
        ChargeTransactionsAux FindByParameters(params object[] parameters);
        IQueryable<ChargeTransactionsAux> GetAll(int skip = 0, int take = -1);
        ChargeTransactionsAux GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(ChargeTransactionsAux entity);
        void InsertOrUpdateGraph(ChargeTransactionsAux entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
