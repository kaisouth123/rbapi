﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;

namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IAccountAccessor
    {
        VendorRepository<Account> AccountRepository { get; }
        void Save();
    }
}
