﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;

namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface IContactAccessor
    {
        VendorRepository<Address> AddressRepository { get; }
        VendorRepository<Contact> ContactRepository { get; }
        VendorRepository<ContactType> ContactTypeRepository { get; }
        void Save();
    }
}
