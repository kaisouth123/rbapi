﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IScheduledPaymentRepository
    {
        IQueryable<ScheduledPayment> AllIncluding(params Expression<Func<ScheduledPayment, object>>[] includeProperties);
        void Delete(ScheduledPayment entity);
        void Delete(int id);
        ScheduledPayment Find(int id);
        IQueryable<ScheduledPayment> FindByFilter(IFilter<ScheduledPayment> filter);
        IQueryable<ScheduledPayment> FindByFilterIncluding(IFilter<ScheduledPayment> filter, params Expression<Func<ScheduledPayment, object>>[] includeProperties);
        ScheduledPayment FindByParameters(params object[] parameters);
        IQueryable<ScheduledPayment> GetAll(int skip = 0, int take = -1);
        ScheduledPayment GetEntityFromCollection(params object[] parameters);
        void InsertOrUpdate(ScheduledPayment entity);
        void InsertOrUpdateGraph(ScheduledPayment entityGraph);
        void SetSaveParams(Type type, ISaveParameters saveParams);
    }
}
