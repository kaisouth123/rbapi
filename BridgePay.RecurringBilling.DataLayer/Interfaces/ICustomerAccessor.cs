﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public interface ICustomerAccessor
    {
        CustomerBaseRepository<ContactType> ContactTypeRepository { get; }
        CustomerBaseRepository<CustomerAddress> CustomerAddressRepository { get; }
        CustomerBaseRepository<Customer> CustomerRepository { get; }
        void Save();
    }
}
