﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Interfaces
{
    public interface IResellerContext : IContext
    {
        DbSet<Reseller> Resellers { get; set; }
        DbSet<MerchantAccount> MerchantAccounts { get; set; }
        DbSet<UnipayMerchant> Merchants { get; set; }
    }
}
