﻿using Bridgepay.RecurringBilling.Models;
using System.Data.Entity.ModelConfiguration;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class WalletMap : EntityTypeConfiguration<Wallet>
    {
        public WalletMap()
            : this("dbo")
        {
        }

        public WalletMap(string schema)
        {
            ToTable("Wallet", schema);
            HasKey(x => x.WalletId);

            Property(x => x.WalletId).HasColumnName(@"WalletId").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.SiteId).HasColumnName(@"SiteId").HasColumnType("uniqueidentifier").IsRequired();
            Property(x => x.Name).HasColumnName(@"Name").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50);
            Property(x => x.Description).HasColumnName(@"Description").HasColumnType("varchar(max)").IsOptional().IsUnicode(false);
            Property(x => x.CustomerAccountNumber).HasColumnName(@"CustomerAccountNumber").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.IsActive).HasColumnName(@"IsActive").HasColumnType("bit").IsRequired();
            Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("datetime").IsRequired();
            Property(x => x.UpdateDate).HasColumnName(@"UpdateDate").HasColumnType("datetime").IsOptional();
            Property(x => x.CustomerWalletId).HasColumnName(@"CustomerWalletID").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(128);
        }
    }
}
