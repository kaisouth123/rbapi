using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class AddressMap : EntityTypeConfiguration<Address>
    {
        public AddressMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.AddressStreet)
                .HasMaxLength(25);

            this.Property(t => t.AddressStreetTwo)
                .HasMaxLength(25);

            this.Property(t => t.AddressCity)
                .HasMaxLength(25);

            this.Property(t => t.AddressState)
                .HasMaxLength(25);

            this.Property(t => t.AddressCountry)
                .HasMaxLength(25);

            this.Property(t => t.AddressZip)
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("Addresses");
            this.Property(t => t.Id).HasColumnName("AddressId");
            this.Property(t => t.AddressStreet).HasColumnName("AddressStreet");
            this.Property(t => t.AddressStreetTwo).HasColumnName("AddressStreetTwo");
            this.Property(t => t.AddressCity).HasColumnName("AddressCity");
            this.Property(t => t.AddressState).HasColumnName("AddressState");
            this.Property(t => t.AddressCountry).HasColumnName("AddressCountry");
            this.Property(t => t.AddressZip).HasColumnName("AddressZip");
            this.Ignore(t => t.ObjectState);
        }
    }
}
