using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ContactTypeMap : EntityTypeConfiguration<ContactType>
    {
        public ContactTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.TypeName)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("ContactTypes");
            this.Property(t => t.Id).HasColumnName("ContactTypeId");
            this.Property(t => t.TypeName).HasColumnName("TypeName");
            this.Ignore(t => t.ObjectState);
        }
    }
}
