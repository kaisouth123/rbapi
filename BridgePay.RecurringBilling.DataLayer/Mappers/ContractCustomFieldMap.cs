﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ContractCustomFieldMap : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<ContractCustomField>
    {
        public ContractCustomFieldMap()
            : this("dbo")
        {
        }

        public ContractCustomFieldMap(string schema)
        {
            ToTable("vw_ContractCustomFields", schema);
            HasKey(x => new { x.OwnerId, x.TableName, x.Id, x.EntityId, x.FieldName, x.DisplayName, x.Value, x.CustomFieldConfigId });

            Property(x => x.OwnerId).HasColumnName(@"OwnerId").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.TableName).HasColumnName(@"TableName").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(150).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.Id).HasColumnName(@"Id").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.EntityId).HasColumnName(@"EntityId").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.FieldName).HasColumnName(@"FieldName").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(150).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.DisplayName).HasColumnName(@"DisplayName").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(150).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.Value).HasColumnName(@"Value").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(255).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.CustomFieldConfigId).HasColumnName(@"CustomFieldConfigID").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            HasRequired(a => a.Contract).WithMany(b => b.ContractCustomFields).HasForeignKey(c => c.EntityId).WillCascadeOnDelete(false);
            Property(x => x.CustomFieldConfigId).HasColumnName(@"CustomFieldConfigID").HasColumnType("int").IsRequired();
        }
    }
}
