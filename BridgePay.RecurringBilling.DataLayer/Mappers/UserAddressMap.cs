﻿using Bridgepay.RecurringBilling.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class UserAddressMap : EntityTypeConfiguration<UserAddress>
    {
        public UserAddressMap()
        {
            // Primary Key
            this.HasKey(t => t.UserAddressId);

            // Properties
            this.Property(t => t.AddressId)
                .IsRequired();

            this.Property(t => t.Primary);

            // Table & Column Mappings
            this.ToTable("UserAddress");
            this.Property(t => t.UserAddressId).HasColumnName("UserAddressId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.AddressId).HasColumnName("AddressId");
            this.Property(t => t.Primary).HasColumnName("Primary");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserAddresses)
                .HasForeignKey(d => d.UserId);
            this.HasRequired(t => t.Address);
            //.WithMany(t => t.UserAddresses)
            //.HasForeignKey(d => d.AddressId);

        }
    }
}
