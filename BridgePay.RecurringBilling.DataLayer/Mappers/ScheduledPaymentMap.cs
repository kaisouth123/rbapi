using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ScheduledPaymentMap : EntityTypeConfiguration<ScheduledPayment>
    {
        public ScheduledPaymentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("ScheduledPayments");
            this.Property(t => t.Id).HasColumnName("SchedulePaymentId");
            this.Property(t => t.ContractId).HasColumnName("ContractID");
            this.Property(t => t.BillDate).HasColumnName("BillDate");
            this.Property(t => t.BillAmount).HasColumnName("BillAmount");
            this.Property(t => t.Paid).HasColumnName("Paid");
            this.Ignore(t => t.ObjectState);

            // Relationships
            this.HasRequired(t => t.Contract)
                .WithMany(t => t.ScheduledPayments)
                .HasForeignKey(d => d.ContractId);

        }
    }
}
