﻿using Bridgepay.RecurringBilling.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class UserNameMap : EntityTypeConfiguration<UserName>
    {
        public UserNameMap()
        {
            // Primary Key
            this.HasKey(t => t.UserNameId);

            // Properties
            this.Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MiddleName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("UserName");
            this.Property(t => t.UserNameId).HasColumnName("UserNameId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.MiddleName).HasColumnName("MiddleName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Primary).HasColumnName("Primary");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserNames)
                .HasForeignKey(d => d.UserId);

        }
    }
}
