using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ChargeTransactionsAuxMap : EntityTypeConfiguration<ChargeTransactionsAux>
    {
        public ChargeTransactionsAuxMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CHARGE_TRANSACTION_AUX");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ContractId).HasColumnName("CHARGE_TRANSACTION_FK");
            this.Ignore(t => t.ObjectState);

            // Relationships


        }
    }
}
