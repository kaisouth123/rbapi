using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class IntervalMap : EntityTypeConfiguration<Interval>
    {
        public IntervalMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(75);

            // Table & Column Mappings
            this.ToTable("Intervals");
            this.Property(t => t.Id).HasColumnName("IntervalId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Ignore(t => t.ObjectState);
        }
    }
}
