using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class BillingFrequencyMap : EntityTypeConfiguration<BillingFrequency>
    {
        public BillingFrequencyMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.FrequencyName)
                .HasMaxLength(200);

            this.Property(t => t.Days)
                .HasMaxLength(7);

            // Table & Column Mappings
            this.ToTable("BillingFrequencies");
            this.Property(t => t.Id).HasColumnName("FrequencyId");
            this.Property(t => t.FrequencyName).HasColumnName("FrequencyName");
            this.Property(t => t.MerchantId).HasColumnName("MerchantId");
            this.Property(t => t.IntervalId).HasColumnName("IntervalId");
            this.Property(t => t.NoEnd).HasColumnName("NoEnd");
            this.Property(t => t.Frequency).HasColumnName("Frequency");
            this.Property(t => t.DayMonth).HasColumnName("DayMonth");
            this.Property(t => t.WeekMonth).HasColumnName("WeekMonth");
            this.Property(t => t.Month).HasColumnName("Month");
            this.Property(t => t.Days).HasColumnName("Days");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Ignore(t => t.ObjectState);
            this.Ignore(t => t.Description);

            // Relationships
            this.HasRequired(t => t.Interval)
                .WithMany(t => t.BillingFrequencies)
                .HasForeignKey(d => d.IntervalId);
        }
    }
}
