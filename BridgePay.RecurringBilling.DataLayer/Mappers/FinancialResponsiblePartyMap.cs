using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class FinancialResponsiblePartyMap : EntityTypeConfiguration<FinancialResponsibleParty>
    {
        public FinancialResponsiblePartyMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("FinancialResponsibleParties");
            this.Property(t => t.Id).HasColumnName("FinancialResponsiblePartyId");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.ContactId).HasColumnName("ContactId");
            this.Property(t => t.MerchantId).HasColumnName("MerchantId");
            this.Property(t => t.Order).HasColumnName("Order");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.Guid).HasColumnName("Guid");
            this.Ignore(t => t.Customer);
            this.Ignore(t => t.ObjectState);

            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.FinancialResponsibleParties)
                .HasForeignKey(d => d.AccountId);
            this.HasRequired(t => t.Contact)
                .WithMany(t => t.FinancialResponsibleParties)
                .HasForeignKey(d => d.ContactId);

        }
    }
}
