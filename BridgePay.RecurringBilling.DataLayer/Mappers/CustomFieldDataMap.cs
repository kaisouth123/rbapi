﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class CustomFieldDataMap : EntityTypeConfiguration<CustomFieldData>
    {
        public CustomFieldDataMap()
           : this("dbo")
        {
        }

        public CustomFieldDataMap(string schema)
        {
            ToTable("CUSTOM_FIELD_DATA", schema);
            Property(x => x.Id).HasColumnName(@"ID").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.CustomFieldConfigId).HasColumnName(@"CUSTOM_FIELD_CONFIG_ID").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.EntityId).HasColumnName(@"ENTITY_ID").HasColumnType("varchar").IsRequired();
            Property(x => x.Value).HasColumnName(@"VALUE").HasColumnType("varchar").IsRequired();
            HasRequired(x => x.CustomField).WithMany(y => y.CustomFieldDatas).HasForeignKey(z => z.CustomFieldConfigId).WillCascadeOnDelete(false);
            //HasRequired(a => a.Contract).WithMany(b => b.CustomFieldDatas).HasForeignKey(c => c.EntityId).WillCascadeOnDelete(false);
        }
    }
}
