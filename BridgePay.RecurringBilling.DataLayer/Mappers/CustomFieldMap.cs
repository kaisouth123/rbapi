﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class CustomFieldMap : EntityTypeConfiguration<CustomField>
    {
        public CustomFieldMap()
           : this("dbo")
        {
        }

        public CustomFieldMap(string schema)
        {
            ToTable("CUSTOM_FIELD_CONFIG", schema);

            Property(x => x.Id).HasColumnName(@"ID").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.OrganizationId).HasColumnName(@"Organization_Id").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.FieldName).HasColumnName(@"Field_Name").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.FieldDisplayName).HasColumnName(@"FIELD_DISPLAY_NAME").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.FieldTypeId).HasColumnName(@"FIELD_TYPE_ID").HasColumnType("int").IsRequired();
            Property(x => x.FieldDefault).HasColumnName(@"Field_Default").HasColumnType("varchar").IsOptional();
            Property(x => x.FieldRegEx).HasColumnName(@"Field_RegEx").HasColumnType("varchar").IsRequired();
            Property(x => x.SourceEntity).HasColumnName(@"SOURCE_ENTITY").HasColumnType("int").IsOptional();
            Property(x => x.SourceField).HasColumnName(@"SOURCE_FIELD").HasColumnType("int").IsOptional();
            Property(x => x.FieldEntityId).HasColumnName(@"CUSTOMIZABLE_ENTITY_ID").HasColumnType("int").IsRequired();
            HasRequired(x => x.CustomFieldType).WithMany(y => y.CustomFields).HasForeignKey(z => z.FieldTypeId).WillCascadeOnDelete(false);
            HasRequired(x => x.Organization).WithMany(y => y.CustomFields).HasForeignKey(z => z.OrganizationId).WillCascadeOnDelete(false);
            HasRequired(x => x.CustomizableEntity).WithMany(y => y.CustomFields).HasForeignKey(z => z.FieldEntityId).WillCascadeOnDelete(false);
        }
    }
}
