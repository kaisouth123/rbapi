﻿using Bridgepay.RecurringBilling.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class UserEmailMap : EntityTypeConfiguration<UserEmail>
    {
        public UserEmailMap()
        {
            // Primary Key
            this.HasKey(t => t.UserEmailId);

            // Properties
            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Type)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("UserEmail");
            this.Property(t => t.UserEmailId).HasColumnName("UserEmailId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Primary).HasColumnName("Primary");

            // Relationships
            this.HasRequired(t => t.User)
                .WithMany(t => t.UserEmails)
                .HasForeignKey(d => d.UserId);

        }
    }
}
