using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ChargeMap : EntityTypeConfiguration<Charge>
    {
        public ChargeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ContractCharges");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.ContractId).HasColumnName("ContractID");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.TaxAmount).HasColumnName("TaxAmount");
            this.Ignore(t => t.ObjectState);
            // Relationships

            this.HasOptional(t => t.Contract)
                .WithMany(t => t.Charges)
                .HasForeignKey(d => d.ContractId);
        }
    }
}
