﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class CustomFieldActivationMap : EntityTypeConfiguration<CustomFieldActivation>
    {
        public CustomFieldActivationMap()
           : this("dbo")
        {
        }

        public CustomFieldActivationMap(string schema)
        {
            ToTable("CUSTOM_FIELD_ACTIVATION", schema);
            Property(x => x.Id).HasColumnName(@"ID").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.OrganizationId).HasColumnName(@"Organization_Id").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.CustomFieldConfigId).HasColumnName(@"CUSTOM_FIELD_CONFIG_ID").HasColumnType("int").IsRequired();
            HasRequired(x => x.CustomField).WithMany(y => y.CustomFieldActivations).HasForeignKey(z => z.CustomFieldConfigId).WillCascadeOnDelete(false);
            HasRequired(x => x.Organization).WithMany(y => y.CustomFieldActivations).HasForeignKey(z => z.OrganizationId).WillCascadeOnDelete(false);
        }
    }
}
