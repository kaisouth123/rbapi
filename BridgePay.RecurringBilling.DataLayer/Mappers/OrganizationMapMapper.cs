﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class OrganizationMapMapper : EntityTypeConfiguration<OrganizationMap>
    {
        public OrganizationMapMapper() : this("dbo")
        {
        }

        public OrganizationMapMapper(string schema)
        {
            ToTable("vw_OrganizationMap", schema);
            HasKey(x => new
            {
                x.OrgId, x.OrgType
            });
            Property(x => x.OrgId).HasColumnName("OrgId").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ParentId).HasColumnName("ParentId").HasColumnType("int").IsOptional();
            Property(x => x.GrandParentId).HasColumnName("GrandParentID").HasColumnType("int").IsOptional();
            Property(x => x.GreatGrandParentId).HasColumnName("GreatGrandParentID").HasColumnType("int").IsOptional();
            Property(x => x.OrgType).HasColumnName("OrgType").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.ParentType).HasColumnName("ParentType").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.GrandParentType).HasColumnName("GrandParentType").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.GreatGrandParentType).HasColumnName("GreatGrandParentType").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.UnipayId).HasColumnName("UnipayID").HasColumnType("int").IsOptional();
            Property(x => x.WalletSiteId).HasColumnName("WalletSiteId").HasColumnType("uniqueidentifier").IsOptional().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(p => p.OrganizationName).HasColumnName("OrganizationName").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(50);
            Property(p => p.ParentName).HasColumnName("ParentName").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(p => p.GrandParentName).HasColumnName("GrandParentName").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(p => p.GreatGrandParentName).HasColumnName("GreatGrandParentName").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(p => p.ParentUnipayId).HasColumnName("ParentUnipayID").HasColumnType("int").IsOptional();
            Property(p => p.GrandParentUnipayId).HasColumnName("GrandParentUnipayID").HasColumnType("int").IsOptional();
            Property(p => p.GreatGrandParentUnipayId).HasColumnName("GreatGrandParentUnipayID").HasColumnType("int").IsOptional();
        }
    }
}
