﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class CustomizableEntityMap : EntityTypeConfiguration<CustomizableEntity>
    {
        public CustomizableEntityMap()
           : this("dbo")
        {
        }

        public CustomizableEntityMap(string schema)
        {
            ToTable("CUSTOMIZABLE_ENTITY", schema);

            Property(x => x.Id).HasColumnName(@"ID").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName(@"Name").HasColumnType("varchar").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.Name).HasColumnName(@"Table_Name").HasColumnType("varchar").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
        }
    }
}
