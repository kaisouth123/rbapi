using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ContractMap : EntityTypeConfiguration<Contract>
    {
        public ContractMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ContractName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Contracts");
            this.Property(t => t.Id).HasColumnName("ContractId");
            this.Property(t => t.ContractName).HasColumnName("ContractName");
            this.Property(t => t.ProductId).HasColumnName("ProductId");
            this.Property(t => t.MerchantId).HasColumnName("MerchantId");
            this.Property(t => t.BillingFrequencyId).HasColumnName("BillingFrequencyId");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.BillAmount).HasColumnName("BillAmount");
            this.Property(t => t.TaxAmount).HasColumnName("TaxAmount");
            this.Property(t => t.AmountRemaining).HasColumnName("AmountRemaining");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.LastBillDate).HasColumnName("LastBillDate");
            this.Property(t => t.NextBillDate).HasColumnName("NextBillDate");
            this.Property(t => t.NumFailures).HasColumnName("NumFailures");
            this.Property(t => t.MaxFailures).HasColumnName("MaxFailures");
            this.Property(t => t.RetryWaitTime).HasColumnName("RetryWaitTime");
            this.Property(t => t.EmailCustomerAtFailure).HasColumnName("EmailCustomerAtFailure");
            this.Property(t => t.EmailMerchantAtFailure).HasColumnName("EmailMerchantAtFailure");
            this.Property(t => t.EmailCustomerReceipt).HasColumnName("EmailCustomerReceipt");
            this.Property(t => t.EmailMerchantReceipt).HasColumnName("EmailMerchantReceipt");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Ignore(t => t.ObjectState);
            this.Ignore(t => t.CreatedOn);

            // Relationships
            this.HasRequired(t => t.Account)
                .WithMany(t => t.Contracts)
                .HasForeignKey(d => d.AccountId);
            this.HasRequired(t => t.BillingFrequency)
                .WithMany(t => t.Contracts)
                .HasForeignKey(d => d.BillingFrequencyId);
            this.HasOptional(t => t.Product)
                .WithMany(t => t.Contracts)
                .HasForeignKey(d => d.ProductId);
            //this.HasMany(t => t.ContractCustomFields).WithRequired(f => f.Contract);
        }
    }
}
