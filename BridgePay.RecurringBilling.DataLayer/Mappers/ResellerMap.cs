﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ResellerMap : EntityTypeConfiguration<Reseller>
    {
        public ResellerMap()
        {
            // Primary Key
            this.HasKey(t => t.ResellerID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("IAPP_RESELLER");
            this.Property(t => t.ResellerID).HasColumnName("CODE");
            this.Property(t => t.Name).HasColumnName("NAME");

        }
    }
}
