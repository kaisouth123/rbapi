using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ContractReferenceMap : EntityTypeConfiguration<ContractReference>
    {
        public ContractReferenceMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ContractName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Contracts");
            this.Property(t => t.Id).HasColumnName("ContractId");
            this.Property(t => t.ContractName).HasColumnName("ContractName");
            this.Property(t => t.ProductId).HasColumnName("ProductId");
            this.Property(t => t.BillingFrequencyId).HasColumnName("BillingFrequencyId");
            this.Property(t => t.AccountId).HasColumnName("AccountId");
            this.Property(t => t.BillAmount).HasColumnName("BillAmount");
            this.Property(t => t.TaxAmount).HasColumnName("TaxAmount");
            this.Property(t => t.MaxAmount).HasColumnName("MaxAmount");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.LastBillDate).HasColumnName("LastBillDate");
            this.Property(t => t.EmailCustomerAtFailure).HasColumnName("EmailCustomerAtFailure");
            this.Property(t => t.EmailMerchantAtFailure).HasColumnName("EmailMerchantAtFailure");
            this.Property(t => t.EmailCustomerReceipt).HasColumnName("EmailCustomerReceipt");
            this.Property(t => t.EmailMerchantReceipt).HasColumnName("EmailMerchantReceipt");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.HasScheduledPayment).HasColumnName("HasScheduledPayment");
            this.Ignore(t => t.ObjectState);
            

            // Relationships
           

        }
    }
}
