﻿using Bridgepay.RecurringBilling.Models;
using System.Data.Entity.ModelConfiguration;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class PaymentMethodMap : EntityTypeConfiguration<PaymentMethod>
    {
        public PaymentMethodMap()
            : this("dbo")
        {
        }

        public PaymentMethodMap(string schema)
        {
            ToTable("PaymentMethod", schema);
            HasKey(x => new { x.WalletId, x.PaymentMethodId });

            Property(x => x.PaymentMethodId).HasColumnName(@"PaymentMethodId").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.PaymentMethodType).HasColumnName(@"PaymentMethodType").HasColumnType("tinyint").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.CardType).HasColumnName(@"CardType").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.Description).HasColumnName(@"Description").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(65);
            Property(x => x.ExpirationDate).HasColumnName(@"ExpirationDate").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(4);
            Property(x => x.LastFour).HasColumnName(@"LastFour").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(4);
            Property(x => x.Token).HasColumnName(@"Token").HasColumnType("varchar").IsRequired().IsUnicode(false).HasMaxLength(30).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.RoutingNo).HasColumnName(@"RoutingNo").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(15);
            Property(x => x.AccountType).HasColumnName(@"AccountType").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.AccountHolderName).HasColumnName(@"AccountHolderName").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.AccountHolderAddress).HasColumnName(@"AccountHolderAddress").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.AccountHolderCity).HasColumnName(@"AccountHolderCity").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.AccountHolderState).HasColumnName(@"AccountHolderState").HasColumnType("char").IsOptional().IsFixedLength().IsUnicode(false).HasMaxLength(2);
            Property(x => x.AccountHolderZip).HasColumnName(@"AccountHolderZip").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(10);
            Property(x => x.AccountHolderPhone).HasColumnName(@"AccountHolderPhone").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(20);
            Property(x => x.AccountHolderEmail).HasColumnName(@"AccountHolderEmail").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(50);
            Property(x => x.IsActive).HasColumnName(@"IsActive").HasColumnType("bit").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.Order).HasColumnName(@"Order").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.WalletId).HasColumnName(@"WalletId").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("datetime").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.UpdateDate).HasColumnName(@"UpdateDate").HasColumnType("datetime").IsOptional();
            Property(x => x.FailureCount).HasColumnName(@"FailureCount").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.LastResult).HasColumnName(@"LastResult").HasColumnType("varchar").IsOptional().IsUnicode(false).HasMaxLength(200);
            Property(x => x.MaxFailures).HasColumnName(@"MaxFailures").HasColumnType("int").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);
            Property(x => x.DetailId).HasColumnName(@"DetailId").HasColumnType("int").IsOptional();
            Property(x => x.ExData).HasColumnName(@"ExData").HasColumnType("nvarchar(max)").IsOptional();
            Property(x => x.PaymentKey).HasColumnName(@"PaymentKey").HasColumnType("int").IsOptional();

            // Foreign keys
            HasRequired(a => a.Wallet).WithMany(b => b.PaymentMethods).HasForeignKey(c => c.WalletId).WillCascadeOnDelete(false);
        }
    }
}
