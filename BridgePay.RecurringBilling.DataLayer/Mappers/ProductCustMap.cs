using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ProductCustMap : EntityTypeConfiguration<Product>
    {
        public ProductCustMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Products");
            this.Property(t => t.Id).HasColumnName("ProductId");
            this.Property(t => t.ProductName).HasColumnName("ProductName");
            this.Property(t => t.ProductDefaultAmount).HasColumnName("ProductDefaultAmount");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.ContactId).HasColumnName("ContactId");
            this.Property(t => t.MerchantId).HasColumnName("MerchantId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Ignore(t => t.ObjectState);

            // Relationships

        }
    }
}
