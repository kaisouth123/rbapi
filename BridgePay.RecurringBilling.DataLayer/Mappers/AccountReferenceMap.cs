﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class AccountReferenceMap: EntityTypeConfiguration<AccountReference>
    {
        public AccountReferenceMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.AccountName)
                .HasMaxLength(50);

            this.Property(t => t.AccountNumber)
                .HasMaxLength(25);

            // Table & Column Mappings
            this.ToTable("Accounts");
            this.Property(t => t.Id).HasColumnName("AccountId");
            this.Property(t => t.MerchantId).HasColumnName("MerchantId");
            this.Property(t => t.AccountName).HasColumnName("AccountName");
            this.Property(t => t.AccountNumber).HasColumnName("AccountNumber");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Ignore(t => t.ObjectState);


            // Relationships
            
        }
    }
}
