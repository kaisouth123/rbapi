using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class ContactMap : EntityTypeConfiguration<Contact>
    {
        public ContactMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.ContactFirstName)
                .HasMaxLength(30);

            this.Property(t => t.ContactLastName)
                .HasMaxLength(30);

            this.Property(t => t.CompanyName)
                .HasMaxLength(35);

            this.Property(t => t.ContactDepartment)
                .HasMaxLength(20);

            this.Property(t => t.ContactTitle)
                .HasMaxLength(20);

            this.Property(t => t.ContactEmail)
                .HasMaxLength(50);

            this.Property(t => t.ContactDayPhone)
                .HasMaxLength(20);

            this.Property(t => t.ContactEveningPhone)
                .HasMaxLength(20);

            this.Property(t => t.ContactMobilePhone)
                .HasMaxLength(20);

            this.Property(t => t.ContactFax)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Contacts");
            this.Property(t => t.Id).HasColumnName("ContactId");
            this.Property(t => t.ContactTypeId).HasColumnName("ContactTypeId");
            this.Property(t => t.ContactFirstName).HasColumnName("ContactFirstName");
            this.Property(t => t.ContactLastName).HasColumnName("ContactLastName");
            this.Property(t => t.CompanyName).HasColumnName("CompanyName");
            this.Property(t => t.AddressId).HasColumnName("AddressId");
            this.Property(t => t.ContactDepartment).HasColumnName("ContactDepartment");
            this.Property(t => t.ContactTitle).HasColumnName("ContactTitle");
            this.Property(t => t.ContactEmail).HasColumnName("ContactEmail");
            this.Property(t => t.ContactDayPhone).HasColumnName("ContactDayPhone");
            this.Property(t => t.ContactEveningPhone).HasColumnName("ContactEveningPhone");
            this.Property(t => t.ContactMobilePhone).HasColumnName("ContactMobilePhone");
            this.Property(t => t.ContactFax).HasColumnName("ContactFax");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Ignore(t => t.ObjectState);

            // Relationships
            this.HasOptional(t => t.ContactAddress)
                .WithMany(t => (ICollection<Contact>)t.Contacts)
                .HasForeignKey(d => d.AddressId);
            this.HasRequired(t => t.ContactType)
                .WithMany(t => (ICollection<Contact>)t.Contacts)
                .HasForeignKey(d => d.ContactTypeId);

        }
    }
}
