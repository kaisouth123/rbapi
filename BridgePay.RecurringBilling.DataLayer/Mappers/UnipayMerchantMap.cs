﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class UnipayMerchantMap : EntityTypeConfiguration<UnipayMerchant>
    {
        public UnipayMerchantMap()
        {
            // Primary Key
            this.HasKey(t => t.MerchantID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("IAPP_MERCHANT");
            this.Property(t => t.MerchantID).HasColumnName("CODE");
            this.Property(t => t.ResellerID).HasColumnName("RESELLER_FK");
            this.Property(t => t.Name).HasColumnName("NAME");

            // Relationships
            this.HasOptional(t => t.Reseller)
                .WithMany(t => t.Merchants)
                .HasForeignKey(d => d.ResellerID);


        }
    }
}
