﻿using Bridgepay.RecurringBilling.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            this.HasKey(t => t.UserId);

            // Properties
            this.Property(t => t.UserName)
                .IsRequired()
                .HasMaxLength(56);

            // Table & Column Mappings
            this.ToTable("User");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.LockedOut).HasColumnName("LockedOut");
            this.Property(t => t.ResellerId).HasColumnName("ResellerId");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.MerchantId).HasColumnName("MerchantId");
            this.Property(t => t.MerchantAccountId).HasColumnName("MerchantAccountId");
            this.Property(t => t.EnableMsrJavaApplet).HasColumnName("EnableMsrJavaApplet");
            this.Property(t => t.IsApiUser).HasColumnName("IsApiUser"); 
        }
    }
}
