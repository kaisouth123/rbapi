using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.RecurringBilling.Models;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class MerchantMap : EntityTypeConfiguration<Merchant>
    {
        public MerchantMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("IAPP_MERCHANT_ACCOUNT");
            this.Property(t => t.Id).HasColumnName("CODE");
            this.Property(t => t.Name).HasColumnName("NAME").HasMaxLength(100);
            this.Property(t => t.IsActive).HasColumnName("IS_ACTIVE");
            this.Property(t => t.MerchantCode).HasColumnName("MERCHANT_FK");
            this.Ignore(t => t.ObjectState);
        }
    }
}
