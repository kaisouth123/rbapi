﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer.Mappers
{
    public class MerchantAccountMap : EntityTypeConfiguration<MerchantAccount>
    {
        public MerchantAccountMap()
        {
            // Primary Key
            this.HasKey(t => t.MerchantAccountID);

            // Properties
            this.Property(t => t.Name)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("IAPP_MERCHANT_ACCOUNT");
            this.Property(t => t.MerchantAccountID).HasColumnName("CODE");
            this.Property(t => t.MerchantID).HasColumnName("MERCHANT_FK");
            this.Property(t => t.Name).HasColumnName("NAME");

            // Relationships
            this.HasRequired(t => t.Merchant)
                .WithMany(t => t.MerchantAccounts)
                .HasForeignKey(d => d.MerchantID);


        }
    }
}
