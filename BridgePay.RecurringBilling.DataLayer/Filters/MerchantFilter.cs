﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.DataLayer
{
    [DataContract]
    public class MerchantFilter : IFilter<Merchant>
    {
        [DataMember]public Merchant entity { get; set; }
        [DataMember]public List<String> IgnoreProperties { get; set; }
        [DataMember]public List<ExpressionOperator> Operators { get; set;}
        [DataMember]public bool SearchFalse { get; set; }
        [DataMember]public bool IncludeGraph {get; set;}
        [DataMember]public int Skip { get; set; }
        [DataMember]public int Take { get; set; }
        [DataMember]public string OrderBy { get; set;}
        [DataMember]public bool SortDesc {get; set;}

        public MerchantFilter()
        {
        }

        public MerchantFilter(Merchant Merchant, List<String> ignoreProperties=null, bool searchFalse=false, bool includeGraph=false)
        {
            this.entity = Merchant;
            this.IgnoreProperties = ignoreProperties;
            this.SearchFalse = searchFalse;
            this.IncludeGraph = includeGraph;
            this.Skip = 0;
            this.Take = -1;
            this.OrderBy = "Id";
            this.SortDesc = false;
        }

        public System.Linq.Expressions.Expression<Func<Merchant, bool>> GetFilterExpression(Merchant Merchant, List<String> ignoreProperties = null, bool searchFalse = false, List<ExpressionOperator> operators = null)
        {
            Expression<Func<Merchant, bool>> condition = null;

            condition = ExpressionBuilder.ReturnExpression<Merchant>(Merchant, ignoreProperties, searchFalse, operators);

            return condition.Expand();
        }
    }

}
