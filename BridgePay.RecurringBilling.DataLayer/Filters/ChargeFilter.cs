﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.DataLayer
{
    [DataContract]
    public class ChargeTransactionsAuxFilter : IFilter<ChargeTransactionsAux>
    {
        [DataMember]public ChargeTransactionsAux entity { get; set; }
        [DataMember]public List<String> IgnoreProperties { get; set; }
        [DataMember]public List<ExpressionOperator> Operators { get; set;}
        [DataMember]public bool SearchFalse { get; set; }
        [DataMember]public bool IncludeGraph { get; set; }
        [DataMember]public int Skip { get; set; }
        [DataMember]public int Take { get; set; }
        [DataMember]public string OrderBy { get; set; }
        [DataMember]public bool SortDesc { get; set; }

        public ChargeTransactionsAuxFilter()
        {
        }

        public ChargeTransactionsAuxFilter(ChargeTransactionsAux ChargeTransactionsAux, List<String> ignoreProperties = null, bool searchFalse = false, bool includeGraph = false, List<ExpressionOperator> operators = null)
        {
            this.entity = ChargeTransactionsAux;
            this.IgnoreProperties = ignoreProperties;
            this.SearchFalse = searchFalse;
            this.IncludeGraph = includeGraph;
            this.Skip = 0;
            this.Take = -1;
            this.OrderBy = "Id";
            this.SortDesc = false;
        }

        public System.Linq.Expressions.Expression<Func<ChargeTransactionsAux, bool>> GetFilterExpression(ChargeTransactionsAux ChargeTransactionsAux, List<String> ignoreProperties = null, bool searchFalse = false, List<ExpressionOperator> operators = null)
        {
            Expression<Func<ChargeTransactionsAux, bool>> condition = null;

            condition = ExpressionBuilder.ReturnExpression<ChargeTransactionsAux>(ChargeTransactionsAux, ignoreProperties, searchFalse);

            return condition.Expand();
        }

    }

}
