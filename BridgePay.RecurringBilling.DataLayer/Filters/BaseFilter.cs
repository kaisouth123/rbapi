﻿using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.DataLayer
{
    [DataContract]
    public class BaseFilter<T> : IFilter<T>  where T : class, IBaseEntity, new()
    {
        private const string MerchantIdPropertyName = "MerchantId";

        public BaseFilter()
        {
        }

        public BaseFilter(T someEntity, List<String> ignoreProperties = null, bool searchFalse = false, bool includeGraph = false, List<ExpressionOperator> operators = null)
        {
            this.entity = someEntity;
            this.IgnoreProperties = ignoreProperties;
            this.SearchFalse = searchFalse;
            this.IncludeGraph = includeGraph;
            this.Skip = 0;
            this.Take = -1;
            this.OrderBy = "Id";
            this.SortDesc = false;
            this.Operators = operators;
        }

        [DataMember]public T entity { get; set; }
        [DataMember]public List<String> IgnoreProperties { get; set; }
        [DataMember]public List<ExpressionOperator> Operators { get; set;}
        [DataMember]public bool SearchFalse { get; set; }
        [DataMember]public bool IncludeGraph {get; set;}
        [DataMember]public int Skip { get; set; }
        [DataMember]public int Take { get; set; }
        [DataMember]public string OrderBy { get; set;}
        [DataMember]public bool SortDesc {get; set;}

        /// <summary>
        /// Gets/sets the merchant group organization ID when filtering on a merchant group.
        /// </summary>
        public int? MerchantGroupOrganizationID { get; set; }

        public Expression<Func<T, bool>> GetFilterExpression(T account, List<String> ignoreProperties = null, bool searchFalse = false, List<ExpressionOperator> operators = null)
        {
            // Is the caller filtering by merchant group?
            Expression<Func<T, bool>> merchantUnipayIDsContains = null;
            if (MerchantGroupOrganizationID.HasValue && account is IHasMerchantId entityWithMerchantId)
            {
                // Get the Unipay IDs of the merchant accounts under the merchant group.
                var merchantUnipayIDs = Helpers.OrganizationHelper.GetMerchantAccountUnipayIDs(MerchantGroupOrganizationID.Value);

                // Dynamically build a "contains" query. This is probably not great in terms of performance, however the
                // organizational tables are in a completely different database than the recurring billing ones.
                // See https://stackoverflow.com/a/36548204 for details on how to build the query.
                var param = Expression.Parameter(typeof(T), "e");
                var method = merchantUnipayIDs.GetType().GetMethod(nameof(merchantUnipayIDs.Contains));
                var call = Expression.Call(Expression.Constant(merchantUnipayIDs), method, Expression.Property(param, MerchantIdPropertyName));
                merchantUnipayIDsContains = Expression.Lambda<Func<T, bool>>(call, param);

                // Ignore the entity's merchant ID property since this should not be part of the filter expression.
                if (ignoreProperties == null)
                {
                    ignoreProperties = new List<string>
                    {
                        MerchantIdPropertyName
                    };
                }
                else if (!ignoreProperties.Contains(MerchantIdPropertyName, StringComparer.InvariantCultureIgnoreCase))
                {
                    ignoreProperties.Add(MerchantIdPropertyName);
                }
            }

            Expression<Func<T, bool>> condition = null;
            if (ignoreProperties == null)
                ignoreProperties = new List<string>();
            ignoreProperties.Add("ObjectState");
            condition = Helpers.ExpressionBuilder.ReturnExpression<T>(account, ignoreProperties, searchFalse, operators).Expand();

            // Append the dynamic "contains" expression, if present.
            if (merchantUnipayIDsContains != null)
            {
                if (condition == null)
                    condition = merchantUnipayIDsContains;
                else
                    condition = condition.And(merchantUnipayIDsContains);
            }

            return condition;
        }

        public Expression<Func<T, object>> GetSortExpression(string sortExpressionStr)
        {
            var param = Expression.Parameter(typeof(T), "x");
            var sortExpression = Expression.Lambda<Func<T, object>>(Expression.Property(param, sortExpressionStr), param);
            return sortExpression;
        }
    }

}
