﻿using Bridgepay.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
namespace Bridgepay.RecurringBilling.DataLayer
{
    [DataContract]
    public class CustomFieldFilter : IFilter<CustomField>
    {

        [DataMember] public int? OrganizationId { get; set; }
        [DataMember] public string EntityName { get; set; } 
        [DataMember] public string FieldName { get; set; }
        public IDictionary<string, OrderByEnum> OrderByList { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Expression<Func<CustomField, bool>> GetFilterExpression()
        {
            Expression<Func<CustomField, bool>> startExpression = t => t.OrganizationId>0;
            Expression<Func<CustomField, bool>> idFilter = t => t.OrganizationId==OrganizationId.Value;
            Expression<Func<CustomField, bool>> entityNameFilter = t => t.CustomizableEntity.Name.Contains(EntityName);
            Expression<Func<CustomField, bool>> fieldNameFilter = t => t.FieldName.Contains(FieldName);
           

            System.Linq.Expressions.Expression<Func<CustomField, bool>> expression = startExpression;

            if (OrganizationId.HasValue) expression = expression.AndAlso(idFilter);
            if (!string.IsNullOrWhiteSpace(EntityName)) expression = expression.AndAlso(entityNameFilter);
            if (!string.IsNullOrWhiteSpace(FieldName)) expression = expression.AndAlso(fieldNameFilter);
           
            return expression;

        }

        public string GetOrderByClause()
        {
            throw new NotImplementedException();
        }

        public Expression<Func<CustomField, object>> GetOrderByExpression()
        {
            throw new NotImplementedException();
        }

        public IQueryable<CustomField> GetQueryable(IQueryable<CustomField> dbQueryable)
        {
            throw new NotImplementedException();
        }
    }
}
