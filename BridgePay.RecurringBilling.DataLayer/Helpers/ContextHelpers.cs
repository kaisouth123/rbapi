﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.Core.Framework.Interfaces;

namespace Bridgepay.RecurringBilling.DataLayer.Helpers
{
    public static class ContextHelpers
    {
        //Only use with short lived contexts 
        public static void ApplyStateChange(this DbContext context)
        {
            foreach (var entry in context.ChangeTracker.Entries<IObjectWithState>())
            {
                IObjectWithState stateInfo = entry.Entity;
                entry.State = StateHelpers.ConvertState(stateInfo.ObjectState);
            }
        }
    }
}
