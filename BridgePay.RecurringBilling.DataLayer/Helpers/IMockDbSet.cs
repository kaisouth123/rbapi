﻿using System;
namespace Bridgepay.RecurringBilling.DataLayer
{
    public interface IMockDbSet<T>
     where T : class, global::Bridgepay.RecurringBilling.Models.IBaseEntity, new()
    {
        T Add(T entity);
        T Attach(T entity);
        T Create();
        TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T;
        Type ElementType { get; }
        global::System.Linq.Expressions.Expression Expression { get; }
        T Find(params object[] keyValues);
        global::System.Collections.Generic.IEnumerator<T> GetEnumerator();
        global::System.Collections.ObjectModel.ObservableCollection<T> Local { get; }
        global::System.Linq.IQueryProvider Provider { get; }
        T Remove(T entity);
    }
}
