﻿using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Bridgepay.RecurringBilling.DataLayer.Helpers
{
    public static class ExpressionBuilder
    {
        public static Expression<Func<T, bool>> True<T>() { return f => true; }
        public static Expression<Func<T, bool>> False<T>() { return f => false; }
        private static readonly MethodInfo stringContains;

        static ExpressionBuilder()
        {
            stringContains = typeof(string).GetMethod("Contains", new[] { typeof(string) });
        }

        public static Expression<Func<T, bool>> ReturnExpression<T>(T entity, List<String> ignoreProperties = null, bool searchFalse=false, List<ExpressionOperator> operators=null)
        {
            Expression<Func<T, bool>> condition = null;
            Expression<Func<T, bool>> condition2 = null;
            if (ignoreProperties == null)
                ignoreProperties = new List<string>();
            ignoreProperties.Add("ObjectState");

            PropertyInfo[] properties = typeof(T).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if(ignoreProperties != null && ignoreProperties.Contains(property.Name))
                    continue;

                condition2 = GetExpression<T>(property, entity, searchFalse, operators);
                if (condition != null && condition2 != null)
                    condition = condition.And(condition2);
                else if(condition2 != null && condition == null)
                    condition = condition2;
            }

            return condition;
        }

        private static Expression<Func<T, bool>> GetExpression<T>(PropertyInfo property, object entity, bool searchFalse=false, List<ExpressionOperator> operators=null)
        {
            Type fieldtype = property.PropertyType;
            object objectValue = property.GetValue(entity);
            var value = Expression.Constant(property.GetValue(entity), property.PropertyType);
            ParameterExpression argParam = Expression.Parameter(typeof(T), property.Name);
            Expression nameProperty = Expression.Property(argParam, property.Name);
            Expression expression = null;

            if (CheckIfNullOrDefault(objectValue, fieldtype, searchFalse))
                return null;

            if (operators != null && operators.Where(c => c.Name == property.Name).ToList().Count > 0)
            {
                ExpressionOperator expOp = operators.Where(c => c.Name == property.Name).ToList().First();
                expression = ReturnExpression(nameProperty, value, fieldtype, expOp);
            }
            else if (property.GetCustomAttribute<AllowPartialMatchAttribute>() != null)
            {
                expression = Expression.Call(nameProperty, stringContains, value);
            }
            else
                expression = Expression.Equal(nameProperty, Expression.Convert(value, fieldtype));

            return Expression.Lambda<Func<T, bool>>(expression, argParam);
        }

        private static Expression ReturnExpression(Expression nameProperty, ConstantExpression value, Type fieldtype, ExpressionOperator expOp)
        {
            if (expOp.Operator == ExpOperator.NotEqual)
                return Expression.NotEqual(nameProperty, Expression.Convert(value, fieldtype));
            if (expOp.Operator == ExpOperator.LessThan)
                return Expression.LessThan(nameProperty, Expression.Convert(value, fieldtype));
            if (expOp.Operator == ExpOperator.LessThanOrEqual)
                return Expression.LessThanOrEqual(nameProperty, Expression.Convert(value, fieldtype));
            if (expOp.Operator == ExpOperator.GreaterThan)
                return Expression.GreaterThan(nameProperty, Expression.Convert(value, fieldtype));
            if (expOp.Operator == ExpOperator.GreaterThanOrEqual)
                return Expression.GreaterThanOrEqual(nameProperty, Expression.Convert(value, fieldtype));
            else
                return Expression.Equal(nameProperty, Expression.Convert(value, fieldtype));

        }

        public static bool CheckIfNullOrDefault(object objectValue, Type fieldType, bool searchFalse = false)
        {
            bool isNull = false;

            if (objectValue == null)
            {
                return true;
            }
            else if (fieldType.Name == typeof(Int32).Name)
            {
                if ((int)objectValue == 0)
                    return true;
            }
            else if (fieldType.Name == typeof(Int64).Name)
            {
                if (Convert.ToInt64(objectValue) == 0 || Convert.ToInt64(objectValue) == null)
                    return true;
            }
            else if (fieldType.Name == typeof(string).Name)
            {
                if (String.IsNullOrEmpty((String)objectValue))
                    return true;
            }
            else if (objectValue is ICollection)
            {
                return true;
            }
            else if (fieldType.Name == typeof(DateTime).Name)
            {
                DateTime date = (DateTime)objectValue;
                if (date == default(DateTime))
                    return true;
            }
            else if (fieldType.FullName == typeof(Boolean?).FullName)
            {
                Boolean? boolNull = (Boolean?)objectValue;
                if (boolNull == default(Boolean?))
                    return true;
            }
            else if (fieldType.FullName == typeof(int?).FullName)
            {
                int? intNull = (int?)objectValue;
                if (intNull == default(int?) || intNull == 0)
                    return true;
            }
            else if (fieldType.FullName == typeof(DateTime?).FullName)
            {
                DateTime? date = (DateTime?)objectValue;
                if (date == default(DateTime?))
                    return true;
            }
            else if (fieldType.Name == typeof(bool).Name && !searchFalse)
            {
                if ((bool)objectValue == false)
                    return true;
            }
            else if (fieldType.Name == typeof(Guid?).Name)
            {
                if ((Guid)objectValue == null)
                    return true;
            }

            return isNull;
        }

        public static IQueryable<T> OrderByExt<T>(this IQueryable<T> source, string orderProp, bool desc)
        {
            try
            {
                string command = desc ? "OrderByDescending" : "OrderBy";
                var type = typeof(T);
                var property = type.GetProperty(orderProp);
                var parameter = Expression.Parameter(type, "p");
                var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                var orderByExpression = Expression.Lambda(propertyAccess, parameter);
                var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType }, source.Expression, Expression.Quote(orderByExpression));
                return source.Provider.CreateQuery<T>(resultExpression);
            }
            catch
            {
                return source;
            }

        }

        public static IQueryable<T> SetUnchanged<T>(this IQueryable<T> source) where T : IBaseEntity
        {
            try
            {
                foreach (T entity in source)
                {
                    entity.SetUnchanged();
                }

                return source;
            }
            catch
            {
                return source;
            }

        }
    }
}