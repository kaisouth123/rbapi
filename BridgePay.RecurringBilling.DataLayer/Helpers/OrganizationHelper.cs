﻿using Bridgepay.RecurringBilling.DataLayer.Accessors;
using System.Collections.Generic;
using System.Linq;

namespace Bridgepay.RecurringBilling.DataLayer.Helpers
{
    /// <summary>
    /// Organization-related helper methods.
    /// </summary>
    public static class OrganizationHelper
    {
        /// <summary>
        /// Gets the Unipay IDs of merchant accounts under a given merchant group organization ID.
        /// </summary>
        /// <param name="merchantGroupOrgID">Merchant group organization ID.</param>
        /// <returns>The Unipay IDs of merchant accounts under a given merchant group organization ID</returns>
        public static List<int> GetMerchantAccountUnipayIDs(int merchantGroupOrgID)
        {
            var accessor = new OrganizationMapAccessor();
            var merchantAccountUnipayIDs = accessor.OrganizationMapRepository.All
                                                   .Where(om => om.ParentId == merchantGroupOrgID && om.UnipayId.HasValue)
                                                   .Select(om => om.UnipayId.Value)
                                                   .ToList();
            return merchantAccountUnipayIDs;
        }
    }
}
