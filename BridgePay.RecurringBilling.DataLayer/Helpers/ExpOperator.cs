﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer
{
    [DataContract]
    public enum ExpOperator
    {
        [EnumMember]
        Equal = 0,
        [EnumMember]
        NotEqual = 1,
        [EnumMember]
        LessThan = 2,
        [EnumMember]
        GreaterThan = 3,
        [EnumMember]
        LessThanOrEqual = 4,
        [EnumMember]
        GreaterThanOrEqual = 5,
    }
}
