﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using System.Data.Entity.Infrastructure;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class MockDbSet<T> : IDbSet<T>, IMockDbSet<T> where T : class, IBaseEntity, new()
    {
        readonly ObservableCollection<T> _items;
        readonly IQueryable _query;

        public MockDbSet()
        {
          _items = new ObservableCollection<T>();
          _query = _items.AsQueryable();
        }

        public T Add(T entity)
        {
            _items.Add(entity);
            return entity;
        }

        public T Attach(T entity)
        {
            _items.Add(entity);
            return entity;
        }

        public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
        {
            return Activator.CreateInstance<TDerivedEntity>();
        }

        public T Create()
        {
            return new T();
        }

        public T Find(params object[] keyValues)
        {
            var keyValue = (int)keyValues.FirstOrDefault();

            return this.SingleOrDefault(a => a.Id == keyValue);
        }

        public ObservableCollection<T> Local
        {
            get { return _items;  }
        }

        public T Remove(T entity)
        {
            if (entity == null)
                return null;

            int count = _items.Where(i => i.Id == entity.Id).Count();

            if (count <= 0)
                return entity;

            for (int x = 0; x < _items.Count; x++)
            {
                if (_items[x].Id == entity.Id)
                    _items.RemoveAt(x);
            }

            return entity;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public Type ElementType
        {
            get { return _query.ElementType; }
        }

        public Expression Expression
        {
            get { return _query.Expression; }
        }

        public IQueryProvider Provider
        {
            get { return _query.Provider; }
        }
    }
}
