﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ExpressionOperator
    {
        public String Name { get; set; }
        public ExpOperator Operator { get; set; }
    }
}
