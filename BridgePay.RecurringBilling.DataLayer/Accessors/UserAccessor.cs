﻿using Bridgepay.Core.DataLayer;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class UserAccessor
    {
        private readonly UnitOfWork<UserContext> _uow;
        private readonly UserRepository<User> _repo;

        public UserAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<UserContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _repo = new UserRepository<User>(_uow);
        }

        public UserRepository<User> UserRepository
        {
            get { return _repo; }
        }

        public User Save(User user)
        {
            _uow.Save();
            return user;
        }

    }
}
