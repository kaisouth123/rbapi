﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class VendorAccessor
    {
        private readonly UnitOfWork<VendorContext> _uow;
        private readonly VendorRepository<Contact> _contactRepo;
        private readonly VendorRepository<ContactType> _contactTypeRepo;
        private readonly VendorRepository<Account> _accountRepo;
        private readonly VendorRepository<Product> _productRepo;
        private readonly VendorRepository<Contract> _contractRepo;

        public VendorAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<VendorContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contactRepo = new VendorRepository<Contact>(_uow);
            _accountRepo = new VendorRepository<Account>(_uow);
            _productRepo = new VendorRepository<Product>(_uow);
            _contractRepo = new VendorRepository<Contract>(_uow);
            _contactTypeRepo = new VendorRepository<ContactType>(_uow);
        }

        public VendorAccessor(VendorRepository<Contact> contactRepo, VendorRepository<ContactType> contactTypeRepo, VendorRepository<Account> accountRepo, VendorRepository<Product> productRepo, VendorRepository<Contract> contractRepo)
        {
            _uow = new UnitOfWork<VendorContext>();
            _contactRepo = contactRepo;
            _accountRepo = accountRepo;
            _productRepo = productRepo;
            _contractRepo = contractRepo;
            _contactTypeRepo = contactTypeRepo;
        }

        public void SetCredentials(string username, string password)
        {
            _uow.Context.SetCredentials(username, password);
        }


        public VendorRepository<Contact> ContactRepository
        {
            get { return _contactRepo; }
        }

        public VendorRepository<ContactType> ContactTypeRepository
        {
            get { return _contactTypeRepo; }
        }


        public VendorRepository<Account> AccountRepository
        {
            get { return _accountRepo; }
        }

        public VendorRepository<Product> ProductRepository
        {
            get { return _productRepo; }
        }

        public VendorRepository<Contract> ContractRepository
        {
            get { return _contractRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}
