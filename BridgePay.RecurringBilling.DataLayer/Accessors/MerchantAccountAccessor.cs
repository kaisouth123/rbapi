﻿using Bridgepay.Core.DataLayer;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class MerchantAccountAccessor
    {
        private readonly UnitOfWork<MerchantAccountContext> _uow;
        private readonly MerchantAccountRepository<MerchantAccount> _repo;

        public MerchantAccountAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<MerchantAccountContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _repo = new MerchantAccountRepository<MerchantAccount>(_uow);
        }

        public MerchantAccountRepository<MerchantAccount> MerchantAccountRepository
        {
            get { return _repo; }
        }

        public MerchantAccount Save(MerchantAccount MerchantAccount)
        {
            _uow.Save();
            return MerchantAccount;
        }

    }
}
