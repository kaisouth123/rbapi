﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class FinancialResponsiblePartyAccessor : IFinancialResponsiblePartyAccessor
    {
        private readonly UnitOfWork<CustomerContext> _uow;
        private readonly CustomerBaseRepository<Contract> _contractRepo;
        private readonly CustomerBaseRepository<FinancialResponsibleParty> _responsibleRepo;
        private readonly CustomerBaseRepository<Customer> _customerRepo;
        private readonly CustomerBaseRepository<Account> _accountRepo;

        public FinancialResponsiblePartyAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<CustomerContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contractRepo = new CustomerBaseRepository<Contract>(_uow);
            _responsibleRepo = new CustomerBaseRepository<FinancialResponsibleParty>(_uow);
            _accountRepo = new CustomerBaseRepository<Account>(_uow);
            _customerRepo = new CustomerBaseRepository<Customer>(_uow);
        }

        public FinancialResponsiblePartyAccessor(Credential credentials, CustomerContext context)
        {
            _uow = new UnitOfWork<CustomerContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contractRepo = new CustomerBaseRepository<Contract>(_uow);
            _responsibleRepo = new CustomerBaseRepository<FinancialResponsibleParty>(_uow);
            _accountRepo = new CustomerBaseRepository<Account>(_uow);
            _customerRepo = new CustomerBaseRepository<Customer>(_uow);
        }

        public CustomerBaseRepository<Contract> ContractRepository
        {
            get { return _contractRepo; }
        }

        public CustomerBaseRepository<FinancialResponsibleParty> FinancialResponsiblePartyRepository
        {
            get { return _responsibleRepo; }
        }

        public CustomerBaseRepository<Customer> CustomerRepository
        {
            get { return _customerRepo; }
        }

        public CustomerBaseRepository<Account> AccountRepository
        {
            get { return _accountRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}
