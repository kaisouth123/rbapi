﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ProductAccessor : IProductAccessor
    {
        private readonly UnitOfWork<VendorContext> _uow;
        private readonly VendorRepository<Product> _productRepo;
        private readonly VendorRepository<Contact> _contactRepo;

        public ProductAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<VendorContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _productRepo = new VendorRepository<Product>(_uow);
            _contactRepo = new VendorRepository<Contact>(_uow);
        }

        public ProductAccessor(Credential credentials, VendorContext context)
        {
            _uow = new UnitOfWork<VendorContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _productRepo = new VendorRepository<Product>(_uow);
            _contactRepo = new VendorRepository<Contact>(_uow);
        }

        public VendorRepository<Product> ProductRepository
        {
            get { return _productRepo; }
        }

        public VendorRepository<Contact> ContactRepository
        {
            get { return _contactRepo; }
        }


        public void Save()
        {
            _uow.Save();
        }
    }
}