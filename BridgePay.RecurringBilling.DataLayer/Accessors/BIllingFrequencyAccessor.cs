﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class BillingFrequencyAccessor : IBillingFrequencyAccessor
    {
        private readonly UnitOfWork<VendorContext> _uow;
        private readonly VendorRepository<Contract> _contractRepo;
        private readonly VendorRepository<BillingFrequency> _billingRepo;
        private readonly VendorRepository<Interval> _intervalRepo;
        

        public BillingFrequencyAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<VendorContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contractRepo = new VendorRepository<Contract>(_uow);
            _billingRepo = new VendorRepository<BillingFrequency>(_uow);
            _intervalRepo = new VendorRepository<Interval>(_uow);
            
        }

        public BillingFrequencyAccessor(Credential credentials, VendorContext context)
        {
            _uow = new UnitOfWork<VendorContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contractRepo = new VendorRepository<Contract>(_uow);
            _billingRepo = new VendorRepository<BillingFrequency>(_uow);
            _intervalRepo = new VendorRepository<Interval>(_uow);
        }

        public VendorRepository<Contract> ContractRepository
        {
            get { return _contractRepo; }
        }

        public VendorRepository<BillingFrequency> BillingFrequencyRepository
        {
            get { return _billingRepo; }
        }

        public VendorRepository<Interval> IntervalRepository
        {
            get { return _intervalRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}
