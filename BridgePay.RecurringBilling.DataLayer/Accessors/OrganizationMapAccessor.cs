﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.DataLayer.Contexts;
using Bridgepay.RecurringBilling.DataLayer.Repositories;
using Bridgepay.RecurringBilling.Models;


namespace Bridgepay.RecurringBilling.DataLayer.Accessors
{
    public class OrganizationMapAccessor
    {
        private readonly Core.DataLayer.UnitOfWork<BridgePayContext> _uow;
        private readonly OrganizationMapRepository _repo;

        public OrganizationMapAccessor()
        {
            _uow = new Core.DataLayer.UnitOfWork<BridgePayContext>();
            _repo = new OrganizationMapRepository(_uow);
        }

        public OrganizationMapRepository OrganizationMapRepository
        {
            get { return _repo; }
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
