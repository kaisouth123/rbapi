﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ScheduledPaymentAccessor : IScheduledPaymentAccessor
    {
        private readonly UnitOfWork<VendorContext> _uow;
        private readonly VendorRepository<Contract> _contractRepo;
        private readonly VendorRepository<ScheduledPayment> _scheduledRepo;

        public ScheduledPaymentAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<VendorContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contractRepo = new VendorRepository<Contract>(_uow);
            _scheduledRepo = new VendorRepository<ScheduledPayment>(_uow);
        }

        public ScheduledPaymentAccessor(Credential credentials, VendorContext context)
        {
            _uow = new UnitOfWork<VendorContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contractRepo = new VendorRepository<Contract>(_uow);
            _scheduledRepo = new VendorRepository<ScheduledPayment>(_uow);
        }

        public VendorRepository<Contract> ContractRepository
        {
            get { return _contractRepo; }
        }

        public VendorRepository<ScheduledPayment> ScheduledPaymentRepository
        {
            get { return _scheduledRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}
