﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class MerchantAccessor : IMerchantAccessor
    {
        private readonly UnitOfWork<MerchantContext> _uow;
        private readonly IMerchantRepository _merchantRepo;

        public MerchantAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<MerchantContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _merchantRepo = new MerchantRepository(_uow);
        }

        public MerchantAccessor(Credential credentials, MerchantContext context) 
        {
            _uow = new UnitOfWork<MerchantContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _merchantRepo = new MerchantRepository(_uow);
        }

        public IMerchantRepository MerchantRepository
        {
            get { return _merchantRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}