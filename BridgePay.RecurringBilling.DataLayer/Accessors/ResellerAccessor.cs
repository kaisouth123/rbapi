﻿using Bridgepay.Core.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ResellerAccessor: IResellerAccessor
    {
        private readonly UnitOfWork<ResellerContext> _uow;
        private readonly ResellerRepository<Reseller> _repo;
        private readonly ResellerRepository<MerchantAccount> _merchantAccountRepo;
        private readonly ResellerRepository<UnipayMerchant> _merchantRepo;
        public ResellerAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<ResellerContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _repo = new ResellerRepository<Reseller>(_uow);
            _merchantAccountRepo = new ResellerRepository<MerchantAccount>(_uow);
            _merchantRepo = new ResellerRepository<UnipayMerchant>(_uow);
        }
        public ResellerAccessor(ResellerRepository<Reseller> Repo, ResellerRepository<MerchantAccount> merchantAccountRepo, ResellerRepository<UnipayMerchant> merchantRepo)
        {
            _uow = new UnitOfWork<ResellerContext>();
            _repo = Repo;
            _merchantAccountRepo = merchantAccountRepo;
            _merchantRepo = merchantRepo;
        }

        public ResellerRepository<Reseller> ResellerRepository
        {
            get { return _repo; }
        }
        public ResellerRepository<MerchantAccount> MerchantAccountRepository
        {
            get { return _merchantAccountRepo; }
        }
        public ResellerRepository<UnipayMerchant> UnipayMerchantRepository
        {
            get { return _merchantRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }

    }
}
