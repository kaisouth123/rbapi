﻿//using Bridgepay.Core.DataLayer;
//using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomFieldAccessor :ICustomFieldAccessor
    {
        private readonly UnitOfWork<CustomFieldContext> _uow;
        private readonly CustomFieldRepository<CustomField> _customFieldRepo;
        private readonly CustomFieldTypeRepository<CustomFieldType> _customFieldTypeRepo;
        private readonly CustomFieldActivationRepository<CustomFieldActivation> _customFieldActivationRepo;
        private readonly CustomizableEntityRepository<CustomizableEntity> _customizableEntityRepo;
        private readonly ContractCustomFieldRepository<ContractCustomField> _contractCustomFieldRepo;
        private readonly CustomFieldDataRepository<CustomFieldData> _customFieldDataRepo;
        public CustomFieldAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<CustomFieldContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _customFieldRepo = new CustomFieldRepository<CustomField>(_uow);
            _customFieldTypeRepo = new CustomFieldTypeRepository<CustomFieldType>(_uow);
            _customFieldActivationRepo = new CustomFieldActivationRepository<CustomFieldActivation>(_uow);
            _customizableEntityRepo = new CustomizableEntityRepository<CustomizableEntity>(_uow);
            _contractCustomFieldRepo = new ContractCustomFieldRepository<ContractCustomField>(_uow);
            _customFieldDataRepo = new CustomFieldDataRepository<CustomFieldData>(_uow);
        }

        public CustomFieldRepository<CustomField> CustomFieldRepository { get { return _customFieldRepo; } }
        public CustomFieldTypeRepository<CustomFieldType> CustomFieldTypeRepository { get { return _customFieldTypeRepo; } }
        public CustomFieldActivationRepository<CustomFieldActivation> CustomFieldActivationRepository { get { return _customFieldActivationRepo; } }
        public CustomizableEntityRepository<CustomizableEntity> CustomizableEntityRepository { get { return _customizableEntityRepo; } }
        public ContractCustomFieldRepository<ContractCustomField> ContractCustomFieldRepository { get { return _contractCustomFieldRepo; } }
        public CustomFieldDataRepository<CustomFieldData> CustomFieldDataRepository { get { return _customFieldDataRepo; } }
        public void Dispose()
        {
            _uow.Dispose();
        }

        public void Save()
        {
            _uow.Save();
        }

    }
}
