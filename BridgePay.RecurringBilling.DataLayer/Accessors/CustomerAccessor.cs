﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.DataLayer;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class CustomerAccessor : ICustomerAccessor
    {
        private readonly UnitOfWork<CustomerContext> _uow;
        private readonly CustomerBaseRepository<Customer> _customerRepo;
        private readonly CustomerBaseRepository<CustomerAddress> _addressRepo;
        private readonly CustomerBaseRepository<ContactType> _contactTypeRepp;

        public CustomerAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<CustomerContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _customerRepo = new CustomerBaseRepository<Customer>(_uow);
            _addressRepo = new CustomerBaseRepository<CustomerAddress>(_uow);
            _contactTypeRepp = new CustomerBaseRepository<ContactType>(_uow);
        }

        public CustomerAccessor(Credential credentials, CustomerContext context)
        {
            _uow = new UnitOfWork<CustomerContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _customerRepo = new CustomerBaseRepository<Customer>(_uow);
            _addressRepo = new CustomerBaseRepository<CustomerAddress>(_uow);
            _contactTypeRepp = new CustomerBaseRepository<ContactType>(_uow);
        }

        public CustomerBaseRepository<Customer> CustomerRepository
        {
            get { return _customerRepo; }
        }

        public CustomerBaseRepository<ContactType> ContactTypeRepository
        {
            get { return _contactTypeRepp; }
        }

        public CustomerBaseRepository<CustomerAddress> CustomerAddressRepository
        {
            get { return _addressRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}
