﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ContractAccessor : IContractAccessor
    {
        private readonly UnitOfWork<VendorContext> _uow;
        private readonly VendorRepository<Contract> _contractRepo;
        private readonly VendorRepository<Product> _productRepo;
        private readonly VendorRepository<BillingFrequency> _billingRepo;
        private readonly VendorRepository<Account> _accountRepo;
        private readonly VendorRepository<Interval> _intervalRepo;
        private readonly VendorRepository<ScheduledPayment> _paymentRepo;
        private readonly VendorRepository<FinancialResponsibleParty> _financialRepo;
        private readonly VendorRepository<Contact> _contactRepo;

        public ContractAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<VendorContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contractRepo = new VendorRepository<Contract>(_uow);
            _productRepo = new VendorRepository<Product>(_uow);
            _billingRepo = new VendorRepository<BillingFrequency>(_uow);
            _accountRepo = new VendorRepository<Account>(_uow);
            _intervalRepo = new VendorRepository<Interval>(_uow);
            _paymentRepo = new VendorRepository<ScheduledPayment>(_uow);
            _financialRepo = new VendorRepository<FinancialResponsibleParty>(_uow);
            _contactRepo = new VendorRepository<Contact>(_uow);
        }

        public ContractAccessor(Credential credentials, VendorContext context)
        {
            _uow = new UnitOfWork<VendorContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contractRepo = new VendorRepository<Contract>(_uow);
            _productRepo = new VendorRepository<Product>(_uow);
            _billingRepo = new VendorRepository<BillingFrequency>(_uow);
            _accountRepo = new VendorRepository<Account>(_uow);
            _intervalRepo = new VendorRepository<Interval>(_uow);
            _paymentRepo = new VendorRepository<ScheduledPayment>(_uow);
            _financialRepo = new VendorRepository<FinancialResponsibleParty>(_uow);
            _contactRepo = new VendorRepository<Contact>(_uow);
        }

        public VendorRepository<Contract> ContractRepository
        {
            get { return _contractRepo; }
        }

        public VendorRepository<Product> ProductRepository
        {
            get { return _productRepo; }
        }

        public VendorRepository<BillingFrequency> BillingFrequencyRepository
        {
            get { return _billingRepo; }
        }

        public VendorRepository<Account> AccountRepository
        {
            get { return _accountRepo; }
        }

        public VendorRepository<Interval> IntervalRepository
        {
            get { return _intervalRepo; }
        }

        public VendorRepository<ScheduledPayment> ScheduledPaymentRepository
        {
            get { return _paymentRepo; }
        }

        public VendorRepository<FinancialResponsibleParty> FinancialResponsiblePartyRepository
        {
            get { return _financialRepo; }
        }

        public VendorRepository<Contact> ContactRepository
        {
            get { return _contactRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}
