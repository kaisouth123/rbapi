﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ContactAccessor : IContactAccessor
    {
        private readonly UnitOfWork<VendorContext> _uow;
        private readonly VendorRepository<Contact> _contactRepo;
        private readonly VendorRepository<ContactType> _contactTypeRepo;
        private readonly VendorRepository<Address> _addressRepo;

        public ContactAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<VendorContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contactRepo = new VendorRepository<Contact>(_uow);
            _contactTypeRepo = new VendorRepository<ContactType>(_uow);
            _addressRepo = new VendorRepository<Address>(_uow);
        }

        public ContactAccessor(Credential credentials, VendorContext context)
        {
            _uow = new UnitOfWork<VendorContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _contactRepo = new VendorRepository<Contact>(_uow);
            _contactTypeRepo = new VendorRepository<ContactType>(_uow);
            _addressRepo = new VendorRepository<Address>(_uow);
        }

        public VendorRepository<Contact> ContactRepository
        {
            get { return _contactRepo; }
        }

        public VendorRepository<ContactType> ContactTypeRepository
        {
            get { return _contactTypeRepo; }
        }

        public VendorRepository<Address> AddressRepository
        {
            get { return _addressRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}
