﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.DataLayer
{
    public class ChargeAccessor : IChargeAccessor
    {
        private readonly UnitOfWork<ChargeContext> _uow;
        private readonly IChargeRepository _chargeRepo;

        public ChargeAccessor(Credential credentials)
        {
            _uow = new UnitOfWork<ChargeContext>();
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _chargeRepo = new ChargeRepository(_uow);
        }

        public ChargeAccessor(Credential credentials, ChargeContext context)
        {
            _uow = new UnitOfWork<ChargeContext>(context);
            _uow.Context.SetCredentials(credentials.UserName, credentials.Password);
            _chargeRepo = new ChargeRepository(_uow);
        }

        public IChargeRepository ChargeRepository
        {
            get { return _chargeRepo; }
        }

        public void Save()
        {
            _uow.Save();
        }
    }
}