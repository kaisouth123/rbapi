[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Bridgepay.RecurringBilling.Api.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Bridgepay.RecurringBilling.Api.App_Start.NinjectWebCommon), "Stop")]

namespace Bridgepay.RecurringBilling.Api.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Bridgepay.RecurringBilling.DataLayer;
    using Bridgepay.RecurringBilling.Models;
    using System.Web.Http;
    using Ninject.Web.Mvc;
    using Bridgepay.RecurringBilling.DataLayer.Interfaces;
    using Ninject.Web.Common.WebHost;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                GlobalConfiguration.Configuration.DependencyResolver = new Ninject.Web.WebApi.NinjectDependencyResolver(kernel);

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            try
            {
                kernel.Bind<VendorContext>().ToSelf().InRequestScope();
                kernel.Bind<CustomerContext>().ToSelf().InRequestScope();
                kernel.Bind<ChargeContext>().ToSelf().InRequestScope();
                kernel.Bind<MerchantContext>().ToSelf().InRequestScope();

                kernel.Bind<UnitOfWork<VendorContext>>().To<UnitOfWork<VendorContext>>().WithConstructorArgument("context", new VendorContext());
                kernel.Bind<UnitOfWork<CustomerContext>>().To<UnitOfWork<CustomerContext>>().WithConstructorArgument("context", new CustomerContext());
                kernel.Bind<UnitOfWork<ChargeContext>>().To<UnitOfWork<ChargeContext>>().WithConstructorArgument("context", new ChargeContext());
                kernel.Bind<UnitOfWork<MerchantContext>>().To<UnitOfWork<MerchantContext>>().WithConstructorArgument("context", new MerchantContext());
            }
            catch (Exception ex)
            {

            }
        }        
    }
}
