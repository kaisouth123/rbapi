﻿using Bridgepay.RecurringBilling.Api.Helpers;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Common.Utilities;
using log4net;
using System;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace Bridgepay.RecurringBilling.Api
{
    public static class WebApiConfig
    {
        private const string CACHE_CONNECTIONSTRING = "RecurringBillingEncrypt";

        public static void Register(HttpConfiguration config)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

            // Web API configuration and services
            ILog thelog = Helpers.LogUtility.APILog;
            try
            {
                Logger.LogEvent(thelog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Info, "API Starting up", null);
                thelog.Info("Test");

                config.EnableCors();

                //if (!WebSecurity.Initialized)
                //{
                //    SimpleAES crypto = new SimpleAES();
                //    string connectionString = crypto.DecryptString(ConfigurationManager.ConnectionStrings["MyBridgePay"].ConnectionString);
                //    string providerName = ConfigurationManager.ConnectionStrings["MyBridgePay"].ProviderName;
                //    WebSecurity.InitializeDatabaseConnection(connectionString, providerName, "User", "UserId", "UserName", autoCreateTables: false);
                //}

                // Web API routes
                config.MapHttpAttributeRoutes();
                config.Routes.MapHttpRoute(
                   name: "Accounts",
                   routeTemplate: "api/accounts/{id}",
                   defaults: new { controller = "accounts", id = RouteParameter.Optional }
               );

                config.Routes.MapHttpRoute(
                  name: "Products",
                  routeTemplate: "api/products/{id}",
                  defaults: new { controller = "products", id = RouteParameter.Optional }
              );

                config.Routes.MapHttpRoute(
                 name: "Contracts",
                 routeTemplate: "api/contracts/{id}",
                 defaults: new { controller = "contracts", id = RouteParameter.Optional }
             );

                config.Routes.MapHttpRoute(
                name: "Contacts",
                routeTemplate: "api/contacts/{id}",
                defaults: new { controller = "contacts", id = RouteParameter.Optional }
            );

                config.Routes.MapHttpRoute(
                name: "ScheduledPayments",
                routeTemplate: "api/contracts/{contractid}/scheduledpayments/{id}",
                defaults: new { controller = "scheduledpayments", id = RouteParameter.Optional }
            );

                config.Routes.MapHttpRoute(
                name: "Frequencies",
                routeTemplate: "api/frequencies/{id}",
                defaults: new { controller = "frequencies", id = RouteParameter.Optional }
            );

                config.Routes.MapHttpRoute(
                   name: "AccountHolders",
                   routeTemplate: "api/accounts/{accountid}/accountholders/{id}",
                   defaults: new { controller = "accountholders", id = RouteParameter.Optional }
               );

                config.Routes.MapHttpRoute(
                  name: "Addresses",
                  routeTemplate: "api/addresses/{id}",
                  defaults: new { controller = "addresses", id = RouteParameter.Optional }
              );

                config.Routes.MapHttpRoute(
                  name: "Wallets",
                  routeTemplate: "api/accounts/{accountid}/accountholders/{accountHolderId}/wallets/{id}",
                  defaults: new { controller = "wallets", id = RouteParameter.Optional }
              );

                config.Routes.MapHttpRoute(
                  name: "PaymentMethods",
                  routeTemplate: "api/accounts/{accountid}/accountholders/{accountHolderId}/wallets/{walletid}/paymentmethods/{id}",
                  defaults: new { controller = "paymentmethods", id = RouteParameter.Optional }
              );

                config.Routes.MapHttpRoute(
                    name: "DefaultApi",
                    routeTemplate: "api/{controller}/{id}",
                    defaults: new { id = RouteParameter.Optional }
                );
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Info, "Routes loaded successfully", null);

                var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
                config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
                GlobalConfiguration.Configuration.Formatters.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("json", "true", "application/json"));
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Info, "Mediatypes configured", null);

                //Replace the Controller Selector with our Selector
                config.Services.Replace(typeof(IHttpControllerSelector), new RBControllerSelector(config));

                // TP: 16667 - general request/response logging.
                var logHandler = new LogRequestResponseHandler();
                logHandler.AddResponseSanitizer(LogRequestResponseHandler.LinksSanitizer);
                config.MessageHandlers.Add(logHandler);
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
            }
        }


    }
}
