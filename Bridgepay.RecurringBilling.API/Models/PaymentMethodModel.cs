﻿using Bridgepay.RecurringBilling.Business.WalletService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class PaymentMethodModel
    {
        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// The unique identifier for the payment method.
        /// </summary>
        public Guid? PaymentMethodId { get; set; }
        /// <summary>
        /// The type of the payment method
        /// </summary>
        public PaymentMethodType PaymentMethodType { get; set; }
        /// <summary>
        /// The card type of the payment method.
        /// </summary>
        /// <value>
        /// Type: 
        /// Visa|Mastercard|AMEX|Discover|ACH
        /// </value>
        public string CardType { get; set; }
        /// <summary>
        /// The description of the payment method.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The expiration date for card type payment methods in the format of MMYY
        /// </summary>
        public string ExpirationDate { get; set; }
        /// <summary>
        /// The last four digits of the PAN or bank account number. Readonly
        /// </summary>
        public string LastFour { get; set; }
        /// <summary>
        /// The tokenized version of the PAN or bank account number.
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// The routing number for ACH payment methods.
        /// </summary>
        public string RoutingNo { get; set; }
        /// <summary>
        /// The checking account type for ACH payment methods.
        /// </summary>
        /// <value>
        /// Type: 
        /// C|S
        /// </value>
        public string AccountType { get; set; }
        /// <summary>
        /// The card holder or check holder name.
        /// </summary>
        public string AccountHolderName { get; set; }
        /// <summary>
        /// The card holder or check holder street address.
        /// </summary>
        public string AccountHolderAddress { get; set; }
        /// <summary>
        /// The card holder or check holder street address.
        /// </summary>
        public string AccountHolderCity { get; set; }
        /// <summary>
        /// The card holder or check holder state
        /// </summary>
        public string AccountHolderState { get; set; }
        /// <summary>
        /// The card holder or check holder zip.
        /// </summary>
        public string AccountHolderZip { get; set; }
        /// <summary>
        /// The card holder or check holder phone.
        /// </summary>
        public string AccountHolderPhone { get; set; }
        /// <summary>
        /// The card holder or check holder email
        /// </summary>
        public string AccountHolderEmail { get; set; }
        /// <summary>
        /// The maximum amount of transaction failuires allowed before the payment method is deactivated.
        /// </summary>
        public bool? MaxFailures { get; set; }
        /// <summary>
        /// A flag indicating if the payment method is active or not.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// The sort order of the payment method.
        /// </summary>
        public int Order { get; set; }
        /// <summary>
        /// The id of the associated wallet of the payment method.
        /// </summary>
        public Guid WalletId { get; set; }
        /// <summary>
        /// The date the payment method was created.
        /// </summary>
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// The date the payment method was last updated.
        /// </summary>
        public DateTime? UpdateDate { get; set; }
        /// <summary>
        /// The number of transaction failures for the payment method since the last successful attempt.
        /// </summary>
        public int FailureCount { get; set; }
        /// <summary>
        /// The last result received from the processing host.
        /// </summary>
        public string LastResult { get; set; }

        /// <summary>
        /// The wallet the payment method belongs to.
        /// </summary>
        public virtual WalletModel Wallet { get; set; }
    }
}