﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class LinkModel
    {
        /// <summary>
        /// Url
        /// </summary>
        public string Href { get; set; }
        public string Rel { get; set; }
        /// <summary>
        /// Http method for the associated link
        /// </summary>
        public string Method { get; set; }
        public bool IsTemplated { get; set; }
    }
}