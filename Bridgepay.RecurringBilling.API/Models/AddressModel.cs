﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class AddressModel
    {
        /// <summary>
        /// Associated helper links for this Entity
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this Entity
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Street Address
        /// </summary>
        public string AddressStreet { get; set; }
        /// <summary>
        /// Additional Informatio for the Street Address such as PO Box, Suite, etc
        /// </summary>
        public string AddressStreetTwo { get; set; }
        /// <summary>
        /// Associated City for the AddressModel
        /// </summary>
        public string AddressCity { get; set; }
        /// <summary>
        /// Associated State for the AddressModel
        /// </summary>
        public string AddressState { get; set; }
        /// <summary>
        /// Associated Country for the AddressModel
        /// </summary>
        public string AddressCountry { get; set; }
        /// <summary>
        /// Associated zip code for the AddressModel
        /// </summary>
        public string AddressZip { get; set; }

        //Navigation
        /// <summary>
        /// ContactModel objects that use the AddressModel
        /// </summary>
        public virtual IEnumerable<AccountHolderModel> Contacts { get; set; }
    }
}
