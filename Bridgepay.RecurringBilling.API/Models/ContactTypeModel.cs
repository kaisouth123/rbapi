﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class ContactTypeModel
    {
        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this Entity
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name for this ContactType
        /// </summary>
        public string Name { get; set; }
    }
}
