﻿using Bridgepay.RecurringBilling.Models;
using System;

namespace Bridgepay.RecurringBilling.Api.Models
{
    /// <summary>
    /// The organizational scope of data that is available to the user.
    /// </summary>
    public class OrganizationScope
    {
        private static readonly string MerchantGroup = "Merchant Group";
        private static readonly string MerchantAccount = "Merchant Account";
        private readonly string orgType;

        /// <summary>
        /// Instantiates an organization scope.
        /// </summary>
        /// <param name="organizationMap">Map of an organization.</param>
        public OrganizationScope(OrganizationMap organizationMap)
        {
            OrganizationID = organizationMap.OrgId;
            orgType = organizationMap.OrgType;

            if (!IsMerchantGroup && !IsMerchantAccount)
            {
                throw new ArgumentException($"Organization must be a {MerchantGroup} or {MerchantAccount}.", nameof(organizationMap));
            }
            else if (IsMerchantAccount && !organizationMap.UnipayId.HasValue)
            {
                throw new ArgumentException("Unipay ID not present.", nameof(organizationMap));
            }

            UnipayID = organizationMap.UnipayId.Value;
        }

        /// <summary>
        /// Gets the organization ID.
        /// </summary>
        public int OrganizationID { get; private set; }

        /// <summary>
        /// Gets the UnipayID.
        /// </summary>
        public int UnipayID { get; private set; }

        /// <summary>
        /// Gets whether the organization is a merchant group.
        /// </summary>
        public bool IsMerchantGroup
        {
            get
            {
                return string.Compare(orgType, MerchantGroup, true) == 0;
            }
        }

        /// <summary>
        /// Gets whether the organization is a merchant account.
        /// </summary>
        public bool IsMerchantAccount
        {
            get
            {
                return string.Compare(orgType, MerchantAccount, true) == 0;
            }
        }
    }
}