﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class IntervalModel
    {    
        /// <summary>
        /// Unique Id for this FrequencyModel
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Name of the Interval
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// A collection of FrequencyModels that use this IntervalModel.
        /// </summary>
        public virtual IEnumerable<FrequencyModel> Frequencies { get; set; }
    }
}
