﻿using Bridgepay.RecurringBilling.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    public class EntityList<T>
    {
        /// <summary>
        /// Current page of the results
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Total number of pages
        /// </summary>
        public int TotalPages { get; set; }
        /// <summary>
        /// Total number of entities
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// Results of type Entity
        /// </summary>
        public IEnumerable<T> Results { get; set; }
        /// <summary>
        /// A collection of helper links for this EntityList for use in the API
        /// </summary>
        public List<LinkModel> Links { get; set; }

        public Exception Exception { get; set; }
    }
}
