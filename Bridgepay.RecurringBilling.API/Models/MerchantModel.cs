﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class MerchantModel
    {
        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        public int Id;
        /// <summary>
        /// Name of the merchant.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Specifies if the Merchant is active in the RecurringBilling system.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// Unique Identifier for this merchant.
        /// </summary>
        public int MerchantCode { get; set; }

    }
}
