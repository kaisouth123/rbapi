﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class ProductModel
    {
        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for the ProductModel.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The name of the product.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Default bill amount for contracts created with this product.
        /// </summary>
        public int? DefaultAmount { get; set; }
        /// <summary>
        /// Description of the product.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Unique Id for the associated ContactModel
        /// </summary>
        public int ContactId { get; set; }
        /// <summary>
        /// Unique Id for the merchant for which the product is associated.
        /// </summary>
        public int MerchantId { get; set; }
        /// <summary>
        /// Specifies whether the product is active in RecurringBilling system.
        /// </summary>
        public bool? IsActive { get; set; }

        //Navigation
        /// <summary>
        /// Collection of ContractModels that associated with this ProductModel
        /// </summary>
        public virtual List<ContractModel> Contracts { get; set; }
        /// <summary>
        /// ContactModel associated with this Entity
        /// </summary>
        public virtual ContactModel Contact { get; set; }
    }
}
