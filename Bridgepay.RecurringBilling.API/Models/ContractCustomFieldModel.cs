﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class ContractCustomFieldModel
    {
        public ContractCustomFieldModel()
        {
            
        }

        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        public int OwnerId { get; set; }
        public string TableName { get; set; }
        public int Id { get; set; }
        public int EntityId { get; set; }
        public string FieldName { get; set; }
        public string DisplayName { get; set; }
        public string Value { get; set; } 
        public int CustomFieldConfigId { get; set; }
        public ContractModel Contract { get; set; }
    }
}
