﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class AccountHolderModel
    {
        /// <summary>
        /// Associated helper links for this Entity
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this Entity.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Required AccountId of the parent AccountModel associated with this AccountHolderModel.
        /// </summary>
        /// <remarks>This cannot be null.</remarks>
        /// <value>int</value>
        public int AccountId { get; set; }
        /// <summary>
        /// ContactId of the child contact object to the AccountholderModel.  This value cannot be null.
        /// </summary>
        /// <value>int</value>
        public int ContactId { get; set; }
        /// <summary>
        /// The Id for the merchant for which the  Parent AccountModel and this Entity are associated.
        /// </summary>
        public int MerchantId { get; set; }
        /// <summary>
        /// An int used for manually ordering of AccountHolderModel entities under the AccountModel
        /// </summary>
        /// <remarks>The AccountHolder with the lowest order is considered the default accountholder sans any entities with a null value for the order.   If not order values are present the Id value will be used for ordering.</remarks>
        /// <value>int, can be null.</value>
        public int? Order { get; set; }
        /// <summary>
        /// Represents whether the AccountHolder is active in the RecurringBilling system.
        /// </summary>
        /// <remarks>if the AccountHolder is not active systems such as RecurringBIlling will not pick up the AccountHolder related wallet information for contract processing.</remarks>
        /// <value>bool</value>
        public bool? IsActive { get; set; }
        /// <summary>
        /// A Guid that represents the WalletId associated with the AccountHolderModel to be used with the Wallet API and Service.
        /// </summary>
        public Guid? Guid {get; set;}

        //Navigation
        /// <summary>
        /// Associated child CustomerModel Entity for the AccountholderModel.
        /// </summary>
        public virtual CustomerModel Customer { get; set; }
        /// <summary>
        /// Parent AccountModel associated with this AccountHolderModel.
        /// </summary>
        /// <value>AccountModel</value>
        public virtual AccountModel Account { get; set; }
        /// <summary>
        /// The wallet associated with this AccountHolderModel, which in turn contain the payment methods for the AccountHolderModel.
        /// </summary>
        public virtual WalletModel Wallet { get; set; }
        public virtual ContactModel Contact { get; set; }

    }
}
