﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class FrequencyModel
    {
        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this Frequency.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The name of the frequency.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Description of the frequency.
        /// </summary>
        public string Description {get; set;}
        /// <summary>
        /// Unique Id of the merchant for which this Frequency is associated.
        /// </summary>
        public int MerchantId { get; set; }
        /// <summary>
        /// Unique Id for the associated Interval.
        /// </summary>
        /// <remarks>1=Daily, 2=Daily on these [Days], 3=Every # week on these [Days], 4=Monthly on this [Day], 5=Monthly on these [Days] of the # Week, 6=Every # year on this [Month] and [Day], 7=Every # year on this [Month] of the # week, on these [Days]</remarks>
        public int IntervalId { get; set; }
        /// <summary>
        /// True or false value whether the frequency has an end date.
        /// </summary>
        public bool? NoEnd { get; set; }
        /// <summary>
        /// This value along with the Interval determines the Frequency of contract processing.  If the Interval type is monthly and Frequency is 2 then the contract would be processed every 2 months.
        /// </summary>
        public int? Frequency { get; set; }
        /// <summary>
        /// Day of the month
        /// </summary>
        /// <remarks>Used in monthly and yearly forecasting</remarks>
        public int? DayMonth { get; set; }
        /// <summary>
        /// Week of the month as a value from 1 to 4, 5 is not used because not every month has 5 weeks.
        /// </summary>
        public int? WeekMonth { get; set; }
        /// <summary>
        /// The month as a number from 1 to 12.
        /// </summary>
        public int? Month { get; set; }
        /// <summary>
        /// Day string mask of seven digits either 1 or 0 such as 1000010 for Sunday and Friday.
        /// </summary>
        /// <remarks>Used in a variety of forecasting models where multiple days of the week need to be specified.</remarks>
        public string Days { get; set; }
        /// <summary>
        /// Specifies wheter the Frequency is active in the system.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// True or false whether this frequency is a template that can be used across multiple merchatns.
        /// </summary>
        public bool? Template { get; set; }

        //Navigation
        /// <summary>
        /// ContractModels that use this FrequencyModel
        /// </summary>
        public virtual IEnumerable<ContractModel> Contracts { get; set; }
    }
}
