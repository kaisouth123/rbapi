﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class CustomFieldDataModel
    {
        public CustomFieldDataModel()
        {
            
        }

        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this Entity.
        /// </summary>
        /// <remarks></remarks>
        public int Id { get; set; }
        public int CustomFieldConfigId { get; set; }
        public string EntityId { get; set; }
        public string Value { get; set; }
        public CustomFieldModel CustomField { get; set; }
        public ContractModel Contract { get; set; }
    }
}
