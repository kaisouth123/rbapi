﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class ContractModel
    {
        public ContractModel()
        {
            
        }

        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this Entity.
        /// </summary>
        /// <remarks></remarks>
        public int Id { get; set; }
        /// <summary>
        /// The name of the contract.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The unique Id for the associated ProductModel.
        /// </summary>
        public int? ProductId { get; set; }
        /// <summary>
        /// The unique id for the MerchantModel associated with this ContractModel.
        /// </summary>
        public int MerchantId { get; set; }
        /// <summary>
        /// The unique Id for the associated BillingFrequencyModel.
        /// </summary>
        public int BillingFrequencyId { get; set; }
        /// <summary>
        /// Unique Id for the associated Account for the ContractModel
        /// </summary>
        public int AccountId { get; set; }
        /// <summary>
        /// The billing amount for this ContractModel
        /// </summary>
        /// <remarks>The amount the contract is charged each time it is processed (not including the tax).</remarks>
        /// <value>int</value>
        public int? BillAmount { get; set; }
        /// <summary>
        /// The tax amount for the contract, which along with the bill amount is charged each time the contract is processed.
        /// </summary>
        public int? TaxAmount { get; set; }
        /// <summary>
        /// Amount Remaining to be paid on the ContractModel
        /// </summary>
        /// <remarks>Each time the contract is processed for a recurring payment the ChargeAmount + TaxAmount is subtracted from the AmountRemaining.  When the AmountRemaining reaches 0, the contract is set to a Status of complete and the IsActive is set to false.  This can only occur on Contracts with valid EndDates.</remarks>
        /// <value>int</value>
        public int? AmountRemaining { get; set; }
        /// <summary>
        /// Numbe of total payments for the contract
        /// </summary>
        /// <value>int</value>
        public int? NumberOfPayments { get; set; }
        /// <summary>
        /// The last time a processing attempt was made against the contract.
        /// </summary>
        /// <remarks>If the contract is new this will be set to the startdate to make sure the payment forecasting is caculated appropriately.</remarks>
        public DateTime? LastBillDate { get; set; }
        /// <summary>
        /// The next time the contract will be processed; this is in Universal Time.
        /// </summary>
        public DateTime? NextBillDate { get; set; }
        /// <summary>
        /// The start date of the contract.
        /// </summary>
        /// <remarks>The earliest a contract will process is the next day from the day its is created.</remarks>
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// End date of the contract.
        /// </summary>
        /// <remarks>Used along with AmountRemaining to specify when an account is complete.  See Amount Remaining for more details.</remarks>
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// The current number of times the contract has been declined since the last time a payment was approved.
        /// </summary>
        public int? NumFailures { get; set; }
        /// <summary>
        /// The max number of failure (declines) an contract can have before the status is changes to Canceled.
        /// </summary>
        public int? MaxFailures { get; set; }
        /// <summary>
        /// The amount of days to wait between processing a declined contract from the LastBillDate.
        /// </summary>
        public int? RetryWaitTime { get; set; }
        /// <summary>
        /// Specifies whether to send the customer an email if the contract is declined during processing.
        /// </summary>
        /// <remarks>The email is sent to the ContactEmail address of the ContactModel for each AccountHolder who is associated with the Account.</remarks>
        /// <value>bool</value>
        public bool? EmailCustomerAtFailure { get; set; }
        /// <summary>
        /// Specifies whether to send the merchant an email if the contract is declined during processing.
        /// </summary>
        /// <remarks>The email is sent to the ProductModel.ContactMode.ContactEmail address for the product associated with the contract.</remarks>
        public bool? EmailMerchantAtFailure { get; set; }
        /// <summary>
        /// Specifies whether to send the customer an email recepit if the contract is approved during processing.
        /// </summary>
        /// <remarks></remarks>
        public bool? EmailCustomerReceipt { get; set; }
        /// <summary>
        /// Specifies whether to send the merchant an email if the contract is declined during processing.
        /// </summary>
        /// <remarks>The email is sent to the ProductModel.ContactMode.ContactEmail address for the product associated with the contract.</remarks>
        public bool? EmailMerchantReceipt { get; set; }
        /// <summary>
        /// Specifies if the Contact is active in the system.
        /// </summary>
        /// <remarks>A false value will cause the contract to be skipped even if it would otherwise be processed.</remarks>
        public bool? IsActive { get; set; }
        /// <summary>
        /// Status of the contract as an int.
        /// </summary>
        /// <remarks>ACTIVE=0, APPROVED=1, ERROR=2, DECLINED=3, CANCELED=4,   PROCESSING=5, DEACTIVATED=6, COMPLETE=7</remarks>
        public int? Status { get; set; }

        //Navigation
        /// <summary>
        /// The ScheduledPaymentModels associated with this ContractModel, which refer to the underlying one time scheduled payments for this contract.
        /// </summary>
        public virtual IEnumerable<ScheduledPaymentModel> ScheduledPayments { get; set; }
        /// <summary>
        /// The Associated ChargeModels for this ContractModel.
        /// </summary>
        public virtual IEnumerable<ChargeModel> Charges { get; set; }
        /// <summary>
        /// The ProductModel associated with this ContractModel.
        /// </summary>
        public virtual ProductModel Product { get; set; }
        /// <summary>
        /// The Associated BillingFrequencyModel for this ContractModel
        /// </summary>
        /// <remarks>The associated billing frequency for this contract determines the processing scheduled (daily, montly, weekly, etc).</remarks>
        /// <value>BillingFrequencyModel</value>
        public virtual FrequencyModel BillingFrequency { get; set; }
        /// <summary>
        /// The Associated AccountModel for the ContractModel
        /// </summary>
        public virtual AccountModel Account { get; set; }
        public virtual IEnumerable<ContractCustomFieldModel> ContractCustomFields { get; set; }
    }
}
