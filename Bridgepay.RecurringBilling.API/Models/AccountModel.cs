﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class AccountModel
    {

        /// <summary>
        /// Constructor for the associated AccountModel in the RecurringBilling system, which is a container for the AccountholderModels to be associated with one or contracts and any shared information for those entities.
        /// </summary>
        public AccountModel()
        {
        }

        /// <summary>
        /// Associated helper links for this Entity
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this Entity.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Id of the associated merchant.
        /// </summary>
        public int MerchantId { get; set; }
        /// <summary>
        /// Name of the account.
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// A string value primarily used as a reference to legacy systems.
        /// </summary>
        public string AccountNumber { get; set; }
        /// <summary>
        /// Specifies whether the AccountModel Entity is active in the system and used for processing.
        /// </summary>
        public bool? IsActive { get; set; }

        //Navigation
        /// <summary>
        /// ContractModel child entities that are associated with this account.
        /// </summary>
        /// <value></value>
        public virtual IEnumerable<ContractModel> Contracts { get; set; }
        /// <summary>
        /// Chilld AccountHolderModel entities associated with this AccountModel.
        /// </summary>
        public virtual IEnumerable<AccountHolderModel> AccountHolders { get; set; }
    }
}