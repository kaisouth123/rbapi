﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class ScheduledPaymentModel
    {
        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this ScheduledPaymentModel.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Unique Id for the contract for which the scheduled payment is associated.
        /// </summary>
        public int ContractId { get; set; }
        /// <summary>
        /// The date the payment is scheduled to process.
        /// </summary>
        public DateTime? BillDate { get; set; }
        /// <summary>
        /// Bill amount to be charged when the payment is processed (includes tax).
        /// </summary>
        public int? BillAmount { get; set; }
        /// <summary>
        /// True or false whether this payment has been processed and the amount charged against the contract.
        /// </summary>
        public bool? Paid {get; set;}

        //Navigation
        /// <summary>
        /// Contract for which this payment is associated.
        /// </summary>
        public virtual ContractModel Contract { get; set; }
    }
}
