﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class DeveloperModel
    {
        /// <summary>
        /// Unique Id for this Entity
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// APIkey
        /// </summary>
        public string APIKey { get; set; }
        /// <summary>
        /// Signature
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// Unique Id of the associated merchant
        /// </summary>
        public int MerchantId { get; set; }
    }
}