﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class ChargeModel
    {
        /// <summary>
        /// Unique Id for this Entity
        /// </summary>
        public int Id { get; set; }
        public int? ContractId { get; set; }
        public int? TaxAmount { get; set; }
        public int? Amount { get; set; }

    }
}
