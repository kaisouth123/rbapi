﻿using System;
using System.Collections.Generic;
namespace Bridgepay.RecurringBilling.Api.Models
{
    public interface IContactModel
    {
        ICollection<LinkModel> Links { get; set; }
        int? AddressId { get; set; }
        string CompanyName { get; set; }
        AddressModel ContactAddress { get; set; }
        string ContactDayPhone { get; set; }
        string ContactDepartment { get; set; }
        string ContactEmail { get; set; }
        string ContactEveningPhone { get; set; }
        string ContactFax { get; set; }
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        string ContactMobilePhone { get; set; }
        string ContactTitle { get; set; }
        int? ContactTypeId { get; set; }
        int Id { get; set; }
        bool? IsActive { get; set; }
    }
}
