﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class CustomerModel : IContactModel
    {
        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// Unique Id for this Entity
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Unique Id of the associated ContactType (1 = Merchant, 2 = Customer)
        /// </summary>
        public int? ContactTypeId { get; set; }
        /// <summary>
        /// First name of the contact
        /// </summary>
        public string ContactFirstName { get; set; }
        /// <summary>
        /// Last name of the contact
        /// </summary>
        public string ContactLastName { get; set; }
        /// <summary>
        /// Company name for the contact
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// Unique Id for the address associated with this ContactModel Entity
        /// </summary>
        public int? AddressId { get; set; }
        /// <summary>
        /// Department for the contact
        /// </summary>
        public string ContactDepartment { get; set; }
        /// <summary>
        /// Title of the Contact
        /// </summary>
        public string ContactTitle { get; set; }
        /// <summary>
        /// Contact email address
        /// </summary>
        public string ContactEmail { get; set; }
        /// <summary>
        /// Day phone for the contact
        /// </summary>
        public string ContactDayPhone { get; set; }
        /// <summary>
        /// Evening phone for the contact
        /// </summary>
        public string ContactEveningPhone { get; set; }
        /// <summary>
        /// Mobile number for the contact
        /// </summary>
        public string ContactMobilePhone { get; set; }
        /// <summary>
        /// Fax number for the contact
        /// </summary>
        public string ContactFax { get; set; }
        /// <summary>
        /// Bool value used to specify if the Entity is active in the system
        /// </summary>
        public bool? IsActive { get; set; }

        //Navigation
        /// <summary>
        /// AddressModel of the associated address for this ContactModel
        /// </summary>
        public virtual AddressModel ContactAddress { get; set; }

    }
}