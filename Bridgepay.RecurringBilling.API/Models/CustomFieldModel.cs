﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class CustomFieldModel
    {
        public CustomFieldModel()
        {
            
        }

        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }

        public int Id { get; set; }

        public string EntityName { get; set; }
        public int? OrganizationId { get; set; }
        public int FieldTypeId { get; set; }
        public int FieldEntityId { get; set; }
        public string FieldName { get; set; }
        public string FieldDisplayName { get; set; }
        public string FieldRegEx { get; set; }
        public string FieldDefault { get; set; }
        public string SourceEntity { get; set; }
        public string SourceField { get; set; }

        public virtual IEnumerable<CustomFieldActivation> CustomFieldActivations { get; set; }
       
    }
}
