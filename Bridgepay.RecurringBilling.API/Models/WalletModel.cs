﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class WalletModel
    {
        /// <summary>
        /// A collection of helper links for this Entity for use in the API
        /// </summary>
        public ICollection<LinkModel> Links { get; set; }
        /// <summary>
        /// The unique identifier for the wallet.
        /// </summary>
        /// <value>Guid</value>
        public Guid? WalletId { get; set; }
        /// <summary>
        /// The site identifier for the wallet.
        /// </summary>
        public Guid? SiteId { get; set; }
        /// <summary>
        /// The name of the wallet.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The description of the wallet.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// A customer defined account number for the wallet.
        /// </summary>
        public string CustomerAccountNumber { get; set; }
        /// <summary>
        /// A flag indicating if the wallet is active or not.
        /// </summary>
        public bool? IsActive { get; set; }
        /// <summary>
        /// The date the wallet was created.
        /// </summary>
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// The date the wallet was last updated.
        /// </summary>
        public DateTime? UpdateDate { get; set; }

        // Navigation
        /// <summary>
        /// The Accountholder who owns this wallet in the system.
        /// </summary>
        public virtual AccountHolderModel AccountHolder { get; set; }
        /// <summary>
        /// A collection of payment methods belonging to the wallet.
        /// </summary>
        public IEnumerable<PaymentMethodModel> PaymentMethods { get; set; }
    }
}