﻿using Bridgepay.RecurringBilling.Business.Helpers.Utility;
using Bridgepay.RecurringBilling.Business.ServiceAdaptors;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Business.WalletService;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Routing;
using Wallet = Bridgepay.RecurringBilling.Business.WalletService.Wallet;

namespace Bridgepay.RecurringBilling.Api.Models
{
    public class ModelFactory
    {
        private UrlHelper _urlHelper;
        private VendorContext _context;
        private Credential _credentials;
        private WalletAdaptor _adaptor; 

        public ModelFactory(HttpRequestMessage request)
        {
            _urlHelper = new UrlHelper(request);
            _adaptor = new WalletAdaptor();
        }

        public ModelFactory(HttpRequestMessage request, VendorContext context)
        {
            _urlHelper = new UrlHelper(request);
            _context = context;
            _adaptor = new WalletAdaptor();
        }

        public void SetCredentials(Credential credentials)
        {
            _credentials = credentials;
        }

        public LinkModel CreateLink(string href, string rel, string method ="Get", bool isTemplate = false)
        {
            return new LinkModel()
            {
                Href = href,
                Rel = rel,
                Method = method,
                IsTemplated = isTemplate,
            };
        }

        public AccountModel Create(Account entity, bool includeGraph = false)
        {
            AccountModel model = new AccountModel();
            model.CopyFrom(entity, null, new Dictionary<string, string> { { "AccountName", "Name" }, }, true);
            model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("Accounts", new { id = entity.Id }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("Accounts", new { }), "newAccount", "Post"),
                };
            model.Contracts = entity.Contracts == null ? null : entity.Contracts.Select(c => Create(c));
            model.AccountHolders = entity.FinancialResponsibleParties != null ? entity.FinancialResponsibleParties.Select(f => Create(f, includeGraph)) : null;

            return model;           
        }
      
        public Account Parse(AccountModel model)
        {
            try
            {
                Account entity = new Account();
                entity.CopyFrom(model, null, new Dictionary<string, string> { { "Name", "AccountName" } }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public Account Merge(AccountModel model, Account entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { { "Name", "AccountName" } }, true);
                entity.ObjectState = Core.Framework.ObjectState.Modified;
                return entity;
            }
            catch
            {
                return null;
            }
        }

        public AddressModel Create(IAddress entity)
        {
            try
            {
                AddressModel model = new AddressModel();
                model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
                model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("Addresses", new { id = entity.Id }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("Addresses", new { }), "newAddress", "Post"),
                };

                return model;
            }
            catch
            {
                return null;
            }
        }

        public IAddress Parse(AddressModel model, bool IsCustomerAddress = false)
        {
            try
            {
                if (IsCustomerAddress)
                {
                    CustomerAddress entity = new CustomerAddress();
                    entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                    entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                    return entity;
                }
                else
                {
                    Address entity = new Address();
                    entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                    entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                    return entity;
                }
            }
            catch
            {
                return null;
            }
        }

        public IAddress Merge(AddressModel model, IAddress entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                return entity;
            }
            catch
            {
                return null;
            }
        }

        public CustomerModel Create(Customer entity)
        {
            try
            {
                CustomerModel model = new CustomerModel();
                model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("Contacts", new { id = entity.Id }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("Contacts", new { }), "newContact", "Post"),
                };
                model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
                model.ContactAddress = entity.ContactAddress == null ? null : this.Create((IAddress)entity.ContactAddress);

                return model;
            }
            catch
            {
                return null;
            }
        }

        public Customer Parse(CustomerModel model)
        {
            try
            {
                Customer entity = new Customer();
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                entity.ContactAddress = (CustomerAddress)Parse(model.ContactAddress);

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public Customer Merge(CustomerModel model, Customer entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = Core.Framework.ObjectState.Modified;
                entity.ContactAddress = model.ContactAddress == null ? entity.ContactAddress : (CustomerAddress)Merge(model.ContactAddress, entity.ContactAddress);
                entity.ContactAddress.ObjectState = model.ContactAddress == null ? entity.ContactAddress.ObjectState : Core.Framework.ObjectState.Modified;

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public ContactModel Create(Contact entity)
        {
            try
            {
                ContactModel model = new ContactModel();
                model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
                model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("Contacts", new { id = entity.Id }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("Contacts", new { }), "newContact", "Post"),
                };
                model.ContactAddress = entity.ContactAddress == null ? null : this.Create((IAddress)entity.ContactAddress);
                return model;
            }
            catch
            {
                return null;
            }
        }

        public Contact Parse(ContactModel model)
        {
            try
            {
                Contact entity = new Contact();
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                entity.ContactAddress = (Address)Parse(model.ContactAddress);
                return entity;
            }
            catch
            {
                return null;
            }
        }

        public Contact Merge(ContactModel model, Contact entity)
        {
            entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
            entity.ObjectState = Core.Framework.ObjectState.Modified;
            entity.ContactAddress = model.ContactAddress == null ? null : (Address)Merge(model.ContactAddress, entity.ContactAddress);
            if(model.ContactAddress != null)
                entity.ContactAddress.ObjectState = Core.Framework.ObjectState.Modified;
            //dont change the contacttype
            entity.ContactType = null;

            return entity;
        }

        public AccountHolderModel Create(FinancialResponsibleParty entity, bool includeGraph = false)
        {
            try
            {
                AccountHolderModel model = new AccountHolderModel();
                model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("AccountHolders", new { accountid = entity.AccountId, id = entity.Id }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("AccountHolders", new { accountid = entity.AccountId, id = entity.Id }), "Post"),
                };
                model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
                model.Account = entity.Account == null ? null : this.Create(entity.Account);
                model.Customer = entity.Customer == null ? null : this.Create(entity.Customer);
                if (includeGraph)
                {
                    if (entity.Wallet != null)
                    {
                        // TP 15409 - The wallet can now be populated by the time it arrives here, so don't fetch it again.
                        model.Wallet = Create(entity.AccountId, entity.Id, entity.Wallet);
                    }
                    else
                    {
                        model.Wallet = entity.Guid == null ? null : this.Create(entity.AccountId, entity.Id, _adaptor.GetWalletById((Guid)entity.Guid, false));
                    }
                }
                return model; 
            }
            catch(Exception ex) 
            {
                return null;
            }
        }

        public FinancialResponsibleParty Parse(AccountHolderModel model)
        {
            try
            {
                FinancialResponsibleParty entity = new FinancialResponsibleParty();
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                entity.Customer = Parse(model.Customer);

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public FinancialResponsibleParty Merge(AccountHolderModel model, FinancialResponsibleParty entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = Core.Framework.ObjectState.Modified;
                entity.Customer = model.Customer == null ? entity.Customer : Merge(model.Customer, entity.Customer);

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public ProductModel Create(Product entity)
        {
            try
            {
                ProductModel model = new ProductModel();
                model.CopyFrom(entity, null, new Dictionary<string, string> { { "ProductName", "Name" }, { "ProductDefaultAmount", "DefaultAmount" }, }, true);
                model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("Products", new { id = entity.Id }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("Products", new { }), "newProduct", "Post"),
                };
                model.Contracts = entity.Contracts == null ? null : entity.Contracts.Select(c => Create(c)).ToList();
                model.Contact = entity.Contact == null ? null : this.Create(entity.Contact);

                return model;  

                
            }
            catch 
            {
                return null;
            }
        }

        public Product Parse(ProductModel model)
        {
            try
            {
                Product entity = new Product();
                entity.CopyFrom(model, null, new Dictionary<string, string> { { "Name", "ProductName" }, { "DefaultAmount", "ProductDefaultAmount" }, }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                entity.Contact = model.Contact == null ? entity.Contact : Parse(model.Contact);

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public Product Merge(ProductModel model, Product entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { { "Name", "ProductName" }, { "DefaultAmount", "ProductDefaultAmount" }, }, true);
                entity.ObjectState = Core.Framework.ObjectState.Modified;
                entity.Contact = model.Contact == null ? entity.Contact : Merge(model.Contact, entity.Contact);

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public ContractModel Create(Contract entity, bool includeGraph = false)
        {
            try
            {
                ContractModel model = new ContractModel();
                model.CopyFrom(entity, null, new Dictionary<string, string> { { "ContractName", "Name" }, }, true);
                // TP 16089 - The last parameter in CopyFrom causes a value of 0 not to get copied to the model, leaving null.
                // Changing it to false may have unintended consequences elsewhere, so just assign AmountRemaining regardless.
                model.AmountRemaining = entity.AmountRemaining;
                model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("Contracts", new { id = entity.Id }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("Contracts", new { }), "newContract", "Post"),
                };
                model.BillingFrequency = entity.BillingFrequency == null ? null : Create(entity.BillingFrequency);
                model.Account = entity.Account == null ? null : Create(entity.Account, includeGraph);
                model.Charges = entity.Charges == null ? null : entity.Charges.Select(c => Create(c));
                model.ScheduledPayments = entity.ScheduledPayments == null ? null : entity.ScheduledPayments.Select(s => Create(s));
                model.Product = entity.Product == null ? null : Create(entity.Product);                
                model.ContractCustomFields = entity.ContractCustomFields == null ? null : entity.ContractCustomFields.Select(f => Create(f));
                model.NumberOfPayments = entity.NumberOfPayments;
                return model;    
            }
            catch 
            {
                return null;
            }
        }

        public Contract Parse(ContractModel model)
        {
            try
            {
                Contract entity = new Contract();
                entity.CopyFrom(model, null, new Dictionary<string, string> { { "Name", "ContractName" } }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                entity.Account = model.Account == null ? null : Parse(model.Account);
                entity.BillingFrequency = model.BillingFrequency == null ? null : Parse(model.BillingFrequency);
                entity.Charges = model.Charges == null ? null : model.Charges.Select(c => Parse(c)).ToList();
                entity.Product = model.Product == null ? null : Parse(model.Product);
                entity.ScheduledPayments = model.ScheduledPayments == null ? null : model.ScheduledPayments.Select(s => Parse(s)).ToList();
                entity.ContractCustomFields = model.ContractCustomFields == null ? null : model.ContractCustomFields.Select(s => Parse(s)).ToList();
                return entity;
            }
            catch
            {
                return null;
            }
        }

        public Contract Merge(ContractModel model, Contract entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { { "Name", "ContractName" } }, true);
                entity.ObjectState =  Core.Framework.ObjectState.Modified;
                entity.Account = model.Account == null ? null : Merge(model.Account, entity.Account);
                entity.BillingFrequency = model.BillingFrequency == null ? null : Merge(model.BillingFrequency, entity.BillingFrequency);
                entity.Product = model.Product == null ? null : Merge(model.Product, entity.Product);

                if (model.ContractCustomFields != null)
                {
                    // Copy the model's custom fields over what the database entity currently has.
                    entity.ContractCustomFields = model.ContractCustomFields
                                                       .Select(f => Merge(f, new ContractCustomField()))
                                                       .ToList();
                }
                return entity;

            }
            catch
            {
                return null;
            }
        }

        public FrequencyModel Create(BillingFrequency entity)
        {
            try
            {
                BillingFrequencyService service = new BillingFrequencyService(_credentials, _context);
                FrequencyModel model = new FrequencyModel();
                model.CopyFrom(entity, null, new Dictionary<string, string> { { "FrequencyName", "Name" }, }, true);
                model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("Frequencies", new { id = entity.Id }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("Frequencies", new { }), "newFrequency", "Post"),
                };
                model.Contracts = entity.Contracts == null ? null : entity.Contracts.Select(c => Create(c));
                model.Description = service.GetDescription(entity.Id);
                return model;   

                
               
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public BillingFrequency Parse(FrequencyModel model)
        {
            try
            {
                BillingFrequency entity = new BillingFrequency();
                entity.CopyFrom(model, null, new Dictionary<string, string> { { "Name", "FrequencyName" } }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public BillingFrequency Merge(FrequencyModel model, BillingFrequency entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { { "Name", "FrequencyName" } }, true);
                entity.ObjectState = Core.Framework.ObjectState.Modified;

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public ScheduledPaymentModel Create(ScheduledPayment entity)
        {
            try
            {
                ScheduledPaymentModel model = new ScheduledPaymentModel();
                model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
                model.Links =  new List<LinkModel>(){
                            CreateLink(_urlHelper.Link("ScheduledPayments", new { contractid = entity.ContractId, id = entity.Id }), "Self or Patch"),
                            CreateLink(_urlHelper.Link("ScheduledPayments", new { contractid = entity.ContractId, id = entity.Id }), "Post"),
                        };
                //model.Contract = entity.Contract == null ? null : Create(entity.Contract);

                return model;  
            }
            catch
            {
                return null;
            }
        }

        public ScheduledPayment Parse(ScheduledPaymentModel model)
        {
            try
            {
                ScheduledPayment entity = new ScheduledPayment();
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                entity.Contract = model.Contract == null ? null : Parse(model.Contract);

                return entity;        
            }
            catch
            {
                return null;
            }
        }

        public ScheduledPayment Merge(ScheduledPaymentModel model, ScheduledPayment entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                entity.Contract = model.Contract == null ? null : Merge(model.Contract, entity.Contract);

                return entity;   
            }
            catch
            {
                return null;
            }
        }

        public ChargeModel Create(Charge entity)
        {
            try
            {
                return new ChargeModel
                {
                    Id = Convert.ToInt32(entity.ID),
                };

            }
            catch
            {
                return null;
            }
        }

        public Charge Parse(ChargeModel model)
        {
            try
            {
                return new Charge
                {

                };
            }
            catch
            {
                return null;
            }
        }

        public PaymentMethodModel Create(int accountId, int frpId, Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod entity)
        {
            try
            {
                PaymentMethodModel model = new PaymentMethodModel();
                model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
                model.Links = new List<LinkModel>(){
                            CreateLink(_urlHelper.Link("PaymentMethods", new { id = entity.PaymentMethodId}), "Self or Patch"),
                            CreateLink(_urlHelper.Link("PaymentMethods", new { id = entity.PaymentMethodId}), "Post"),
                        };
                model.Wallet = entity.Wallet == null ? null : Create(accountId, frpId, entity.Wallet);

                return model;
            }
            catch
            {
                return null;
            }
        }

        public PaymentMethodModel Create(int accountId, int frpId, RecurringBilling.Models.PaymentMethod paymentMethod)
        {
            PaymentMethodModel model = new PaymentMethodModel();
            model.CopyFrom(paymentMethod, null, new Dictionary<string, string> { }, true);
            model.Links = new List<LinkModel>(){
                        CreateLink(_urlHelper.Link("PaymentMethods", new { id = paymentMethod.PaymentMethodId}), "Self or Patch"),
                        CreateLink(_urlHelper.Link("PaymentMethods", new { id = paymentMethod.PaymentMethodId}), "Post"),
                    };
            
            return model;
        }

        public Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod Merge(PaymentMethodModel model, Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod entity)
        {
            try
            {
                if(!string.IsNullOrEmpty(model.Token) && model.Token != entity.Token)
                {
                    if (entity.PaymentMethodType == PaymentMethodType.Cards)
                        model.Token = ServiceUtility.GetTokenRequest(entity, true).Token;
                    else
                        model.Token = ServiceUtility.GetTokenRequest(entity, false).Token;
                }

                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.PaymentMethodId == null ? ObjectState.Added : ObjectState.Modified;
                entity.Wallet = model.Wallet == null ? null : Merge(model.Wallet, entity.Wallet);

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod Parse(PaymentMethodModel model)
        {
            try
            {
                Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod entity = new Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod();
                if (string.IsNullOrEmpty(model.Token))
                {
                    if (entity.PaymentMethodType == PaymentMethodType.Cards)
                        model.Token = ServiceUtility.GetTokenRequest(entity, true).Token;
                    else
                        model.Token = ServiceUtility.GetTokenRequest(entity, false).Token;
                }

                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.PaymentMethodId == null ?  ObjectState.Added : ObjectState.Modified;
                entity.Wallet = model.Wallet == null ? null : Parse(model.Wallet);
                return entity;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public WalletModel Create(int accountId, int frpId, Wallet entity)
        {
            try
            {
                WalletModel model = new WalletModel();
                model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
                model.WalletId = entity.WalletId;
                model.Links = new List<LinkModel>(){
                    CreateLink(_urlHelper.Link("Wallets", new { accountid = accountId, accountholderid = frpId, id = entity.WalletId }), "Self or Patch"),
                    CreateLink(_urlHelper.Link("Wallets", new { accountid = accountId, accountholderid = frpId, id = entity.WalletId }), "Post"),
                        };
                model.PaymentMethods = entity.PaymentMethods == null ? null : entity.PaymentMethods.Select(c => Create(accountId, frpId, c));

                return model;
            }
            catch
            {
                return null;
            }
        }

        public WalletModel Create(int accountId, int frpId, RecurringBilling.Models.Wallet entity)
        {
            var model = new WalletModel();
            model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
            model.Links = new List<LinkModel>() {
                CreateLink(_urlHelper.Link("Wallets", new { accountid = accountId, accountholderid = frpId, id = entity.WalletId }), "Self or Patch"),
                CreateLink(_urlHelper.Link("Wallets", new { accountid = accountId, accountholderid = frpId, id = entity.WalletId }), "Post"),
            };

            if (entity.PaymentMethods != null)
                model.PaymentMethods = entity.PaymentMethods.Select(p => Create(accountId, frpId, p));

            return model;
        }

        public Wallet Merge(WalletModel model, Wallet entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.WalletId == null ? ObjectState.Added : ObjectState.Modified;

                return entity;
            }
            catch
            {
                return null;
            }
        }

        public Wallet Parse(WalletModel model)
        {
            try
            {
                Wallet entity = new Wallet();
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.WalletId == null ? ObjectState.Added : ObjectState.Modified;
                return entity;
            }
            catch
            {
                return null;
            }
        }
        public ContractCustomField Parse(ContractCustomFieldModel model)
        {
            try
            {
                ContractCustomField entity = new ContractCustomField();
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;

                return entity;
            }
            catch
            {
                return null;
            }
        }
        public CustomFieldData Parse(CustomFieldDataModel model)
        {
            try
            {
                CustomFieldData entity = new CustomFieldData();
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = model.Id == 0 ? Bridgepay.Core.Framework.ObjectState.Added : Core.Framework.ObjectState.Modified;
                return entity;
            }
            catch
            {
                return null;
            }
        }
        public ContractCustomFieldModel Create(ContractCustomField entity)
        {
            ContractCustomFieldModel model = new ContractCustomFieldModel();
            model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
            return model;
        }
        public CustomFieldDataModel Create(CustomFieldData entity)
        {
            CustomFieldDataModel model = new CustomFieldDataModel();
            model.CopyFrom(entity, null, new Dictionary<string, string> { }, true);
            return model;
        }
        public ContractCustomField Merge(ContractCustomFieldModel model, ContractCustomField entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = Core.Framework.ObjectState.Modified;
                return entity;
            }
            catch
            {
                return null;
            }
        }
        public ContractCustomField Create(ContractCustomFieldModel model, ContractCustomField entity)
        {
            try
            {
                entity.CopyFrom(model, null, new Dictionary<string, string> { }, true);
                entity.ObjectState = Core.Framework.ObjectState.Added;
                return entity;
            }
            catch
            {
                return null;
            }
        }
        //public List<ContractCustomField> Merge(List<ContractCustomFieldModel> model, List<ContractCustomField> entity)
        //{
        //    try
        //    {
        //       entity=model.Select(x=>x.Id==0?Create(x):)
        //        return entity;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}
    }
}