﻿using Bridgepay.Core.Logging;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Models.Interfaces;
using System;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Bridgepay.RecurringBilling.Api.Filters
{
    /// <summary>
    /// Controller filter that determines organization scope.
    /// </summary>
    /// <remarks>
    /// See https://stackoverflow.com/questions/20219656/onactionexecuted-being-called-twice-in-web-api
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class OrganizationScopeFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                if (actionContext.ControllerContext.Controller is IHasOrganizationScope hasOrganizationScope)
                {
                    hasOrganizationScope.OrganizationScope = ProfileService.GetOrganizationScope(actionContext.Request);
                }
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Error("Failed to get organization scope.", ex);
            }
        }
    }
}