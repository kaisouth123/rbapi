﻿using Bridgepay.RecurringBilling.Business;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using WebMatrix.WebData;
using Bridgepay.Core.Cryptography;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using System.Net.Http.Formatting;
using System.IO;
using Ninject.Activation;

namespace Bridgepay.RecurringBilling.Api.Filters
{
    public class RBBasicAuthenthorize : AuthorizationFilterAttribute
    {
        private bool _perUser;

        public RBBasicAuthenthorize(bool perUser = true)
        {
            _perUser = perUser;
        }
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!DebugHelper.IsDebugMode())
            {
                var request = actionContext.Request;

                if (request.RequestUri.Scheme != Uri.UriSchemeHttps)
                {
                    var html = "<p>Https is required</p>";
                    if (request.Method.Method == "GET")
                    {
                        actionContext.Response = request.CreateResponse(HttpStatusCode.Found);
                        actionContext.Response.Content = new StringContent(html, Encoding.UTF8, "text/html");

                        var uriBuilder = new UriBuilder(request.RequestUri);
                        uriBuilder.Scheme = Uri.UriSchemeHttps;
                        uriBuilder.Port = 443;

                        actionContext.Response.Headers.Location = uriBuilder.Uri;
                    }
                    else
                    {
                        actionContext.Response = request.CreateResponse(HttpStatusCode.NotFound);
                        actionContext.Response.Content = new StringContent(html, Encoding.UTF8, "text/html");
                    }
                }
            }


            if (_perUser)
            {
                if (Thread.CurrentPrincipal.Identity.IsAuthenticated)
                {
                    return;
                }

                var authHeader = actionContext.Request.Headers.Authorization;

                if (authHeader != null)
                {
                    if (authHeader.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(authHeader.Parameter))
                    {
                        var rawCredentials = authHeader.Parameter;
                        var encoding = Encoding.GetEncoding("iso-8859-1");
                        var credentials = encoding.GetString(Convert.FromBase64String(rawCredentials));
                        var split = credentials.Split(':');
                        var username = split[0];
                        var password = split[1];

                        Credentials bpnCredentials = new Credentials() { UserName=username, Password=password};

                        String ipAddress = Helpers.RequestInfo.GetClientIp(actionContext.Request);

                        if (SecurityService.Authenticate(bpnCredentials) != null)
                        {
                            var principal = new GenericPrincipal(new GenericIdentity(username), null);
                            Thread.CurrentPrincipal = principal;
                            var credential = new Credential() { UserName = username, Password = password };
                            CheckPermissions(actionContext, credential);
                            if (Helpers.LogUtility.LogMode == LogMode.Verbose)
                                Logger.LogLogin(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), username, DateTime.Now, true, "RecurringBilling API/Database", ipAddress, "Login Successful using Basic Authentication");
                            return;
                        }
                        else
                        {
                            if (Helpers.LogUtility.LogMode != LogMode.ErrorOnly)
                                Logger.LogLogin(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), username, DateTime.Now, false, "RecurringBilling API/Database", ipAddress, "Login Failure using Basic Authentication");
                        }
                    }
                }
            }
            else
                return;

            HandleUnauthorized(actionContext);
        }

        private void HandleUnauthorized(HttpActionContext actionContext)
        {
            string url = ConfigurationManager.AppSettings["MyBridgePayLogin"];
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            if (_perUser)
                actionContext.Response.Headers.Add("WWW-Authenticate",
                    "Basic Scheme='RBBasicAuthentication' location='" + url + "'");

        }
        private void CheckPermissions(HttpActionContext actionContext, Credential credentials)
        {
            try
            {
                string merchantAccountValue = string.Empty;
                string organizationIdValue = string.Empty;

                if (actionContext.Request.Headers.Contains("X-RB-MerchantId")) merchantAccountValue = actionContext.Request.Headers.GetValues("X-RB-MerchantId").FirstOrDefault();
                if (actionContext.Request.Headers.Contains("X-RB-OrganizationId")) organizationIdValue = actionContext.Request.Headers.GetValues("X-RB-OrganizationId").FirstOrDefault();

                if (string.IsNullOrWhiteSpace(merchantAccountValue) && string.IsNullOrWhiteSpace(organizationIdValue))
                {
                    Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, "Permission denied: missing X-RB-MerchantId or X-RB-OrganizationId", null);
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                    actionContext.Response.Headers.Add("WWW-Authenticate",
                        "Permission denied: missing X-RB-MerchantId or X-RB-OrganizationId");
                }
                else
                {

                    int.TryParse(merchantAccountValue, out var merchantAccountId);
                    int.TryParse(organizationIdValue, out var organizationId);

                    if (merchantAccountId > 0 || organizationId > 0)
                    {
                        var hasPermission = SecurityService.CheckAccess(credentials.UserName, merchantAccountId, organizationId);
                        if (hasPermission) return;
                        Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, "Unauthorized Access", null);
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                        actionContext.Response.Headers.Add("WWW-Authenticate",
                            "Unauthorized Access");
                    }
                    else
                    {
                        Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, "Permission denied: incorrect X-RB-MerchantId or X-RB-OrganizationId", null);
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                        actionContext.Response.Headers.Add("WWW-Authenticate",
                            "Permission denied: incorrect X-RB-MerchantId or X-RB-OrganizationId");
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
                actionContext.Response.Headers.Add("WWW-Authenticate",
                    ex.Message);
            }
        }
    }
}