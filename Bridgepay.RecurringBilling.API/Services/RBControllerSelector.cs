﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace Bridgepay.RecurringBilling.Api.Services
{
    public class RBControllerSelector : DefaultHttpControllerSelector
    {
        private HttpConfiguration _config;

        public RBControllerSelector(HttpConfiguration config) : base(config)
        {
            _config = config;
        }

        public override HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            var controllers = GetControllerMapping();
            var routeData = request.GetRouteData();
            var controllerName = (string)routeData.Values["controller"];
            HttpControllerDescriptor descriptor;

            if (controllers.TryGetValue(controllerName, out descriptor))
            {
                var version = GetVersionFromHeader(request);
                var versionName = string.Concat(controllerName, "V", version);
                HttpControllerDescriptor versionDescriptor;

                if(controllers.TryGetValue(versionName, out versionDescriptor))
                {
                    return versionDescriptor;
                }

                return descriptor;
            }
            
            return base.SelectController(request);
        }

        private string GetVersionFromHeader(HttpRequestMessage request)
        {
            const string HEADER_VERSION = "X-RB-Version";
            var acceptHeaders = request.Headers.Accept;

            if (request.Headers.Contains(HEADER_VERSION))
            {
                var header = request.Headers.GetValues(HEADER_VERSION).FirstOrDefault();

                if (header != null)
                    return header;
            }

            return "1";
        }
    }
}