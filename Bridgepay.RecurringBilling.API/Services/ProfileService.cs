﻿using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc.Filters;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.Core.Logging;
using Bridgepay.Core.Security.Managers;

namespace Bridgepay.RecurringBilling.Api.Services
{
    public static class ProfileService
    {
        private const string HEADER_MERCHANTID = "X-RB-MerchantId";
        private const string HEADER_ORGANIZATIONID = "X-RB-OrganizationId";

        public static int GetMerchantId(DeveloperModel entity, HttpRequestMessage Request)
        {
            if (Request.Headers.Contains(HEADER_MERCHANTID))
            {
                string value = Request.Headers.GetValues(HEADER_MERCHANTID).FirstOrDefault();
                int Id;
                if (int.TryParse(value, out Id))
                {
                    return Id;
                }
            }
            else if (Request.Headers.Contains(HEADER_ORGANIZATIONID))
            {
                string value = Request.Headers.GetValues(HEADER_ORGANIZATIONID).FirstOrDefault();
                int Id;
                if (int.TryParse(value, out Id))
                {
                    OrganizationMapAccessor accessor = new OrganizationMapAccessor();
                    OrganizationMap orgMap = accessor.OrganizationMapRepository.All.FirstOrDefault(x => x.OrgId == Id);
                    if (orgMap != null && orgMap.UnipayId.HasValue)
                        return orgMap.UnipayId.Value;
                }
            }
            return 0;
        }

        /// <summary>
        /// Gets an organization scope from an API request.
        /// </summary>
        /// <param name="request">API request.</param>
        /// <returns>An organization scope.</returns>
        public static OrganizationScope GetOrganizationScope(HttpRequestMessage request)
        {
            try
            {
                var accessor = new OrganizationMapAccessor();
                OrganizationScope scope = null; 

                var header = request.Headers.FirstOrDefault(h => string.Compare(HEADER_MERCHANTID, h.Key, true) == 0);
                if (header.Key != null)
                {
                    var merchantID = int.Parse(header.Value.FirstOrDefault());
                    var orgMap = accessor.OrganizationMapRepository.All.Single(o => o.UnipayId == merchantID);
                    scope = new OrganizationScope(orgMap);
                }
                else if ((header = request.Headers.FirstOrDefault(h => string.Compare(HEADER_ORGANIZATIONID, h.Key, true) == 0)).Key != null)
                {
                    var orgId = int.Parse(header.Value.FirstOrDefault());
                    var orgMap = accessor.OrganizationMapRepository.All.Single(o => o.OrgId == orgId);
                    scope = new OrganizationScope(orgMap);
                }
                else
                {
                    throw new ArgumentException("Organization or merchant ID must be provided.", nameof(request));
                }

                var credentials = GetCredentials(request);
                if (!UserManager.CheckOrganizationalAccess(scope.OrganizationID, credentials.UserName))
                {
                    throw new ArgumentException("User cannot access this organization.", nameof(request));
                }

                return scope;
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Error("Failed to get organization scope.", ex);
                throw;
            }
        }

        public static Credential GetCredentials(HttpRequestMessage Request)
        {
            try
            {
                var authHeader = Request.Headers.Authorization;
                var credentials = Encoding.Default.GetString(Convert.FromBase64String(authHeader.Parameter));
                var userAndPass = credentials.Split(':');
                return new Credential()
                {
                    UserName = userAndPass[0],
                    Password = userAndPass[1]
                };
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Error("Failed to extract credentials.", ex);
                return new Credential();
            }
        }
    }
}