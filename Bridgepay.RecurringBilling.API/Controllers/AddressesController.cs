﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class AddressesController : BaseVendorController<Address>, IHasOrganizationScope
    {
        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private VendorRepository<Address> _repo;
        private UnitOfWork<VendorContext> _uow;
        private ProductAccessor _productAccessor;
        private ProductService productService;

        public AddressesController(VendorContext context) : base(context)
        {
            _uow = new UnitOfWork<VendorContext>(context);
            _repo = new VendorRepository<Address>(_uow);
            _productAccessor = new ProductAccessor(credentials, context);
            productService = new ProductService(credentials, context);
            OrganizationScope = new OrganizationScope();
        }

        public OrganizationScope OrganizationScope { get; set; }

        // GET: api/Addresses
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="Entity">The Entity whose child properties can be used a filter values in the query string:  AddressStreet, AddressStreetTwo, AddressCity, AddressState, AddressCountry, and AddressZip.</param>
        [OrganizationScopeFilter]
        public async Task<EntityList<AddressModel>> GetAddresses(int Page = 0, int Page_Size = PAGE_SIZE, [FromUri]Address entity = null)
        {
            try
            {
                Page_Size = 10000;
                entity = entity == null ? new Address { } : entity;
                BaseFilter<Address> filter = new BaseFilter<Address> { entity = entity, IncludeGraph = false, Take = Page_Size, OrderBy = "Id", Skip = (Page * Page_Size), SearchFalse = false, SortDesc = false };
                filter.entity = entity;
                IQueryable<Address> entities = _repo.FindByFilter(filter).ToList().AsQueryable();
                var helper = new UrlHelper(this.Request);
                var productEntity = new Product { };
                var productFilter = new BaseFilter<Product>
                {
                    entity = productEntity,
                    Take = Page_Size,
                    IncludeGraph = true,
                    OrderBy = "Id",
                    Skip = (Page * Page_Size),
                    SearchFalse = false,
                    SortDesc = false
                };
                if (OrganizationScope.IsMerchantAccount)
                    productFilter.entity.MerchantId = OrganizationScope.UnipayID;
                else if (OrganizationScope.IsMerchantGroup)
                    productFilter.MerchantGroupOrganizationID = OrganizationScope.OrganizationID;
                var products = productService.GetProducts(productFilter, true);             
                var totalCount = products.TotalQueryCount; //_repo.GetAll().Count();
                var listAddressId = products.Entities.Select(x => x.Contact.AddressId);
                entities=entities.Where(x => listAddressId.Contains(x.Id));
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.Count() / Page_Size));
                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Addresses", new { page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Addresses", new { page = Page + 1 }), "nextPage"));


                return await Task.Run<EntityList<AddressModel>>(() =>
                {
                    var results = entities.Select(a => this.ModelFactory.Create(a));
                    entities.ShortCircuitCollection();

                    return new EntityList<AddressModel>
                    {
                        TotalCount = entities.Count(),
                        TotalPages = totalPages,
                        Page = Page,
                        Links = links,
                        Results = results,
                    };
                });
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<AddressModel> { Exception = newEx };
            }
        }

        // GET: api/Addresses/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(AddressModel))]
        public async Task<IHttpActionResult> GetAddress(int id)
        {
            try
            {
                Address entity = _repo.Find(id);
                if (entity == null)
                {
                    return NotFound();
                }


                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/Addresss
        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        /// <param name="Address">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(AddressModel))]
        public async Task<IHttpActionResult> PostAddress([FromBody]AddressModel Address)
        {
            try
            {
                var stringCheck = new List<string>();
                if (string.IsNullOrEmpty(Address.AddressStreet))
                { stringCheck.Add("Invalid AddressStreet"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                Address entity = (Address)this.ModelFactory.Parse(Address);
                if (entity == null)
                    return BadRequest("Invalid Address data.");

                _repo.InsertOrUpdate(entity);
                _repo.Save();

                return Created(Request.GetUrlHelper().Link("Addresses", new { id = entity.Id }), this.ModelFactory.Create(entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="address">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PatchAddress(int id, AddressModel address)
        {
            try
            {
                if (!AddressExists(id))
                {
                    return NotFound();
                }
                Address dbAddress = _repo.Find(id);
                Address mergedAddress = (Address)this.ModelFactory.Merge(address, dbAddress);

                if (mergedAddress == null)
                    return BadRequest("Error parsing AddressModel");

                mergedAddress.ObjectState = Bridgepay.Core.Framework.ObjectState.Modified;


                _repo.InsertOrUpdate(mergedAddress);
                _repo.Save();
                return Ok(this.ModelFactory.Create(mergedAddress));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }
        private bool AddressExists(int id)
        {
            return _repo.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}