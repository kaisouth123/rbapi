﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class FrequenciesController : BaseVendorController<BillingFrequency>, IHasOrganizationScope
    {
        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private BillingFrequencyAccessor _accessor;
        private BillingFrequencyService service;

        public FrequenciesController(VendorContext context, MerchantContext mcontext)
            : base(context)
        {
            _accessor = new BillingFrequencyAccessor(credentials, context);
            service = new BillingFrequencyService(credentials, context, mcontext);
            OrganizationScope = new OrganizationScope();
        }

        public OrganizationScope OrganizationScope { get; set; }

        // GET: api/Frequencies
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="Entity">A Frequency Entity whose values can be used for searching.  Each of the following properties can be used in the query string to filter the results: FrequencyName, Description, MerchantId, IntervalId, NoEnd, Frequency, DayMonth, WeekMonth, Month, Days, and IsActive.</param>
        /// <returns>EntityList of FrequencyModels</returns>
        [OrganizationScopeFilter]
        public async Task<EntityList<FrequencyModel>> GetFrequencies(bool IncludeGraph = false, int Page = 0, int Page_Size = PAGE_SIZE, bool SearchFalse = false, [FromUri]BillingFrequency entity = null)
        {
            try
            {
                entity = entity == null ? new BillingFrequency { } : entity;
                var filter = new BaseFilter<BillingFrequency>
                {
                    entity = entity,
                    Take = Page_Size,
                    IncludeGraph = IncludeGraph,
                    OrderBy = "Id",
                    Skip = (Page * Page_Size),
                    SearchFalse = SearchFalse
                };

                if (OrganizationScope.IsMerchantAccount)
                    entity.MerchantId = OrganizationScope.UnipayID;
                else if (OrganizationScope.IsMerchantGroup)
                    filter.MerchantGroupOrganizationID = OrganizationScope.OrganizationID;

                WCFList<BillingFrequency> entities = service.GetBillingFrequencies(filter, IncludeGraph);
                var helper = new UrlHelper(this.Request);
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.TotalQueryCount / Page_Size));
                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Frequencies", new { page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Frequencies", new { page = Page + 1 }), "nextPage"));

                this.ModelFactory.SetCredentials(credentials);


                var results = entities.Entities.Select(a => this.ModelFactory.Create(a));
                entities.Entities.ShortCircuitCollection();

                return new EntityList<FrequencyModel>
                {
                    TotalCount = entities.TotalQueryCount,
                    TotalPages = totalPages,
                    Page = Page,
                    Links = links,
                    Results = results,
                };
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<FrequencyModel> { Exception = newEx };
            }
        }

        // GET: api/Frequencies/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(FrequencyModel))]
        public async Task<IHttpActionResult> GetFrequency(int id, bool IncludeGraph = false)
        {
            try
            {
                BillingFrequency entity = IncludeGraph ? service.GetBillingFrequencyGraph(id) : service.GetBillingFrequencyById(id);
                if (entity == null)
                {
                    return NotFound();
                }
                var stringCheck = new List<string>();
                if (entity.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                { stringCheck.Add("Permission denied"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                entity.Description = service.GetDescription(entity.Id);
                this.ModelFactory.SetCredentials(credentials);


                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="model">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PatchFrequency([FromBody]FrequencyModel model)
        {
            try
            {
                if (!FrequencyExists(model.Id))
                {
                    return NotFound();
                }
                else if (model.Frequency == null)
                {
                    return BadRequest(nameof(model.Frequency) + " cannot be null.");
                }
                BillingFrequencyService tempService = new BillingFrequencyService(credentials);
                BillingFrequency dbFrequency = tempService.GetBillingFrequencyGraph(model.Id);
                var stringCheck = new List<string>();
                if (dbFrequency.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                { stringCheck.Add("Permission denied"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                BillingFrequency mergedFrequency = this.ModelFactory.Merge(model, dbFrequency);
                this.ModelFactory.SetCredentials(credentials);
                if (mergedFrequency == null)
                    return BadRequest("Error parsing FrequencyModel");

                mergedFrequency.ObjectState = Bridgepay.Core.Framework.ObjectState.Modified;


                BillingFrequency returnedFrequency = tempService.MergeBillingFrequency(mergedFrequency);
                return Ok(this.ModelFactory.Create(mergedFrequency));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/Frequencies
        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        /// <param name="model">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(FrequencyModel))]
        public async Task<IHttpActionResult> PostFrequency([FromBody]FrequencyModel model)
        {
            try
            {
                var stringCheck = new List<string>();
                if (string.IsNullOrEmpty(model.Name))
                { stringCheck.Add("Invalid Name"); }
                if(model.MerchantId==0)
                {
                    stringCheck.Add("Invalid MerchantId");
                }
                else if (model.Frequency == null)
                {
                    return BadRequest(nameof(model.Frequency) + " cannot be null.");
                }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                BillingFrequency entity = this.ModelFactory.Parse(model);
                if (entity == null)
                    return BadRequest("Invalid Frequency data.");

                BillingFrequency returnedFrequency = service.PostBillingFrequency(entity);
                this.ModelFactory.SetCredentials(credentials);
                if (returnedFrequency != null)
                    return Created(Request.GetUrlHelper().Link("Frequencies", new { id = returnedFrequency.Id }), this.ModelFactory.Create(entity));
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }

        }
        private bool FrequencyExists(int id)
        {
            return _accessor.BillingFrequencyRepository.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}