﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Helpers;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Common;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class ScheduledPaymentsController : BaseVendorController<ScheduledPayment>
    {
        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private ScheduledPaymentAccessor _accessor;
        private ScheduledPaymentService service;

        public ScheduledPaymentsController(VendorContext context) : base(context)
        {
            _accessor = new ScheduledPaymentAccessor(credentials, context);
            service = new ScheduledPaymentService(credentials, context);
        }

        // GET: api/ScheduledPayments
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <param name="contractid">Unique id for the Parent Contact.</param>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="Entity">The Entity whose child properties can be used a filter values in the query string:  ContractId, BillDate, BillAmount, and Paid.</param>
        public async Task<EntityList<ScheduledPaymentModel>> GetScheduledPayments(int contractid, bool IncludeGraph = false, int Page = 0, int Page_Size = PAGE_SIZE, bool SearchFalse = false, [FromUri]ScheduledPayment entity = null)
        {
            try
            {
                entity = entity == null ? new ScheduledPayment { } : entity;
                WCFList<ScheduledPayment> entities = service.GetScheduledPayments(new BaseFilter<ScheduledPayment> { entity = entity, Take = Page_Size, IncludeGraph = IncludeGraph, OrderBy = "Id", Skip = (Page * Page_Size), SearchFalse = SearchFalse }, IncludeGraph);
                entities.Entities.ShortCircuitCollection();
                var helper = new UrlHelper(this.Request);
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.TotalQueryCount / Page_Size));
                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("ScheduledPayments", new { contractid = contractid, page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("ScheduledPayments", new { contractid = contractid, page = Page + 1 }), "nextPage"));


                var results = entities.Entities.Select(a => this.ModelFactory.Create(a));
                entities.Entities.ShortCircuitCollection();

                return new EntityList<ScheduledPaymentModel>
                {
                    TotalCount = entities.TotalQueryCount,
                    TotalPages = totalPages,
                    Page = Page,
                    Links = links,
                    Results = results,
                };
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<ScheduledPaymentModel> { Exception = newEx };
            }
        }

        // GET: api/ScheduledPayments/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <param name="contractid">Unique id for the parent ContractModel Entity.</param>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(ScheduledPaymentModel))]
        public async Task<IHttpActionResult> GetScheduledPayment(int contractid, int id, bool IncludeGraph = false)
        {
            try
            {
                ScheduledPayment entity = IncludeGraph ? service.GetScheduledPaymentGraph(id) : service.GetScheduledPaymentById(id);
                if (entity == null)
                {
                    return NotFound();
                }


                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="contractid">Unique id for the parent Contact of the entity to be updated.</param>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="model">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PatchScheduledPayment(int contractid, int id, [FromBody]ScheduledPaymentModel model)
        {
            try
            {
                if (!ScheduledPaymentExists(id))
                {
                    return NotFound();
                }
                if(model.BillDate!=null)
                {
                    var date = (DateTime)model.BillDate;
                    model.BillDate = new DateTime(date.Year, date.Month, date.Day).ToUniversalTime();
                }
                ScheduledPaymentService tempService = new ScheduledPaymentService(credentials);
                ScheduledPayment entity = tempService.GetScheduledPaymentGraph(id);
                entity.ShortCircuitReference();
                ScheduledPayment mergedScheduledPayment = this.ModelFactory.Merge(model, entity);

                if (mergedScheduledPayment == null)
                    return BadRequest("Error parsing ScheduledPaymentModel");

                mergedScheduledPayment.ObjectState = Bridgepay.Core.Framework.ObjectState.Modified;


                ScheduledPayment returnedScheduledPayment = tempService.MergeScheduledPayment(mergedScheduledPayment);
                return Ok(this.ModelFactory.Create(returnedScheduledPayment));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/ScheduledPayments
        /// <summary>
        /// The entity model to be inserted.
        /// </summary>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(ScheduledPaymentModel))]
        public async Task<IHttpActionResult> PostScheduledPayment([FromBody]ScheduledPaymentModel ScheduledPayment)
        {
            try
            {
                var stringCheck = new List<string>();
                if (ScheduledPayment.BillDate==null)
                { stringCheck.Add("Invalid BillDate"); }
                else
                {
                    var date = (DateTime)ScheduledPayment.BillDate;
                    ScheduledPayment.BillDate = new DateTime(date.Year,date.Month,date.Day).ToUniversalTime();
                }
                if (ScheduledPayment.BillAmount == null || ScheduledPayment.BillAmount == 0)
                { stringCheck.Add("Invalid DefaultAmount"); }
                if (ScheduledPayment.ContractId == 0)
                { stringCheck.Add("Invalid ContractId"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                ScheduledPayment entity = this.ModelFactory.Parse(ScheduledPayment);
                if (entity == null)
                    Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid ScheduledPayment data.");

                ScheduledPayment returnedScheduledPayment = service.PostScheduledPayment(entity);

                if (returnedScheduledPayment != null)
                    return Created(Request.GetUrlHelper().Link("ScheduledPayments", new { contractid = returnedScheduledPayment.ContractId, id = returnedScheduledPayment.Id }), this.ModelFactory.Create(entity));
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }

        }
        private bool ScheduledPaymentExists(int id)
        {
            return _accessor.ScheduledPaymentRepository.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}