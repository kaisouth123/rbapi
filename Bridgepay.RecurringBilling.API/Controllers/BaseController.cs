﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    public class BaseController : ApiController
    {
        HttpResponseMessage rm = new HttpResponseMessage();
        public class Filter
        {
            public string FieldName { get; set; }
            public string Value { get; set; }
        }
        public static Expression<Func<TInput, bool>> CreateFilterExpression<TInput>(IEnumerable<Filter> filters)
        {
            ParameterExpression param = Expression.Parameter(typeof(TInput), "");
            Expression lambdaBody = null;
            if (filters != null)
            {
                foreach (Filter f in filters)
                {
                    Expression compareExpression = Expression.Equal(Expression.Property(param, f.FieldName), Expression.Constant(f.Value));
                    if (lambdaBody == null)
                        lambdaBody = compareExpression;
                    else
                        lambdaBody = Expression.AndAlso(lambdaBody, compareExpression);
                }
            }
            if (lambdaBody == null)
                return Expression.Lambda<Func<TInput, bool>>(Expression.Constant(false));
            else
                return Expression.Lambda<Func<TInput, bool>>(lambdaBody, param);
        }
        public string Base64Encode(string obj)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(obj));
        }
        public string Base64Decode(string obj)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecodebyte = Convert.FromBase64String(obj);
                int charCount = utf8Decode.GetCharCount(todecodebyte, 0, todecodebyte.Length);
                char[] decodedchar = new char[charCount];
                utf8Decode.GetChars(todecodebyte, 0, todecodebyte.Length, decodedchar, 0);
                string result = new String(decodedchar);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Base64 Decode error");
            }
        }

        [Obsolete("Do not use this method to log models containing Credentials; use LogSafeSerializer instead.")]
        public string Serialize<T>(T value)
        {
            MediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            if (Request != null)
            {
                if (Request.Content.Headers.ContentType.MediaType.Contains("json"))
                {
                    formatter = new JsonMediaTypeFormatter();
                }
                else if (Request.Content.Headers.ContentType.MediaType.Contains("xml"))
                {
                    formatter = new XmlMediaTypeFormatter();
                }
            }
            Stream stream = new MemoryStream();
            var content = new StreamContent(stream);
            formatter.WriteToStreamAsync(typeof(T), value, stream, content, null).Wait();
            stream.Position = 0;
            return content.ReadAsStringAsync().Result;
        }
        //public string EncryptSerialize<T>(T value)
        //{
        //    Stream stream = new MemoryStream();
        //    var content = new StreamContent(stream);
        //    MediaTypeFormatter formatter = new JsonMediaTypeFormatter(); ;
        //    if (Request != null)
        //    {
        //        if (Request.Content.Headers.ContentType.MediaType == "application/json")
        //        {
        //            formatter = new JsonMediaTypeFormatter();
        //        }
        //        else if (Request.Content.Headers.ContentType.MediaType == "application/xml")
        //        {
        //            formatter = new XmlMediaTypeFormatter();
        //        }
        //        else { formatter = new XmlMediaTypeFormatter(); }
        //    }
        //    formatter.WriteToStreamAsync(typeof(T), value, stream, content, null).Wait();
        //    stream.Position = 0;
        //    return Base64Encode(content.ReadAsStringAsync().Result);
        //}
        public T DecryptDeserialize<T>(string str) where T : class
        {
            str = Base64Decode(WebUtility.UrlDecode(str));
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            MediaTypeFormatter formatter = new JsonMediaTypeFormatter(); ;
            if (Request != null)
            {
                if (Request.Content.Headers.ContentType.MediaType == "application/json")
                {
                    formatter = new JsonMediaTypeFormatter();
                }
                else if (Request.Content.Headers.ContentType.MediaType == "application/xml")
                {
                    formatter = new XmlMediaTypeFormatter();
                }
                else { formatter = new XmlMediaTypeFormatter(); }
            }
            return formatter.ReadFromStreamAsync(typeof(T), stream, null, null).Result as T;
        }
        public T Deserialize<T>(string str) where T : class
        {
            MediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            str = WebUtility.UrlDecode(str);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            if (Request != null)
            {
                if (Request.Content.Headers.ContentType.MediaType.Contains("json"))
                {
                    formatter = new JsonMediaTypeFormatter();
                }
                else if (Request.Content.Headers.ContentType.MediaType.Contains("xml"))
                {
                    formatter = new XmlMediaTypeFormatter();
                }
            }
            return formatter.ReadFromStreamAsync(typeof(T), stream, null, null).Result as T;
        }
        public string _privateKey = "<RSAKeyValue><Modulus>x3d/4OiQiymFIzFzQi7AWHmLcCE1CtWAS9zLDTRDX+T86PN5H8LDpg2wfyl6LBvn1RFsuL72n1vEl0mDdhllWg205nVlY6xvKFXaRfnytj2GoQNkF4xguL5+C0vTZ5BXe38E30N/+QhkNHaZ7KxPpfx3LW/urmMmtQGTAYXUHT8nLBn05sTMxOocfe5w8QYt8ZCr3CqHEvJ+VIndLKFJ06wU6WSRE8TFkQMelRltZQ7EY4bAhFx0eST9646H+eXPUxGC/kFAI/LBJMPYwwUCt4p87eBOkvkHitlNnfe4Q0C5NLQtx2AiAmbPxsFSZGR9vsTsNUA3/0QLZ1n8twnhvQ==</Modulus><Exponent>AQAB</Exponent><P>3HFl/Qq9nEujyP2jlsX4xiSelB05siXL1xOWHx1uTjixj0TSsCo6jiPOPhe97Ea+wpbx7RUzE+4PSKDTpzks9g3h1HRrHjzbVCzqSZf96ICehRuDQ7zsl7YSZT6olxhrUIIhQwN+rSHbqzOoc6iiCh06mlBEctmnUdvj07cpeWs=</P><Q>56PyTSjrwg9Pj8nLcBQOunhRSaOZj4Z2QPfLdjIQyIjsXIy5icRhQORqghh72sUnmq4jtQJ1FcDFTYe38bxghcIwEtkcN5DG/MhBozpzsm+A38+n7csEkZpoLxRIqP+1scKDFLzULBY0McVl3Q9t7mqqjqPEAxPdpuzWYPaek3c=</Q><DP>wIt/9Kyb/1eGILmCWOBksdUsedypF3Sx/hNKjbTsMwCL/sKzcFICQUL/Evcz2ZvVZSvo9hcNPTwzN1HabPag0e5AMYzaP1gSjwyIO225xooJa+QjHFh5Iu5SVQKMyST4HyL6oIj97YVkVL5TRWqm1kS/eifgEZ0Q64wwDC2w2S8=</DP><DQ>jTiLmXUQPrB87gm16K+hxxdhX4bJMRJl9/faaEXfq5Kmn0oAN7itHhXjxUrN/7NhENGH3WwRG+D5qOnMQCl7ozCfhG4u4MV4O1aPhcc2EBwgstJz6+pw75/2UnWIUtgK2sPejH8Oz4395jYVgy9L8yW+s9Wtw76RzJnpnRXd2cM=</DQ><InverseQ>MmBgkZCleQV/FP5tOMHurtcmWe8o6GoPkfZ6kA80Fb+ZLQvAwLLANmLhceOuOTe0hNsXD6rE9AQLhcJJ5VNcFFlJUi8jvj/tgLoMW3RHcpIZEDAASclFNzEmQP1b+iQZEoxid1EYuRGJWKQDbRRQphs7YAbxV22kHpJRuGePXvU=</InverseQ><D>YHmp/kw97TEt1fHK/43BNonDKX7GOvE81nR6OW63z5/fu4RXZ0c6fgxM+5RCJSdyIBoHbevfOQCXu1yxGuP5o27Ruk25tlXmhanQotvbys1DWolbVhBtj62DJU+Ndp/X0EGFR79MHFU0sjNTC6fj9jKAKhfjZ7FJPW7/fVMPXv0b+hu9xldUNihGTPSfiXOwvifwyESTqrFTJ7s11vz9mpQCyCe5Lp37b/oBYeTSfnkONykbQBSLwALXzPiURpO6ALhEw1uIviq1uC9QzNFe9pMTz+ttmrI5iB3cnS8J3EHAp6fhKxP/yu33Dgqwmnte20WrGNYxJYg3J0W86M1QeQ==</D></RSAKeyValue>";
        public string _publicKey = "<RSAKeyValue><Modulus>x3d/4OiQiymFIzFzQi7AWHmLcCE1CtWAS9zLDTRDX+T86PN5H8LDpg2wfyl6LBvn1RFsuL72n1vEl0mDdhllWg205nVlY6xvKFXaRfnytj2GoQNkF4xguL5+C0vTZ5BXe38E30N/+QhkNHaZ7KxPpfx3LW/urmMmtQGTAYXUHT8nLBn05sTMxOocfe5w8QYt8ZCr3CqHEvJ+VIndLKFJ06wU6WSRE8TFkQMelRltZQ7EY4bAhFx0eST9646H+eXPUxGC/kFAI/LBJMPYwwUCt4p87eBOkvkHitlNnfe4Q0C5NLQtx2AiAmbPxsFSZGR9vsTsNUA3/0QLZ1n8twnhvQ==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        public void AssignNewKey()
        {
            RSA rsa = new RSACryptoServiceProvider(2048);
            _privateKey = rsa.ToXmlString(true);
            _publicKey = rsa.ToXmlString(false);
        }

        private static UnicodeEncoding _encoder = new UnicodeEncoding();

        public string Decrypt(string data)
        {
            var rsa = new RSACryptoServiceProvider();
            var dataArray = data.Split(new char[] { ',' });
            byte[] dataByte = new byte[dataArray.Length];
            for (int i = 0; i < dataArray.Length; i++)
            {
                dataByte[i] = Convert.ToByte(dataArray[i]);
            }

            rsa.FromXmlString(_privateKey);
            var decryptedByte = rsa.Decrypt(dataByte, false);
            return _encoder.GetString(decryptedByte);
        }

        public string Encrypt(string data)
        {
            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(_publicKey);
            var dataToEncrypt = _encoder.GetBytes(data);
            var encryptedByteArray = rsa.Encrypt(dataToEncrypt, false).ToArray();
            var length = encryptedByteArray.Count();
            var item = 0;
            var sb = new StringBuilder();
            foreach (var x in encryptedByteArray)
            {
                item++;
                sb.Append(x);

                if (item < length)
                    sb.Append(",");
            }

            return sb.ToString();
        }
    }
}
