﻿using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Business.WalletService;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    public class BaseCustomerController<T> : ApiController where T: class, IBaseEntity, new()
    {
        protected CustomerContext _customerContext;
        private ModelFactory _modelFactory;
        protected const int PAGE_SIZE = 25;
        protected readonly MerchantModel _merchant;

        public BaseCustomerController(CustomerContext context)
        {
            _customerContext = context;
            //this will be returned with security service later
            _merchant = new MerchantModel { Id = 1, IsActive = true, MerchantCode = 2001, Name = "Test Merchant" };
        }
        //public Credential Credentials(HttpActionContext actionContext)
        //{
        //    var authHeader = actionContext.Request.Headers.Authorization;

        //    if (authHeader != null)
        //    {
        //        if (authHeader.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(authHeader.Parameter))
        //        {
        //            var rawCredentials = authHeader.Parameter;
        //            var encoding = Encoding.GetEncoding("iso-8859-1");
        //            var credentials = encoding.GetString(Convert.FromBase64String(rawCredentials));
        //            var split = credentials.Split(':');
        //            var username = split[0];
        //            var password = split[1];
        //            return new Credential() {UserName= username,Password=password };
        //        }                  
        //    }
        //    return new Credential();
        //}

        protected ModelFactory ModelFactory
        {
            get
            {
                if(_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(this.Request);
                }
                return _modelFactory;
            }
        }

    }
}
