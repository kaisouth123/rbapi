﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class AccountHoldersController : BaseCustomerController<FinancialResponsibleParty>, IHasOrganizationScope
    {
        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private FinancialResponsiblePartyAccessor _accessor;
        private FinancialResponsiblePartyService service;

        public AccountHoldersController(CustomerContext context)
            : base(context)
        {
            //credentials = ProfileService.GetCredentials(this.Request);
            service = new FinancialResponsiblePartyService(credentials, context);
            _accessor = new FinancialResponsiblePartyAccessor(credentials, context);
            OrganizationScope = new OrganizationScope();
        }

        public OrganizationScope OrganizationScope { get; set; }

        // GET: api/Accounts
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="entity">The entity whose child properties can be used a filter values in the query string:  AccountId, ContactId, MerchantId, Order, Guid, and IsActive.</param>
        [HttpGet]
        [OrganizationScopeFilter]
        public async Task<EntityList<AccountHolderModel>> GetAccountHolders(int accountid, int Page = 0, int Page_Size = PAGE_SIZE, bool IncludeGraph = false, bool? SearchFalse = null, [FromUri]FinancialResponsibleParty entity = null)
        {
            try
            {
                SearchFalse = SearchFalse == null ? false : SearchFalse;
                FinancialResponsibleParty party = entity == null ? new FinancialResponsibleParty { } : entity;
                //TODO: Implement ProfileService and Security
                var filter = new BaseFilter<FinancialResponsibleParty>
                {
                    entity = party,
                    Take = Page_Size,
                    IncludeGraph = IncludeGraph,
                    OrderBy = "Id",
                    Skip = (Page * Page_Size),
                    SearchFalse = (bool)SearchFalse,
                    SortDesc = false
                };
                if (OrganizationScope.IsMerchantAccount)
                    filter.entity.MerchantId = OrganizationScope.UnipayID;
                else if (OrganizationScope.IsMerchantGroup)
                    filter.MerchantGroupOrganizationID = OrganizationScope.OrganizationID;
                WCFList<FinancialResponsibleParty> entities = service.GetFinancialResponsibleParties(filter, IncludeGraph);
                var helper = new UrlHelper(this.Request);
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.TotalQueryCount / Page_Size));

                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("AccountHolders", new { accountid = accountid, page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("AccountHolders", new { accountid = accountid, page = Page + 1 }), "nextPage"));

                return await Task.Run<EntityList<AccountHolderModel>>(() =>
                {
                    entities.Entities.ShortCircuitCollection();
                var results = entities.Entities.Select(a => this.ModelFactory.Create(a, IncludeGraph));
                    return new EntityList<AccountHolderModel>
                    {
                        TotalCount = entities.TotalQueryCount,
                        TotalPages = totalPages,
                        Page = Page,
                        Links = links,
                        Results = results,
                    };
                });
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<AccountHolderModel> { Exception = newEx };
            }
        }

        // GET: api/AccountHolders/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="includeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpGet]
        [ResponseType(typeof(AccountHolderModel))]
        public async Task<IHttpActionResult> GetAccountHolder(int accountid, int id, bool includeGraph = false)
        {
            try
            {
                var merchantId = ProfileService.GetMerchantId(null, this.Request);
                FinancialResponsibleParty entity = service.GetFinancialResponsiblePartyGraph(id);
                if (entity == null)
                {
                    return NotFound();
                }
                if (entity.Account.MerchantId != merchantId)
                {
                    return BadRequest("Permission denied");
                }
                if (!includeGraph)
                {
                    entity.Account = null;
                }

                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(entity, includeGraph));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        /// <param name="AccountHolder">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPost]
        [ResponseType(typeof(AccountHolderModel))]
        public async Task<IHttpActionResult> PostAccountHolder([FromBody]AccountHolderModel AccountHolder)
        {
            try
            {
                var stringCheck = new List<string>();
                if (AccountHolder.AccountId==0)
                { stringCheck.Add("Invalid AccountId"); }
                else
                {
                    var merchantId = ProfileService.GetMerchantId(null, this.Request);
                    if(merchantId!=AccountHolder.MerchantId)
                    {
                        stringCheck.Add("Permission denied");
                    }
                }
                if (AccountHolder.ContactId == 0)
                { stringCheck.Add("Invalid ContactId"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                FinancialResponsibleParty entity = this.ModelFactory.Parse(AccountHolder);
                if (entity == null)
                    return BadRequest("Invalid AccountHolder data.");

                FinancialResponsibleParty returnedParty = service.PostFinancialResponsibleParty(entity);

                if (returnedParty != null)
                    return Created(Request.GetUrlHelper().Link("AccountHolders", new { accountid = returnedParty.AccountId, id = returnedParty.Id }), this.ModelFactory.Create(entity));
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }

        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="accountHolder">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PatchAccountHolder(int accountid, int id, [FromBody]AccountHolderModel accountHolder)
        {
            try
            {
                var stringCheck = new List<string>();              
                if (!AccountHolderExists(id))
                {
                    return NotFound();
                }
                FinancialResponsiblePartyService test = new FinancialResponsiblePartyService(credentials);

                FinancialResponsibleParty dbAccountHolder = test.GetFinancialResponsiblePartyGraph(id);
                dbAccountHolder.ShortCircuitReference();
                var merchantId = ProfileService.GetMerchantId(null, this.Request);
                if (merchantId != dbAccountHolder.Account.MerchantId)
                {
                    stringCheck.Add("Permission denied");
                }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                FinancialResponsibleParty mergedParty = this.ModelFactory.Merge(accountHolder, dbAccountHolder);

                if (mergedParty == null)
                    return BadRequest("Error parsing AccountHolderModel");

                mergedParty.ObjectState = Bridgepay.Core.Framework.ObjectState.Modified;


                FinancialResponsibleParty returnedParty = test.PostFinancialResponsibleParty(mergedParty);
                return Ok(this.ModelFactory.Create(returnedParty));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }
        private bool AccountHolderExists(int id)
        {
            return _accessor.FinancialResponsiblePartyRepository.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}