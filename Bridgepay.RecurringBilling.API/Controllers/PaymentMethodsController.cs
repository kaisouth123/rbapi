﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Helpers;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.ServiceAdaptors;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Business.WalletService;
using Bridgepay.RecurringBilling.Common;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class PaymentMethodsController : BaseCustomerController<FinancialResponsibleParty>
    {
        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private WalletAdaptor _adaptor;
        private FinancialResponsiblePartyAccessor _accessor;
        private FinancialResponsiblePartyService service;

        public PaymentMethodsController(CustomerContext context)
            : base(context)
        {
            service = new FinancialResponsiblePartyService(credentials, context);
            _accessor = new FinancialResponsiblePartyAccessor(credentials, context);
            _adaptor = new WalletAdaptor();
        }

        // GET: api/PaymentMethods
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <remarks>Note, currently graphs are not supported for this Entity.</remarks>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="accountHolderId">Unique id for the parent AccountHolder.</param>
        /// <param name="walletid">Unique id for the parent Wallet.</param>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="Entity">The entity whose child properties can be used a filter values in the query string:  WalletId, ExpirationDate, and IsActive.</param>
        /// <returns>EntityList</returns>
        public async Task<EntityList<PaymentMethodModel>> GetPaymentMethods(int accountid, int accountHolderId, Guid walletid, bool IncludeGraph = false, int Page = 0, int Page_Size = PAGE_SIZE, bool? SearchFalse = null, [FromUri]PaymentMethodModel Entity = null)
        {
            try
            {
                SearchFalse = SearchFalse == null ? false : SearchFalse;  
                FinancialResponsibleParty party = new FinancialResponsibleParty { };
                party.MerchantId = ProfileService.GetMerchantId(null, this.Request);
                party.Guid = walletid;
                WCFList<FinancialResponsibleParty> AccountHolderEntities = service.GetFinancialResponsibleParties(new BaseFilter<FinancialResponsibleParty> { entity = party, Take = Page_Size, IncludeGraph = IncludeGraph, OrderBy = "Id", Skip = (Page * Page_Size), SearchFalse = (bool)SearchFalse, SortDesc = false }, IncludeGraph);
                if(AccountHolderEntities.Entities.Count<=0)
                {
                    Exception newEx = new Exception("Invalid MerchantId/WalletId.");
                    return new EntityList<PaymentMethodModel> { Exception = newEx, };
                }
                PaymentMethodModel entity = Entity == null ? new PaymentMethodModel { } : Entity;
                PaymentMethodFilter filter = new PaymentMethodFilter { IsActive = entity.IsActive, WalletId = new System.Guid[] { walletid}, ExpirationDate = entity.ExpirationDate };
                PagingFilter pagingFilter = new PagingFilter { Skip = (Page * Page_Size), Take = Page_Size, SortField = "PaymentMethodId", };
                PagedListOfPaymentMethodLTquZG_Pa entities = _adaptor.GetPaymentMethodsByFilter(filter, pagingFilter);

                var helper = new UrlHelper(this.Request);
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.TotalQueryCount / Page_Size));

                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("PaymentMethods", new { page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("PaymentMethods", new { page = Page + 1 }), "nextPage"));


                var results = entities.Entities.Select(a => this.ModelFactory.Create(accountid, accountHolderId, a));
                entities.Entities.ShortCircuitCollection();

                return new EntityList<PaymentMethodModel>
                {
                    TotalCount = entities.TotalQueryCount,
                    TotalPages = totalPages,
                    Page = Page,
                    Links = links,
                    Results = results,
                };
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<PaymentMethodModel> { Exception = newEx };
            }
        }

        // GET: api/PaymentMethods/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <remarks>Note, currently graphs are not supported for this Entity.</remarks>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="accountHolderId">Unique id for the parent AccountHolder.</param>
        /// <param name="walletid">Unique id for the parent Wallet.</param>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="includeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(PaymentMethodModel))]
        public async Task<IHttpActionResult> GetPaymentMethod(int accountid, int accountHolderId, Guid walletid, Guid id, bool includeGraph = false)
        {
            try
            {
                Business.WalletService.PaymentMethod entity = _adaptor.GetPaymentMethodById(id);

                if (entity == null)
                {
                    return NotFound();
                }
                var stringCheck = new List<string>();
                FinancialResponsibleParty party = _accessor.FinancialResponsiblePartyRepository.Find(accountHolderId);
                if (party.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                { stringCheck.Add("Permission denied"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }

                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(accountid, accountHolderId, entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="accountHolderId">Unique id for the parent AccountHolder.</param>
        /// <param name="walletid">Unique id for the parent Wallet.</param>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="model">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PatchPaymentMethod(int accountid, int accountHolderId, Guid walletid, Guid id, [FromBody]PaymentMethodModel model)
        {
            try
            {
                model.WalletId = walletid;
                Business.WalletService.PaymentMethod entity = _adaptor.GetPaymentMethodById(id);
                if (entity == null)
                {
                    return NotFound();
                }
                var stringCheck = new List<string>();
                FinancialResponsibleParty party = _accessor.FinancialResponsiblePartyRepository.Find(accountHolderId);
                if (party.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                { stringCheck.Add("Permission denied"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                Business.WalletService.PaymentMethod mergedEntity = this.ModelFactory.Merge(model, entity);

                if (mergedEntity == null)
                    return BadRequest("Error parsing PaymentMethodModel");

                mergedEntity.ObjectState = ObjectState.Modified;


                Business.WalletService.PaymentMethod returnedPaymentMethod = _adaptor.InsertOrUpdate(entity);
                return Ok(this.ModelFactory.Create(accountid, accountHolderId, returnedPaymentMethod));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/PaymentMethods
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="accountHolderId">Unique id for the parent AccountHolder.</param>
        /// <param name="model">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(PaymentMethodModel))]
        public async Task<IHttpActionResult> PostPaymentMethod(int accountid, int accountHolderId, [FromBody]PaymentMethodModel model)
        {
            try
            {
                var stringCheck = new List<string>();
                if (string.IsNullOrEmpty(model.Token))
                { stringCheck.Add("Invalid Token"); }
                //if (model.PaymentMethodType==0)
                //{ stringCheck.Add("Invalid PaymentmethodType"); }
                if (string.IsNullOrEmpty(model.AccountHolderName))
                { stringCheck.Add("Invalid AccountHolderName"); }
                if (model.WalletId.ToString() == "00000000-0000-0000-0000-000000000000" || model.WalletId==null)
                { stringCheck.Add("Invalid WalletId"); }
                FinancialResponsibleParty party = _accessor.FinancialResponsiblePartyRepository.Find(accountHolderId);
                if (party.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                { stringCheck.Add("Permission denied"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                Business.WalletService.PaymentMethod entity = this.ModelFactory.Parse(model);
                if (entity == null)
                    return BadRequest("Invalid PaymentMethod data.");

                Business.WalletService.PaymentMethod returnedEntity = _adaptor.InsertOrUpdate(entity);

                if (returnedEntity != null)
                    return Created(Request.GetUrlHelper().Link("PaymentMethods", new { accountid = accountid, accountHolderId = accountHolderId, walletid = returnedEntity.WalletId, id = returnedEntity.PaymentMethodId }), this.ModelFactory.Create(accountid, accountHolderId, returnedEntity));
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }

        }
        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        private bool PaymentMethodExists(Guid id)
        {
            return _adaptor.GetPaymentMethodsById(id) != null;
        }
    }
}