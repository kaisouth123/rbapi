﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class ProductsController : BaseVendorController<Product>, IHasOrganizationScope
    {
        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private ProductAccessor _accessor;
        private ProductService service;

        public ProductsController(VendorContext context) : base(context)
        {
            _accessor = new ProductAccessor(credentials, context);
            service = new ProductService(credentials, context);
            OrganizationScope = new OrganizationScope();
        }

        public OrganizationScope OrganizationScope { get; set; }

        // GET: api/Products
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="Entity">The Entity whose child properties can be used a filter values in the query string:  ProductName, ProductDefaultAmount, Description, ContactId, MerchantId, and IsActive.</param>
        [OrganizationScopeFilter]
        public async Task<EntityList<ProductModel>> GetProducts(bool IncludeGraph = false, int Page = 0, int Page_Size = PAGE_SIZE, bool SearchFalse = false, [FromUri]Product entity = null)
        {
            try
            {
                entity = entity == null ? new Product { } : entity;
                var filter = new BaseFilter<Product>
                {
                    entity = entity,
                    Take = Page_Size,
                    IncludeGraph = IncludeGraph,
                    OrderBy = "Id",
                    Skip = (Page * Page_Size),
                    SearchFalse = SearchFalse
                };
                if (OrganizationScope.IsMerchantAccount)
                    filter.entity.MerchantId = OrganizationScope.UnipayID;
                else if (OrganizationScope.IsMerchantGroup)
                    filter.MerchantGroupOrganizationID = OrganizationScope.OrganizationID;
                WCFList<Product> entities = service.GetProducts(filter, IncludeGraph);
                var helper = new UrlHelper(this.Request);
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.TotalQueryCount / Page_Size));

                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Products", new { page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Products", new { page = Page + 1 }), "nextPage"));
                var results = entities.Entities.Select(a => this.ModelFactory.Create(a));
                entities.Entities.ShortCircuitCollection();

                return new EntityList<ProductModel>
                {
                    TotalCount = entities.TotalQueryCount,
                    TotalPages = totalPages,
                    Page = Page,
                    Links = links,
                    Results = results,
                };
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<ProductModel> { Exception = newEx };
            }
        }

        // GET: api/Products/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="includeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(ProductModel))]
        public async Task<IHttpActionResult> GetProduct(int id, bool includeGraph = false)
        {
            try
            {
                Product entity = service.GetProductGraph(id);
                if (entity == null)
                {
                    return NotFound();
                }
                if (entity.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                {
                    return BadRequest("Permission denied");
                }
                if (!includeGraph)
                    entity.Contracts = null;


                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="model">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PatchProduct(int id, [FromBody]ProductModel model)
        {
            try
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
   
                ProductService tempService = new ProductService(credentials);
                Product entity = tempService.GetProductGraph(id);
                if (entity.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                {
                    return BadRequest("Permission denied");
                }
                entity.Contracts = null;
                Product mergedProduct = this.ModelFactory.Merge(model, entity);

                if (mergedProduct == null)
                    return BadRequest("Error parsing ProductModel");

                mergedProduct.ObjectState = Bridgepay.Core.Framework.ObjectState.Modified;


                Product returnedProduct = tempService.MergeProduct(mergedProduct);
                return Ok(this.ModelFactory.Create(returnedProduct));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/Products
        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        /// <param name="Product">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(ProductModel))]
        public async Task<IHttpActionResult> PostProduct([FromBody]ProductModel Product)
        {
            try
            {
                var stringCheck = new List<string>();
                if (string.IsNullOrEmpty(Product.Name))
                { stringCheck.Add("Invalid Name"); }
                if (Product.DefaultAmount==null || Product.DefaultAmount == 0)
                { stringCheck.Add("Invalid DefaultAmount"); }
                if (Product.ContactId == 0)
                { stringCheck.Add("Invalid ContactId"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                Product entity = this.ModelFactory.Parse(Product);
                if (entity == null)
                    return BadRequest("Invalid Product data.");

                Product returnedProduct = service.PostProduct(entity);

                if (returnedProduct != null)
                    return Created(Request.GetUrlHelper().Link("Products", new { id = returnedProduct.Id }), this.ModelFactory.Create(entity));
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }

        }

        private bool ProductExists(int id)
        {
            return _accessor.ProductRepository.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}
