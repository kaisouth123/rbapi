﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.ServiceAdaptors;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Business.WalletService;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;
using Wallet = Bridgepay.RecurringBilling.Business.WalletService.Wallet;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class WalletsController : BaseCustomerController<FinancialResponsibleParty>
    {
        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private WalletAdaptor _adaptor;
        private FinancialResponsiblePartyAccessor _accessor;
        private FinancialResponsiblePartyService service;

        public WalletsController(CustomerContext context)
            : base(context)
        {
            service = new FinancialResponsiblePartyService(credentials, context);
            _accessor = new FinancialResponsiblePartyAccessor(credentials, context);
            _adaptor = new WalletAdaptor();
        }

        // GET: api/Accounts
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <remarks>Note, currently graphs are not supported for this Entity.</remarks>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="accountHolderId">Unique id for the parent AccountHolder.</param>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="entity">The Entity whose child properties can be used a filter values in the query string:  WalletId, SiteId, and IsActive.</param>
        public async Task<EntityList<WalletModel>> GetWallets(int accountid, int accountHolderId, bool IncludeGraph = false, int Page = 0, int Page_Size = PAGE_SIZE, bool? SearchFalse = null, [FromUri]WalletModel entity = null)
        {
            //TODO: Implement ProfileService and Security
            try
            {
                SearchFalse = SearchFalse == null ? false : SearchFalse;
                FinancialResponsibleParty party = _accessor.FinancialResponsiblePartyRepository.Find(accountHolderId);
                if(party.MerchantId!=ProfileService.GetMerchantId(null, this.Request))
                {
                    Exception newEx = new Exception("permission denied");
                    return new EntityList<WalletModel> { Exception = newEx, };
                }
                entity = entity == null ? new WalletModel { WalletId = party.Guid } : entity;
                entity.WalletId = party.Guid;
                WalletFilter filter = new WalletFilter { IsActive = entity.IsActive, WalletId = entity.WalletId, SiteId = entity.SiteId };
                PagingFilter pagingFilter = new PagingFilter { Skip = (Page * Page_Size), Take = Page_Size, SortField = "WalletId", };
                PagedListOfWalletLTquZG_Pa entities = _adaptor.GetWalletsByFilter(filter, pagingFilter);

                var helper = new UrlHelper(this.Request);
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.TotalQueryCount / Page_Size));

                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Wallets", new { page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Wallets", new { page = Page + 1 }), "nextPage"));


                var results = entities.Entities.Select(wallet => this.ModelFactory.Create(accountid, accountHolderId, wallet));
                entities.Entities.ShortCircuitCollection();

                return new EntityList<WalletModel>
                {
                    TotalCount = entities.TotalQueryCount,
                    TotalPages = totalPages,
                    Page = Page,
                    Links = links,
                    Results = results,
                };
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<WalletModel> { Exception = newEx };
            }
        }

        // GET: api/Wallets/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <remarks>Note, currently graphs are not supported for this Entity.</remarks>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="accountHolderId">Unique id for the parent AccountHolder.</param>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="includeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(WalletModel))]
        public async Task<IHttpActionResult> GetWallet(int accountid, int accountHolderId, Guid id, bool includeGraph = false)
        {
            try
            {
                Wallet entity = _adaptor.GetWalletById(id, includeGraph);
                if (entity == null)
                {
                    return NotFound();
                }
                var stringCheck = new List<string>();
                FinancialResponsibleParty party = _accessor.FinancialResponsiblePartyRepository.Find(accountHolderId);
                if (party.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                { stringCheck.Add("Permission denied"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(accountid, accountHolderId, entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="accountHolderId">Unique id for the parent AccountHolder.</param>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="model">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(WalletModel))]
        public async Task<IHttpActionResult> PatchWallet(int accountid, int accountHolderId, Guid id, [FromBody]WalletModel model)
        {
            try
            {
                Wallet entity = _adaptor.GetWalletById(id, false);
                if (entity == null)
                {
                    return NotFound();
                }
                var stringCheck = new List<string>();
                FinancialResponsibleParty party = _accessor.FinancialResponsiblePartyRepository.Find(accountHolderId);
                if (party.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                { stringCheck.Add("Permission denied"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                Wallet mergedEntity = this.ModelFactory.Merge(model, entity);

                if (mergedEntity == null)
                    return BadRequest("Error parsing WalletModel");

                mergedEntity.ObjectState = ObjectState.Modified;


                Wallet returnedWallet = _adaptor.InsertOrUpdate(entity);
                return Ok(this.ModelFactory.Create(accountid, accountHolderId, returnedWallet));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/Wallets
        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        /// <param name="accountid">Unique id for the parent Account.</param>
        /// <param name="accountHolderId">Unique id for the parent Accountholder.</param>
        /// <param name="model">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(WalletModel))]
        public async Task<IHttpActionResult> PostWallet(int accountid, int accountHolderId, [FromBody]WalletModel model)
        {
            try
            {
                var stringCheck = new List<string>();
                if (string.IsNullOrEmpty(model.Name))
                { stringCheck.Add("Invalid Name"); }
               
                FinancialResponsibleParty party = _accessor.FinancialResponsiblePartyRepository.Find(accountHolderId);
                if (party.MerchantId!=ProfileService.GetMerchantId(null, this.Request))
                { stringCheck.Add("Permission denied"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                model.WalletId = party.Guid;
                Wallet entity = this.ModelFactory.Parse(model);
                if (entity == null)
                    return BadRequest("Invalid Wallet data.");

                entity.SiteId =string.IsNullOrEmpty(model.SiteId.ToString())? _adaptor.GetSiteID(): entity.SiteId;
                Wallet returnedEntity = _adaptor.InsertOrUpdate(entity);

                if (returnedEntity != null)
                {
                    party.Guid = returnedEntity.WalletId;
                    party.ObjectState = Core.Framework.ObjectState.Modified;
                    _accessor.FinancialResponsiblePartyRepository.InsertOrUpdate(party);
                    _accessor.Save();
                    return Created(Request.GetUrlHelper().Link("Wallets", new { accountid = accountid, accountHolderId = accountHolderId, id = returnedEntity.WalletId }), this.ModelFactory.Create(accountid, accountHolderId, returnedEntity));
                }
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }

        }

        private bool WalletExists(Guid id)
        {
            return _adaptor.GetWalletById(id, false) != null;
        }
    }
}