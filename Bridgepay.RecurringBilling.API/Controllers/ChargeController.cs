﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    public class ChargeController : ApiController
    {
        private ChargeContext db = new ChargeContext();

        // GET: api/Charge
        public IQueryable<ChargeTransactionsAux> GetChargeTransactionsAuxes()
        {
            return db.ChargeTransactionsAuxes;
        }

        // GET: api/Charge/5
        [ResponseType(typeof(ChargeTransactionsAux))]
        public IHttpActionResult GetChargeTransactionsAux(long id)
        {
            ChargeTransactionsAux chargeTransactionsAux = db.ChargeTransactionsAuxes.Find(id);
            if (chargeTransactionsAux == null)
            {
                return NotFound();
            }

            return Ok(chargeTransactionsAux);
        }

        // PUT: api/Charge/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutChargeTransactionsAux(long id, ChargeTransactionsAux chargeTransactionsAux)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != chargeTransactionsAux.ID)
            {
                return BadRequest();
            }

            db.Entry(chargeTransactionsAux).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChargeTransactionsAuxExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Charge
        [ResponseType(typeof(ChargeTransactionsAux))]
        public IHttpActionResult PostChargeTransactionsAux(ChargeTransactionsAux chargeTransactionsAux)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ChargeTransactionsAuxes.Add(chargeTransactionsAux);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = chargeTransactionsAux.ID }, chargeTransactionsAux);
        }

        // DELETE: api/Charge/5
        [ResponseType(typeof(ChargeTransactionsAux))]
        public IHttpActionResult DeleteChargeTransactionsAux(long id)
        {
            ChargeTransactionsAux chargeTransactionsAux = db.ChargeTransactionsAuxes.Find(id);
            if (chargeTransactionsAux == null)
            {
                return NotFound();
            }

            db.ChargeTransactionsAuxes.Remove(chargeTransactionsAux);
            db.SaveChanges();

            return Ok(chargeTransactionsAux);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ChargeTransactionsAuxExists(long id)
        {
            return db.ChargeTransactionsAuxes.Count(e => e.ID == id) > 0;
        }
    }
}