﻿using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Business.WalletService;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    public class BaseVendorController<T> : ApiController where T: class, IBaseEntity, new()
    {
        protected VendorContext _vendorContext;
        private ModelFactory _modelFactory;
        protected const int PAGE_SIZE = 25;
        protected readonly MerchantModel _merchant;

        public BaseVendorController(VendorContext context)
        {
            _vendorContext = context;
            //this will be returned with security service later
            _merchant = new MerchantModel { Id = 1, IsActive = true, MerchantCode = 2001, Name = "Test Merchant" };
        }

        protected ModelFactory ModelFactory
        {
            get
            {
                if(_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(this.Request, _vendorContext);
                }
                return _modelFactory;
            }
        }
        public T Deserialize<T>(string str) where T : class
        {
            MediaTypeFormatter formatter = new JsonMediaTypeFormatter();
            Stream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            if (Request != null)
            {
                if (Request.Content.Headers.ContentType.MediaType.Contains("json"))
                {
                    formatter = new JsonMediaTypeFormatter();
                }
                else if (Request.Content.Headers.ContentType.MediaType.Contains("xml"))
                {
                    formatter = new XmlMediaTypeFormatter();
                }
            }
            return formatter.ReadFromStreamAsync(typeof(T), stream, null, null).Result as T;
        }
    }
}
