﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class AccountsController : BaseVendorController<Account>, IHasOrganizationScope
    {
     
        private Credential credentials =  new Credential { UserName = "Test", Password = "Test" };
        private AccountAccessor _accessor;
        private AccountService service;

        public AccountsController(VendorContext context, MerchantContext mcontext) : base(context)
        {
            credentials = ProfileService.GetCredentials(this.Request);
            _accessor = new AccountAccessor(credentials, context);
            service = new AccountService(credentials, context, mcontext);
            OrganizationScope = new OrganizationScope();
        }

        public OrganizationScope OrganizationScope { get; set; }

        // GET: api/Accounts
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <remarks>Returns an EntityList of the request entities.</remarks>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="Entity">Any of the fields in this object can be used as query string filters:  MerchantId, AccountName, AccountNumber, CreatedOn, ModifiedOn, and IsActive.</param>
        /// <returns>EntityList</returns>
        [OrganizationScopeFilter]
        public async Task<EntityList<AccountModel>> GetAccounts(bool IncludeGraph = false, int Page = 0, int Page_Size = PAGE_SIZE, bool? SearchFalse = null, [FromUri]Account Entity = null)
        {
            try
            {
                credentials = ProfileService.GetCredentials(this.Request);
                //var user = ProfileService.GetUserByUserName(credentials);
                
                SearchFalse = SearchFalse == null ? false : SearchFalse;
                Account filterEntity = Entity == null ? new Account { } : Entity;
                var filter = new BaseFilter<Account>
                {
                    entity = filterEntity,
                    Take = Page_Size,
                    IncludeGraph = IncludeGraph,
                    OrderBy = "Id",
                    Skip = (Page * Page_Size),
                    SearchFalse = (bool)SearchFalse,
                    SortDesc = false
                };
                if (OrganizationScope.IsMerchantAccount)
                    filter.entity.MerchantId = OrganizationScope.UnipayID;
                else if (OrganizationScope.IsMerchantGroup)
                    filter.MerchantGroupOrganizationID = OrganizationScope.OrganizationID;
                WCFList<Account> entities = service.GetAccounts(filter, IncludeGraph);

                var helper = new UrlHelper(this.Request);
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.TotalQueryCount / Page_Size));

                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Accounts", new { page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Accounts", new { page = Page + 1 }), "nextPage"));


                return await Task.Run<EntityList<AccountModel>>(() =>
                {
                    var results = entities.Entities.Select(a => this.ModelFactory.Create(a));
                    entities.Entities.ShortCircuitCollection();

                    return new EntityList<AccountModel>
                    {
                        TotalCount = entities.TotalQueryCount,
                        TotalPages = totalPages,
                        Page = Page,
                        Links = links,
                        Results = results,
                    };
                });
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<AccountModel> { Exception = newEx };
            }
        }

        // GET: api/Accounts/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="includeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(AccountModel))]
        public async Task<IHttpActionResult> GetAccount(int id, bool includeGraph = false)
        {
            try
            {
                Account entity = includeGraph ? service.GetAccountGraph(id) : service.GetAccountById(id);
                if (entity == null)
                {
                    return NotFound();
                }
                if(entity.MerchantId!=ProfileService.GetMerchantId(null,this.Request))
                {
                    return BadRequest("Permission denied");
                }
                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="account">The entity model to be updated.</param>
        /// <returns>IHttpActionResults</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        //[ValidateHttpAntiForgeryToken]
        public async Task<IHttpActionResult> PatchAccount(int id, [FromBody]AccountModel account)
        {
            try
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                Account dbAccount = service.GetAccountById(id);
                if (dbAccount.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                {
                    return BadRequest("Permission denied");
                }
                Account mergedAccount = this.ModelFactory.Merge(account, dbAccount);

                if (mergedAccount == null)
                    return BadRequest("Error parsing AccountModel");

                mergedAccount.ObjectState = Bridgepay.Core.Framework.ObjectState.Modified;


                Account returnedAccount = service.MergeAccount(mergedAccount);
                return Ok(this.ModelFactory.Create(returnedAccount));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/Accounts
        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        /// <param name="account">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(AccountModel))]
        //[ValidateHttpAntiForgeryToken]
        public async Task<IHttpActionResult> PostAccount([FromBody]AccountModel account)
        {
            try
            {
                var stringCheck = new List<string>();
                if (string.IsNullOrEmpty(account.Name))
                { stringCheck.Add("Invalid Name"); }
                if (account.MerchantId == 0)
                { stringCheck.Add("Invalid MerchantId"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                Account entity = this.ModelFactory.Parse(account);
                if (entity == null)
                    return BadRequest("Invalid account data.");

                Account returnedAccount = service.PostAccount(entity);

                if (returnedAccount != null)
                    return Created(Request.GetUrlHelper().Link("Accounts", new { id = returnedAccount.Id }), this.ModelFactory.Create(entity));
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }

        }

        private bool AccountExists(int id)
        {
            return _accessor.AccountRepository.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}