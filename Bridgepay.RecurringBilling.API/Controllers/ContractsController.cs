﻿using Bridgepay.Core.Cryptography;
using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Api.Services;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;


namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class ContractsController : BaseVendorController<Contract>, IHasOrganizationScope
    {

        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private ContractAccessor _accessor;
        private ContractService service;
        private CustomFieldAccessor _accessorCustomField;

        public ContractsController(VendorContext context, CustomerContext ucontext, ChargeContext ccontext,CustomFieldContext customFieldContext) : base(context)
        {
            _accessor = new ContractAccessor(credentials, context);
            service = new ContractService(credentials, context, ccontext);
            _accessorCustomField = new CustomFieldAccessor(credentials);
            OrganizationScope = new OrganizationScope();
        }

        public OrganizationScope OrganizationScope { get; set; }

        // GET: api/Contracts
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="Entity">The Entity whose child properties can be used a filter values in the query string:  ContractName, ProductId, MerchantId, BillingFrequencyId, AccountId, BillAmount, TaxAmount, AmountRemaining, CreatedOn, ModifiedOn, LastBillDate, NextBillDate, StartDate, EndDate, NumFailures, MaxFailures, EmailCustomerAtFailure, EmailMerchantAtFailure, EmailCustomerReceipt, EmailMerchantReceipt, IsActive, and Status.</param>
        [OrganizationScopeFilter]
        public async Task<IHttpActionResult> GetContracts(bool IncludeGraph = false, int Page = 0, int Page_Size = PAGE_SIZE, bool? SearchFalse = null, [FromUri]Contract entity = null)
        {
            try
            {
                SearchFalse = SearchFalse == null ? false : SearchFalse;
                BaseController baseController = new BaseController();               
                Contract filterEntity = entity == null ? new Contract { } : entity;
                var contractFilter = System.Web.HttpUtility.ParseQueryString(Request.RequestUri.Query)["contractFilter"];
                ContractFilterModel filter = new ContractFilterModel();
                if (!string.IsNullOrEmpty(contractFilter))
                {
                    filter = baseController.Deserialize<ContractFilterModel>(contractFilter);
                }
                else { filter = null; }
                var baseFilter = new BaseFilter<Contract>
                {
                    entity = filterEntity,
                    Take = Page_Size,
                    IncludeGraph = IncludeGraph,
                    OrderBy = "Id",
                    Skip = (Page * Page_Size),
                    SearchFalse = SearchFalse ?? false,
                    SortDesc = false
                };

                //only be possible get Contracts with merchant accounts and merchant groups.
                if (OrganizationScope.IsMerchantAccount)
                    baseFilter.entity.MerchantId = OrganizationScope.UnipayID;
                else if (OrganizationScope.IsMerchantGroup)
                    baseFilter.MerchantGroupOrganizationID = OrganizationScope.OrganizationID;
                else
                {
                    return Content(HttpStatusCode.Forbidden, "403 - Forbidden");
                }

                WCFList<Contract> entities = service.GetContracts(baseFilter, IncludeGraph, filter);
                entities.Entities.ShortCircuitCollection();
                
                var helper = new UrlHelper(this.Request);
                var totalPages = Convert.ToInt32(Math.Ceiling((double)entities.TotalQueryCount / Page_Size));

                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Contracts", new { page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Contracts", new { page = Page + 1 }), "nextPage"));

                SimpleAES crypto = new SimpleAES();
                credentials.UserName = crypto.DecryptString(ConfigurationManager.AppSettings["MBPUserName"]);
                credentials.Password = crypto.DecryptString(ConfigurationManager.AppSettings["MBPPassword"]);
                this.ModelFactory.SetCredentials(credentials);


                var results = entities.Entities.Select(a => this.ModelFactory.Create(a, IncludeGraph));
                entities.Entities.ShortCircuitCollection();

                EntityList<ContractModel> okResult = new EntityList<ContractModel>
                {
                    TotalCount = entities.TotalQueryCount,
                    TotalPages = totalPages,
                    Page = Page,
                    Links = links,
                    Results = results,
                };
                return Ok(okResult);
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }
        // GET: api/Contracts/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(ContractModel))]
        public async Task<IHttpActionResult> GetContract(int id, bool IncludeGraph = false)
        {
            try
            {
                Contract entity = IncludeGraph == true ? service.GetContractGraph(id) : service.GetContractById(id);
                if (entity.BillingFrequency != null)

                    if (entity == null)
                {
                    return NotFound();
                }

                // Is the contract's merchant ID available to the caller's merchant group or merchant account ID?
                var scope = ProfileService.GetOrganizationScope(Request);
                if (scope.IsMerchantGroup)
                {
                    // TP: 15831 - For legitimate requests, there should be a merchant account org with:
                    // 1. A Unipay ID matching the contract's merchant ID.
                    // 2. A parent merchant group ID matching the caller's X-RB-OrganizationId/X-RB-MerchantId.
                    var accessor = new OrganizationMapAccessor();
                    var contractMerchantGroupID = accessor.OrganizationMapRepository
                                                          .All
                                                          .FirstOrDefault(om => om.UnipayId == entity.MerchantId && om.ParentUnipayId == scope.UnipayID);
                    if (contractMerchantGroupID == null)
                    {
                        return BadRequest("Permission denied.");
                    }
                }
                else if (entity.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                {
                    return BadRequest("Permission denied");
                }
                if (!IncludeGraph)
                {
                    entity.ScheduledPayments = null;
                    entity.Charges = null;
                }

                SimpleAES crypto = new SimpleAES();
                credentials.UserName = crypto.DecryptString(ConfigurationManager.AppSettings["MBPUserName"]);
                credentials.Password = crypto.DecryptString(ConfigurationManager.AppSettings["MBPPassword"]);
                this.ModelFactory.SetCredentials(credentials);

                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(entity, IncludeGraph));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="model">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PatchContract([FromBody]ContractModel model)
        {
            try
            {
                ContractService tempService = new ContractService(credentials);
                model.LastBillDate = null;
                Contract entity = tempService.GetContractGraph(model.Id);
                if (entity == null)
                {
                    return NotFound();
                }
                else if (entity.MerchantId != ProfileService.GetMerchantId(null, this.Request))
                {
                    return BadRequest("Permission denied");
                }

                var updatedContract = ModelFactory.Parse(model);
                if (entity.SpecifiesEndDateAndNumberOfPayments())
                {
                    if (updatedContract.BillingFrequencyId <= 0)
                    {
                        // TP: 17153 - The contract check requires a billing frequency ID,
                        // but the request could be a PATCH that doesn't include it.
                        updatedContract.BillingFrequencyId = entity.BillingFrequencyId;
                    }

                    var errorMsg = string.Empty;
                    if (!updatedContract.SuppliedContractMatchesCalculatedContract(credentials, ref errorMsg))
                        return BadRequest(errorMsg);
                }

                entity.ShortCircuitReference();
                entity.ScheduledPayments = null;
                entity.Charges = null;
                Contract mergedContract = this.ModelFactory.Merge(model, entity);

                if (mergedContract == null)
                    return BadRequest("Error parsing ContractModel");

                mergedContract.ObjectState = Core.Framework.ObjectState.Modified;

                Contract returnedContract = tempService.MergeContract(mergedContract);
                return Ok(this.ModelFactory.Create(returnedContract));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/Contracts
        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        /// <param name="Contract">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(ContractModel))]
        public async Task<IHttpActionResult> PostContract([FromBody]ContractModel Contract)
        {
            try
            {
                var stringCheck = new List<string>();
                if (string.IsNullOrEmpty(Contract.Name))
                { stringCheck.Add("Invalid Name"); }
                if (Contract.BillAmount==null || Contract.BillAmount==0)
                { stringCheck.Add("Invalid BillAmount"); }
                if (Contract.AccountId==0)
                { stringCheck.Add("Invalid AccountId"); }
                if (Contract.MerchantId == 0)
                { stringCheck.Add("Invalid MerchantId"); }
                if (Contract.BillingFrequencyId == 0)
                { stringCheck.Add("Invalid BillingFrequencyId"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                //Contract.LastBillDate = Contract.StartDate == null ? DateTime.Today.ToUniversalTime() : Contract.StartDate;
                Contract entity = this.ModelFactory.Parse(Contract);
                if (entity == null)
                    return BadRequest("Invalid Contract data.");
                else if (entity.SpecifiesEndDateAndNumberOfPayments())
                {
                    var errorMsg = string.Empty;
                    if (!entity.SuppliedContractMatchesCalculatedContract(credentials, ref errorMsg))
                        return BadRequest(errorMsg);
                }

                Contract returnedContract = service.PostContract(entity,true);
                if (returnedContract != null)
                {
                    return Created(Request.GetUrlHelper().Link("Contracts", new { id = returnedContract.Id }), this.ModelFactory.Create(entity));
                }
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }

        }
        private bool ContractExists(int id)
        {
            return _accessor.ContractRepository.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}