﻿using Bridgepay.RecurringBilling.Api.Filters;
using Bridgepay.RecurringBilling.Api.Models;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Common.Utilities;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Models.Interfaces;
using CacheCow.Server.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bridgepay.RecurringBilling.Api.Controllers
{
    [RBBasicAuthenthorize(perUser: true)]
    [HttpCache(DefaultExpirySeconds = 60)]
    public class ContactsController : BaseVendorController<Contact>, IHasOrganizationScope
    {
        private Credential credentials = new Credential { UserName = "Test", Password = "Test" };
        private ContactAccessor _accessor;
        private ContactService service;
        private ProductAccessor _productAccessor;
        private ProductService productService;

        public ContactsController(VendorContext context) : base(context)
        {
            _accessor = new ContactAccessor(credentials, context);
            service = new ContactService(credentials, context);
            _productAccessor = new ProductAccessor(credentials, context);
            productService = new ProductService(credentials, context);
            OrganizationScope = new OrganizationScope();
        }

        public OrganizationScope OrganizationScope { get; set; }

        // GET: api/Contacts
        /// <summary>
        /// Returns the requested entities wrapped in a EntityList object which includes values for TotalCount, TotalPages, Page, Links, and Results which contains a collection of the Entity.
        /// </summary>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the entities requested.</param>
        /// <param name="Page">The page or results to return.</param>
        /// <param name="Page_Size">The size of the page of results to return, the default being 25.</param>
        /// <param name="SearchFalse">True/False whether to search false values for boolean arguments in the querystring.</param>
        /// <param name="Entity">The Entity whose child properties can be used a filter values in the query string:  ContactTypeId, ContactFirstName, ContactLastName, CompanyName, AddressId, ContactDepartment, ContactTitle, ContactEmail, ContactDayPhone, ContactEveningPhone, ContactMobilePhone, ContactFax, and IsActive.</param>
        /// <returns>EntityList</returns>
        [OrganizationScopeFilter]
        public async Task<EntityList<ContactModel>> GetContacts(bool IncludeGraph = false, int Page = 0, int Page_Size = PAGE_SIZE, bool SearchFalse = false, [FromUri]Contact entity = null)
        {
            try
            {
                entity = entity == null ? new Contact { } : entity;
                WCFList<Contact> entities = service.GetContacts(new BaseFilter<Contact> { entity = entity, Take = Page_Size, IncludeGraph = IncludeGraph, OrderBy = "Id", Skip = (Page * Page_Size), SearchFalse = SearchFalse, SortDesc = false }, IncludeGraph);
                var helper = new UrlHelper(this.Request);
                var productEntity = new Product { };
                var productFilter = new BaseFilter<Product>
                {
                    entity = productEntity,
                    Take = Page_Size,
                    IncludeGraph = true,
                    OrderBy = "Id",
                    Skip = (Page * Page_Size),
                    SearchFalse = SearchFalse,
                    SortDesc = false
                };
                if (OrganizationScope.IsMerchantAccount)
                    productFilter.entity.MerchantId = OrganizationScope.UnipayID;
                else if (OrganizationScope.IsMerchantGroup)
                    productFilter.MerchantGroupOrganizationID = OrganizationScope.OrganizationID;
                var products = productService.GetProducts(productFilter, IncludeGraph=true );
                var totalPages = Convert.ToInt32(Math.Ceiling((double)products.TotalQueryCount / Page_Size));

                var links = new List<LinkModel>();
                if (Page > 0)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Contacts", new { page = Page - 1 }), "prevPage"));
                if (Page < totalPages - 1)
                    links.Add(this.ModelFactory.CreateLink(helper.Link("Contacts", new { page = Page + 1 }), "nextPage"));

                entities.Entities = products.Entities.Select(x => x.Contact).ToList();
                var results = entities.Entities.Select(a => this.ModelFactory.Create(a));
                entities.Entities.ShortCircuitCollection();

                return new EntityList<ContactModel>
                {
                    TotalCount = products.TotalQueryCount,
                    TotalPages = totalPages,
                    Page = Page,
                    Links = links,
                    Results = results,
                };
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return new EntityList<ContactModel> { Exception = newEx };
            }
        }

        // GET: api/Contacts/5
        /// <summary>
        /// Returns the Entity for a given unique id wrapped in a IHttpActionResult.
        /// </summary>
        /// <param name="id">Unique id for the requested Entity.</param>
        /// <param name="IncludeGraph">True/False whether to return the child entities of the Entity requested.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(ContactModel))]
        public async Task<IHttpActionResult> GetContact(int id, bool IncludeGraph = false)
        {
            try
            {
                Contact entity = service.GetContactsGraph(id);
                if (entity == null)
                {
                    return NotFound();
                }

                if (!IncludeGraph)
                {
                    entity.ContactAddress = null;
                }


                entity.ShortCircuitReference();
                return Ok(this.ModelFactory.Create(entity));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        /// <summary>
        /// Updates an entity based on the specified fields.
        /// </summary>
        /// <remarks>This method works for both Http methods put and patch, however put requies values for every field.</remarks>
        /// <param name="id">Unique id for the entity to be updated.</param>
        /// <param name="model">The entity model to be updated.</param>
        /// <returns>IHttpActionResult</returns>
        [HttpPatch]
        [HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PatchContact(int id, [FromBody]ContactModel model)
        {
            try
            {
                if (!ContactExists(id))
                {
                    return NotFound();
                }
                ContactService tempService = new ContactService(credentials);
                Contact entity = tempService.GetContactsGraph(id);
                entity.Products = null;
                entity.FinancialResponsibleParties = null;
                Contact mergedContact = this.ModelFactory.Merge(model, entity);

                if (mergedContact == null)
                    return BadRequest("Error parsing ContactModel");

                mergedContact.ObjectState = Bridgepay.Core.Framework.ObjectState.Modified;


                Contact returnedContact = tempService.MergeContact(mergedContact);
                return Ok(this.ModelFactory.Create(returnedContact));
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }

        // POST: api/Contacts
        /// <summary>
        /// Adds an entity to the database from the model provided. Ons success, returns the updated entity wrapped in an IHttpActionResult, with the  Id value of the entity set.
        /// </summary>
        /// <param name="Contact">The entity model to be inserted.</param>
        /// <returns>IHttpActionResult</returns>
        [ResponseType(typeof(ContactModel))]
        public async Task<IHttpActionResult> PostContact([FromBody]ContactModel Contact)
        {
            try
            {
                var stringCheck = new List<string>();
                if (string.IsNullOrEmpty(Contact.ContactFirstName))
                { stringCheck.Add("Invalid First Name"); }
                if (string.IsNullOrEmpty(Contact.ContactLastName))
                { stringCheck.Add("Invalid Last Name"); }
                if (string.IsNullOrEmpty(Contact.ContactEmail))
                { stringCheck.Add("Invalid Email"); }
                if (Contact.ContactTypeId == 0)
                { stringCheck.Add("Invalid ContactTypeId"); }
                if (stringCheck.Count > 0)
                {
                    return BadRequest(string.Join(";", stringCheck.ToArray()));
                }
                Contact entity = this.ModelFactory.Parse(Contact);
                if (entity == null)
                    return BadRequest("Invalid Contact data.");

                Contact returnedContact = service.PostContact(entity);

                if (returnedContact != null)
                    return Created(Request.GetUrlHelper().Link("Contacts", new { id = returnedContact.Id }), this.ModelFactory.Create(entity));
                else
                    return BadRequest("Could not save to database");
            }
            catch (Exception ex)
            {
                Logger.LogEvent(Helpers.LogUtility.APILog, Bridgepay.RecurringBilling.Common.Utilities.Utility.GetCurrentMethod(), LogType.Error, null, ex);
                Exception newEx = new Exception("An internal server error occured.");
                return InternalServerError(newEx);
            }
        }
        private bool ContactExists(int id)
        {
            return _accessor.ContactRepository.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}