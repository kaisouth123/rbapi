﻿using Bridgepay.RecurringBilling.Common.Utilities;
using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web;

namespace Bridgepay.RecurringBilling.Api.Helpers
{
    public static class LogUtility
    {
        public static ILog APILog;
        public static LogMode LogMode;
        static LogUtility()
        {
            LogMode = (LogMode)Enum.Parse(typeof(LogMode), ConfigurationManager.AppSettings["LogMode"].ToString());
            string logFile = HttpContext.Current.Server.MapPath(@"log4net.config");
            //var logFile = AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory + "\\" + @"log4net.config";
            XmlConfigurator.ConfigureAndWatch(new FileInfo(logFile));
            APILog = LogManager.GetLogger(typeof(HttpApplication)); 
        }
    }

    public static class RequestInfo
    {
        public static String GetClientIp(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                HttpContextWrapper ctx =
                    (HttpContextWrapper)request.Properties["MS_HttpContext"];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
                else
                    return "";
            }
            else
                return "";
        }
    }

}