﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.ModelBinding;

namespace Bridgepay.RecurringBilling.Api.Helpers
{
    /// <summary>
    /// Helper methods for <see cref="ModelStateDictionary"/>.
    /// </summary>
    public static class ModelStateHelper
    {
        /// <summary>
        /// Gets a list of all the error messages in the model state.
        /// </summary>
        /// <param name="modelState"><see cref="ModelStateDictionary"/>.</param>
        /// <returns>A list of all the error messages in the model state.</returns>
        public static List<string> GetErrorMessages(this ModelStateDictionary modelState)
        {
            var errors = modelState.Values
                                   .SelectMany(v => v.Errors)
                                   .Where(e => !string.IsNullOrWhiteSpace(e.ErrorMessage))
                                   .Select(e => e.ErrorMessage)
                                   .ToList();
            return errors;
        }
    }
}