﻿using Bridgepay.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using SanitizerFunc = System.Func<string, string>;

namespace Bridgepay.RecurringBilling.Api.Helpers
{
    /// <summary>
    /// Logs HTTP requests and responses tailored to BridgePay's requirements.
    /// </summary>
    public class LogRequestResponseHandler : DelegatingHandler
    {
        /// <summary>
        /// HTTP request methods to log.
        /// </summary>
        private static readonly string[] MethodsToLog =
        {
            "POST",
            "PATCH",
            "PUT"
        };

        /// <summary>
        /// Instantiates a logging object for HTTP requests and responses.
        /// </summary>
        public LogRequestResponseHandler()
        {
            responseSanitizierQueue = new Queue<SanitizerFunc>();
        }

        private Queue<SanitizerFunc> responseSanitizierQueue;
        private const string BasicAuthHeader = "Basic ";
        private static readonly char[] BasicAuthSeparator = new char[] { ':' };

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response = null;
            try
            {
                if (!MethodsToLog.Contains(request.Method.Method, StringComparer.InvariantCultureIgnoreCase))
                {
                    // Nothing of interest to log.
                    return await base.SendAsync(request, cancellationToken);
                }

                // Log the request.
                var requestLog = new StringBuilder();
                var correlationID = request.GetCorrelationId();
                requestLog.AppendLine($"REQUEST Correlation ID: {correlationID}");
                requestLog.AppendLine($"{request.Method} {request.RequestUri}");
                requestLog.AppendLine($"Username: \"{GetUserName(request.Headers.Authorization?.ToString())}\"");
                requestLog.AppendLine("Body:");
                if (request.Content != null)
                {
                    requestLog.AppendLine(await request.Content.ReadAsStringAsync());
                }
                LogProvider.LoggerInstance.Info(requestLog.ToString());
                
                // Wait for the request to be completed by the appropriate controller.
                response = await base.SendAsync(request, cancellationToken);

                // Log the response.
                var responseLog = new StringBuilder();
                responseLog.AppendLine($"RESPONSE Correlation ID: {correlationID}");
                responseLog.AppendLine($"HTTP {(int)response.StatusCode} {response.ReasonPhrase}");
                responseLog.AppendLine("Body:");
                if (response.Content != null)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    foreach (var sanitizer in responseSanitizierQueue)
                    {
                        body = sanitizer(body);
                    }
                    responseLog.AppendLine(body);
                }
                LogProvider.LoggerInstance.Info(responseLog.ToString());
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Error("Failure logging request or response.", ex);
            }

            return response ?? await base.SendAsync(request, cancellationToken);
        }

        /// <summary>
        /// Adds a response sanitizer function to the processing queue.
        /// </summary>
        /// <param name="sanitizerFunc">Function to sanitize the response body.</param>
        public void AddResponseSanitizer(SanitizerFunc sanitizerFunc)
        {
            if (sanitizerFunc == null)
            {
                throw new ArgumentNullException(nameof(sanitizerFunc));
            }

            responseSanitizierQueue.Enqueue(sanitizerFunc);
        }

        /// <summary>
        /// Sanitize the "Links" JSON array from a response body.
        /// </summary>
        /// <param name="responseBody">HTTP response body.</param>
        /// <returns>Sanitized response body.</returns>
        public static string LinksSanitizer(string responseBody)
        {
            try
            {
                // (Links":[)(Everything that isn't a right bracket) => Links":[
                return Regex.Replace(responseBody, @"(Links"":\[)([^\]]+)", m => m.Groups[1].Value);
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Warn("Failed to sanitize links.", ex);
                return responseBody;
            }
        }

        internal static string GetUserName(string authorizationHeader)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(authorizationHeader) ||
                !authorizationHeader.StartsWith(BasicAuthHeader))
                {
                    return string.Empty;
                }

                // Basic VXNlcjpQYXNzd29yZA== -> VXNlcjpQYXNzd29yZA==
                var encoded = authorizationHeader.Substring(BasicAuthHeader.Length).Trim();

                // VXNlcjpQYXNzd29yZA== -> User:Password
                var decoded = Encoding.UTF8.GetString(Convert.FromBase64String(encoded));
                
                return decoded.Split(BasicAuthSeparator, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Warn("Failed to parse authorization header: " + authorizationHeader, ex);
                return string.Empty;
            }
        }
    }
}