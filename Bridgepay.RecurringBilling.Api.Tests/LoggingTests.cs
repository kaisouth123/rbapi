﻿using Bridgepay.RecurringBilling.Api.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Bridgepay.RecurringBilling.Api.Tests
{
    [TestClass]
    public class LoggingTests
    {
        private const string LogJson = @"{""Links"":[{""Href"":""http://localhost:58500/api/accounts/4654/accountholders/47197/wallets/6D28B9B3-A0FA-4878-8D3A-A956CDF4D50F/paymentmethods/cec407e0-c03b-46ae-8b37-01ec499188e8"",""Rel"":""Self or Patch"",""Method"":""Get"",""IsTemplated"":false},{""Href"":""http://localhost:58500/api/accounts/4654/accountholders/47197/wallets/6D28B9B3-A0FA-4878-8D3A-A956CDF4D50F/paymentmethods/cec407e0-c03b-46ae-8b37-01ec499188e8"",""Rel"":""Post"",""Method"":""Get"",""IsTemplated"":false}],""PaymentMethodId"":""cec407e0-c03b-46ae-8b37-01ec499188e8"",""PaymentMethodType"":0,""CardType"":""Visa"",""Description"":""Testing TP 16667"",""ExpirationDate"":""0222"",""LastFour"":""1006"",""Token"":""11110000000068811006"",""RoutingNo"":null,""AccountType"":null,""AccountHolderName"":""Test Swiped"",""AccountHolderAddress"":null,""AccountHolderCity"":null,""AccountHolderState"":null,""AccountHolderZip"":null,""AccountHolderPhone"":null,""AccountHolderEmail"":null,""MaxFailures"":null,""IsActive"":false,""Order"":7,""WalletId"":""6d28b9b3-a0fa-4878-8d3a-a956cdf4d50f"",""CreateDate"":""2021-02-08T15:10:20.1547544-05:00"",""UpdateDate"":null,""FailureCount"":0,""LastResult"":null,""Wallet"":null}";

        [TestMethod]
        public void ShouldGetUserName()
        {
            string basicAuth = "Basic VXNlcjpQYXNzd29yZA==";

            var username = LogRequestResponseHandler.GetUserName(basicAuth);

            Assert.AreEqual("User", username);
        }

        [TestMethod]
        public void LinksShouldSanitize()
        {
            var body = LogRequestResponseHandler.LinksSanitizer(LogJson);

            var expected = @"{""Links"":[],""PaymentMethodId"":""cec407e0-c03b-46ae-8b37-01ec499188e8"",""PaymentMethodType"":0,""CardType"":""Visa"",""Description"":""Testing TP 16667"",""ExpirationDate"":""0222"",""LastFour"":""1006"",""Token"":""11110000000068811006"",""RoutingNo"":null,""AccountType"":null,""AccountHolderName"":""Test Swiped"",""AccountHolderAddress"":null,""AccountHolderCity"":null,""AccountHolderState"":null,""AccountHolderZip"":null,""AccountHolderPhone"":null,""AccountHolderEmail"":null,""MaxFailures"":null,""IsActive"":false,""Order"":7,""WalletId"":""6d28b9b3-a0fa-4878-8d3a-a956cdf4d50f"",""CreateDate"":""2021-02-08T15:10:20.1547544-05:00"",""UpdateDate"":null,""FailureCount"":0,""LastResult"":null,""Wallet"":null}";

            Assert.AreEqual(expected, body);
        }
    }
}
