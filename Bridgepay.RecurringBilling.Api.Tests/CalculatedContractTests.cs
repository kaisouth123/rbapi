﻿using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Bridgepay.RecurringBilling.Api.Tests
{
    [TestClass]
    public class CalculatedContractTests
    {
        enum Intervals
        {
            Daily = 1,
            DailyByDays,
            WeeklyByDays,
            MonthlyByDayOfMonth,
            MonthlyByDaysWeekOfMonth,
            YearlyByDayOfMonthMonth,
            YearlyByDaysWeekOfMonthMonth,
            SubWeekly,
            SubMonthly,
            SubYearly
        };

        [TestMethod]
        public void ShouldCalculateDailyEveryOtherDayEndDate()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.Daily,
                Frequency = 2,
                FrequencyName = "Every Other Day"
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2021, 1, 1),
                NumberOfPayments = 4
            };
            // 4 payments starting 1/1/21 => [ 1/1/21, 1/3/21, 1/5/21, 1/7/21 ]
            var expectedEndDate = new DateTime(2021, 1, 7);

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(expectedEndDate, contract.EndDate);
        }

        [TestMethod]
        public void ShouldCalculateWeeklyEveryFriday()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.WeeklyByDays,
                Frequency = 1,
                Days = "0000010",
                FrequencyName = "Every Friday",
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2021, 2, 1),
                NumberOfPayments = 4,
            };
            // 4 Friday payments starting 2/1/21 => [ 2/5/21, 2/12/21, 2/19/21, 2/26/21 ]
            var expectedEndDate = new DateTime(2021, 2, 26);

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(expectedEndDate, contract.EndDate);
        }

        [TestMethod]
        public void ShouldCalculate15thOfEveryMonth()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.MonthlyByDayOfMonth,
                Frequency = 1,
                DayMonth = 15,
                FrequencyName = "15th of every month",
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2021, 1, 1),
                NumberOfPayments = 4,
            };
            // 4 payments on the 15th of each month starting 1/1/21 => [ 1/15/21, 2/15/21, 3/15/21, 4/15/21 ]
            var expectedEndDate = new DateTime(2021, 4, 15);

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(expectedEndDate, contract.EndDate);
        }

        [TestMethod]
        public void ShouldCalculateFirstMondayOfEveryTwoMonths()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.MonthlyByDaysWeekOfMonth,
                Frequency = 2,
                WeekMonth = 1,
                Days = "0100000",
                FrequencyName = "First Monday of every two months"
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2021, 1, 1),
                NumberOfPayments = 4,
            };
            // 4 Payments on the first Monday of every two months starting 1/1/21 => [ 1/5/21, 3/1/21, 5/3/21, 7/5/21 ]
            var expectedEndDate = new DateTime(2021, 7, 5);

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(expectedEndDate, contract.EndDate);
        }

        [TestMethod]
        public void ShouldCalculateFourthOfJulyAnnually()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.YearlyByDayOfMonthMonth,
                Frequency = 1,
                DayMonth = 4,
                Month = 7,
                FrequencyName = "July 4th annually"
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2020, 1, 1),
                NumberOfPayments = 3
            };
            // 3 payments annually on July 4 starting 1/1/20 => [ 7/4/20, 7/4/21, 7/4/22 ]
            var expectedEndDate = new DateTime(2022, 7, 4);

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(expectedEndDate, contract.EndDate);
        }

        [TestMethod]
        public void ShouldCalculateFirstTuesdayInFebruaryAnnually()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.YearlyByDaysWeekOfMonthMonth,
                Frequency = 1,
                WeekMonth = 1,
                Month = 2,
                Days = "0010000",
                FrequencyName = "First Tuesday in February annually"
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2020, 2, 1),
                NumberOfPayments = 3
            };
            // 3 Payments annually on the first Tuesday in February starting 2/1/20 => [ 2/4/20, 2/2/21, 2/1/22 ]
            var expectedEndDate = new DateTime(2022, 2, 1);

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(expectedEndDate, contract.EndDate);
        }

        [TestMethod]
        public void ShouldCalculateNumberOfPaymentsEveryThirdDay()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int) Intervals.Daily,
                Frequency = 3,
                FrequencyName = "Every Third Day"
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2021, 3, 11),
                EndDate = new DateTime(2021, 3, 28)
            };

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(6, contract.NumberOfPayments);
        }

        [TestMethod]
        public void ShouldCalculateNumberOfPaymentsEveryWednesday()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.WeeklyByDays,
                Frequency = 1,
                Days = "0001000",
                FrequencyName = "Every Wednesday",
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2021, 3, 9),
                EndDate = new DateTime(2021, 4, 1)
            };

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(4, contract.NumberOfPayments);
        }

        [TestMethod]
        public void ShouldCalculateNumberOfPayments3rdOfEveryMonth()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.MonthlyByDayOfMonth,
                Frequency = 1,
                DayMonth = 3,
                FrequencyName = "3rd of every month",
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2020, 12, 1),
                EndDate = new DateTime(2021, 5, 31)
            };

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(6, contract.NumberOfPayments);
        }

        [TestMethod]
        public void ShouldCalculateNumberOfPaymentsFirstThursdayOfEveryThreeMonths()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.MonthlyByDaysWeekOfMonth,
                Frequency = 2,
                WeekMonth = 1,
                Days = "0000100",
                FrequencyName = "First Thurdsay of every two months"
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2021, 1, 1),
                EndDate = new DateTime(2021, 11, 3)
            };

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(5, contract.NumberOfPayments);
        }

        [TestMethod]
        public void ShouldCalculateNumberOfPaymentsFourteenthOfMarchAnnually()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.YearlyByDayOfMonthMonth,
                Frequency = 1,
                DayMonth = 14,
                Month = 7,
                FrequencyName = "March 14th annually"
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2020, 1, 1),
                EndDate = new DateTime(2023, 4, 13)
            };

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(3, contract.NumberOfPayments);
        }

        [TestMethod]
        public void ShouldCalculateNumberOfPaymentsFirstFridayInAprilAnnually()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.YearlyByDaysWeekOfMonthMonth,
                Frequency = 1,
                WeekMonth = 1,
                Month = 4,
                Days = "0000010",
                FrequencyName = "First Friday in April annually"
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2020, 2, 1),
                EndDate = new DateTime(2025, 4, 3)
            };

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.AreEqual(5, contract.NumberOfPayments);
        }

        [TestMethod]
        public void ShouldCreateOpenEndedContract()
        {
            var frequency = new BillingFrequency
            {
                IntervalId = (int)Intervals.MonthlyByDayOfMonth,
                Frequency = 1,
                DayMonth = 6,
                FrequencyName = "6th of every month",
            };
            var contract = new Contract
            {
                StartDate = new DateTime(2021, 3, 1)
            };

            ContractHelper.GetCalculatedContract(contract, frequency);

            Assert.IsNull(contract.NumberOfPayments);
            Assert.IsNull(contract.EndDate);
        }
    }
}
