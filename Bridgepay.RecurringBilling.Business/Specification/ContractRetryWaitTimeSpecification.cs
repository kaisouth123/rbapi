﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Specification
{
    public class ContractRetryWaitTimeSpecification : ISpecification<Contract>
    {
        public bool IsSatisfiedBy(Contract entity)
        {
            bool test = entity.LastBillDate != null && ((DateTime)entity.LastBillDate).AddDays(Convert.ToDouble(entity.RetryWaitTime)) <= DateTime.Today.ToUniversalTime();
            return test;
        }
    }
}
