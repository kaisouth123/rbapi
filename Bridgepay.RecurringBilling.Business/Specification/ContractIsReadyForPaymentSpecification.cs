﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Specification
{
    public class ContractIsReadyForPaymentSpecification : ISpecification<Contract>
    {
        public bool IsSatisfiedBy(Contract entity)
        {
            ContractShouldRetrySpecification retry = new ContractShouldRetrySpecification();
            ContractIsReadySpecification ready = new ContractIsReadySpecification();
            return entity.IsActive == true && (ready.IsSatisfiedBy(entity) || retry.IsSatisfiedBy(entity));
        }
    }
}
