﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Specification
{
    public class ContractBillDateSpecification: ISpecification<Contract>
    {
        public bool IsSatisfiedBy(Contract entity)
        {
            return entity.NextBillDate <= DateTime.Today.AddDays(1).ToUniversalTime();
        }
    }
}
