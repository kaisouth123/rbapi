﻿using Bridgepay.RecurringBilling.Business.Enums;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Specification
{
    public class ContractIsReadySpecification: ISpecification<Contract>
    {
        public bool IsSatisfiedBy(Contract entity)
        {
            return entity.Status == (int)StatusCode.ACTIVE || entity.Status == (int)StatusCode.ERROR || entity.Status == (int)StatusCode.PROCESSING || entity.Status == null;
        }
    }
}
