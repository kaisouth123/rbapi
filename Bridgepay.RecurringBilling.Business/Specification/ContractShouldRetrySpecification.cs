﻿using Bridgepay.RecurringBilling.Business.Enums;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Specification
{
    public class ContractShouldRetrySpecification : ISpecification<Contract>
    {
        public bool IsSatisfiedBy(Contract entity)
        {
            ContractRetryWaitTimeSpecification waitTime = new ContractRetryWaitTimeSpecification();

            return waitTime.IsSatisfiedBy(entity) && entity.Status == (int)StatusCode.DECLINED;
        }
    }
}
