﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Specification
{
    public class ContractCompleteSpecification : ISpecification<Contract>
    {
        public bool IsSatisfiedBy(Contract entity)
        {
            ContractEndDateSpecification endDatePassed = new ContractEndDateSpecification();
            ContractTotalReachedSpecification totalReached = new ContractTotalReachedSpecification();

            ISpecification<Contract> ContractComplete = endDatePassed.And(totalReached);

            return ContractComplete.IsSatisfiedBy(entity);
        }
    }
}
