﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Specification
{
    public class ContractWillCancelSpecification : ISpecification<Contract>
    {
        public bool IsSatisfiedBy(Contract entity)
        {
            ContractFailuresReachedSpecification failures = new ContractFailuresReachedSpecification();

            return failures.IsSatisfiedBy(entity);
        }
    }
}
