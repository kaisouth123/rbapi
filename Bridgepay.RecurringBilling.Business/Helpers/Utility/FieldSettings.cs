﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers.Utility
{
    public class FieldSettings
    {
        public FieldSettings()
        {
        }

        public FieldSettings(object entity, PropertyInfo info, string displayName = "", bool includeObjects = false, bool includeCollections = false)
        {
            DisplayName = String.IsNullOrEmpty(displayName) ? info.Name : displayName;
            FieldName = info.Name;
            Value = entity.ToString();
            FieldValue = entity;
            ColumnSpan = 1;
            IncludeCollections = includeCollections;
            IncludeObjects = includeObjects;
            LabelCss = "formLabel";
            BodyCss = "formBody";
            HeaderCss = "formHeader";
            TableCss = "formTable";
            TableStyle = "border-style: outset; border-width: 2px; background-color: ghostwhite; width: 100%;";

            if (info != null)
            {
                if (Convert.GetTypeCode(FieldValue) != TypeCode.Object && !(FieldValue is ICollection))
                    FieldType = FieldType.Primitive;
                else if (FieldValue is ICollection)
                    FieldType = FieldType.Collection;
                else
                    FieldType = FieldType.Object;

            }

            if (FieldType == FieldType.Object && !(FieldValue is ICollection))
            {
                Fields = new List<FieldSettings>();
                PropertyInfo[] fis = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                foreach (PropertyInfo prop in fis)
                {
                    object propValue = prop.GetValue(entity);
                    if (propValue != null)
                    {
                        if (Convert.GetTypeCode(FieldValue) != TypeCode.Object && !(FieldValue is ICollection))
                        {
                            Fields.Add(new FieldSettings(propValue, prop));
                        }
                        else if (FieldValue is ICollection && IncludeCollections)
                        {
                            ICollection collection = FieldValue as ICollection;
                            Fields.Add(new FieldSettings(collection, prop.Name));
                        }
                        else if (Convert.GetTypeCode(FieldValue) == TypeCode.Object && includeObjects)
                        {
                            Fields.Add(new FieldSettings(propValue, prop, "", true, false));
                        }
                    }
                }
            }

        }

        public FieldSettings(object entity, string displayName, bool includeObjects = false, bool includeCollections = false)
        {
            DisplayName = displayName;
            FieldName = displayName.Replace(" ", "");
            Value = entity.ToString();
            FieldValue = entity;
            ColumnSpan = 1;
            IncludeCollections = includeCollections;
            IncludeObjects = includeObjects;
            LabelCss = "formLabel";
            BodyCss = "formBody";
            TableStyle = "border-style:outset;border-width:2px;background-color:ghostwhite;width:100%;";


            if (Convert.GetTypeCode(FieldValue) != TypeCode.Object && !(FieldValue is ICollection))
                FieldType = FieldType.Primitive;
            else if (FieldValue is ICollection)
                FieldType = FieldType.Collection;
            else
                FieldType = FieldType.Object;



            if (FieldType == FieldType.Object && !(FieldValue is ICollection))
            {
                Fields = new List<FieldSettings>();
                PropertyInfo[] fis = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                foreach (PropertyInfo prop in fis)
                {
                    object propValue = prop.GetValue(entity);
                    if (propValue != null)
                    {
                        if (Convert.GetTypeCode(FieldValue) != TypeCode.Object && !(FieldValue is ICollection))
                        {
                            Fields.Add(new FieldSettings(propValue, prop));
                        }
                        else if (FieldValue is ICollection && IncludeCollections)
                        {
                            ICollection collection = FieldValue as ICollection;
                            Fields.Add(new FieldSettings(collection, prop.Name));
                        }
                        else if (Convert.GetTypeCode(FieldValue) == TypeCode.Object && includeObjects)
                        {
                            Fields.Add(new FieldSettings(propValue, prop, "", true, false));
                        }
                    }
                }
            }


        }

        public FieldSettings(ICollection collection, string displayName, bool includeObjects = false, int columnSpan = 1)
        {
            DisplayName = displayName;
            FieldName = displayName.Replace(" ", "");
            ColumnSpan = columnSpan;
            IncludeCollections = false;
            IncludeObjects = includeObjects;
            LabelCss = "formLabel";
            BodyCss = "formBody";
            TableStyle = "border-style: outset; border-width: 2px; background-color: ghostwhite; width: 100%;";
            Collection = collection;
            FieldType = FieldType.Collection;
        }

        public string FieldName { get; set; }
        public string DisplayName { get; set; }
        public string LabelStyle { get; set; }
        public string LabelCss { get; set; }
        public string BodyCss { get; set; }
        public string HeaderCss { get; set; }
        public string TableCss { get; set; }
        public string BodyStyle { get; set; }
        public FieldType FieldType { get; set; }
        public string FormatValue { get; set; }
        public string Value { get; set; }
        public object FieldValue { get; set; }
        public int ColumnSpan { get; set; }
        public List<FieldSettings> Fields { get; set; }
        public String TableStyle { get; set; }
        public bool IncludeCollections { get; set; }
        public bool IncludeObjects { get; set; }
        public ICollection Collection { get; set; }

    }

    public enum FieldType
    {
        Primitive = 1,
        Object = 2,
        Collection = 3,

    }

}
