﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers.Utility
{
    public class ObjectTable
    {
        public string TableHeader { get; set; }
        private string rowBegin = "<tr><td class='formLabel'>";
        private string rowMiddle = "</td><td class='formBody'>";
        private string rowEnd = "</td></tr>";
        private string bodyEnd = "</table></body></html>";
        public string LogoUrl { get; set; }
        public string formLabelCss { get; set; }
        public string formBodyCss { get; set; }
        public string formTabelCss { get; set; }
        public List<FieldSettings> Fields { get; set; }

        public string BuilderHeader()
        {
            StringBuilder shead = new StringBuilder();

            StringBuilder sHead = new StringBuilder();
            sHead.Append("<html><head>");
            sHead.Append("<style type='text/css'>");
            sHead.Append("<!--");
            if (string.IsNullOrEmpty(formBodyCss))
                sHead.Append(".formBody { border-style: dotted; border-width: 1px; dotted; border-color: dimgray; background-color:white;}");
            else
                shead.Append(".formBody {" + formBodyCss + "}");
            if (string.IsNullOrEmpty(formLabelCss))
                sHead.Append(".formLabel { border-style: solid; border-width: 1px; border-color: dimgray; width: 30%; background-color:aliceblue; text-align: right; font-family: 'Verdana', Verdana, monospace; color: #003f59; padding: 2px;}");
            else
                sHead.Append(".formLabel {" + formBodyCss + "}");
            sHead.Append(".formHeader { border-style: solid; border-width: 1px; border-color: dimgray;  background-color: lightblue; font-family: 'Verdana', Verdana, monospace; color: #003f59; padding: 2px;} ");
            sHead.Append(".formTop { border-style: solid; border-width: 1px; border-color: dimgray; background-color:aliceblue; font-weight: bold; font-size: 20px; text-align: left; font-family: Verdana, Courier, monospace; color: #003f59; padding: 10px; height: 100px; }");
            sHead.Append(".formTable { width:100%; }");
            sHead.Append("-->");
            sHead.Append("</style></head>");


            return sHead.ToString();
        }

        public string BuildTable<T>(T entity, List<String> includeFields, List<String> includeCollections = null, List<String> childFields = null, bool objectVertical = false)
        {
            StringBuilder tableBuilder = new StringBuilder();
            tableBuilder.Append(BuilderHeader());
            tableBuilder.Append(StartBody());

            Fields = ReturnFields<T>(entity, includeFields);

            PropertyInfo[] fis = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (PropertyInfo fi in fis)
            {
                if (includeFields.Count > 0 && Fields.Where(c => c.FieldName == fi.Name).Count() > 0)
                {
                    FieldSettings setting = Fields.Where(c => c.FieldName == fi.Name).FirstOrDefault();
                    if (setting.FieldType != FieldType.Collection && Convert.GetTypeCode(setting.FieldValue) != TypeCode.Object)
                        tableBuilder.Append(ReturnPropertyRow(setting));
                    else if (setting.FieldType == FieldType.Collection)
                    {
                        ICollection collection = setting.Collection;
                        if (collection != null && collection.Count > 0)
                        {
                            if (childFields != null && childFields.Count >= includeCollections.IndexOf(fi.Name))
                            {
                                String[] fields = childFields[includeCollections.IndexOf(fi.Name)].Split(",".ToCharArray());
                                tableBuilder.Append("<tr><td colspan='2' style='width:100%; text-align: left'>" + BuildCollectionTable(setting, fields.ToList()) + "</td></tr>");
                            }
                            else
                                tableBuilder.Append("<tr><td colspan='2' style='width:100%; text-align: left'>" + BuildCollectionTable(setting) + "</td></tr>");
                        }

                    }
                    else if (Convert.GetTypeCode(setting.FieldValue) == TypeCode.Object)
                    {
                        tableBuilder.Append(ReturnObjectRow(setting, objectVertical));
                    }
                }
            }

            tableBuilder.Append(bodyEnd);

            return tableBuilder.ToString();

        }

        private List<FieldSettings> ReturnFields<T>(T entity, List<string> includeFields)
        {
            List<FieldSettings> fields = new List<FieldSettings>();
            List<PropertyInfo> fis = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).ToList();

            foreach (string field in includeFields)
            {
                PropertyInfo info = fis.Where(c => c.Name == field.Replace(" ", "")).FirstOrDefault();

                if (info != null)
                {
                    object fieldValue = info.GetValue(entity);
                    Type type = info.GetType();
                    if (Convert.GetTypeCode(fieldValue) != TypeCode.Object && !(fieldValue is ICollection))
                        fields.Add(new FieldSettings(fieldValue, info));
                    else if (fieldValue is ICollection)
                        fields.Add(new FieldSettings((ICollection)fieldValue, info.Name));
                    else
                        fields.Add(new FieldSettings(fieldValue, info, "", true));

                }
            }

            return fields;
        }

        private string StartBody()
        {
            StringBuilder bodyStart = new StringBuilder();

            if (string.IsNullOrEmpty(formTabelCss))
                bodyStart.Append("<body><table style='border-style: outset; border-width: 2px; background-color: ghostwhite; width: 100%;' >");
            else
                bodyStart.Append("<body><table style='" + formTabelCss + "' >");

            if (!string.IsNullOrEmpty(TableHeader))
                bodyStart.Append("<tr><td class='formTop' colspan='2'><img src='" + LogoUrl + "' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + TableHeader + "</tr></td>");

            return bodyStart.ToString();
        }

        private string ReturnPropertyRow(FieldSettings settings)
        {
            string labelCss = String.IsNullOrEmpty(settings.LabelCss) ? "formLabel" : settings.LabelCss;
            string bodyCss = String.IsNullOrEmpty(settings.LabelCss) ? "formBody" : settings.BodyCss;
            string rowStart = String.IsNullOrEmpty(settings.LabelStyle) ? "<tr><td class='" + labelCss + "'>" : "<tr><td class='" + labelCss + "' style='" + settings.LabelStyle + "'>";
            string rowMid = String.IsNullOrEmpty(settings.BodyStyle) ? "</td><td class='" + bodyCss + "'>" : "</td><td class='" + bodyCss + "' style='" + settings.BodyStyle + "'>";

            if (settings.FieldValue != null)
            {
                return rowStart + settings.DisplayName + ":" + rowMid + settings.Value + this.rowEnd;
            }
            else
            {
                return rowStart + settings.DisplayName + ":" + rowMid + " " + this.rowEnd;
            }
        }

        public string BuildCollectionTable(FieldSettings setting, List<String> includeFields = null)
        {
            StringBuilder tableBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(formTabelCss))
                tableBuilder.Append("<table style='border-style: outset; border-width: 2px; background-color: ghostwhite; width: 100%;' >");
            else
                tableBuilder.Append("<table style='" + formTabelCss + "' >");
            int x = 0;
            foreach (object entity in setting.Collection)
            {
                PropertyInfo[] props = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                FieldSettings fieldSetting = new FieldSettings(entity, entity.GetType().Name);
                if (x == 0)
                    tableBuilder.Append(ReturnCollectionHeader(fieldSetting, includeFields));
                x++;
                tableBuilder.Append(ReturnCollectionRow(fieldSetting, includeFields));
            }

            tableBuilder.Append("</table>");

            return tableBuilder.ToString();

        }

        private string ReturnCollectionHeader(FieldSettings settings, List<string> includeFields = null)
        {
            StringBuilder rowbuilder = new StringBuilder();
            if (includeFields == null)
                includeFields = new List<string>();

            PropertyInfo[] fis = settings.FieldValue.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            rowbuilder.Append("<tr>");
            string labelCss = String.IsNullOrEmpty(settings.HeaderCss) ? "formHeader" : settings.LabelCss;
            string labelStyle = String.IsNullOrEmpty(settings.LabelStyle) ? "text-align: left;" : settings.BodyStyle;
            foreach (PropertyInfo fi in fis)
            {
                if ((includeFields.Count > 0 && includeFields.Contains(fi.Name)) || includeFields.Count <= 0)
                {
                    rowbuilder.Append("<td class='" + labelCss + "' style='" + labelStyle + "'> " + fi.Name + " </td>");
                }
                else if (settings.Fields.Where(c => c.FieldName == fi.Name).Count() > 0)
                {
                    FieldSettings setting = settings.Fields.Where(c => c.FieldName == fi.Name).FirstOrDefault();
                    rowbuilder.Append("<td class='" + labelCss + "' style='" + labelStyle + "'> " + fi.Name + " </td>");
                }
            }
            rowbuilder.Append("</tr>");

            return rowbuilder.ToString();
        }

        private string ReturnCollectionRow(FieldSettings settings, List<string> includeFields = null)
        {
            StringBuilder rowbuilder = new StringBuilder();
            if (includeFields == null)
                includeFields = new List<string>();

            PropertyInfo[] fis = settings.FieldValue.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            rowbuilder.Append("<tr>");
            string bodyCss = String.IsNullOrEmpty(settings.BodyCss) ? "formBody" : settings.BodyCss;
            string bodyStyle = String.IsNullOrEmpty(settings.BodyStyle) ? "text-align: left;" : settings.BodyStyle;
            foreach (PropertyInfo fi in fis)
            {
                if ((includeFields.Count > 0 && includeFields.Contains(fi.Name)) || includeFields.Count <= 0)
                {
                    if (fi.GetValue(settings.FieldValue) != null)
                        rowbuilder.Append("<td class='" + bodyCss + "' style='" + bodyStyle + "'>" + fi.GetValue(settings.FieldValue).ToString() + "</td>");
                    else
                        rowbuilder.Append("<td class='" + bodyCss + "' style='" + bodyStyle + "'>" + " " + "</td>");
                }
                else if (settings.Fields.Where(c => c.FieldName == fi.Name).Count() > 0)
                {
                    FieldSettings setting = settings.Fields.Where(c => c.FieldName == fi.Name).FirstOrDefault();
                    string subBodyCss = String.IsNullOrEmpty(setting.BodyCss) ? "formBody" : settings.BodyCss;
                    string subBodyStyle = String.IsNullOrEmpty(setting.BodyStyle) ? "text-align: left;" : setting.BodyStyle;
                    rowbuilder.Append("<td class='" + subBodyCss + "' style='" + subBodyStyle + "'>" + fi.GetValue(settings.FieldValue).ToString() + "</td>");
                }

            }
            rowbuilder.Append("</tr>");

            return rowbuilder.ToString();

        }

        private string ReturnObjectRow(FieldSettings settings, bool vertical)
        {
            StringBuilder rowbuilder = new StringBuilder();

            PropertyInfo[] fis = settings.FieldValue.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            string labelCss = String.IsNullOrEmpty(settings.LabelCss) ? "formLabel" : settings.LabelCss;
            string bodyCss = String.IsNullOrEmpty(settings.BodyCss) ? "formBody" : settings.BodyCss;
            string rowStart = String.IsNullOrEmpty(settings.LabelStyle) ? "<tr><td class='" + labelCss + "'>" : "<tr><td class='" + labelCss + "' style='" + settings.LabelStyle + "'>";
            string rowMid = String.IsNullOrEmpty(settings.BodyStyle) ? "</td><td class='" + bodyCss + "'>" : "</td><td class='" + bodyCss + "' style='" + settings.BodyStyle + "'>";

            if (vertical)
            {
                rowbuilder.Append(rowStart + settings.DisplayName + ": </td><td><table style='" + settings.TableStyle + "'>");
                foreach (PropertyInfo fi in fis)
                {
                    if (settings.Fields.Where(c => c.FieldName == fi.Name).Count() > 0)
                    {
                        if (fi.GetValue(settings.FieldValue) != null)
                        {
                            FieldSettings setting = settings.Fields.Where(c => c.FieldName == fi.Name).FirstOrDefault();
                            string fieldCss = String.IsNullOrEmpty(setting.BodyCss) ? "formBody" : setting.BodyCss;
                            String rowFieldMod = String.IsNullOrEmpty(setting.BodyStyle) ? "<tr><td class='" + fieldCss + "'>" : "<tr><td class='" + fieldCss + "' style='" + setting.BodyStyle + "'>";
                            rowbuilder.Append(rowFieldMod + setting.FieldValue.ToString() + "</td></tr>");
                        }
                    }

                }
                rowbuilder.Append("</table></td></tr>");
            }
            else
            {
                rowbuilder.Append(rowStart + settings.DisplayName + ": </td><td><table style='" + settings.TableStyle + "'><tr><td class='" + bodyCss + "'>");
                foreach (PropertyInfo fi in fis)
                {
                    if (settings.Fields.Where(c => c.FieldName == fi.Name).Count() > 0)
                    {
                        if (fi.GetValue(settings.FieldValue) != null)
                        {
                            FieldSettings setting = settings.Fields.Where(c => c.FieldName == fi.Name).FirstOrDefault();
                            string fieldCss = String.IsNullOrEmpty(setting.BodyCss) ? "formBody" : setting.BodyCss;
                            String rowFieldMod = String.IsNullOrEmpty(setting.BodyStyle) ? "</td><td class='" + fieldCss + "'>" : "<tr><td class='" + fieldCss + "' style='" + setting.BodyStyle + "'>";
                            rowbuilder.Append(rowFieldMod + setting.FieldValue.ToString());
                        }
                    }

                }
                rowbuilder.Append("</td></tr></table></td></tr>");
            }

            return rowbuilder.ToString();
        }

    }

}
