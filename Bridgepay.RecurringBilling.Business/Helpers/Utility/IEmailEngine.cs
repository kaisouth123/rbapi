﻿using Bridgepay.RecurringBilling.Business.Models;
using System;
namespace Bridgepay.RecurringBilling.Business.Helpers.Utility
{
    public interface IEmailEngine
    {
        bool SendEmail(Email email);
        bool SendEmail(string senderName, string replyTo, string recipients, string subject, string body, System.Collections.Generic.List<Bridgepay.RecurringBilling.Business.Models.ReceiptStream> attachments);
        bool SendEmail(string senderName, string replyTo, string sendTo, string ccTo, string bccTo, string subject, string body, System.Collections.Generic.List<Bridgepay.RecurringBilling.Business.Models.ReceiptStream> attachments, bool isBodyHTML = true);
    }
}
