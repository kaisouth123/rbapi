﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public static class UtilityHelper
    {
        public static void MaskValue(this XElement element, string elementName, Func<string, string> maskingFunction)
        {
            if (element == null)
            {
                return;
            }

            var elementToMask = element.Element(elementName);

            if (elementToMask != null)
            {
                elementToMask.Value = maskingFunction(elementToMask.Value);
            }

        }

        public static string GetTrack1(string magData)
        {
            if (!string.IsNullOrEmpty(magData))
            {
                string[] tokens = magData.Split(';');
                return tokens[0];
            }
            return null;
        }

        public static string GetTrack2(string magData)
        {
            if (!string.IsNullOrEmpty(magData))
            {
                string[] tokens = magData.Split(';');
                if (tokens.Length > 1)
                {
                    return ";" + tokens[1];
                }
            }
            return null;
        }

        public static string MaskCardNumber(string cardNum, char maskChar = PaymentConstants.Security.DefaultMaskCharacter, int maxLength = 0)
        {
            string rv;

            if (string.IsNullOrEmpty(cardNum) || cardNum.Length <= PaymentConstants.Security.UnmaskedCardLength)
            {
                rv = cardNum;
            }
            else
            {
                rv = new string(maskChar, cardNum.Length - PaymentConstants.Security.UnmaskedCardLength) + cardNum.Substring(cardNum.Length - PaymentConstants.Security.UnmaskedCardLength);
            }

            if (rv != null && (maxLength != 0 && rv.Length > maxLength))
                rv = rv.Remove(0, rv.Length - maxLength);

            return rv;
        }

        public static string MaskCardNumber(string cardNum, int maxLength)
        {
            return MaskCardNumber(cardNum, PaymentConstants.Security.DefaultMaskCharacter, maxLength);
        }

        public static string MaskCardNumber(string cardNum)
        {
            return MaskCardNumber(cardNum, 0);
        }

        public static string MaskACHAccountNumber(string accountNum, char maskChar = PaymentConstants.Security.DefaultMaskCharacter, int maxLength = 0)
        {
            return MaskCardNumber(accountNum, maskChar, maxLength);
        }

        public static string MaskACHAccountNumber(string accountNum, int maxLength)
        {
            return MaskCardNumber(accountNum, PaymentConstants.Security.DefaultMaskCharacter, maxLength);
        }

        public static string MaskACHAccountNumber(string accountNum)
        {
            return MaskCardNumber(accountNum);
        }

        public static string GetNumberName(this int number)
        {
            if (number == 0)
                return number.ToString() + "th";
            else if (number == 1)
                return number.ToString() + "st";
            else if (number == 2)
                return number.ToString() + "nd";
            else if (number == 3)
                return number.ToString() + "rd";
            else if (number > 3)
                return number.ToString() + "th";
            else
                return number.ToString() + "th";

        }

        public static IEnumerable<T> ShortCircuitCollection<T>(this IEnumerable<T> source) where T : class
        {
            foreach (T entity in source)
            {
                entity.ShortCircuitReference();
            }
            return source;
        }

        public static IQueryable<T> ShortCircuitCollection<T>(this IQueryable<T> source, bool isActive) where T : class
        {
            foreach (T entity in source)
            {
                entity.ShortCircuitReference();
            }
            return source;
        }

        public static T ShortCircuit<T, Y>(this T original, Y parent) where T: class where Y : class
        {
            FieldInfo[] properties = original.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (FieldInfo property in properties)
            {
                try
                {
                    object objectValue = property.GetValue(original);
                    if (typeof(BaseEntity).IsAssignableFrom(property.FieldType) && objectValue != null)
                    {
                        if (property.FieldType.FullName == parent.GetType().FullName)
                        {
                            object fieldValue = null;
                            property.SetValue(original, fieldValue);
                        }
                        else
                        {
                            objectValue.ShortCircuit(original);
                        }
                    }

                    if (objectValue is ICollection && objectValue != null)
                    {
                        ICollection collection = objectValue as ICollection;
                        foreach (object someEntity in collection)
                        {
                            if (someEntity.GetType().FullName == parent.GetType().FullName)
                            {
                                property.SetValue(original, null);
                                break;
                            }
                            else
                                someEntity.ShortCircuit(original);
                        }
                    }
                }
                catch (Exception ex)
                {
   
                }
            }
            return original;
        }

        public static T ShortCircuitReference<T>(this T original) where T : class
        {

            FieldInfo[] properties = original.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (FieldInfo property in properties)
            {
                object objectValue = property.GetValue(original);
                if (typeof(IBaseEntity).IsAssignableFrom(property.FieldType) && objectValue != null)
                {
                        objectValue.ShortCircuit(original);
                }

                if (objectValue is ICollection && objectValue != null)
                {
                    ICollection collection = objectValue as ICollection;
                    foreach (object someEntity in collection)
                    {
                        someEntity.ShortCircuit(original);
                    }
                }
            }
            return original;
        }
    }
}
