﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime ReturnNextDay(this DateTime date, int next)
        {
            DateTime nextDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0, DateTimeKind.Local);

            int now = (int)nextDate.DayOfWeek;

            if (next > now)
                nextDate = nextDate.AddDays(next - now);
            else if (next < now)
                nextDate = nextDate.AddDays(7 - now + next);

            return nextDate;
        }

        public static DateTime ReturnAnotherDayOfWeek(this DateTime date, int next)
        {
            DateTime nextDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0, DateTimeKind.Local);

            int now = (int)nextDate.DayOfWeek;
            nextDate = nextDate.AddDays(next - now);

            return nextDate;
        }

        public static int GetWeekNumber(this DateTime date)
        {
            return GetWeekNumber(date, CultureInfo.CurrentCulture);
        }

        public static int GetWeekNumber(this DateTime date, CultureInfo culture)
        {
            return culture.Calendar.GetWeekOfYear(date,
                culture.DateTimeFormat.CalendarWeekRule,
                culture.DateTimeFormat.FirstDayOfWeek);
        }

        public static DateTime ReturnNextMonth(this DateTime date, int next)
        {
            DateTime nextDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0, DateTimeKind.Local);

            int now = (int)nextDate.Month;

            if (next > now)
                nextDate = nextDate.AddMonths(next - now);
            else if (next < now)
                nextDate = nextDate.AddDays(12 - now + next);

            return nextDate;
        }

        public static DateTime ReturnNextMonthOfWeek(this DateTime date, int next, int day=-1)
        {
            DateTime nextDate = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0, DateTimeKind.Local);

            if(day < 0)
                day = (int)nextDate.DayOfWeek;

            int now = (int)nextDate.Month;

            if (next > now)
                nextDate = nextDate.AddMonths(next - now);
            else if (next < now)
                nextDate = nextDate.AddDays(12 - now + next);

            int month = nextDate.Month;
            int week = nextDate.GetWeekNumber();

            return nextDate;
        }

        public static int GetWeekNumberOfMonth(this DateTime date)
        {
            return GetWeekNumberOfMonth(date, CultureInfo.CurrentCulture);
        }

        public static int GetWeekNumberOfMonth(this DateTime date, CultureInfo culture)
        {
            return date.GetWeekNumber(culture)
                 - new DateTime(date.Year, date.Month, 1).GetWeekNumber(culture)
                 + 1; 
        }

        public static DateTime GetNthDayOfMonth(this DateTime date, int nth, int day=-1)
        {
            DateTime firstday = new DateTime(date.Year, date.Month, 1);

            int startDay = (int)firstday.DayOfWeek;
            if(day < 0)
                day = (int)date.DayOfWeek;

            if (day < startDay)
                return firstday.AddDays(7 - startDay + day + (7 * (nth - 1)));
            else
                return firstday.AddDays(day - startDay + (7 * (nth - 1)));

        }

        public static Boolean IsLastDay(this DateTime date)
        {
            return date.Day == DateTime.DaysInMonth(date.Year, date.Month);
        }

        public static int GetStartingIndex(this int lastDay, List<int> days)
        {
            //If the lastDay was Sunday, just get the first day again
            if (lastDay == 6)
                return 0;

            //Get the index of the last day in the days list
            int index = days.IndexOf(lastDay);

            //if the index is not present get the first day that is greater then the last day
            //useful if the startday is not equal to one of the days
            if (index <= 0)
            {
                foreach (int day in days)
                {
                    if (day > lastDay)
                    {
                        index = days.IndexOf(day);
                        break;
                    }

                    index = 0;
                }
            }
            else
            {
                index = index + 1 >= days.Count ? 0 : index + 1;
            }

            return index;
        }

        public static DateTime GetMaxDayForNthWeek(this DateTime lastDay, List<int> days, int nth)
        {
            List<DateTime> Dates = new List<DateTime>();

            foreach(int day in days)
            {
                Dates.Add(lastDay.GetNthDayOfMonth(nth, day));
            }


            return Dates.Max();
        }

        public static int GetDayOrLast(this int day, DateTime lastPayment)
        {
            int numberOfDays = DateTime.DaysInMonth(lastPayment.Year, lastPayment.Month);
            if (day > numberOfDays)
                return numberOfDays;
            else
                return day;
        }
    }
}
