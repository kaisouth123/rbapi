﻿using Bridgepay.Core.Framework;
using Bridgepay.Core.Security.Factories;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.Business
{
    public class Authenticator : IAuthenticator
    {
        public bool Authenticate(Credential credentials)
        {
            Core.Framework.Credentials creds = new Credentials() {UserName = credentials.UserName, Password = credentials.Password};
            Core.Framework.Interfaces.IAuthenticator authenticator = AuthenticatorFactory.CreateAuthenticator(creds);
            return authenticator?.Authenticate(creds) != null;
        }
    }
}
