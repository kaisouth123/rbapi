﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business
{
    public static class ObjectHelper
    {
        public static void SetValueFromString(this object target, string propertyName, string propertyValue)
        {
            PropertyInfo oProperty = target.GetType().GetProperty(propertyName);
            if (oProperty == null) return;

            Type tProp = oProperty.PropertyType;

            if (tProp.IsGenericType && tProp.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (propertyValue == null)
                {
                    oProperty.SetValue(target, null, null);
                    return;
                }

                tProp = new NullableConverter(oProperty.PropertyType).UnderlyingType;
            }

            if (oProperty.PropertyType.IsValueType && propertyValue == null)
            {
                oProperty.SetValue(target, null);
            }
            else
            {
                oProperty.SetValue(target, Convert.ChangeType(propertyValue, tProp), null);
            }
        }
    }
}
