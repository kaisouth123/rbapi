﻿using Bridgepay.Core.Cryptography;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.ServiceAdaptors;
using Bridgepay.RecurringBilling.Business.WalletService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers.Utility
{
    public static class ServiceUtility
    {
        public static MultiUseTokenResponse GetTokenRequest(Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod method, bool card)
        {
            MultiUseTokenRequest tokenRequest = new MultiUseTokenRequest();
            SimpleAES crypto = new SimpleAES();
            string user = crypto.DecryptString(ConfigurationManager.AppSettings["MBPUserName"]);
            string password = crypto.DecryptString(ConfigurationManager.AppSettings["MBPPassword"]);

            if (card)
            {
                tokenRequest = new MultiUseTokenRequest
                {
                    PaymentAccountNumber = method.Token,
                    ExpirationDate = method.ExpirationDate,
                    ClientIdentifier = PaymentConstants.ClientIdentifier.VT,
                    TransactionID = Guid.NewGuid().ToString(),
                    RequestDateTime = DateTime.UtcNow.ToShortDateString(),
                    User = user,
                    Password = password,
                };
            }
            else
            {
                tokenRequest = new MultiUseTokenRequest
                {
                    BankAccountNum = method.Token,
                    ClientIdentifier = PaymentConstants.ClientIdentifier.VT,
                    TransactionID = Guid.NewGuid().ToString(),
                    RequestDateTime = DateTime.UtcNow.ToShortDateString(),
                    User = user,
                    Password = password,
                };
            }

            BridgeCommProcessingAdaptor adaptor = new BridgeCommProcessingAdaptor();
            return adaptor.ProcessTransaction(tokenRequest) as MultiUseTokenResponse;
        }
    }
}
