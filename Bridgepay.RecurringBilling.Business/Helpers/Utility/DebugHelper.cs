﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business
{
    public static class DebugHelper
    {
        public static bool IsDebugMode()
        {
            bool isDebug = false;

            ConfigurationSection section;
            section = ConfigurationManager.GetSection("system.web/compilation") as ConfigurationSection;
            if (null != section)
            {
                return true;
            }

            return isDebug;
        }
    }
}
