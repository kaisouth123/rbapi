﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business.Helpers;

namespace Bridgepay.RecurringBilling.Business
{
    public enum IntervalType
    {
        Daily,
        DailyByDays,
        Weekly,
        Monthly,
        MonthlyByDaysAndWeek,
        Yearly,
        YearlyByDaysMonthAndWeek
    }

    public static class IntervalHelper
    {
        public static IntervalType ReturnIntervalType(Contract contract, Credential credentials)
        {
            IntervalType type = IntervalType.Daily;

            ContractAccessor accessor = new ContractAccessor(credentials);
            BillingFrequency frequency = accessor.BillingFrequencyRepository.AllIncluding(b => b.Frequency).ToList().Where(f => f.Id == contract.BillingFrequencyId).ToList().FirstOrDefault();
            String intervalName = frequency.Interval.Name;

            if (intervalName == PaymentIntervalConstants.Daily)
            {
                return IntervalType.Daily;
            }
            else if (intervalName == PaymentIntervalConstants.DailyByDays)
            {
                return IntervalType.DailyByDays;
            }
            else if (intervalName == PaymentIntervalConstants.Monthly)
            {
                return IntervalType.Monthly;
            }
            else if (intervalName == PaymentIntervalConstants.MonthlyByDaysAndWeek)
            {
                return IntervalType.MonthlyByDaysAndWeek;
            }
            else if (intervalName == PaymentIntervalConstants.Weekly)
            {
                return IntervalType.Weekly;
            }
            else if (intervalName == PaymentIntervalConstants.Yearly)
            {
                return IntervalType.Yearly;
            }
            else if (intervalName == PaymentIntervalConstants.YearlyByDaysMonthAndWeek)
            {
                return IntervalType.YearlyByDaysMonthAndWeek;
            }

            return type;
        }
    }
    
}
