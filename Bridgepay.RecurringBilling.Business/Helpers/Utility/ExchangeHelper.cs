﻿using Bridgepay.Core.Cryptography;
using Bridgepay.RecurringBilling.Business.Helpers.Utility;
using Bridgepay.RecurringBilling.Business.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class ExchangeHelper : IEmailEngine 
    {
        public bool SendEmail(string senderName, string replyTo, string recipients, string subject, string body, List<ReceiptStream> attachments)
        {
            return SendEmail(senderName, replyTo, recipients, string.Empty, string.Empty, subject, body, attachments);
        }

        public bool SendEmail(Email email)
        {
            return SendEmail(email.SenderName, email.ReplyTo, email.To, email.CC, email.BCC, email.Subject, email.Body, null, email.IsHTML);
        }

        public bool SendEmail(string senderName, string replyTo, string sendTo, string ccTo, string bccTo, string subject, string body, List<ReceiptStream> attachments, bool isBodyHTML = true)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();
            StringBuilder sb = new StringBuilder();
            SimpleAES crypto = new SimpleAES();

            string senderEmail = crypto.DecryptString(ConfigurationManager.AppSettings["senderEmail"]);
            string exchangeServer = crypto.DecryptString(ConfigurationManager.AppSettings["exchangeServer"]);
            int exchangePort = Convert.ToInt32(crypto.DecryptString(ConfigurationManager.AppSettings["exchangePort"]));

            try
            {
                //you can provide invalid from address. but to address Should be valil
                MailAddress fromAddress = new MailAddress(replyTo, senderName);

                smtpClient.Host = exchangeServer;
                smtpClient.Port = exchangePort;

                string replyToString = (string.IsNullOrWhiteSpace(replyTo) ? string.Empty : "<br/><br/>" + string.Format("Do not reply to this e-mail.  Send all inquiries about this transaction to {0}", replyTo));

                smtpClient.UseDefaultCredentials = true;
                message.From = fromAddress;
                if ((sendTo != null) && (sendTo.Trim() != ""))
                    message.To.Add(sendTo);
                if ((ccTo != null) && (ccTo.Trim() != ""))
                    message.CC.Add(ccTo);
                if ((bccTo != null) && (bccTo.Trim() != ""))
                    message.Bcc.Add(bccTo);
                message.Subject = subject;
                message.Body = sb.AppendFormat("{0}{1}", body, replyToString).ToString();
                message.IsBodyHtml = isBodyHTML;

                if (attachments != null)
                {
                    foreach (var attachment in attachments)
                    {
                        Attachment _attachment = new Attachment(attachment.Stream, new System.Net.Mime.ContentType("application/pdf"));
                        _attachment.ContentDisposition.FileName = string.Format("TransactionReceipt{0}.pdf", attachment.TransactionID);
                        message.Attachments.Add(_attachment);
                    }
                }

                //smtpClient.EnableSsl = true; 

                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

                smtpClient.Send(message);
                smtpClient.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                //Add Logging Here
                return false;
            }

        }
    }
}
