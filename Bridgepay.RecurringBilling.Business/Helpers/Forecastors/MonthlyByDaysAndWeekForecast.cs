﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class MonthlyByDayAndWeekForecaster : IPaymentForecastor
    {
        public int ContractID { get; set; }
        private Credential Credentials { get; set; }
        public PaymentInfo PaymentsInfo { get; set; }

        public MonthlyByDayAndWeekForecaster(Credential credentials, int contractID)
        {
            ContractID = contractID;
            Credentials = credentials;
        }

        public List<ScheduledPayment> ForecastPayments(int number, IContractAccessor accessor = null)
        {
            PaymentInfo paymentInfo = accessor == null ? new PaymentInfo(Credentials, ContractID) : new PaymentInfo(Credentials, ContractID, accessor);
            List<ScheduledPayment> payments = new List<ScheduledPayment>();
            List<int> days = paymentInfo.ReturnDayNumbers();
            DateTime start = paymentInfo.LastBillDate;
            DateTime startDate = paymentInfo.StartDate != null ? (DateTime)paymentInfo.StartDate : start;
            int Frequency = (int)paymentInfo.Frequency;
            int BillAmount = paymentInfo.BillAmount;
            int Week = 1;
            int take = number;
            if (days.Count > number)
            {
                number = days.Count;
            }

            if (paymentInfo.WeekMonth != null)
                Week = (int)paymentInfo.WeekMonth;
            else
                Week = start.GetWeekNumberOfMonth();


            //Get the starting index
            int i = ((int)start.DayOfWeek).GetStartingIndex(days);
            int max = days.Max();
            
            DateTime add = start;
            int month = 0;  //increment for the month
            for (int x = 0; x < number; x++)
            {                
                int day = days[i];
                
                bool moveToNextMonth = (int)add.DayOfWeek == 7;

                add = add.AddMonths(Frequency * month).GetNthDayOfMonth(Week, day).Date.ToUniversalTime();

                if (month > 0) //Reset the month
                    month = 0;
                //DateTime maxDay = add.GetMaxDayForNthWeek(days, Week);
                //bool maxResult = add.CompareTo(maxDay) >= 0;
                bool maxResult = i + 1 >= days.Count;
                
                if (add.Date.CompareTo(start.Date) >= 0 && add.Date.CompareTo(DateTime.UtcNow) > 0) 
                {
                    payments.Add(new ScheduledPayment { BillAmount = BillAmount, BillDate = add, ContractId = ContractID });

                    if (moveToNextMonth || maxResult)
                    {
                        month++;
                        i = 0;
                    }
                    else
                    {
                        i = ((int)add.DayOfWeek).GetStartingIndex(days);
                    }
                }
                else
                {
                    x--;
                    month++;
                    i = 0;
                }
            }

            payments.Sort((a, b) => a.BillDate.CompareTo(b.BillDate));
            return payments.Take(take).ToList();
        }

    }
}
