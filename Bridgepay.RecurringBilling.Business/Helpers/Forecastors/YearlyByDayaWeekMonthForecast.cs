﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class YearlyByDayaWeekMonthForecaster : IPaymentForecastor
    {

        public int ContractID { get; set; }
        private Credential Credentials { get; set; }
        public PaymentInfo PaymentsInfo { get; set; }


        public YearlyByDayaWeekMonthForecaster(Credential credentials, int contractID)
        {
            ContractID = contractID;
            Credentials = credentials;
        }

        public List<ScheduledPayment> ForecastPayments(int number, IContractAccessor accessor = null)
        {
            PaymentInfo paymentInfo = accessor == null ? new PaymentInfo(Credentials, ContractID) : new PaymentInfo(Credentials, ContractID, accessor);

            List<ScheduledPayment> payments = new List<ScheduledPayment>();
            List<int> days = paymentInfo.ReturnDayNumbers();
            DateTime start = paymentInfo.LastBillDate;
            DateTime startDate = paymentInfo.StartDate != null ? (DateTime)paymentInfo.StartDate : start;
            int Frequency = (int)paymentInfo.Frequency;
            int BillAmount = paymentInfo.BillAmount;
            int Week = 1;
            int Month = 0;
            int take = number;
            if (days.Count > number)
            {
                number = days.Count;
            }

            if (paymentInfo.Month != null)
                Month = (int)paymentInfo.Month;
            else
                Month = start.Month;

            if (paymentInfo.WeekMonth != null)
                Week = (int)paymentInfo.WeekMonth;
            else
                Week = start.GetWeekNumberOfMonth();

            int i = days.IndexOf((int)start.DayOfWeek);
            DateTime fixDate = new DateTime(start.Year, Month, 1);
            DateTime add = new DateTime(start.Year, Month, fixDate.GetNthDayOfMonth(Week, i).Day).Date.ToUniversalTime();
            for (int x = 0; x <= number; x++)
            {
                if (DateTime.UtcNow.CompareTo(add) > 0)
                    i++;


                if (i >= days.Count || i < 0)
                    i = 0;
                int day = days[i];

                //check to see if the start is greater then the day and its new
                if ((int)add.DayOfWeek > day)
                {
                    add = add.AddYears(Frequency * x).GetNthDayOfMonth(Week, day).Date.ToUniversalTime();
                }
                else if ((int)add.DayOfWeek < day)
                    add = add.GetNthDayOfMonth(Week, day).Date.ToUniversalTime();
                else if ((int)add.DayOfWeek == day && DateTime.UtcNow.CompareTo(add) < 0)
                    add = add.AddYears(Frequency * x).GetNthDayOfMonth(Week, day).Date.ToUniversalTime();
                else if ((int)add.DayOfWeek == day && DateTime.UtcNow.CompareTo(add) < 0 && payments.Count == 0)
                    add = add.GetNthDayOfMonth(Week, day).Date.ToUniversalTime();
                else
                    add = add.AddYears(Frequency * x).GetNthDayOfMonth(Week, day).Date.ToUniversalTime();

                if (add.Date.CompareTo(startDate.Date) >= 0)
                {
                    payments.Add(new ScheduledPayment { BillAmount = BillAmount, BillDate = add, ContractId = ContractID });
                }
                else
                {
                    x--;
                    continue;
                }
            }

            payments.Sort((a, b) => a.BillDate.CompareTo(b.BillDate));
            return payments.Take(take).ToList();
        }

    }
}
