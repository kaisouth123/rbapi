﻿using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class MonthlyForecaster : IPaymentForecastor
    {
        public int ContractID { get; set; }
        private Credential Credentials { get; set; }
        public PaymentInfo PaymentsInfo { get; set; }

        public MonthlyForecaster(Credential credentials, int contractID)
        {
            ContractID = contractID;
            Credentials = credentials;
        }

        public List<ScheduledPayment> ForecastPayments(int number, IContractAccessor accessor = null)
        {
            PaymentInfo paymentInfo = accessor == null ? new PaymentInfo(Credentials, ContractID) : new PaymentInfo(Credentials, ContractID, accessor);
            List<ScheduledPayment> payments = new List<ScheduledPayment>();
            DateTime lastPayment = (DateTime)paymentInfo.LastBillDate;
            paymentInfo.DayMonth = paymentInfo.DayMonth == null ? paymentInfo.LastBillDate.Day : (int)paymentInfo.DayMonth;
 
            int Frequency = (int)paymentInfo.Frequency;

            for (int x = 0; x < number; x++)
            {
                DateTime nextDate = lastPayment.AddMonths(x * Frequency);
                int day = ((int)paymentInfo.DayMonth).GetDayOrLast(nextDate);
                nextDate = new DateTime(nextDate.Year, nextDate.Month, day);
                nextDate.ToUniversalTime();

                bool NewDateIsGreaterThanLastBillDate = nextDate.Date.CompareTo(paymentInfo.LastBillDate.Date) > 0;
                bool NewDateIsGreaterThanNow = nextDate.Date.CompareTo(DateTime.UtcNow.Date) >= 0;
                bool NewDateIsGreaterOrEqualStartDate = nextDate.Date.CompareTo(paymentInfo.StartDate) >= 0;
                if (NewDateIsGreaterThanLastBillDate && NewDateIsGreaterThanNow && NewDateIsGreaterOrEqualStartDate) 
                { 
                    payments.Add(new ScheduledPayment { BillAmount = paymentInfo.BillAmount, ContractId = ContractID, BillDate = nextDate });
                }
                    
                else
                {
                    number = number + 1;
                    continue;
                }
            }

            return payments;
        }

        

    }
}
