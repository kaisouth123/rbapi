﻿using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class DailyForecaster : IPaymentForecastor
    {
        public int ContractID { get; set; }
        private Credential Credentials { get; set; }
        public PaymentInfo PaymentsInfo { get; set; }

        public DailyForecaster(Credential credentials, int contractID)
        {
            ContractID = contractID;
            Credentials = credentials;
        }

        public List<ScheduledPayment> ForecastPayments(int number, IContractAccessor accessor = null)
        {
            PaymentInfo paymentInfo = accessor == null ? new PaymentInfo(Credentials, ContractID) : new PaymentInfo(Credentials, ContractID, accessor);
            DateTime start = (DateTime)paymentInfo.StartDate;
            paymentInfo.LastBillDate = paymentInfo.LastBillDate;
            List<ScheduledPayment> payments = new List<ScheduledPayment>();

            int Frequency = (int)paymentInfo.Frequency;
            

            for (int x = 1; x <= number; x++)
            {
                 payments.Add(new ScheduledPayment { BillAmount=paymentInfo.BillAmount, ContractId=ContractID, BillDate=paymentInfo.LastBillDate.AddDays(x * Frequency).Date.ToUniversalTime()});
            }

            return payments;
        }
    }
}
