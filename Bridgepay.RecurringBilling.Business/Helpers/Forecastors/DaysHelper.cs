﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public static class DaysHelper
    {
        public static List<String> ReturnDays(BillingFrequency frequency)
        {
            List<String> days = new List<string>();

            String daysString = frequency.Days;

            Char[] dayMasks = daysString.ToCharArray();

            if (dayMasks[0].ToString() == "1")
                days.Add("Sunday");
            if (dayMasks[1].ToString() == "1")
                days.Add("Monday");
            if (dayMasks[2].ToString() == "1")
                days.Add("Tuesday");
            if (dayMasks[3].ToString() == "1")
                days.Add("Wednesday");
            if (dayMasks[4].ToString() == "1")
                days.Add("Thursday");
            if (dayMasks[5].ToString() == "1")
                days.Add("Friday");
            if (dayMasks[6].ToString() == "1")
                days.Add("Saturday");

            return days;
        }
    }
}
