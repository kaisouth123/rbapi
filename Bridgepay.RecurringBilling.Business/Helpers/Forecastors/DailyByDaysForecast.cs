﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business;
using Bridgepay.RecurringBilling.DataLayer.Accessors;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class DailyByDaysForecaster : IPaymentForecastor
    {
        public int ContractID { get; set; }
        private Credential Credentials { get; set; }
        public PaymentInfo PaymentsInfo { get; set; }

        public DailyByDaysForecaster(Credential credentials, int contractID)
        {
            ContractID = contractID;
            Credentials = credentials;
        }

        public List<ScheduledPayment> ForecastPayments(int number, IContractAccessor accessor = null)
        {
            PaymentInfo paymentInfo = accessor == null ? new PaymentInfo(Credentials, ContractID) : new PaymentInfo(Credentials, ContractID, accessor);

            List<ScheduledPayment> payments = new List<ScheduledPayment>();
            List<int> days = paymentInfo.ReturnDayNumbers();
            DateTime lastPayment = paymentInfo.LastBillDate;
            DateTime startDate = paymentInfo.StartDate != null ? (DateTime)paymentInfo.StartDate : lastPayment;
            int Frequency = (int)paymentInfo.Frequency;
            int BillAmount = paymentInfo.BillAmount;

            //Get either the number or the days
            int take = number;
            if (days.Count > number)
            {
                number = days.Count;
            }

            int index = ((int)lastPayment.DayOfWeek).GetStartingIndex(days);
            int interval = 0;
            int day = days[index];

            while (payments.Count < number)
            {
                //try and get the first index
                day = days[index];
                DateTime addDate = lastPayment.ReturnAnotherDayOfWeek(day).AddDays((Frequency * interval) * 7).Date;
                addDate = addDate.ToUniversalTime();

                //Check whether that  date is in the future and past the startdate, if so add it and increase the index
                if (addDate >= startDate && addDate > DateTime.UtcNow)
                {
                    payments.Add(new ScheduledPayment { BillAmount = BillAmount, BillDate = addDate, ContractId = ContractID });
                }

                if (index >= days.Count - 1)
                {
                    index = 0;
                    interval++;
                }
                else
                {
                    index++;
                }
            }

            payments.Sort((a, b) => a.BillDate.CompareTo(b.BillDate));
            return payments.Take(take).ToList();
        }
    }
}
