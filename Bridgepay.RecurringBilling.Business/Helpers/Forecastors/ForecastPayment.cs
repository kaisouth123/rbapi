﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class ForecastPayment
    {
        public int NumberOfPayments { get; set; }
        public int ContractId { get; set; }
        public IPaymentForecastor Forecastor { get; set; }
        private IContractAccessor _accessor;

        public ForecastPayment(Credential credentials, int numberOfPayments, int contractId, string intervalName = "", IContractAccessor accessor = null)
        {
            NumberOfPayments = numberOfPayments;
            ContractId = contractId;
            _accessor = accessor;
            
            if (intervalName == "")
            {
                accessor = _accessor == null ? new ContractAccessor(credentials) : _accessor;
                Contract contract = accessor.ContractRepository.Find(contractId);
                BillingFrequency frequency = accessor.BillingFrequencyRepository.GetAll().Where( b => b.Id == contract.BillingFrequencyId).ToList().FirstOrDefault();
                frequency.Interval = accessor.IntervalRepository.Find(frequency.IntervalId);
                intervalName = frequency.Interval.Name;
            }

            Forecastor = ForecastorFactory.ReturnForecaster(credentials, contractId, intervalName);
        }

        public List<ScheduledPayment> ReturnPayments()
        {
            return Forecastor.ForecastPayments(NumberOfPayments, _accessor);
        }
    }
}
