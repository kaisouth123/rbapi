﻿using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class YearlytForecaster : IPaymentForecastor
    {
        public int ContractID { get; set; }
        private Credential Credentials { get; set; }
        public PaymentInfo PaymentsInfo { get; set; }

        public YearlytForecaster(Credential credentials, int contractID)
        {
            ContractID = contractID;
            Credentials = credentials;
        }

        public List<ScheduledPayment> ForecastPayments(int number, IContractAccessor accessor = null)
        {
            PaymentInfo paymentInfo = accessor == null ? new PaymentInfo(Credentials, ContractID) : new PaymentInfo(Credentials, ContractID, accessor);
            List<ScheduledPayment> payments = new List<ScheduledPayment>();
            int Frequency = (int)paymentInfo.Frequency;

            DateTime lastPayment = paymentInfo.LastBillDate;
            if (paymentInfo.DayMonth != null && paymentInfo.Month != null)
            {
                int numberOfDays = DateTime.DaysInMonth(lastPayment.Year, (int)paymentInfo.Month);
                if ((int)paymentInfo.DayMonth > numberOfDays)
                    lastPayment = new DateTime(lastPayment.Year, (int)paymentInfo.Month, numberOfDays).Date.ToUniversalTime();
                else
                    lastPayment = new DateTime(lastPayment.Year, (int)paymentInfo.Month, (int)paymentInfo.DayMonth).Date.ToUniversalTime();
            }

            for (int x = 0; x < number; x++)
            {
                if (lastPayment.AddYears(x * Frequency).Date.CompareTo(paymentInfo.LastBillDate.Date) >= 0)
                    payments.Add(new ScheduledPayment { BillAmount = paymentInfo.BillAmount, ContractId = ContractID, BillDate = lastPayment.AddYears(x * Frequency) });
                else
                {
                    number = number + 1;
                    continue;
                }
            }

            return payments;
        }
    }

}
