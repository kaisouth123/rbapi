﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Interfaces;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class DailyDescriptor : IFrequencyDescriptor
    {
        public BillingFrequency Frequency { get; set; }
        private Credential _credentials;


        public DailyDescriptor(Credential credentials, BillingFrequency frequency)
        {
            Frequency = frequency;
            _credentials = credentials;
        }

        public string GetFrequencyDescriptor(IBillingFrequencyAccessor accessor = null)
        {
            accessor = accessor == null ? new BillingFrequencyAccessor(_credentials) : accessor;
            try
            {
                if (Frequency.Frequency > 1)
                    return "Every " + Frequency.Frequency + " days";
                else
                    return "Every day";
            }
            catch
            {
                return "Issue with Billing Frequency";
            }
        }

        
    }
}
