﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Interfaces;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class YearlyDescriptor : IFrequencyDescriptor
    {
        public BillingFrequency Frequency { get; set; }
        private Credential _credentials;


        public YearlyDescriptor(Credential credentials, BillingFrequency frequency)
        {
            Frequency = frequency;
            _credentials = credentials;
        }

        public string GetFrequencyDescriptor(IBillingFrequencyAccessor accessor = null)
        {
            accessor = accessor == null ? new BillingFrequencyAccessor(_credentials) : accessor;

            try
            {

                if (Frequency.Frequency > 1)
                    return "Every " + Frequency.Frequency + " years, on the " + UtilityHelper.GetNumberName((int)Frequency.DayMonth) + " day of the " + UtilityHelper.GetNumberName((int)Frequency.Month) + " month";
                else
                    return "Every year, on the " + UtilityHelper.GetNumberName((int)Frequency.DayMonth) + " day of the " + UtilityHelper.GetNumberName((int)Frequency.Month) + " month";
            }
            catch
            {
                return "Issue with Billing Frequency";
            }

        }

        
    }
}
