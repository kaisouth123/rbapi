﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Interfaces;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class YearlyByDayaWeekMonthDescriptor : IFrequencyDescriptor
    {
        public BillingFrequency Frequency { get; set; }
        private Credential _credentials;


        public YearlyByDayaWeekMonthDescriptor(Credential credentials, BillingFrequency frequency)
        {
            Frequency = frequency;
            _credentials = credentials;
        }

        public string GetFrequencyDescriptor(IBillingFrequencyAccessor accessor = null)
        {
            accessor = accessor == null ? new BillingFrequencyAccessor(_credentials) : accessor;
            try
            {
                List<String> days = DaysHelper.ReturnDays(Frequency);

                string theDays = days.Aggregate((x, y) => x + ", " + y);

                int WeekMonth = Frequency.WeekMonth == null ? 1 : (int)Frequency.WeekMonth;

                if (Frequency.Frequency > 1)
                    return "Every " + Frequency.Frequency + " years, on the " + UtilityHelper.GetNumberName((int)Frequency.Month) + " month, on the " + UtilityHelper.GetNumberName(WeekMonth) + " occurence of the following days: " + theDays;
                else
                    return "Every " + Frequency.Frequency + " years, on the " + UtilityHelper.GetNumberName((int)Frequency.Month) + " month, on the " + UtilityHelper.GetNumberName(WeekMonth) + " occurence of " + days[0];
            }
            catch
            {
                return "Issue with Billing Frequency";
            }

        }

        
    }
}
