﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Interfaces;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class WeeklyDescriptor : IFrequencyDescriptor
    {
        public BillingFrequency Frequency { get; set; }
        private Credential _credentials;


        public WeeklyDescriptor(Credential credentials, BillingFrequency frequency)
        {
            Frequency = frequency;
            _credentials = credentials;
        }

        public string GetFrequencyDescriptor(IBillingFrequencyAccessor accessor = null)
        {
            accessor = accessor == null ? new BillingFrequencyAccessor(_credentials) : accessor;
            try
            {

                List<String> days = DaysHelper.ReturnDays(Frequency);

                string theDays = days.Aggregate((x, y) => x + ", " + y);


                if (Frequency.Frequency > 1 && days.Count > 1)
                    return "Every " + Frequency.Frequency + " weeks on these days: " + theDays;
                else if (Frequency.Frequency > 1 && days.Count <= 1)
                    return "Every " + Frequency.Frequency + " weeks on this day: " + theDays;
                else if (Frequency.Frequency <= 1 && days.Count > 1)
                    return "Every week on these days: " + theDays;
                else
                    return "Every week on this day: " + theDays;

            }
            catch
            {
                return "Issue with Billing Frequency";
            }
        }

        
    }
}
