﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Interfaces;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class MonthlyByDaysAndWeekDescriptor : IFrequencyDescriptor
    {
        public BillingFrequency Frequency { get; set; }
        private Credential _credentials;


        public MonthlyByDaysAndWeekDescriptor(Credential credentials, BillingFrequency frequency)
        {
            Frequency = frequency;
            _credentials = credentials;
        }

        public string GetFrequencyDescriptor(IBillingFrequencyAccessor accessor = null)
        {
            accessor = accessor == null ? new BillingFrequencyAccessor(_credentials) : accessor;
            try
            {
                List<String> days = DaysHelper.ReturnDays(Frequency);

                string theDays = days.Aggregate((x, y) => x + ", " + y);

                string occurence = "1st";

                if (Frequency.WeekMonth != null)
                    occurence = UtilityHelper.GetNumberName((int)Frequency.WeekMonth);

                if (Frequency.Frequency > 1 && days.Count > 1)
                    return "Every " + Frequency.Frequency + " months on the " + occurence + " occurence of these days: " + theDays;
                else if (Frequency.Frequency > 1 && days.Count <= 1)
                    return "Every " + Frequency.Frequency + " months on the " + occurence + " occurence of " + days[0];
                else if (Frequency.Frequency <= 1 && days.Count > 1)
                    return "Every month of the " + occurence + " occurence of these days: " + theDays;
                else
                    return "Every month of the " + occurence + " occurence of " + days[0];
            }
            catch
            {
                return "Issue with Billing Frequency";
            }
        }

        
    }
}
