﻿using Bridgepay.RecurringBilling.Business.Helpers.Descriptors;
using Bridgepay.RecurringBilling.Business.Interfaces;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public class FrequencyDescriptorGenerator
    {
        public BillingFrequency Frequency { get; set; }
        public IFrequencyDescriptor Descriptor { get; set; }
        public string IntervalName { get; set; }
        private readonly IBillingFrequencyAccessor _accessor;
        private readonly Credential _credentials;                       

        public FrequencyDescriptorGenerator(Credential credentials, int frequencyId, IBillingFrequencyAccessor accessor = null)
        {
            _accessor = accessor;
            accessor = accessor == null ? new BillingFrequencyAccessor(credentials) : accessor;
            _credentials = credentials;
            Frequency = accessor.BillingFrequencyRepository.Find(frequencyId);
            Frequency.Interval = accessor.IntervalRepository.Find(Frequency.IntervalId);
            IntervalName = Frequency.Interval.Name;
            Descriptor = FrequencyDescriptorFactory.ReturnDescriptor(credentials, Frequency, IntervalName);
        }

        public string ReturnDescription()
        {
            return Descriptor.GetFrequencyDescriptor(_accessor);
        }
    }
}
