﻿
using Bridgepay.RecurringBilling.Business.Interfaces;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.ServiceAdaptors;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Enums;
using Bridgepay.RecurringBilling.Business.Helpers.Utility;
using Bridgepay.Core.Cryptography;
using System.Configuration;
using Bridgepay.RecurringBilling.Business.Specification;
using Bridgepay.RecurringBilling.Common.Utilities;
using log4net;

namespace Bridgepay.RecurringBilling.Business
{
    public class ContractPaymentProcessor : IContractPaymentProcessor
    {
        public ContractPaymentProcessor(Credential credentials, Credential merchantCredentials, Contract contract, IContractAccessor accessor = null, IMerchantAccessor maccessor = null)
        {
            Contract = contract;
            _accessor = accessor;
            _maccessor = maccessor;
            _credentials = credentials;
            _mcredentials = merchantCredentials;
            _log = LogUtility.log;
        }

        public ContractPaymentProcessor(Credential credentials, Credential merchantCredentials, Contract contract, VendorContext context, MerchantContext mcontext)
        {
            Contract = contract;
            _context = context;
            _mcontext = mcontext;
            _accessor = new ContractAccessor(credentials, _context);
            _maccessor = new MerchantAccessor(credentials, _mcontext);
            _credentials = credentials;
            _mcredentials = merchantCredentials;
            _log = LogUtility.log;
        }

        public Contract Contract { get; set; }
        public IContractAccessor _accessor { get; set; }
        public IMerchantAccessor _maccessor { get; set; }
        private Credential _credentials { get; set; }
        private Credential _mcredentials { get; set; }
        private VendorContext _context { get; set; }
        private MerchantContext _mcontext { get; set; }
        private ILog _log;

        public ContractPayment ProcessContractPayment(Contract contract)
        {
            try
            {
                Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Info, "Processing Contract: " + contract.Id.ToString(), null);

                ContractService service = new ContractService(_credentials, _accessor);
                IMerchantAccessor maccessor = _maccessor == null ? new MerchantAccessor(_mcredentials) : _maccessor;
                IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
                Account account = accessor.AccountRepository.Find(contract.AccountId);
                Merchant merchant = maccessor.MerchantRepository.Find(account.MerchantId);

                if(merchant != null)
                { 
                    ContractPayment paymentInfo = new ContractPayment(contract, merchant.Id, merchant.MerchantCode, merchant.Name);
                    paymentInfo.Account = account.AccountName;
                    paymentInfo.Status = StatusCode.PROCESSING;

                    List<FinancialResponsibleParty> parties = service.GetFinancialResponsibleParties(Contract.Id).Where(c => c.IsActive = true).ToList();
                    return ProcessContractParties(parties, paymentInfo);
                }
                else
                {
                    Exception newEx = new Exception("Merchant with the Id of " + contract.MerchantId + " was null, check databse.");
                    Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, newEx);
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
                return new ContractPayment { Responses = new List<PaymentProcessResponse> { new PaymentProcessResponse{Message = "Error getting Merchant Id"} }, Status = StatusCode.ERROR };
            }
        }

        public ContractPayment ProcessContractParties(List<FinancialResponsibleParty> parties, ContractPayment paymentInfo)
        {
            if (parties.Count <= 0)
            {
                if (paymentInfo.Responses == null)
                    paymentInfo.Responses = new List<PaymentProcessResponse>();
                paymentInfo.Responses.Add(new PaymentProcessResponse { AmountPaid = 0, FinancialResponsiblePartyId = 0, Status = StatusCode.DECLINED, Name = "System", Message = "No FinancialResponsibleParties for this Contract", LastFour = "NA" });
                paymentInfo.Status = StatusCode.DECLINED;
            }

            WalletAdaptor adaptor = new WalletAdaptor();

            foreach (FinancialResponsibleParty party in parties)
            {
                if (paymentInfo.Status == StatusCode.APPROVED)
                    break;

                if (party.Guid == null)
                {
                    paymentInfo.Responses.Add(new PaymentProcessResponse { AmountPaid = 0, FinancialResponsiblePartyId = party.Id, Status = StatusCode.DECLINED, Name = "System", Message = "No Wallet for this FRP", LastFour = "NA" });
                    paymentInfo.Status = StatusCode.DECLINED;
                    continue;
                }

                try
                {
                    List<Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod> methods = adaptor.GetPaymentMethodsById((Guid)party.Guid);

                    if (methods.Count < 1)
                    {
                        paymentInfo.Responses.Add(new PaymentProcessResponse { AmountPaid = 0, FinancialResponsiblePartyId = party.Id, Status = StatusCode.DECLINED, Name = "System", Message = "No payment methods in wallet", LastFour = "NA" });
                        paymentInfo.Status = StatusCode.DECLINED;
                        Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, "No payments methods found for Contract " + paymentInfo.Id.ToString(), null);
                        continue;
                    }

                    foreach (var method in methods)
                    {
                        PaymentProcessResponse response = ProcessPayment(paymentInfo, method);
                        response.FinancialResponsiblePartyId = party.Id;
                        response.LastFour = method.LastFour;
                        response.Name = method.Description;
                        paymentInfo.Responses.Add(response);

                        if (response.Status == StatusCode.APPROVED)
                        {
                            paymentInfo.Status = StatusCode.APPROVED;
                            paymentInfo.Processed = true;
                            break;
                        }
                        else if (response.Status == StatusCode.DECLINED)
                        {
                            paymentInfo.Status = StatusCode.DECLINED;
                        }
                        else if (response.Status == StatusCode.ERROR)
                        {
                            paymentInfo.Status = StatusCode.ERROR;
                        }
                        else
                            paymentInfo.Status = StatusCode.ACTIVE;
                    }
                }
                catch (Exception ex)
                {
                    paymentInfo.Responses.Add(new PaymentProcessResponse { AmountPaid = 0, FinancialResponsiblePartyId = party.Id, Status = StatusCode.DECLINED, Name = "System", Message = ex.Message, LastFour = "NA" });
                    if (paymentInfo.Exceptions == null)
                        paymentInfo.Exceptions = new List<Exception>();
                    paymentInfo.Exceptions.Add(ex);
                    paymentInfo.Status = StatusCode.ERROR;
                    Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
                    continue;
                }
            }

            if (paymentInfo.Status != StatusCode.ERROR || paymentInfo.Status == StatusCode.ACTIVE)
                paymentInfo.Processed = true;

            return paymentInfo;

        }

        public ContractPayment ProcessScheduledPayment(ScheduledPayment payment)
        {
            ContractService service = new ContractService(_credentials, _accessor);
            IMerchantAccessor maccessor = _maccessor == null ? new MerchantAccessor(_mcredentials) : _maccessor;
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            Contract contract = accessor.ContractRepository.Find(payment.ContractId);
            Account account = accessor.AccountRepository.Find(contract.AccountId);
            Merchant merchant = maccessor.MerchantRepository.Find(account.MerchantId);

            merchant.Id = 2001;//"JFT"

            ContractPayment paymentInfo = new ContractPayment(contract, payment, merchant.Id, merchant.MerchantCode, merchant.Name);
            paymentInfo.Account = account.AccountName;
            paymentInfo.Status = StatusCode.PROCESSING;

            List<FinancialResponsibleParty> parties = service.GetFinancialResponsibleParties(Contract.Id);
            return ProcessContractParties(parties, paymentInfo);
        }

        public PaymentProcessResponse ProcessPayment(ContractPayment paymentInfo, Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod method)
        {
            PaymentProcessResponse response = new PaymentProcessResponse();
            try
            {
                SimpleAES crypto = new SimpleAES();

                AuthTransaction transaction = new AuthTransaction();
                if (method.PaymentMethodType == WalletService.PaymentMethodType.Cards)
                {
                    transaction.WalletPaymentMethodID = (Guid)method.PaymentMethodId;
                }
                else
                {
                    transaction.WalletPaymentMethodID = (Guid)method.PaymentMethodId;
                }

                string accountType = "R";

                if (method.AccountType == "Checking")
                    accountType = "C";
                else if (method.AccountType == "Savings")
                    accountType = "R";

                DomainServiceAdaptor domainAdaptor = new DomainServiceAdaptor();
                string user = crypto.DecryptString(ConfigurationManager.AppSettings["MBPUserName"]);
                string password = crypto.DecryptString(ConfigurationManager.AppSettings["MBPPassword"]);
                DomainService.Credentials creds = new DomainService.Credentials{UserName=user, Password=password};
 
                transaction.ClientIdentifier = "MyBridgepay VT";
                transaction.TransactionID = Guid.NewGuid().ToString();
                transaction.RequestDateTime = DateTime.UtcNow.ToShortDateString();
                transaction.User = user;
                transaction.Password = password;
                transaction.MerchantAccountCode = paymentInfo.MerchantId;
                transaction.MerchantCode = paymentInfo.MerchantCode;
                transaction.Amount = Convert.ToInt32(paymentInfo.ChargedAmount);
                transaction.TransactionType = "sale";
                transaction.TransIndustryType = domainAdaptor.GetIdustryCode(creds, paymentInfo.MerchantId, paymentInfo.MerchantCode.ToString());
                transaction.AcctType = accountType;
                transaction.HolderType = "P";

                string test = transaction.ToXml();

                BridgeCommProcessingAdaptor adaptor = new BridgeCommProcessingAdaptor();
                AuthorizationResponse authRespone = adaptor.ProcessAuthorization(transaction);

                if (authRespone.IsApproval)
                {
                    response.Status = StatusCode.APPROVED;
                    response.Message = authRespone.GatewayMessage + " with card *****" + method.LastFour;
                    Logger.LogTransaction(_log, Utility.GetCurrentMethod(), "Billing Service", true, paymentInfo.MerchantId, paymentInfo.Name, (int)paymentInfo.ChargedAmount, method.LastFour, "sale", response.Message);
                }
                else
                {
                    response.Status = StatusCode.DECLINED;
                    response.Message = authRespone.GatewayMessage + "Declined: " + authRespone.ResponseDescription;
                    Logger.LogTransaction(_log, Utility.GetCurrentMethod(), "Billing Service", false, paymentInfo.MerchantId, paymentInfo.Name, (int)paymentInfo.ChargedAmount, method.LastFour, "sale", response.Message);
                }
            }
            catch(Exception ex)
            {
                response.Status = StatusCode.ERROR;
                response.Message = "Error processing" + paymentInfo.ChargedAmount.ToString() + " with card *****" + method.LastFour;
                Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
            }

            return response;
        }

        public void ProcessMerchantEmail(ContractPayment paymentInfo, ObjectTable emailTemplate, Contact contact)
        {
            try
            {
                if (contact == null)
                    return;
                //Specification
                if (paymentInfo.Status == StatusCode.APPROVED && paymentInfo.EmailMerchantReceipt.GetValueOrDefault(true) || paymentInfo.Status == StatusCode.DECLINED && paymentInfo.EmailMerchantAtFailure.GetValueOrDefault(true))
                {
                    MerchantEmail email = new MerchantEmail();
                    email.To = contact.ContactEmail;
                    email.ReplyTo = ConfigurationManager.AppSettings["ReplyToEmail"].ToString();
                    email.SenderName = "BridgePay Network";
                    email.Subject = "The following contract payment was " + paymentInfo.Status.ToString();
                    paymentInfo.Subject = paymentInfo.Status == StatusCode.APPROVED ? "The following payment has been applied successfully to the contract" : "A contract payment was declined for this account.  Please log into MyBridgePay and update payment information under the Accounts section.";
                    email.SendEmail(paymentInfo, email.Subject);
                }
            }
            catch (Exception ex)
            {
                Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
            }
        }

        private void ProcessCustomerEmails(ContractPayment paymentInfo, ObjectTable emailTemplate)
        {
            if (paymentInfo.Status == StatusCode.APPROVED && paymentInfo.EmailCustomerReceipt.GetValueOrDefault(true) || paymentInfo.Status == StatusCode.DECLINED && paymentInfo.EmailCustomerAtFailure.GetValueOrDefault(true))
            {
                ContractService service = new ContractService(_credentials, _accessor);
                IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
                CustomerEmail email = new CustomerEmail();
                email.ReplyTo = ConfigurationManager.AppSettings["ReplyToEmail"].ToString();
                email.SenderName = "BridgePay Network";
                email.Subject = "The following contract payment was processed ";
                List<FinancialPartyPaymentInfo> partiesInfo = paymentInfo.GeneratePartyInfo();
                foreach (FinancialPartyPaymentInfo info in partiesInfo)
                {
                    try
                    {
                        FinancialResponsibleParty party = service.GetFinancialResponsibleParties(paymentInfo.ContractId).Where(c => c.Id == info.FinancialResponsiblePartyId).FirstOrDefault();
                        email.To = party.Contact.ContactEmail;
                        email.SendEmail(info, email.Subject);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
                    }
                }
            }
        }

        private Contract ProcessContractRules(ContractPayment paymentInfo, ScheduledPayment payment, Contract contract, Contact contact)
        {
            ContractService service = new ContractService(_credentials, _accessor);

            contract = paymentInfo.UpdateContract(contract);
            contract.ObjectState = Core.Framework.ObjectState.Modified;
            try
            {

                ContractCompleteSpecification complete = new ContractCompleteSpecification();
                ContractWillCancelSpecification canceled = new ContractWillCancelSpecification();

                if (paymentInfo.Status == StatusCode.APPROVED)
                {
                    if (complete.IsSatisfiedBy(contract))
                    {
                        contract.IsActive = false;
                        contract.Status = (int)StatusCode.COMPLETE;
                        GenerateCompletionEmail(paymentInfo, new ObjectTable(), contact);
                    }
                    else
                    {
                        contract = ProcessNextBillingDate(paymentInfo, contract, payment);
                    }
                }
                else if (paymentInfo.Status == StatusCode.DECLINED)
                {
                    if (canceled.IsSatisfiedBy(contract))
                    {
                        contract.IsActive = false;
                        contract.Status = (int)StatusCode.CANCELED;
                        GenerateCancelationEmail(paymentInfo, new ObjectTable(), contact);
                    }
                    else
                        contract.Status = (int)StatusCode.DECLINED;


                }
                else
                {
                    paymentInfo.Processed = false;
                    contract.Status = (int)StatusCode.ERROR;
                    GenerateErrorEmail(paymentInfo, new ObjectTable(), contact);
                }

            }
            catch (Exception ex)
            {
                Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
            }

            return contract;

        }

        public void GenerateErrorEmail(ContractPayment paymentInfo, ObjectTable emailTemplate, Contact contact)
        {
            if (contact == null)
                return;
            //Specification
            if (paymentInfo.Status == StatusCode.APPROVED && paymentInfo.EmailMerchantReceipt.GetValueOrDefault(true) || paymentInfo.Status == StatusCode.DECLINED && paymentInfo.EmailMerchantAtFailure.GetValueOrDefault(true))
            {
                try
                {
                    AdminEmail email = new AdminEmail();
                    email.To = ConfigurationManager.AppSettings["AdminEmail"].ToString();
                    email.ReplyTo = ConfigurationManager.AppSettings["ReplyToEmail"].ToString();
                    email.SenderName = "BridgePay Network";
                    email.Subject = "The following contract payment was processed with a code of" + paymentInfo.Status.ToString();
                    paymentInfo.Subject = "The following error occured processing the contract.";
                    email.SendEmail(paymentInfo, email.Subject);
                }
                catch (Exception ex)
                {
                    Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
                }
            }
        }

        private void GenerateCancelationEmail(ContractPayment paymentInfo, ObjectTable objectTable, Contact contact)
        {
            if (paymentInfo.Status == StatusCode.APPROVED && paymentInfo.EmailCustomerReceipt.GetValueOrDefault(true) || paymentInfo.Status == StatusCode.DECLINED && paymentInfo.EmailCustomerAtFailure.GetValueOrDefault(true))
            {
                ContractService service = new ContractService(_credentials, _accessor);
                IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
                paymentInfo.Subject = "The following contract payment was canceled due to non-payment. The contract will not be charged again until the payment methods are updated and the contract is reactivated in MyBridgePay.";
                MerchantEmail merchantEmail = new MerchantEmail();
                merchantEmail.ReplyTo = ConfigurationManager.AppSettings["ReplyToEmail"].ToString();
                merchantEmail.SenderName = "BridgePay Network";
                merchantEmail.Subject = "The following contract payment was CANCELED due to non-payment.";
                merchantEmail.To = contact.ContactEmail;
                paymentInfo.ChargedAmount = paymentInfo.ChargedAmount / 100;
                merchantEmail.SendEmail(paymentInfo, merchantEmail.Subject);
                paymentInfo.ChargedAmount = paymentInfo.ChargedAmount * 100;

                CustomerEmail email = new CustomerEmail();
                email.ReplyTo = ConfigurationManager.AppSettings["ReplyToEmail"].ToString();
                email.SenderName = "BridgePay Network";
                email.Subject = "The following contract payment was CANCELED due to non-payment.";
                email.To = contact.ContactEmail;


                List<FinancialPartyPaymentInfo> partiesInfo = paymentInfo.GeneratePartyInfo();
                foreach (FinancialPartyPaymentInfo info in partiesInfo)
                {
                    try
                    {
                        FinancialResponsibleParty party = service.GetFinancialResponsibleParties(paymentInfo.ContractId).Where(c => c.Id == info.FinancialResponsiblePartyId).FirstOrDefault();
                        email.To += party.Contact.ContactEmail;
                        email.SendEmail(info, email.Subject);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
                    }
                }
            }
        }

        private void GenerateCompletionEmail(ContractPayment paymentInfo, ObjectTable objectTable, Contact contact)
        {
            try
            {
                if (paymentInfo.Status == StatusCode.APPROVED && paymentInfo.EmailCustomerReceipt.GetValueOrDefault(true) || paymentInfo.Status == StatusCode.DECLINED && paymentInfo.EmailCustomerAtFailure.GetValueOrDefault(true))
                {
                    ContractService service = new ContractService(_credentials, _accessor);
                    IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;

                    MerchantEmail merchantEmail = new MerchantEmail();
                    merchantEmail.ReplyTo = ConfigurationManager.AppSettings["ReplyToEmail"].ToString();
                    merchantEmail.SenderName = "BridgePay Network";
                    merchantEmail.Subject = "The following contract has completed and will be deactivated.";
                    merchantEmail.To = contact.ContactEmail;
                    paymentInfo.ChargedAmount = paymentInfo.ChargedAmount / 100;
                    merchantEmail.SendEmail(paymentInfo, merchantEmail.Subject);
                    paymentInfo.ChargedAmount = paymentInfo.ChargedAmount * 100;

                    CustomerEmail email = new CustomerEmail();
                    email.ReplyTo = ConfigurationManager.AppSettings["ReplyToEmail"].ToString();
                    email.SenderName = "BridgePay Network";
                    email.Subject = "The following contract has completed and will be deactivated.";
                    email.To = contact.ContactEmail;


                    List<FinancialPartyPaymentInfo> partiesInfo = paymentInfo.GeneratePartyInfo();
                    foreach (FinancialPartyPaymentInfo info in partiesInfo)
                    {
                        FinancialResponsibleParty party = service.GetFinancialResponsibleParties(paymentInfo.ContractId).Where(c => c.Id == info.FinancialResponsiblePartyId).FirstOrDefault();
                        email.To += party.Contact.ContactEmail;
                        email.SendEmail(info, email.Subject);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
            }
        }

        public Contract ProcessNextBillingDate(ContractPayment paymentInfo, Contract contract, ScheduledPayment payment)
        {
            try
            {
                ContractService service = new ContractService(_credentials, _accessor);
                IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;

                if (paymentInfo.ScheduledPaymentId == null && paymentInfo.Status == StatusCode.APPROVED)
                {
                    contract.NextBillDate = service.ForcastPayments(contract.Id, 1).FirstOrDefault().BillDate;
                    contract.Status = (int)StatusCode.ACTIVE;
                }
                else if (payment != null && paymentInfo.Status == StatusCode.APPROVED)
                {
                    payment.Paid = true;
                    payment.ObjectState = Core.Framework.ObjectState.Modified;
                    accessor.ScheduledPaymentRepository.InsertOrUpdate(payment);
                    accessor.Save();
                }
            }
            catch (Exception ex)
            {
                Logger.LogEvent(_log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
            }

            return contract;
        }
    }
}
