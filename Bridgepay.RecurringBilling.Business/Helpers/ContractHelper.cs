﻿using Bridgepay.Core.Logging;
using Bridgepay.RecurringBilling.Business.Services;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    /// <summary>
    /// Utility methods for <see cref="Contract"/>.
    /// </summary>
    public static class ContractHelper
    {
        /// <summary>
        /// Calculates the number of payments and/or end date for a contract.
        /// </summary>
        /// <param name="contract"><see cref="Contract"/>.</param>
        /// <param name="billingFrequency"><see cref="BillingFrequency"/>.</param>
        /// <returns>Contract with the calculated number of payments and/or end date.</returns>
        /// <remarks>
        /// This function was copied from the Recurring Billing Service, but it has been modified
        /// in one significant way: payments are calculated from the start date without regard to
        /// the current date. The RBS version ensures the payment dates are in the future.
        /// </remarks>
        public static void GetCalculatedContract(Contract contract, BillingFrequency billingFrequency)
        {
            try
            {
                if (contract.NumberOfPayments == null && contract.EndDate == null)
                {
                    // Open-ended contract, nothing to calculate.
                    return;
                }

                var DaysString = billingFrequency.Days;
                List<int> days = new List<int>();
                if (DaysString != null)
                {
                    Char[] dayMasks = DaysString.ToCharArray();
                    int count = dayMasks.Count();
                    for (int x = 0; x < count; x++)
                    {
                        if (dayMasks[x].ToString() == "1")
                            days.Add(x);
                    }
                }
                List<ScheduledPayment> payments = new List<ScheduledPayment>();
                var Frequency = (int)billingFrequency.Frequency;
                var loopNumber = contract.NumberOfPayments != null ? (int)contract.NumberOfPayments : 0;
                var paymentInfo = new { LastBillDate = contract.StartDate, BillAmount = 0, Frequency = (int)billingFrequency.Frequency, WeekMonth = billingFrequency.WeekMonth, Month = billingFrequency.Month, DayMonth = billingFrequency.DayMonth != null ? (int)billingFrequency.DayMonth : 0, StartDate = (DateTime)contract.StartDate, EndDate = contract.EndDate, ContractID = contract.Id };
                if (loopNumber <= 1)
                {
                    if (contract.EndDate != null)
                        loopNumber = (contract.EndDate.Value.Date - contract.StartDate.Date).Days;
                }
                var number = loopNumber;
                if (billingFrequency.IntervalId == 1)
                {
                    for (int x = 0; x <= loopNumber; x++)
                    {
                        var newBillDate = contract.StartDate.AddDays(x * (int)billingFrequency.Frequency).Date;
                        if ((contract.EndDate == null || newBillDate.Date < contract.EndDate.Value.Date) && newBillDate.Date >= contract.StartDate.Date)
                        { payments.Add(new ScheduledPayment { BillAmount = 0, ContractId = 0, BillDate = newBillDate }); }
                        else
                        {
                            loopNumber++;
                        }
                        if ((contract.EndDate != null && newBillDate.Date >= contract.EndDate.Value.Date) || payments.Count == loopNumber)
                        {
                            break;
                        }
                    }
                }
                else if (billingFrequency.IntervalId == 2 || billingFrequency.IntervalId == 3)
                {
                    int take = loopNumber;
                    if (days.Count > loopNumber)
                    {
                        loopNumber = days.Count;
                    }
                    DateTime lastPayment = paymentInfo.LastBillDate;
                    DateTime startDate = paymentInfo.StartDate != null ? (DateTime)paymentInfo.StartDate : lastPayment;
                    int BillAmount = paymentInfo.BillAmount;
                    int index = ((int)lastPayment.DayOfWeek).GetStartingIndex(days);
                    int intervalNumber = 0;
                    int day = days[index];

                    while (payments.Count < loopNumber)
                    {
                        //try and get the first index
                        day = days[index];
                        DateTime addDate = lastPayment.ReturnAnotherDayOfWeek(day).AddDays((Frequency * intervalNumber) * 7).Date;
                        addDate = addDate.ToUniversalTime();

                        //Check whether that  date is past the startdate, if so add it and increase the index
                        if (addDate.Date >= startDate.Date && (paymentInfo.EndDate == null || addDate.Date < paymentInfo.EndDate.Value.Date))
                        {
                            payments.Add(new ScheduledPayment { BillAmount = BillAmount, BillDate = addDate, ContractId = (int)paymentInfo.ContractID });
                        }

                        if (index >= days.Count - 1)
                        {
                            index = 0;
                            intervalNumber++;
                        }
                        else
                        {
                            index++;
                        }
                        if (contract.EndDate != null && addDate.Date > contract.EndDate.Value.Date)
                        {
                            break;
                        }
                    }
                }
                else if (billingFrequency.IntervalId == 4)
                {
                    DateTime lastPayment = (DateTime)paymentInfo.LastBillDate;
                    for (int x = 0; x < loopNumber; x++)
                    {
                        DateTime nextDate = lastPayment.AddMonths(x * Frequency);
                        int day = ((int)paymentInfo.DayMonth).GetDayOrLast(nextDate);
                        nextDate = new DateTime(nextDate.Year, nextDate.Month, day).ToUniversalTime();
                        bool NewDateIsGreaterThanLastBillDate = nextDate.Date.CompareTo(paymentInfo.LastBillDate.Date) >= 0;
                        bool NewDateIsGreaterOrEqualStartDate = nextDate.Date.CompareTo(paymentInfo.StartDate.Date) >= 0;
                        bool EndDateIsGreaterOrEqualNewDate = paymentInfo.EndDate == null || nextDate.Date.CompareTo(paymentInfo.EndDate.Value.Date) < 0;
                        if (NewDateIsGreaterThanLastBillDate && NewDateIsGreaterOrEqualStartDate && EndDateIsGreaterOrEqualNewDate)
                        {
                            payments.Add(new ScheduledPayment { BillAmount = paymentInfo.BillAmount, ContractId = (int)paymentInfo.ContractID, BillDate = nextDate });
                        }
                        if (payments.Count >= number || (paymentInfo.EndDate != null && nextDate > paymentInfo.EndDate))
                        {
                            break;
                        }
                        loopNumber++;
                    }
                }
                else if (billingFrequency.IntervalId == 5)
                {
                    int Week = 1;
                    int take = number;
                    DateTime start = paymentInfo.LastBillDate;
                    if (paymentInfo.WeekMonth != null)
                        Week = (int)paymentInfo.WeekMonth;
                    else
                        Week = start.GetWeekNumberOfMonth();


                    //Get the starting index
                    int i = ((int)start.DayOfWeek).GetStartingIndex(days);
                    int max = days.Max();

                    DateTime add = start;
                    int month = 0;  //increment for the month
                    for (int x = 0; x < loopNumber; x++)
                    {
                        int day = days[i];

                        bool moveToNextMonth = (int)add.DayOfWeek == 7;

                        add = add.AddMonths(Frequency * month).GetNthDayOfMonth(Week, day).Date.ToUniversalTime();

                        if (month > 0) //Reset the month
                            month = 0;
                        bool maxResult = i + 1 >= days.Count;

                        if (add.Date.CompareTo(start.Date) >= 0 && (paymentInfo.EndDate == null || add.Date.CompareTo(paymentInfo.EndDate.Value.Date) < 0))
                        {
                            payments.Add(new ScheduledPayment { BillAmount = paymentInfo.BillAmount, BillDate = add, ContractId = (int)paymentInfo.ContractID });

                            if (moveToNextMonth || maxResult)
                            {
                                month++;
                                i = 0;
                            }
                            else
                            {
                                i = ((int)add.DayOfWeek).GetStartingIndex(days);
                            }
                        }
                        else
                        {
                            month++;
                            i = 0;
                        }
                        if (payments.Count >= number || (paymentInfo.EndDate != null && add >= paymentInfo.EndDate.Value.AddDays(-1)))
                        {
                            break;
                        }
                        loopNumber++;
                    }

                    payments.Sort((a, b) => a.BillDate.CompareTo(b.BillDate));
                    payments = payments.Take(take).ToList();
                }
                else if (billingFrequency.IntervalId == 6)
                {
                    DateTime lastPayment = paymentInfo.LastBillDate;
                    if (paymentInfo.Month != null)
                    {
                        int numberOfDays = DateTime.DaysInMonth(lastPayment.Year, (int)paymentInfo.Month);
                        if ((int)paymentInfo.DayMonth > numberOfDays)
                            lastPayment = new DateTime(lastPayment.Year, (int)paymentInfo.Month, numberOfDays).Date.ToUniversalTime();
                        else
                            lastPayment = new DateTime(lastPayment.Year, (int)paymentInfo.Month, (int)paymentInfo.DayMonth).Date.ToUniversalTime();
                    }
                    for (int x = 0; x < loopNumber; x++)
                    {
                        var addDate = lastPayment.AddYears(x * Frequency).Date.ToUniversalTime();
                        if (addDate.Date.CompareTo(paymentInfo.LastBillDate.Date) >= 0 && (paymentInfo.EndDate == null || addDate.Date.CompareTo(paymentInfo.EndDate) < 0))
                            payments.Add(new ScheduledPayment { BillAmount = paymentInfo.BillAmount, ContractId = (int)paymentInfo.ContractID, BillDate = lastPayment.AddYears(x * Frequency) });
                        if (payments.Count >= number || (paymentInfo.EndDate != null && lastPayment.AddYears(x * Frequency) >= paymentInfo.EndDate.Value.AddDays(-1)))
                        {
                            break;
                        }
                        loopNumber++;
                    }
                }
                else if (billingFrequency.IntervalId == 7)
                {
                    DateTime start = paymentInfo.LastBillDate;
                    DateTime startDate = paymentInfo.StartDate != null ? (DateTime)paymentInfo.StartDate : start;
                    int BillAmount = paymentInfo.BillAmount;
                    int Week = 1;
                    int Month = 0;
                    int take = number;
                    if (paymentInfo.Month != null)
                        Month = (int)paymentInfo.Month;
                    else
                        Month = start.Month;

                    if (paymentInfo.WeekMonth != null)
                        Week = (int)paymentInfo.WeekMonth;
                    else
                        Week = start.GetWeekNumberOfMonth();

                    int i = ((int)start.DayOfWeek).GetStartingIndex(days);
                    int max = days.Max();

                    DateTime add = new DateTime(start.Year, Month, 1);
                    int year = 0;  //increment for the month
                    for (int x = 0; x < loopNumber; x++)
                    {
                        int day = days[i];
                        bool moveToNextYear = (int)add.DayOfWeek == 7;
                        add = add.AddYears(Frequency * year).GetNthDayOfMonth(Week, day).Date.ToUniversalTime();
                        if (year > 0) //Reset the month
                            year = 0;
                        bool maxResult = i + 1 >= days.Count;

                        if (add.Date.CompareTo(start.Date) >= 0 && (paymentInfo.EndDate == null || add.Date.CompareTo(paymentInfo.EndDate) < 0))
                        {
                            payments.Add(new ScheduledPayment { BillAmount = BillAmount, BillDate = add, ContractId = (int)paymentInfo.ContractID });

                            if (moveToNextYear || maxResult)
                            {
                                year++;
                                i = 0;
                            }
                            else
                            {
                                i = ((int)add.DayOfWeek).GetStartingIndex(days);
                            }
                        }
                        else
                        {
                            year++;
                            i = 0;
                        }
                        if (payments.Count >= number || (paymentInfo.EndDate != null && add >= paymentInfo.EndDate.Value.AddDays(-1)))
                        {
                            break;
                        }
                        loopNumber++;
                    }

                    payments.Sort((a, b) => a.BillDate.CompareTo(b.BillDate));
                    payments = payments.Take(take).ToList();
                }
                else if (billingFrequency.IntervalId == 8)
                {
                    DateTime start = (DateTime)paymentInfo.StartDate;
                    int frequency = (int)paymentInfo.Frequency;
                    DateTime add = paymentInfo.LastBillDate.Date.ToUniversalTime();

                    while (payments.Count < number)
                    {
                        for (int x = 0; x < loopNumber; x++)
                        {
                            var newDate = new DateTime();
                            if (frequency < 2)
                            {
                                newDate = paymentInfo.LastBillDate.AddDays(x * 7).Date.ToUniversalTime();
                            }
                            else if (frequency >= 2)
                            {
                                newDate = paymentInfo.LastBillDate.AddDays(x * (7 / frequency)).Date.ToUniversalTime();
                            }
                            if (newDate.Date >= start.Date && newDate.Date >= DateTime.Now.Date && (paymentInfo.EndDate == null || newDate.Date < paymentInfo.EndDate))
                            {
                                payments.Add(new ScheduledPayment { BillAmount = paymentInfo.BillAmount, ContractId = (int)paymentInfo.ContractID, BillDate = newDate });
                            }
                            if (paymentInfo.EndDate != null && newDate >= paymentInfo.EndDate.Value.AddDays(-1))
                            {
                                break;
                            }
                            loopNumber++;
                        }
                    }
                    payments.Sort((a, b) => a.BillDate.CompareTo(b.BillDate));
                    payments = payments.Take(number).ToList();
                }
                else if (billingFrequency.IntervalId == 9)
                {
                    DateTime start = (DateTime)paymentInfo.StartDate;
                    int frequency = (int)paymentInfo.Frequency;
                    DateTime add = paymentInfo.LastBillDate.Date.ToUniversalTime();
                    while (payments.Count < number)
                    {
                        for (int x = 0; x < loopNumber; x++)
                        {
                            if (frequency < 2)
                            {
                                add = paymentInfo.LastBillDate.AddDays(x * 7).Date.ToUniversalTime();
                            }
                            else if (frequency == 2)
                            {
                                int xDays = 30 / frequency;

                                if (add.Day > xDays)
                                {
                                    add = add.AddMonths(1).AddDays(-xDays).Date.ToUniversalTime();
                                }
                                else
                                {
                                    if (add.AddDays(xDays).Month != add.Month)
                                        xDays = DateTime.DaysInMonth(add.Year, add.Month) - add.Day;

                                    add = add.AddDays(xDays).Date.ToUniversalTime();
                                }
                            }
                            else if (frequency > 2)
                            {
                                int xDays = 30 / frequency;
                                add = add.AddDays(xDays);
                            }
                            if (add.Date >= start.Date && add.Date >= DateTime.Now.Date && (paymentInfo.EndDate == null || add.Date < paymentInfo.EndDate))
                                payments.Add(new ScheduledPayment { BillAmount = paymentInfo.BillAmount, ContractId = (int)paymentInfo.ContractID, BillDate = add });
                            if (paymentInfo.EndDate != null && add >= paymentInfo.EndDate.Value.AddDays(-1))
                            {
                                break;
                            }
                            loopNumber++;
                        }
                    }
                    payments.Sort((a, b) => a.BillDate.CompareTo(b.BillDate));
                    payments = payments.Take(number).ToList();
                }
                else if (billingFrequency.IntervalId == 10)
                {
                    DateTime start = (DateTime)paymentInfo.StartDate;
                    int frequency = (int)paymentInfo.Frequency;
                    DateTime add = paymentInfo.LastBillDate.Date.ToUniversalTime();

                    while (payments.Count < number)
                    {
                        for (int x = 0; x < loopNumber; x++)
                        {
                            if (frequency < 2)
                            {
                                add = add.AddYears(frequency);
                            }
                            else if (frequency >= 2)
                            {
                                int xMonths = 12 / frequency;
                                add = add.AddMonths(xMonths);
                            }
                            if (add.Date >= start.Date && add.Date >= DateTime.Now.Date && (paymentInfo.EndDate == null || add.Date < paymentInfo.EndDate))
                                payments.Add(new ScheduledPayment { BillAmount = paymentInfo.BillAmount, ContractId = (int)paymentInfo.ContractID, BillDate = add });
                            if (paymentInfo.EndDate != null && add >= paymentInfo.EndDate.Value.AddDays(-1))
                            {
                                break;
                            }
                            loopNumber++;
                        }
                    }
                    payments.Sort((a, b) => a.BillDate.CompareTo(b.BillDate));
                    payments = payments.Take(number).ToList();
                }
                payments = payments.Where(p => p.BillDate >= contract.StartDate.Date && (contract.EndDate == null || p.BillDate <= contract.EndDate.Value.Date)).ToList();
                if (contract.NumberOfPayments == null)
                {
                    contract.NumberOfPayments = payments.Count;
                }
                else
                {
                    contract.EndDate = payments.Max(p => p.BillDate).Date;
                }
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Error(nameof(GetCalculatedContract), ex);
                throw;
            }
        }

        /// <summary>
        /// Should a given <see cref="Contract"/> have its end date or number of payments calculated?
        /// </summary>
        /// <param name="contract"><see cref="Contract"/>.</param>
        /// <returns>True if a given <see cref="Contract"/> should have its end date or number of payments calculated.</returns>
        public static bool NeedsEndDateOrNumberOfPayments(this Contract contract)
        {
            if (contract == null)
            {
                throw new ArgumentNullException(nameof(contract));
            }
            else if (contract.EndDate == null && contract.NumberOfPayments == null)
            {
                // "If neither the End Date nor the Number of Payments are provided, the End Date will be stored as NULL (never ending contract)."
                return false;
            }
            else if (contract.EndDate.HasValue && contract.NumberOfPayments == null)
            {
                // "When providing the EndDate only, the Number of Payments will be calculated and both stored in the table."
                return true;
            }
            else if (contract.NumberOfPayments.HasValue && contract.EndDate == null)
            {
                // "When providing the Number of Payments only, the End Date will be calculated and both stored in the table."
                return true;
            }

            return false;
        }

        /// <summary>
        /// Does a given <see cref="Contract"/> specify its end date and number of payments?
        /// </summary>
        /// <param name="contract"><see cref="Contract"/>.</param>
        /// <returns>True if a given <see cref="Contract"/> specifies its end date and number of payments.</returns>
        public static bool SpecifiesEndDateAndNumberOfPayments(this Contract contract)
        {
            if (contract == null)
            {
                throw new ArgumentNullException(nameof(contract));
            }

            return contract.EndDate.HasValue && contract.NumberOfPayments.HasValue;
        }

        /// <summary>
        /// Does the end date and number of payments of a given <see cref="Contract"/> match the calculated version?
        /// </summary>
        /// <param name="contract"><see cref="Contract"/>.</param>
        /// <param name="credential"><see cref="Credential"/>.</param>
        /// <param name="errorMessage">Error message indicating reason for failure.</param>
        /// <returns>True if the supplied end date and number of payments match the calculated version.</returns>
        public static bool SuppliedContractMatchesCalculatedContract(this Contract contract, Credential credential, ref string errorMessage)
        {
            // Work with a copy of the original contract - don't change the original.
            var calculated = new Contract
            {
                Id = contract.Id,
                BillingFrequencyId = contract.BillingFrequencyId,
                StartDate = contract.StartDate,
                NumberOfPayments = contract.NumberOfPayments,
            };

            var billingFrequency = new BillingFrequencyService(credential).GetBillingFrequencyById(contract.BillingFrequencyId);
            ContractHelper.GetCalculatedContract(calculated, billingFrequency);

            if (calculated.EndDate != contract.EndDate || calculated.NumberOfPayments != contract.NumberOfPayments)
            {
                var msg = new StringBuilder();
                msg.AppendLine("The calculated end date and/or number of payments does not match the supplied contract.");
                msg.AppendLine($"Supplied end date: {contract.EndDate} number of payments: {contract.NumberOfPayments}.");
                msg.AppendLine($"Calculated end date: {calculated.EndDate} number of payments: {calculated.NumberOfPayments}.");
                errorMessage = msg.ToString();
                return false;
            }

            return true;
        }
    }
}
