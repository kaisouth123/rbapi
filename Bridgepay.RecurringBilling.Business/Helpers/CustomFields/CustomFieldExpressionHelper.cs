﻿using Bridgepay.RecurringBilling.DataLayer.Helpers;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Bridgepay.RecurringBilling.Business.Helpers.CustomFields
{
    /// <summary>
    /// Provides expressions for <see cref="List{T}"/> of <see cref="ContractCustomField"/>.
    /// </summary>
    public static class CustomFieldExpressionHelper
    {
        /// <summary>
        /// Converts a <see cref="List{T}"/> of <see cref="ContractCustomField"/> to a filter <see cref="Expression"/>.
        /// </summary>
        /// <param name="customFields"><see cref="List{T}"/> of <see cref="ContractCustomField"/> to convert.</param>
        /// <returns>A filter <see cref="Expression"/>.</returns>
        public static Expression<Func<ContractCustomField, bool>> ToFilterExpression(this List<ContractCustomField> customFields)
        {
            if (customFields == null)
            {
                throw new ArgumentNullException(nameof(customFields));
            }

            Expression<Func<ContractCustomField, bool>> configIdAndValueMatch = null;
            foreach (var searchCriteria in customFields)
            {
                // Define an expression where the CustomFieldConfigId and Value properties match.
                Expression<Func<ContractCustomField, bool>> expression = f => f.CustomFieldConfigId == searchCriteria.CustomFieldConfigId &&
                                                                              f.Value == searchCriteria.Value;

                if (configIdAndValueMatch == null)
                    configIdAndValueMatch = expression;
                else
                    configIdAndValueMatch = configIdAndValueMatch.Or(expression);
            }

            return configIdAndValueMatch;
        }
    }
}
