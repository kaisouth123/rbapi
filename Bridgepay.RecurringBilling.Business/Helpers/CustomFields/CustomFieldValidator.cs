﻿using Bridgepay.Core.Common.Helpers;
using Bridgepay.Core.DataContracts.CustomFields;
using Bridgepay.Core.Logging;
using Bridgepay.RecurringBilling.Models;
using System.Collections.Generic;
using System.Linq;
using CustomFieldData = Bridgepay.RecurringBilling.Models.CustomFieldData;

namespace Bridgepay.RecurringBilling.Business.Helpers.CustomFields
{
    public static class CustomFieldHelper
    {
        /// <summary>
        /// Validate the request's incoming custom fields against the activated custom fields for a given merchant account.
        /// </summary>
        /// <param name="activatedCustomFields">Activated custom fields for a given merchant account.</param>
        /// <param name="incomingCustomFields">The request's incoming custom fields.</param>
        /// <param name="contractID">Contract ID.</param>
        /// <param name="validCustomFields">The collection of validated custom fields, if any.</param>
        /// <returns>A <see cref="CustomFieldValidationResult"/>.</returns>
        public static CustomFieldValidationResult ValidateCustomFields(IEnumerable<CustomFieldConfig> activatedCustomFields, List<ContractCustomField> incomingCustomFields, int contractID, ref List<CustomFieldData> validCustomFields)
        {
            var validated = new List<CustomFieldData>();
            foreach (var activated in activatedCustomFields)
            {
                // Validate any incoming fields that match by configuration ID.
                var incoming = incomingCustomFields?.FirstOrDefault(f => f.CustomFieldConfigId == activated.Id);
                var notProvided = incoming == null;
                if (activated.IsRequired && notProvided)
                {
                    LogProvider.LoggerInstance.Error($"The required custom field config id \"{activated.Id}\" was not found in the incoming request.");
                    return CustomFieldValidationResult.Missing;
                }
                else if (notProvided)
                {
                    // Field isn't required, and wasn't provided, so there's nothing to validate.
                    continue;
                }

                var validationResult = activated.Validate(incoming.Value);
                if (validationResult == CustomFieldValidationResult.Valid)
                {
                    var validatedField = new CustomFieldData
                    {
                        CustomFieldConfigId = activated.Id,
                        Value = incoming.Value,
                    };

                    // This code trusts what the client is providing in order to determine the object state.
                    if (contractID != 0 &&
                        contractID == incoming.EntityId &&
                        incoming.Id != 0 &&
                        incoming.CustomFieldConfigId == activated.Id)
                    {
                        validatedField.EntityId = contractID.ToString();
                        validatedField.Id = incoming.Id;
                        validatedField.ObjectState = Core.Framework.ObjectState.Modified;
                    }
                    else
                    {
                        validatedField.ObjectState = Core.Framework.ObjectState.Added;
                    }

                    validated.Add(validatedField);
                }
                else
                {
                    // At least one field is invalid, no point in processing further.
                    LogProvider.LoggerInstance.Error($"Custom field \"{incoming.CustomFieldConfigId}\" value \"{incoming.Value}\" is {validationResult.ToString()}.");
                    return validationResult;
                }
            }

            // Any "extra" fields sent that aren't activated are considered a failure condition.
            if (incomingCustomFields != null)
            {
                var activatedCustomFieldConfigIds = activatedCustomFields.Select(f => f.Id);
                var incomingCustomFieldConfigIds = incomingCustomFields.Select(f => f.CustomFieldConfigId);
                var extraIncomingFieldConfigIds = incomingCustomFieldConfigIds.Except(activatedCustomFieldConfigIds);
                if (extraIncomingFieldConfigIds.Any())
                {
                    LogProvider.LoggerInstance.Error($"Custom field config (s) not found: \"{extraIncomingFieldConfigIds.Select(e => e.ToString()).Aggregate((a, b) => a + ", " + b)}\".");
                    return CustomFieldValidationResult.UnknownCustomField;
                }
            }

            if (validated.Count() > 0)
            {
                validCustomFields = validated;
            }
            return CustomFieldValidationResult.Valid;
        }
    }
}
