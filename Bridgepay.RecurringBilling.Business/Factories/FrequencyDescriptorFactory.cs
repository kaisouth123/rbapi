﻿using Bridgepay.RecurringBilling.Business.Interfaces;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers.Descriptors
{
    public static class FrequencyDescriptorFactory
    {
        public static IFrequencyDescriptor ReturnDescriptor(Credential credentials, BillingFrequency frequency, string intervalName)
        {
            if (intervalName == PaymentIntervalConstants.Daily)
                return new DailyDescriptor(credentials, frequency);
            else if (intervalName == PaymentIntervalConstants.DailyByDays)
                return new DailyByDaysDescriptor(credentials, frequency);
            else if (intervalName == PaymentIntervalConstants.Weekly)
                return new WeeklyDescriptor(credentials, frequency);
            else if (intervalName == PaymentIntervalConstants.Monthly)
                return new MonthlyDescriptor(credentials, frequency);
            else if (intervalName == PaymentIntervalConstants.MonthlyByDaysAndWeek)
                return new MonthlyByDaysAndWeekDescriptor(credentials, frequency);
            else if (intervalName == PaymentIntervalConstants.Yearly)
                return new YearlyDescriptor(credentials, frequency);
            else if (intervalName == PaymentIntervalConstants.YearlyByDaysMonthAndWeek)
                return new YearlyByDayaWeekMonthDescriptor(credentials, frequency);

            return new DailyByDaysDescriptor(credentials, frequency);
        }


    }
}
