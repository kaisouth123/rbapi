﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Models;


namespace Bridgepay.RecurringBilling.Business
{
    public static class ForecastorFactory
    {
       public static IPaymentForecastor ReturnForecaster(Credential credentials, int contractId, string intervalName)
       {
           if(intervalName == PaymentIntervalConstants.Daily)
               return new DailyForecaster(credentials, contractId);
           else if(intervalName == PaymentIntervalConstants.DailyByDays)
               return new DailyByDaysForecaster(credentials, contractId);
           else if(intervalName == PaymentIntervalConstants.Weekly)
               return new DailyByDaysForecaster(credentials, contractId);
           else if(intervalName == PaymentIntervalConstants.Monthly)
               return new MonthlyForecaster(credentials, contractId);
           else if(intervalName == PaymentIntervalConstants.MonthlyByDaysAndWeek)
               return new MonthlyByDayAndWeekForecaster(credentials, contractId);
           else if(intervalName == PaymentIntervalConstants.Yearly)
               return new YearlytForecaster(credentials, contractId);
           else if(intervalName == PaymentIntervalConstants.YearlyByDaysMonthAndWeek)
               return new YearlyByDayaWeekMonthForecaster(credentials, contractId);

           return new DailyByDaysForecaster(credentials, contractId);
       }


    }
}
