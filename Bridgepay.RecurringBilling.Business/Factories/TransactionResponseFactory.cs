﻿using Bridgepay.RecurringBilling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Factories
{
    static class TransactionResponseFactory
    {
        public static BaseResponse MakeResponse(BaseTransaction transaction, string response)
        {
            if (transaction.GetType() == typeof(AuthTransaction))
            {
                return new AuthorizationResponse(response);
            }
            else if (transaction.GetType() == typeof(VoidTransaction))
            {
                return new VoidRefundResponse(response);
            }
            else if (transaction.GetType() == typeof(CaptureTransaction))
            {
                return new CaptureResponse(response);
            }
            else if (transaction.GetType() == typeof(RefundTransaction))
            {
                return new VoidRefundResponse(response);
            }
            else if (transaction.GetType() == typeof(CloseBatchRequest))
            {
                return new CloseBatchResponse(response);
            }

            return null;
        }
    }
}
