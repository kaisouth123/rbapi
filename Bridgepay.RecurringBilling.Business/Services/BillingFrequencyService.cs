﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.Business.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using System.Linq.Expressions;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class BillingFrequencyService
    {
        private Credential _credentials;
        private Credential _merchantCredentials;
        private IBillingFrequencyAccessor _accessor;
        private IMerchantAccessor _merchantAccessor;

        public BillingFrequencyService(Credential credentials, IBillingFrequencyAccessor accessor = null, IMerchantAccessor merchantAccessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
            _merchantAccessor = merchantAccessor;
        }

        public BillingFrequencyService(Credential credentials, VendorContext context, MerchantContext mcontext = null)
        {
            _credentials = credentials;
            _accessor = new BillingFrequencyAccessor(credentials, context);
            _merchantAccessor = mcontext == null ? new MerchantAccessor(credentials) : new MerchantAccessor(credentials, mcontext);
        }

        public BillingFrequencyService(Credential credentials, Credential merchantCredentials, IBillingFrequencyAccessor accessor = null, IMerchantAccessor merchantAccessor = null)
        {
            _credentials = credentials;
            _merchantCredentials = merchantCredentials;
            _accessor = accessor;
            _merchantAccessor = merchantAccessor;
        }

        public WCFList<BillingFrequency> GetBillingFrequencies(int skip = 0, int take = -1)
        {
            WCFList<BillingFrequency> list = new WCFList<BillingFrequency>();
            IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.BillingFrequencyRepository.GetAll().OrderBy(c=> c.Id).Skip(skip).ToList<BillingFrequency>() : accessor.BillingFrequencyRepository.GetAll().OrderBy(c=> c.Id).Skip(skip).Take(take).ToList<BillingFrequency>();
            list.Entities.ForEach(c => { c.Description = GetDescription(c.Id); });
            list.TotalQueryCount = accessor.BillingFrequencyRepository.GetAll().Count();
            return list;
        }

        public WCFList<BillingFrequency> GetActiveBillingFrequencies(int skip = 0, int take = -1)
        {
            WCFList<BillingFrequency> list = new WCFList<BillingFrequency>();
            IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.BillingFrequencyRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).ToList<BillingFrequency>() : accessor.BillingFrequencyRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).Take(take).ToList<BillingFrequency>();
            list.TotalQueryCount = accessor.BillingFrequencyRepository.GetAll().Count();
            return list;
        }

        public BillingFrequency GetBillingFrequencyById(int id)
        {
            IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;
            BillingFrequency frequency = accessor.BillingFrequencyRepository.Find(id);
            frequency.Description = GetDescription(frequency.Id);
            return frequency;
        }

        public void DeleteById(int id)
        {
            IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;
            accessor.BillingFrequencyRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(BillingFrequency BillingFrequency)
        {
            if (BillingFrequency != null)
                DeleteById(BillingFrequency.Id);
        }

        public BillingFrequency PostBillingFrequency(BillingFrequency billingFrequency)
        {
            BillingFrequency nullBillingFrequency = null;

            if (billingFrequency != null)
            {
                IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;

                if (billingFrequency.Id == default(int))
                    billingFrequency.ObjectState = ObjectState.Added;
                else
                    billingFrequency.ObjectState = ObjectState.Modified;

                if (billingFrequency.Interval != null)
                {
                    List<String> ignoreProperties = new List<string> {"Interval", };
                    billingFrequency = billingFrequency.clone<BillingFrequency>(ignoreProperties);
                }
 
                accessor.BillingFrequencyRepository.InsertOrUpdateGraph(billingFrequency);
                accessor.Save();

                if (billingFrequency.Interval != null)
                    billingFrequency.Interval = billingFrequency.Interval.clone<Interval>(new List<String> { "BillingFrequencies" });

                billingFrequency.Description = GetDescription(billingFrequency.Id);

                return billingFrequency.ShortCircuitReference<BillingFrequency>();
            }

            return nullBillingFrequency;
        }

        public BillingFrequency MergeBillingFrequency(BillingFrequency billingFrequency)
        {
            BillingFrequency nullBillingFrequency = null;

            if (billingFrequency != null)
            {
                IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;

                if (billingFrequency.Id == default(int))
                    billingFrequency.ObjectState = ObjectState.Added;
                else
                    billingFrequency.ObjectState = ObjectState.Modified;

                if (billingFrequency.Interval != null)
                {
                    List<String> ignoreProperties = new List<string> { "Interval", };
                    billingFrequency = billingFrequency.clone<BillingFrequency>(ignoreProperties);
                }

                accessor.BillingFrequencyRepository.InsertOrUpdateGraph(billingFrequency);
                accessor.Save();

                if (billingFrequency.Interval != null)
                    billingFrequency.Interval = billingFrequency.Interval.clone<Interval>(new List<String> { "BillingFrequencies" });

                billingFrequency.Description = GetDescription(billingFrequency.Id);

                return billingFrequency.ShortCircuitReference<BillingFrequency>();
            }

            return nullBillingFrequency;
        }

        public BillingFrequency GetBillingFrequencyGraph(int ContactID)
        {
            IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;

            if (accessor.BillingFrequencyRepository.Find(ContactID) != null)
            {
                return accessor.BillingFrequencyRepository.AllIncludingSetUnchanged(p => p.Contracts).
                    Where(p => p.Id == ContactID).ToList().FirstOrDefault().ShortCircuitReference(); 
            }
            else
                return null;
        }

        public WCFList<BillingFrequency> GetBillingFrequencies(BaseFilter<BillingFrequency> filter, bool includeGraph)
        {
            WCFList<BillingFrequency> list = new WCFList<BillingFrequency>();
            IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;
            Expression<Func<BillingFrequency, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            filter.IncludeGraph = includeGraph;

            if (includeGraph == true)
            {
                Expression<Func<BillingFrequency, object>>[] includeProperties = {a=> a.Contracts, a=> a.Interval};
                list.Entities = accessor.BillingFrequencyRepository.FindByFilterIncluding(filter, includeProperties).ToList();
                list.Entities.ShortCircuitCollection().ToList();
                
            }
            else
                list.Entities = accessor.BillingFrequencyRepository.FindByFilter(filter).ToList();
            filter.Take = 0;
            filter.Skip = 0;
            list.TotalQueryCount = expression != null ? accessor.BillingFrequencyRepository.FindByFilter(filter).Count() : accessor.BillingFrequencyRepository.GetAll().Count();
            list.Entities.ForEach(a => { a.Description = GetDescription(a.Id); });
            return list;
        }

        public Merchant GetMerchant(BillingFrequency BillingFrequency)
        {
            IMerchantAccessor accessor = _merchantAccessor == null ? new MerchantAccessor(_credentials) : _merchantAccessor;
            return accessor.MerchantRepository.Find(BillingFrequency.MerchantId);
        }

        public void Deactivate(BillingFrequency billingFrequency)
        {
            billingFrequency.IsActive = false;
            this.MergeBillingFrequency(billingFrequency);
        }

        public WCFList<BillingFrequency> GetFrequencyTemplates(int merchantId)
        {
            WCFList<BillingFrequency> list = new WCFList<BillingFrequency>();
            IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;
            BaseFilter<BillingFrequency> filter = new BaseFilter<BillingFrequency>(new BillingFrequency { MerchantId=merchantId, Template=true });
            list.Entities = accessor.BillingFrequencyRepository.FindByFilter(filter).ToList();
            list.TotalQueryCount = accessor.BillingFrequencyRepository.FindByFilter(filter).Count();
            return list;
        }

        public String GetDescription(int frequencyId)
        {
            FrequencyDescriptorGenerator generator = new FrequencyDescriptorGenerator(_credentials, frequencyId, _accessor);
            return generator.ReturnDescription();
        }

        public WCFList<Interval> GetIntervals()
        {
            WCFList<Interval> list = new WCFList<Interval>();
            IBillingFrequencyAccessor accessor = _accessor == null ? new BillingFrequencyAccessor(_credentials) : _accessor;
            list.Entities = accessor.IntervalRepository.GetAll().ToList();
            list.TotalQueryCount = accessor.BillingFrequencyRepository.GetAll().Count();
            return list;
        }
    }
}
