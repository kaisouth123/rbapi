﻿using Bridgepay.Core.Framework;
using Bridgepay.Core.Logging;
using Bridgepay.Core.Security.Factories;
using Bridgepay.Core.Security.Managers;
using Bridgepay.Core.Security.Models;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using System;
using System.Linq;
using OrganizationMap = Bridgepay.RecurringBilling.Models.OrganizationMap;

namespace Bridgepay.RecurringBilling.Business.Services
{
    public static class SecurityService
    {
        public static AuthenticationToken Authenticate(Credentials credentials)
        {
            try
            {
                var authenticator = AuthenticatorFactory.CreateAuthenticator(credentials);
                LogProvider.LoggerInstance.Info(credentials.UserName);
                return authenticator.Authenticate(credentials);
            }
            catch (Exception ex)
            {
                LogProvider.LoggerInstance.Error("Authentication failed.", ex);
                return null;
            }
        }

        public static bool CheckAccess(string userName, int merchantId, int organizationId)
        {
            var searchId = 0;
            User user = UserManager.GetUser(userName);
            if (user == null) return false;
            //if (!user.IsApiUser) return false;

            if (organizationId == 0)
            {
                OrganizationMapAccessor accessor = new OrganizationMapAccessor();
                OrganizationMap organizationMap = accessor.OrganizationMapRepository.All.FirstOrDefault(x => x.UnipayId == merchantId);
                if (organizationMap == null) return false;
                searchId = organizationMap.OrgId;
            }
            else
                searchId = organizationId;

            return UserManager.CheckOrganizationalAccess(searchId, user);
        }
    }
}
