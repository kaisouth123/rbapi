﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.Business.Helpers;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Models;
using System.Linq.Expressions;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class ContactService
    {
        Credential _credentials;
        IContactAccessor _accessor;

        public ContactService(Credential credentials, IContactAccessor accessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
        }

        public ContactService(Credential credentials, VendorContext context)
        {
            _credentials = credentials;
            _accessor = new ContactAccessor(credentials, context);
        }

        public WCFList<Contact> GetAllContacts(int skip = 0, int take = -1)
        {
            WCFList<Contact> list = new WCFList<Contact>();
            IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.ContactRepository.GetAll().OrderBy(c => c.Id).Skip(skip).ToList<Contact>() : accessor.ContactRepository.GetAll().OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Contact>();
            list.TotalQueryCount = accessor.ContactRepository.GetAll().Count();
            return list;
        }

        public WCFList<Contact> GetActiveContacts(int skip = 0, int take = -1)
        {
            WCFList<Contact> list = new WCFList<Contact>();
            IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.ContactRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).ToList<Contact>() : accessor.ContactRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Contact>();
            list.TotalQueryCount = accessor.ContactRepository.GetAll().Count();
            return list;
        }

        public Contact GetContactById(int id)
        {
            IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;
            return accessor.ContactRepository.Find(id);
        }

        public void DeleteById(int id)
        {
            IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;
            accessor.ContactRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(Contact Contact)
        {
            if (Contact != null)
                DeleteById(Contact.Id);
        }

        public Contact PostContact(Contact contact, bool checkGraphs = false)
        {
            Contact nullContact = null;
            if (contact != null)
            {
                IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if(contact.Id == default(int))
                    contact.ObjectState = ObjectState.Added;
                else
                    contact.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (contact.ContactType != null)
                    {
                        if (contact.ContactType.Id != default(int))
                        {
                            int countContactisNotModified = accessor.ContactTypeRepository.FindByFilter(new BaseFilter<ContactType>(contact.ContactType, null, true)).ToList().Count();

                            if (countContactisNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("ContactType");
                            }
                            else
                                contact.ContactType.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            contact.ContactType.ObjectState = ObjectState.Added;
                        }
                    }

                    if (contact.ContactAddress != null)
                    {
                        if (contact.ContactAddress.Id != default(int))
                        {
                            int countContactisNotModified = accessor.AddressRepository.FindByFilter(new BaseFilter<Address>(contact.ContactAddress, null, true)).ToList().Count();

                            if (countContactisNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("ContactAddress");
                            }
                            else
                            {
                                contact.ContactAddress.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            contact.ContactAddress.ObjectState = ObjectState.Added;
                        }
                    }

                    if (contact.ContactAddress == null && contact.ContactType == null && contact.ObjectState != ObjectState.Added)
                    {
                        int countContact = accessor.ContactRepository.FindByFilter(new BaseFilter<Contact>(contact, null, true)).ToList().Count();

                        if (countContact > 0)
                            return contact;
                    }

                    if (copy)
                        contact = contact.clone<Contact>(ignoreProperties); 
                }


                accessor.ContactRepository.InsertOrUpdateGraph(contact);
                accessor.Save();

                if (contact.ContactType != null)
                    contact.ContactType = contact.ContactType.clone<ContactType>(new List<String> { "Contacts" });

                if (contact.ContactAddress != null)
                    contact.ContactAddress = contact.ContactAddress.clone<Address>(new List<String> { "Contacts" });

                return contact.ShortCircuitReference<Contact>();
            }

            return nullContact;
        }

        public Contact MergeContact(Contact contact, bool checkGraphs = false)
        {
            Contact nullContact = null;
            if (contact != null)
            {
                IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (contact.Id == default(int))
                    contact.ObjectState = ObjectState.Added;
                else
                    contact.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (contact.ContactType != null)
                    {
                        if (contact.ContactType.Id != default(int))
                        {
                            int countContactisNotModified = accessor.ContactTypeRepository.FindByFilter(new BaseFilter<ContactType>(contact.ContactType, null, true)).ToList().Count();

                            if (countContactisNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("ContactType");
                            }
                            else
                                contact.ContactType.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            contact.ContactType.ObjectState = ObjectState.Added;
                        }
                    }

                    if (contact.ContactAddress != null)
                    {
                        if (contact.ContactAddress.Id != default(int))
                        {
                            int countContactisNotModified = accessor.AddressRepository.FindByFilter(new BaseFilter<Address>(contact.ContactAddress, null, true)).ToList().Count();

                            if (countContactisNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("ContactAddress");
                            }
                            else
                            {
                                contact.ContactAddress.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            contact.ContactAddress.ObjectState = ObjectState.Added;
                        }
                    }

                    if (contact.ContactAddress == null && contact.ContactType == null && contact.ObjectState != ObjectState.Added)
                    {
                        int countContact = accessor.ContactRepository.FindByFilter(new BaseFilter<Contact>(contact, null, true)).ToList().Count();

                        if (countContact > 0)
                            return contact;
                    }

                    if (copy)
                        contact = contact.clone<Contact>(ignoreProperties);
                }


                accessor.ContactRepository.InsertOrUpdateGraph(contact);
                accessor.Save();

                if (contact.ContactType != null)
                    contact.ContactType = contact.ContactType.clone<ContactType>(new List<String> { "Contacts" });

                if (contact.ContactAddress != null)
                    contact.ContactAddress = contact.ContactAddress.clone<Address>(new List<String> { "Contacts" });

                return contact.ShortCircuitReference<Contact>();
            }

            return nullContact;
        }

        public Contact GetContactsGraph(int ContactID)
        {
            IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;

            if (accessor.ContactRepository.Find(ContactID) != null)
            {
                return accessor.ContactRepository.AllIncludingSetUnchanged(p => p.Products, p => p.FinancialResponsibleParties, p => p.ContactAddress, p => p.ContactType).
                    Where(p => p.Id == ContactID).ToList().FirstOrDefault().ShortCircuitReference();
            }
            else
                return null;
        }

        public WCFList<Contact> GetContacts(BaseFilter<Contact> filter, bool includeGraph)
        {
            WCFList<Contact> list = new WCFList<Contact>();
            IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;
            filter.IncludeGraph = includeGraph;
            if (includeGraph == true)
            {
                Expression<Func<Contact, object>>[] includeProperties = { a => a.ContactAddress, a => a.ContactType, a=>a.Products };
                list.Entities = accessor.ContactRepository.FindByFilterIncluding(filter, includeProperties).ToList();
                list.Entities.ShortCircuitCollection().ToList();
            }
            else
                list.Entities = accessor.ContactRepository.FindByFilter(filter).ToList();
            filter.Take = 0;
            filter.Skip = 0;
            list.TotalQueryCount = accessor.ContactRepository.FindByFilter(filter).Count();
            return list;
        }

        public void Deactivate(Contact Contact)
        {
            Contact.IsActive = false;
            this.MergeContact(Contact);
        }

        public ContactType GetMerchantType()
        {
            IContactAccessor accessor = _accessor == null ? new ContactAccessor(_credentials) : _accessor;
            return accessor.ContactTypeRepository.GetAll().Where(c => c.TypeName == "Merchant").ToList().FirstOrDefault();
        }
    }
}
