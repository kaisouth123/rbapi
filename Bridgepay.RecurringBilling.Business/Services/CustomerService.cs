﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Models;
using System.Linq.Expressions;
using Bridgepay.RecurringBilling.Business.Helpers;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class CustomerService
    {
        Credential _credentials;
        ICustomerAccessor _accessor;

        public CustomerService(Credential credentials, ICustomerAccessor accessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
        }

        public CustomerService(Credential credentials, CustomerContext context)
        {
            _credentials = credentials;
            _accessor = new CustomerAccessor(credentials, context);
        }

        public WCFList<Customer> GetAllCustomers(int skip = 0, int take = -1)
        {
            WCFList<Customer> list = new WCFList<Customer>();
            ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;
            ContactType customerType = GetCustomerType();
            list.Entities = take == -1 ? accessor.CustomerRepository.GetAll().Where(c => c.ContactTypeId == customerType.Id).OrderBy(c => c.Id).Skip(skip).ToList<Customer>() : accessor.CustomerRepository.GetAll().Where(c => c.ContactTypeId == customerType.Id).OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Customer>();
            list.TotalQueryCount = accessor.CustomerRepository.GetAll().Where(c => c.ContactTypeId == customerType.Id).ToList().Count();
            return list;
        }

        public WCFList<Customer> GetActiveCustomers(int skip = 0, int take = -1)
        {
            WCFList<Customer> list = new WCFList<Customer>();
            ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;
            ContactType customerType = accessor.ContactTypeRepository.GetAll().Where(c => c.TypeName == "Customer").ToList().FirstOrDefault();
            list.Entities = take == -1 ? accessor.CustomerRepository.GetAll().Where(c => c.ContactTypeId == customerType.Id && c.IsActive == true).OrderBy(c => c.Id).Skip(skip).ToList<Customer>() : accessor.CustomerRepository.GetAll().Where(c => c.ContactTypeId == customerType.Id && c.IsActive == true).OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Customer>();
            list.TotalQueryCount = accessor.CustomerRepository.GetAll().Where(c => c.ContactTypeId == customerType.Id && c.IsActive== true).Count();
            return list;
        }

        public Customer GetCustomerById(int id)
        {
            ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;
            return accessor.CustomerRepository.Find(id);
        }

        public void DeleteById(int id)
        {
            ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;
            accessor.CustomerRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(Customer Customer)
        {
            if (Customer != null)
                DeleteById(Customer.Id);
        }

        public Customer PostCustomer(Customer customer, bool checkGraphs = false)
        {
            Customer nullCustomer = null;
            if (customer != null)
            {
                ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (customer.Id == default(int))
                    customer.ObjectState = ObjectState.Added;
                else
                    customer.ObjectState = ObjectState.Modified;

                if (customer.ContactTypeId != 2)
                {
                    customer.ContactTypeId = 2;
                    if (customer.ObjectState != ObjectState.Added)
                        customer.ObjectState = ObjectState.Modified;
                }

                if (checkGraphs)
                {
                    if (customer.ContactAddress != null)
                    {
                        if (customer.ContactAddress.Id != default(int))
                        {
                            int countIsNotModified = accessor.CustomerAddressRepository.FindByFilter(new BaseFilter<CustomerAddress>(customer.ContactAddress, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("ContractAddress");
                            }
                            else
                            {
                                customer.ContactAddress.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            customer.ContactAddress.ObjectState = ObjectState.Added;
                        }
                    }

                    if (customer.ContactType != null)
                    {
                        copy = true;
                        ignoreProperties.Add("ContactType");
                    }

                    if (customer.ContactAddress == null && customer.ContactType == null && customer.ObjectState != ObjectState.Added)
                    {
                        int countCustomer = accessor.CustomerRepository.FindByFilter(new BaseFilter<Customer>(customer, null, true)).ToList().Count();

                        if (countCustomer > 0)
                            return customer;
                    }

                    if (copy)
                        customer = customer.clone<Customer>(ignoreProperties); 
                }


                accessor.CustomerRepository.InsertOrUpdateGraph(customer);
                accessor.Save();

                return customer.ShortCircuitReference();
            }

            return nullCustomer;
        }

        public Customer MergeCustomer(Customer customer, bool checkGraphs = false)
        {
            Customer nullCustomer = null;
            if (customer != null)
            {
                ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (customer.Id == default(int))
                    customer.ObjectState = ObjectState.Added;
                else
                    customer.ObjectState = ObjectState.Modified;

                if (customer.ContactTypeId != 2)
                {
                    customer.ContactTypeId = 2;
                    if (customer.ObjectState != ObjectState.Added)
                        customer.ObjectState = ObjectState.Modified;
                }

                if (checkGraphs)
                {
                    if (customer.ContactAddress != null)
                    {
                        if (customer.ContactAddress.Id != default(int))
                        {
                            int countIsNotModified = accessor.CustomerAddressRepository.FindByFilter(new BaseFilter<CustomerAddress>(customer.ContactAddress, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("ContractAddress");
                            }
                            else
                            {
                                customer.ContactAddress.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            customer.ContactAddress.ObjectState = ObjectState.Added;
                        }
                    }

                    if (customer.ContactType != null)
                    {
                        copy = true;
                        ignoreProperties.Add("ContactType");
                    }

                    if (customer.ContactAddress == null && customer.ContactType == null && customer.ObjectState != ObjectState.Added)
                    {
                        int countCustomer = accessor.CustomerRepository.FindByFilter(new BaseFilter<Customer>(customer, null, true)).ToList().Count();

                        if (countCustomer > 0)
                            return customer;
                    }

                    if (copy)
                        customer = customer.clone<Customer>(ignoreProperties);
                }


                accessor.CustomerRepository.InsertOrUpdateGraph(customer);
                accessor.Save();

                return customer.ShortCircuitReference();
            }

            return nullCustomer;
        }

        public List<FinancialResponsibleParty> GetFinancialResponsibleParties(int CustomerID)
        {
            ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;

            if (accessor.CustomerRepository.Find(CustomerID) != null)
            {
                return accessor.CustomerRepository.AllIncluding(p => p.FinancialResponsibleParties).
                    Where(p => p.Id == CustomerID).ToList().FirstOrDefault().FinancialResponsibleParties.ToList();
            }
            else
                return null;
        }

        public Customer GetCustomerGraph(int CustomerID)
        {
            ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;

            if (accessor.CustomerRepository.Find(CustomerID) != null)
            {
                return accessor.CustomerRepository.AllIncluding(p => p.FinancialResponsibleParties, p => p.ContactAddress, p => p.ContactType).
                     Where(p => p.Id == CustomerID).ToList().FirstOrDefault().ShortCircuitReference();
            }
            else
                return null;
        }

        public WCFList<Customer> GetCustomers(BaseFilter<Customer> filter, bool includeGraph)
        {
            WCFList<Customer> list = new WCFList<Customer>();
            ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;
            ContactType customerType = accessor.ContactTypeRepository.GetAll().Where(c => c.TypeName == "Customer").ToList().FirstOrDefault();
            filter.IncludeGraph = includeGraph;
            if (includeGraph == true)
            {
                Expression<Func<Customer, object>>[] includeProperties = { a => a.FinancialResponsibleParties, a=>a.ContactType, a=>a.ContactAddress };
                list.Entities = accessor.CustomerRepository.FindByFilterIncluding(filter, includeProperties).Where(c => c.ContactTypeId == customerType.Id).ToList();
                list.Entities = list.Entities.ShortCircuitCollection().ToList();
            }
            else
                list.Entities = accessor.CustomerRepository.FindByFilter(filter).Where(c => c.ContactTypeId == customerType.Id).ToList();
            filter.Take = 0;
            filter.Skip = 0;
            list.TotalQueryCount = accessor.CustomerRepository.FindByFilter(filter).Where(c => c.ContactTypeId == customerType.Id).Count();
            return list;
        }

        public void Deactivate(Customer Customer)
        {
            Customer.IsActive = false;
            this.MergeCustomer(Customer);
        }

        public ContactType GetCustomerType()
        {
            ICustomerAccessor accessor = _accessor == null ? new CustomerAccessor(_credentials) : _accessor;
            return accessor.ContactTypeRepository.GetAll().Where(c => c.TypeName == "Customer").ToList().FirstOrDefault();
        }
    }
}
