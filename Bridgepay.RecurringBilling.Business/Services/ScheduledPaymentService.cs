﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Helpers;
using System.Linq.Expressions;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class ScheduledPaymentService
    {
        Credential _credentials;
        IScheduledPaymentAccessor _accessor;

        public ScheduledPaymentService(Credential credentials, IScheduledPaymentAccessor accessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
        }

        public ScheduledPaymentService(Credential credentials, VendorContext context)
        {
            _credentials = credentials;
            _accessor = new ScheduledPaymentAccessor(credentials, context);
        }

        public WCFList<ScheduledPayment> GetScheduledPayments(int skip = 0, int take = -1)
        {
            WCFList<ScheduledPayment> list = new WCFList<ScheduledPayment>();
            IScheduledPaymentAccessor accessor = _accessor == null ? new ScheduledPaymentAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.ScheduledPaymentRepository.GetAll().OrderBy(c => c.Id).Skip(skip).ToList<ScheduledPayment>() : accessor.ScheduledPaymentRepository.GetAll().OrderBy(c => c.Id).Skip(skip).Take(take).ToList<ScheduledPayment>();
            list.TotalQueryCount = accessor.ScheduledPaymentRepository.GetAll().Count();
            return list;
        }

        public ScheduledPayment GetScheduledPaymentById(int id)
        {
            IScheduledPaymentAccessor accessor = _accessor == null ? new ScheduledPaymentAccessor(_credentials) : _accessor;
            return accessor.ScheduledPaymentRepository.Find(id);
        }

        public void DeleteById(int id)
        {
            IScheduledPaymentAccessor accessor = _accessor == null ? new ScheduledPaymentAccessor(_credentials) : _accessor;
            accessor.ScheduledPaymentRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(ScheduledPayment ScheduledPayment)
        {
            if (ScheduledPayment != null)
                DeleteById(ScheduledPayment.Id);
        }

        public ScheduledPayment PostScheduledPayment(ScheduledPayment scheduledPayment, bool checkGraphs = false)
        {
            ScheduledPayment nullScheduledPayment = null;
            if (scheduledPayment != null)
            {
                IScheduledPaymentAccessor accessor = _accessor == null ? new ScheduledPaymentAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (scheduledPayment.Id == default(int))
                    scheduledPayment.ObjectState = ObjectState.Added;
                else
                    scheduledPayment.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (scheduledPayment.Contract != null)
                    {
                        if (scheduledPayment.Contract.Id != default(int))
                        {
                            int countIsNotModified = accessor.ContractRepository.FindByFilter(new BaseFilter<Contract>(scheduledPayment.Contract, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Contract");
                            }
                            else
                            {
                                scheduledPayment.Contract.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            scheduledPayment.Contract.ObjectState = ObjectState.Added;
                        }
                    }


                    if (copy)
                        scheduledPayment = scheduledPayment.clone<ScheduledPayment>(ignoreProperties); 
                }


                accessor.ScheduledPaymentRepository.InsertOrUpdateGraph(scheduledPayment);
                accessor.Save();

                return scheduledPayment.ShortCircuitReference();
            }

            return nullScheduledPayment;
        }

        public ScheduledPayment MergeScheduledPayment(ScheduledPayment scheduledPayment, bool checkGraphs = false)
        {
            ScheduledPayment nullScheduledPayment = null;
            if (scheduledPayment != null)
            {
                IScheduledPaymentAccessor accessor = _accessor == null ? new ScheduledPaymentAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (scheduledPayment.Id == default(int))
                    scheduledPayment.ObjectState = ObjectState.Added;
                else
                    scheduledPayment.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (scheduledPayment.Contract != null)
                    {
                        if (scheduledPayment.Contract.Id != default(int))
                        {
                            int countIsNotModified = accessor.ContractRepository.FindByFilter(new BaseFilter<Contract>(scheduledPayment.Contract, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Contract");
                            }
                            else
                            {
                                scheduledPayment.Contract.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            scheduledPayment.Contract.ObjectState = ObjectState.Added;
                        }
                    }


                    if (copy)
                        scheduledPayment = scheduledPayment.clone<ScheduledPayment>(ignoreProperties);
                }


                accessor.ScheduledPaymentRepository.InsertOrUpdateGraph(scheduledPayment);
                accessor.Save();

                return scheduledPayment.ShortCircuitReference();
            }

            return nullScheduledPayment;
        }

        public ScheduledPayment GetScheduledPaymentGraph(int ScheduledPaymentID)
        {
            IScheduledPaymentAccessor accessor = _accessor == null ? new ScheduledPaymentAccessor(_credentials) : _accessor;

            if (accessor.ScheduledPaymentRepository.Find(ScheduledPaymentID) != null)
            {
                return accessor.ScheduledPaymentRepository.AllIncludingSetUnchanged(p => p.Contract).
                    Where(p => p.Id == ScheduledPaymentID).ToList().FirstOrDefault().ShortCircuitReference();
            }
            else
                return null;
        }

        public WCFList<ScheduledPayment> GetScheduledPayments(BaseFilter<ScheduledPayment> filter, bool includeGraph = false)
        {
            WCFList<ScheduledPayment> list = new WCFList<ScheduledPayment>();
            IScheduledPaymentAccessor accessor = _accessor == null ? new ScheduledPaymentAccessor(_credentials) : _accessor;
            filter.IncludeGraph = includeGraph;
            Expression<Func<ScheduledPayment, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            if (includeGraph == true)
            {
                Expression<Func<ScheduledPayment, object>>[] includeProperties = { a => a.Contract };
                list.Entities = accessor.ScheduledPaymentRepository.FindByFilterIncluding(filter, includeProperties).ToList();
                list.Entities.ShortCircuitCollection().ToList();
            }
            else
                list.Entities = accessor.ScheduledPaymentRepository.FindByFilter(filter).ToList();
            filter.Take = 0;
            filter.Skip = 0;
            list.TotalQueryCount = accessor.ScheduledPaymentRepository.FindByFilter(filter).Count();
            return list;
        }
    }
}
