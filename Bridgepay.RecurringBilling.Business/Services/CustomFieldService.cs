﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Helpers;
using System.Linq.Expressions;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class CustomFieldService
    {
        Credential _credentials;
        ICustomFieldAccessor _accessor;

        public CustomFieldService(Credential credentials, ICustomFieldAccessor accessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
        }

        public CustomFieldService(Credential credentials)
        {
            _credentials = credentials;
            _accessor = new CustomFieldAccessor(credentials);
        }

        public WCFList<CustomField> GetCustomFields(int skip = 0, int take = -1)
        {
            WCFList<CustomField> list = new WCFList<CustomField>();
            ICustomFieldAccessor accessor = _accessor == null ? new CustomFieldAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.CustomFieldRepository.GetAll().OrderBy(c => c.Id).Skip(skip).ToList<CustomField>() : accessor.CustomFieldRepository.GetAll().OrderBy(c => c.Id).Skip(skip).Take(take).ToList<CustomField>();
            list.TotalQueryCount = accessor.CustomFieldRepository.GetAll().Count();
            return list;
        }

        public CustomField GetCustomFieldById(int id)
        {
            ICustomFieldAccessor accessor = _accessor == null ? new CustomFieldAccessor(_credentials) : _accessor;
            return accessor.CustomFieldRepository.Find(id);
        }

        public void DeleteById(int id)
        {
            ICustomFieldAccessor accessor = _accessor == null ? new CustomFieldAccessor(_credentials) : _accessor;
            accessor.CustomFieldRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(CustomField CustomField)
        {
            if (CustomField != null)
                DeleteById(CustomField.Id);
        }

        public CustomField PostCustomField(CustomField CustomField, bool checkGraphs = false)
        {
            CustomField nullCustomField = null;
            if (CustomField != null)
            {
                ICustomFieldAccessor accessor = _accessor == null ? new CustomFieldAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (CustomField.Id == default(int))
                    CustomField.ObjectState = ObjectState.Added;
                else
                    CustomField.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (CustomField.CustomFieldActivations != null)
                    {
                        if (CustomField.CustomizableEntity.Id != default(int))
                        {
                            int countIsNotModified = accessor.CustomizableEntityRepository.FindByFilter(new BaseFilter<CustomizableEntity>(CustomField.CustomizableEntity, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("CustomizableEntity");
                            }
                            else
                            {
                                CustomField.CustomizableEntity.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            CustomField.CustomizableEntity.ObjectState = ObjectState.Added;
                        }
                    }


                    if (copy)
                        CustomField = CustomField.clone<CustomField>(ignoreProperties); 
                }


                accessor.CustomFieldRepository.InsertOrUpdateGraph(CustomField);
                accessor.Save();

                return CustomField.ShortCircuitReference();
            }

            return nullCustomField;
        }

        public CustomField MergeCustomField(CustomField CustomField, bool checkGraphs = false)
        {
            CustomField nullCustomField = null;
            if (CustomField != null)
            {
                ICustomFieldAccessor accessor = _accessor == null ? new CustomFieldAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (CustomField.Id == default(int))
                    CustomField.ObjectState = ObjectState.Added;
                else
                    CustomField.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (CustomField.CustomizableEntity != null)
                    {
                        if (CustomField.CustomizableEntity.Id != default(int))
                        {
                            int countIsNotModified = accessor.CustomizableEntityRepository.FindByFilter(new BaseFilter<CustomizableEntity>(CustomField.CustomizableEntity, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("CustomizableEntity");
                            }
                            else
                            {
                                CustomField.CustomizableEntity.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            CustomField.CustomizableEntity.ObjectState = ObjectState.Added;
                        }
                    }


                    if (copy)
                        CustomField = CustomField.clone<CustomField>(ignoreProperties);
                }


                accessor.CustomFieldRepository.InsertOrUpdateGraph(CustomField);
                accessor.Save();

                return CustomField.ShortCircuitReference();
            }

            return nullCustomField;
        }

        public CustomField GetCustomFieldGraph(int CustomFieldID)
        {
            ICustomFieldAccessor accessor = _accessor == null ? new CustomFieldAccessor(_credentials) : _accessor;

            if (accessor.CustomFieldRepository.Find(CustomFieldID) != null)
            {
                return accessor.CustomFieldRepository.AllIncluding(p => p.CustomizableEntity).
                    Where(p => p.Id == CustomFieldID).ToList().FirstOrDefault().ShortCircuitReference();
            }
            else
                return null;
        }

        public WCFList<CustomField> GetCustomFields(BaseFilter<CustomField> filter, bool includeGraph = false)
        {
            WCFList<CustomField> list = new WCFList<CustomField>();
            ICustomFieldAccessor accessor = _accessor == null ? new CustomFieldAccessor(_credentials) : _accessor;
            filter.IncludeGraph = includeGraph;
            Expression<Func<CustomField, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            if (includeGraph == true)
            {
                Expression<Func<CustomField, object>>[] includeProperties = { a => a.CustomizableEntity };
                list.Entities = accessor.CustomFieldRepository.FindByFilterIncluding(filter, includeProperties).ToList();
                list.Entities.ShortCircuitCollection().ToList();
            }
            else
                list.Entities = accessor.CustomFieldRepository.FindByFilter(filter).ToList();
            filter.Take = 0;
            filter.Skip = 0;
            list.TotalQueryCount = accessor.CustomFieldRepository.FindByFilter(filter).Count();
            return list;
        }
    }
}
