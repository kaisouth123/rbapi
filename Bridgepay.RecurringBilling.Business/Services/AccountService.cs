﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Models;
using System.Linq.Expressions;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.DataLayer.Interfaces;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class AccountService
    {
        private Credential _credentials;
        private Credential _merchantCredentials;
        private IAccountAccessor _accessor;
        private IMerchantAccessor _merchantAccessor;

        public AccountService(Credential credentials, IAccountAccessor accessor = null, IMerchantAccessor merchantAccessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
            _merchantAccessor = merchantAccessor;
        }

        public AccountService(Credential credentials, VendorContext context, MerchantContext mcontext)
        {
            _credentials = credentials;
            _accessor = new AccountAccessor(credentials, context);
            _merchantAccessor = new MerchantAccessor(credentials, mcontext);
        }

        public AccountService(Credential credentials, Credential merchantCredentials, IAccountAccessor accessor = null, IMerchantAccessor merchantAccessor = null)
        {
            _credentials = credentials;
            _merchantCredentials = merchantCredentials;
            _accessor = accessor;
            _merchantAccessor = merchantAccessor;
        }

        public WCFList<Account> GetAllAccounts(int skip = 0, int take = -1)
        {
            WCFList<Account> list = new WCFList<Account>();
            IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;
            list.Entities =  take == -1 ? accessor.AccountRepository.GetAll().OrderBy(c=> c.Id).Skip(skip).ToList<Account>() : accessor.AccountRepository.GetAll().OrderBy(c=> c.Id).Skip(skip).Take(take).ToList<Account>();
            list.TotalQueryCount = accessor.AccountRepository.GetAll().Count();
            return list;
        }

        public WCFList<Account> GetActiveAccounts(int skip = 0, int take = -1)
        {
            WCFList<Account> list = new WCFList<Account>();
            IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.AccountRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).ToList<Account>() : accessor.AccountRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Account>();
            list.TotalQueryCount = accessor.AccountRepository.GetAll().Count();
            return list;
        }

        public Account GetAccountById(int id)
        {
            IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;
            return accessor.AccountRepository.Find(id);
        }

        public void DeleteById(int id)
        {
            IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;
            accessor.AccountRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(Account Account)
        {
            if (Account != null)
                DeleteById(Account.Id);
        }

        public Account PostAccount(Account Account)
        {
            Account nullAccount = null;

            if (Account != null)
            {
                IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;
                Account.ModifiedOn = DateTime.UtcNow;

                if (Account.Id == default(int))
                {
                    Account.ObjectState = ObjectState.Added;
                }
                else
                    Account.ObjectState = ObjectState.Modified;

                accessor.AccountRepository.InsertOrUpdateGraph(Account);
                accessor.Save();
                return Account.ShortCircuitReference<Account>();
            }

            return nullAccount;
        }

        public Account MergeAccount(Account Account)
        {
            Account nullAccount = null;
            if (Account != null)
            {
                IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;
                Account.ModifiedOn = DateTime.UtcNow;

                if (Account.Id == default(int))
                    Account.ObjectState = ObjectState.Added;
                else
                    Account.ObjectState = ObjectState.Modified;

                accessor.AccountRepository.InsertOrUpdateGraph(Account);
                accessor.Save();

                return Account.ShortCircuitReference<Account>();
            }

            return nullAccount;
        }

        public Account GetAccountGraph(int ContactID)
        {
            IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;

            if (accessor.AccountRepository.Find(ContactID) != null)
            {
                return  accessor.AccountRepository.AllIncludingSetUnchanged(p => p.Contracts, p => p.FinancialResponsibleParties).
                    Where(p => p.Id == ContactID).ToList().FirstOrDefault().ShortCircuitReference();
            }
            else
                return null;
        }

        public WCFList<Account> GetAccounts(BaseFilter<Account> filter, bool includeGraph = false, Expression<Func<Account, object>>[] includes = null)
        {
            WCFList<Account> list = new WCFList<Account>();
            IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;
            filter.IncludeGraph = includeGraph;
            if (includeGraph == true)
            {
                Expression<Func<Account, object>>[] includeProperties = { a => a.Contracts, a => a.FinancialResponsibleParties, };
                if (includes != null)
                    includeProperties = includes;
                list.Entities = accessor.AccountRepository.FindByFilterIncluding(filter, includeProperties).ToList();
                list.Entities = list.Entities.ShortCircuitCollection().ToList();
            }
            else
                list.Entities = accessor.AccountRepository.FindByFilter(filter).ToList();
            filter.Take = 0;
            filter.Skip = 0;
            list.TotalQueryCount = accessor.AccountRepository.FindByFilter(filter).Count();
            return list;
        }

        public Merchant GetMerchant(Account account)
        {
            IMerchantAccessor accessor = _merchantAccessor == null ? new MerchantAccessor(_credentials) : _merchantAccessor;
            return accessor.MerchantRepository.Find(account.MerchantId);
        }

        public void Deactivate(Account account)
        {
            account.IsActive = false;
            this.MergeAccount(account);
        }

        public WCFList<FinancialResponsibleParty> GetPartiesByMerchant(int merchantId, BaseFilter<FinancialResponsibleParty> filter, bool includeGraph)
        {
            WCFList<FinancialResponsibleParty> list = new WCFList<FinancialResponsibleParty>();
            IAccountAccessor accessor = _accessor == null ? new AccountAccessor(_credentials) : _accessor;
            Expression<Func<FinancialResponsibleParty, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            Expression<Func<FinancialResponsibleParty, object>>[] includeProperties = { a=> a.Contact, a=> a.Contact.ContactAddress };
            filter.OrderBy = string.IsNullOrEmpty(filter.OrderBy) ? "Id" : filter.OrderBy;
            if (includeGraph)
            {
                if (filter.Take == -1)
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).Where(expression).Include(f => f.Contact).Include(f => f.Contact.ContactAddress).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<FinancialResponsibleParty>() :
                                    (from accounts in accessor.AccountRepository.GetAll()
                                     where accounts.MerchantId == merchantId
                                     select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).Include(f => f.Contact).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<FinancialResponsibleParty>();
                }
                else
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).Where(expression).Include(f => f.Contact).Include(f => f.Contact.ContactAddress).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<FinancialResponsibleParty>() :
                                    (from accounts in accessor.AccountRepository.GetAll()
                                     where accounts.MerchantId == merchantId
                                     select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).Include(f => f.Contact).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<FinancialResponsibleParty>();
                }
                list.Entities = list.Entities.ShortCircuitCollection().ToList();
            }
            else
            {
                if (filter.Take == -1)
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).Where(expression).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<FinancialResponsibleParty>() :
                                    (from accounts in accessor.AccountRepository.GetAll()
                                     where accounts.MerchantId == merchantId
                                     select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<FinancialResponsibleParty>();

                }
                else
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).Where(expression).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<FinancialResponsibleParty>() :
                                    (from accounts in accessor.AccountRepository.GetAll()
                                     where accounts.MerchantId == merchantId
                                     select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).OrderBy(c => c.Id).Skip(filter.Skip).OrderByExt(filter.OrderBy, filter.SortDesc).ToList<FinancialResponsibleParty>();
                }
            }

            list.TotalQueryCount = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                         where accounts.MerchantId == merchantId
                                                         select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).Where(expression).Include(f => f.Contact).Count<FinancialResponsibleParty>() :
                                  (from accounts in accessor.AccountRepository.GetAll()
                                   where accounts.MerchantId == merchantId
                                   select accounts).Include(a => a.FinancialResponsibleParties).SelectMany(a => a.FinancialResponsibleParties).Include(f => f.Contact).Count<FinancialResponsibleParty>();

            return list;
        }
    }
}
