﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqKit;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Interfaces;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using Bridgepay.RecurringBilling.Business.Enums;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class FinancialResponsiblePartyService : IOrderType<FinancialResponsibleParty>
    {
        Credential _credentials;
        IFinancialResponsiblePartyAccessor _accessor;

        public FinancialResponsiblePartyService(Credential credentials, IFinancialResponsiblePartyAccessor accessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
        }

        public FinancialResponsiblePartyService(Credential credentials, CustomerContext context)
        {
            _credentials = credentials;
            _accessor = new FinancialResponsiblePartyAccessor(credentials, context);
        }

        public WCFList<FinancialResponsibleParty> GetFinancialResponsibleParties(int skip = 0, int take = -1)
        {
            WCFList<FinancialResponsibleParty> list = new WCFList<FinancialResponsibleParty>();
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.FinancialResponsiblePartyRepository.GetAll().OrderBy(c => c.Id).Skip(skip).ToList<FinancialResponsibleParty>() : accessor.FinancialResponsiblePartyRepository.GetAll().OrderBy(c => c.Id).Skip(skip).Take(take).ToList<FinancialResponsibleParty>();
            list.TotalQueryCount = accessor.FinancialResponsiblePartyRepository.GetAll().Count();
            return list;
        }

        public WCFList<FinancialResponsibleParty> GetActiveFinancialResponsibleParties(int skip = 0, int take = -1)
        {
            WCFList<FinancialResponsibleParty> list = new WCFList<FinancialResponsibleParty>();
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.FinancialResponsiblePartyRepository.GetAll().Where(c => c.IsActive == true).OrderBy(c => c.Id).Skip(skip).ToList<FinancialResponsibleParty>() : accessor.FinancialResponsiblePartyRepository.GetAll().Where(c => c.IsActive == true).OrderBy(c => c.Id).Skip(skip).Take(take).ToList<FinancialResponsibleParty>();
            list.TotalQueryCount = accessor.FinancialResponsiblePartyRepository.GetAll().Count();
            return list;
        }

        public FinancialResponsibleParty GetFinancialResponsiblePartyById(int id)
        {
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
            return accessor.FinancialResponsiblePartyRepository.Find(id);
        }

        public void DeleteById(int id)
        {
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
            accessor.FinancialResponsiblePartyRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(FinancialResponsibleParty FinancialResponsibleParty)
        {
            if (FinancialResponsibleParty != null)
                DeleteById(FinancialResponsibleParty.Id);
        }

        public FinancialResponsibleParty PostFinancialResponsibleParty(FinancialResponsibleParty financialResponsibleParty, bool checkGraphs = false)
        {
            FinancialResponsibleParty nullFinancialResponsibleParty = null;
            if (financialResponsibleParty != null)
            {
                IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                //if (financialResponsibleParty.Id == default(int))
                //    financialResponsibleParty.ObjectState = ObjectState.Added;
                //else
                //    financialResponsibleParty.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (financialResponsibleParty.Customer != null)
                    {
                        if (financialResponsibleParty.Customer.Id != default(int))
                        {
                            int countIsNotModified = accessor.CustomerRepository.FindByFilter(new BaseFilter<Customer>(financialResponsibleParty.Customer, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Customer");
                            }
                            else
                            {
                                financialResponsibleParty.Customer.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            financialResponsibleParty.Customer.ObjectState = ObjectState.Added;
                        }
                    }

                    if (financialResponsibleParty.Account != null)
                    {
                        if (financialResponsibleParty.Account.Id != default(int))
                        {
                            int countIsNotModified = accessor.AccountRepository.FindByFilter(new BaseFilter<Account>(financialResponsibleParty.Account, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Account");
                            }
                            else
                            {
                                financialResponsibleParty.Account.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            financialResponsibleParty.Account.ObjectState = ObjectState.Added;
                        }
                    }

                    if (financialResponsibleParty.Contact != null)
                    {
                        copy = true;
                        ignoreProperties.Add("Contact");
                    }

                    if (financialResponsibleParty.Customer == null && financialResponsibleParty.Account == null && financialResponsibleParty.ObjectState != ObjectState.Added)
                    {
                        int countFinancialResponsibleParty = accessor.FinancialResponsiblePartyRepository.FindByFilter(new BaseFilter<FinancialResponsibleParty>(financialResponsibleParty, null, true)).ToList().Count();

                        if (countFinancialResponsibleParty > 0)
                            return financialResponsibleParty;
                    }

                    if (copy)
                        financialResponsibleParty = financialResponsibleParty.clone<FinancialResponsibleParty>(ignoreProperties);
                }
                if (financialResponsibleParty.Order == null || financialResponsibleParty.Order == 0)
                { financialResponsibleParty.Order = 1; }
                List<FinancialResponsibleParty> parties = accessor.FinancialResponsiblePartyRepository.GetAll().Where(f => f.AccountId == financialResponsibleParty.AccountId).OrderBy(f=>f.Order).ToList();
                int i = 1;
                foreach (FinancialResponsibleParty party in parties)
                {
                    if(party.Order==null)
                    {
                        party.Order = i;
                    }
                    if(party.Order>= financialResponsibleParty.Order)
                    { party.Order++; }
                    party.ObjectState = ObjectState.Modified;
                    accessor.FinancialResponsiblePartyRepository.InsertOrUpdate(party);
                    i++;
                }
                accessor.FinancialResponsiblePartyRepository.InsertOrUpdateGraph(financialResponsibleParty);
                accessor.Save();

                //if (financialResponsibleParty.Customer != null)
                //{
                //    financialResponsibleParty.Customer = financialResponsibleParty.Customer.clone<Customer>(new List<String> { "FinancialResponsibleParties" });
                //    if (financialResponsibleParty.Customer.ContactAddress != null)
                //        financialResponsibleParty.Customer.ContactAddress.Customers = null;
                //}


                return financialResponsibleParty.ShortCircuitReference();
            }

            return nullFinancialResponsibleParty;
        }

        public FinancialResponsibleParty MergeFinancialResponsibleParty(FinancialResponsibleParty financialResponsibleParty, bool checkGraphs = false)
        {
            FinancialResponsibleParty nullFinancialResponsibleParty = null;
            if (financialResponsibleParty != null)
            {
                IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                //if (financialResponsibleParty.Id == default(int))
                //    financialResponsibleParty.ObjectState = ObjectState.Added;
                //else
                //    financialResponsibleParty.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (financialResponsibleParty.Customer != null)
                    {
                        if (financialResponsibleParty.Customer.Id != default(int))
                        {
                            int countIsNotModified = accessor.CustomerRepository.FindByFilter(new BaseFilter<Customer>(financialResponsibleParty.Customer, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Customer");
                            }
                            else
                            {
                                financialResponsibleParty.Customer.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            financialResponsibleParty.Customer.ObjectState = ObjectState.Added;
                        }
                    }

                    if (financialResponsibleParty.Account != null)
                    {
                        if (financialResponsibleParty.Account.Id != default(int))
                        {
                            int countIsNotModified = accessor.AccountRepository.FindByFilter(new BaseFilter<Account>(financialResponsibleParty.Account, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Account");
                            }
                            else
                            {
                                financialResponsibleParty.Account.ObjectState = ObjectState.Modified;
                            }
                        }
                        else
                        {
                            financialResponsibleParty.Account.ObjectState = ObjectState.Added;
                        }
                    }

                    if (financialResponsibleParty.Contact != null)
                    {
                        copy = true;
                        ignoreProperties.Add("Contact");
                    }

                    if (financialResponsibleParty.Customer == null && financialResponsibleParty.Account == null && financialResponsibleParty.ObjectState != ObjectState.Added)
                    {
                        int countFinancialResponsibleParty = accessor.FinancialResponsiblePartyRepository.FindByFilter(new BaseFilter<FinancialResponsibleParty>(financialResponsibleParty, null, true)).ToList().Count();

                        if (countFinancialResponsibleParty > 0)
                            return financialResponsibleParty;
                    }

                    if (copy)
                        financialResponsibleParty = financialResponsibleParty.clone<FinancialResponsibleParty>(ignoreProperties);
                }

                accessor.FinancialResponsiblePartyRepository.InsertOrUpdateGraph(financialResponsibleParty);
                accessor.Save();

                //if (financialResponsibleParty.Customer != null)
                //{
                //    financialResponsibleParty.Customer = financialResponsibleParty.Customer.clone<Customer>(new List<String> { "FinancialResponsibleParties" });
                //    if (financialResponsibleParty.Customer.ContactAddress != null)
                //        financialResponsibleParty.Customer.ContactAddress.Customers = null;
                //}

                return financialResponsibleParty.ShortCircuitReference();
            }

            return nullFinancialResponsibleParty;
        }

        public List<Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod> GetPaymentMethods(int financialResponsiblePartyID)
        {
            List<Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod> list = new List<Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod>();
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
            FinancialResponsibleParty party = accessor.FinancialResponsiblePartyRepository.Find(financialResponsiblePartyID);
            Bridgepay.RecurringBilling.Business.ServiceAdaptors.WalletAdaptor adaptor = new ServiceAdaptors.WalletAdaptor();

            return party.Guid != null ? adaptor.GetPaymentMethodsById((Guid)party.Guid) : list;
        }

        public FinancialResponsibleParty GetFinancialResponsiblePartyGraph(int FinancialResponsiblePartyID)
        {
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;

            if (accessor.FinancialResponsiblePartyRepository.Find(FinancialResponsiblePartyID) != null)
            {
                return accessor.FinancialResponsiblePartyRepository.AllIncluding(p => p.Account, p => p.Customer, p => p.Customer.ContactAddress).
                    Where(p => p.Id == FinancialResponsiblePartyID).ToList().FirstOrDefault().ShortCircuitReference();
            }
            else
                return null;
        }

        public WCFList<FinancialResponsibleParty> GetFinancialResponsibleParties(BaseFilter<FinancialResponsibleParty> filter, bool includeGraph)
        {
            WCFList<FinancialResponsibleParty> list = new WCFList<FinancialResponsibleParty>();
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
            filter.IncludeGraph = includeGraph;
            Expression<Func<FinancialResponsibleParty, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            if (includeGraph == true)
            {
                Expression<Func<FinancialResponsibleParty, object>>[] includeProperties = { a => a.Account, a => a.Customer, a => a.Customer.ContactAddress };
                list.Entities = accessor.FinancialResponsiblePartyRepository.FindByFilterIncluding(filter, includeProperties).ToList();
                list.Entities.ShortCircuitCollection().ToList();
            }
            else
                list.Entities = accessor.FinancialResponsiblePartyRepository.FindByFilter(filter).ToList();
            filter.Take = 0;
            filter.Skip = 0;
            list.TotalQueryCount = expression != null ? accessor.FinancialResponsiblePartyRepository.FindByFilter(filter).Count() : accessor.FinancialResponsiblePartyRepository.GetAll().Count();

            return list;
        }

        public void Deactivate(FinancialResponsibleParty FinancialResponsibleParty)
        {
            FinancialResponsibleParty.IsActive = false;
            this.MergeFinancialResponsibleParty(FinancialResponsibleParty);
        }

        public int ReturnMaxOrder(int entityId)
        {
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;
            List<FinancialResponsibleParty> list = accessor.FinancialResponsiblePartyRepository.GetAll().Where(f => f.AccountId == entityId && f.Order != null).ToList();
            return list.Count > 0 ? (int)list.Max(c => c.Order) : 0;
        }

        public void SetNewOrder(FinancialResponsibleParty entity)
        {
            IFinancialResponsiblePartyAccessor accessor = _accessor == null ? new FinancialResponsiblePartyAccessor(_credentials) : _accessor;

            List<FinancialResponsibleParty> parties = accessor.FinancialResponsiblePartyRepository.GetAll().Where(f => f.AccountId == entity.AccountId).ToList();

            parties.Where(p => p.Order >= entity.Order).ToList().ForEach(p => p.Order++);

            foreach (FinancialResponsibleParty party in parties)
            {
                party.ObjectState = ObjectState.Modified;
                accessor.FinancialResponsiblePartyRepository.InsertOrUpdate(party);
            }
            accessor.Save();
        }
    }
}
