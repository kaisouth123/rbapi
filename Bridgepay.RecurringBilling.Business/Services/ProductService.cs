﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Helpers;
using System.Linq.Expressions;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class ProductService
    {
        Credential _credentials;
        IProductAccessor _accessor;

        public ProductService(Credential credentials, IProductAccessor accessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
        }

        public ProductService(Credential credentials, VendorContext context)
        {
            _credentials = credentials;
            _accessor = new ProductAccessor(credentials, context);
        }

        public WCFList<Product> GetAllProducts(int skip = 0, int take = -1)
        {
            WCFList<Product> list = new WCFList<Product>();
            IProductAccessor accessor = _accessor == null ? new ProductAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.ProductRepository.GetAll().OrderBy(c => c.Id).Skip(skip).ToList<Product>() : accessor.ProductRepository.GetAll().OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Product>();
            list.TotalQueryCount = accessor.ProductRepository.GetAll().Count();
            return list;
        }

        public WCFList<Product> GetActiveProducts(int skip = 0, int take = -1)
        {
            WCFList<Product> list = new WCFList<Product>();
            IProductAccessor accessor = _accessor == null ? new ProductAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.ProductRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).ToList<Product>() : accessor.ProductRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Product>();
            list.TotalQueryCount = accessor.ProductRepository.GetAll().Count();
            return list;
        }

        public Product GetProductById(int id)
        {
            IProductAccessor accessor = _accessor == null ? new ProductAccessor(_credentials) : _accessor;
            return accessor.ProductRepository.Find(id);
        }

        public void DeleteById(int id)
        {
            IProductAccessor accessor = _accessor == null ? new ProductAccessor(_credentials) : _accessor;
            accessor.ProductRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(Product product)
        {
            if (product != null)
                DeleteById(product.Id);
        }

        public Product PostProduct(Product product, bool checkGraphs = false)
        {
            Product nullProduct = null;
            if (product != null)
            {
                IProductAccessor accessor = _accessor == null ? new ProductAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (product.Id == default(int))
                    product.ObjectState = ObjectState.Added;
                else
                    product.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (product.Contact != null)
                    {
                        if (product.Contact.Id != default(int))
                        {
                            int countContactisNotModified = accessor.ContactRepository.FindByFilter(new BaseFilter<Contact>(product.Contact, null, true)).ToList().Count();

                            if (countContactisNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Contact");
                            }
                            else
                                product.Contact.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            product.Contact.ObjectState = ObjectState.Added;
                            if (product.ObjectState != ObjectState.Added)
                                product.ObjectState = ObjectState.Modified;

                        }
                    }
                    if (copy)
                        product = product.clone<Product>(ignoreProperties); 
                }

                accessor.ProductRepository.InsertOrUpdateGraph(product);
                accessor.Save();

                //if (product.Contact != null)
                //{
                //    product.Contact.Products = null;
                //    if (product.Contact.ContactAddress != null)
                //        product.Contact.ContactAddress.Contacts = null;
                //}

                return product.ShortCircuitReference();
            }

            return nullProduct;
        }

        public Product MergeProduct(Product product, bool checkGraphs = false)
        {
            Product nullProduct = null;
            if (product != null)
            {
                IProductAccessor accessor = _accessor == null ? new ProductAccessor(_credentials) : _accessor;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (product.Id == default(int))
                    product.ObjectState = ObjectState.Added;
                else
                    product.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (product.Contact != null)
                    {
                        if (product.Contact.Id != default(int))
                        {
                            int countContactisNotModified = accessor.ContactRepository.FindByFilter(new BaseFilter<Contact>(product.Contact, null, true)).ToList().Count();

                            if (countContactisNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Contact");
                            }
                            else
                                product.Contact.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            product.Contact.ObjectState = ObjectState.Added;
                            if (product.ObjectState != ObjectState.Added)
                                product.ObjectState = ObjectState.Modified;

                        }
                    }
                    if (copy)
                        product = product.clone<Product>(ignoreProperties);
                }

                accessor.ProductRepository.InsertOrUpdateGraph(product);
                accessor.Save();

                //if (product.Contact != null)
                //{
                //    product.Contact.Products = null;
                //    if (product.Contact.ContactAddress != null)
                //        product.Contact.ContactAddress.Contacts = null;
                //}

                return product.ShortCircuitReference();
            }

            return nullProduct;
        }

        public Product GetProductGraph(int productID)
        {
            IProductAccessor accessor = _accessor == null ? new ProductAccessor(_credentials) : _accessor;

            if (accessor.ProductRepository.Find(productID) != null)
            {
                return accessor.ProductRepository.AllIncludingSetUnchanged(p => p.Contracts, p => p.Contact, p => p.Contact.ContactAddress).
                    Where(p => p.Id == productID).ToList().FirstOrDefault().ShortCircuitReference();

            }
            else
                return null;
        }

        public WCFList<Product> GetProducts(BaseFilter<Product> filter, bool includeGraph = false)
        {
            WCFList<Product> list = new WCFList<Product>();
            IProductAccessor accessor = new ProductAccessor(_credentials);
            filter.IncludeGraph = includeGraph;
            Expression<Func<Product, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            if (includeGraph == true)
            {
                Expression<Func<Product, object>>[] includeProperties = { a => a.Contracts, a => a.Contact };
                list.Entities = accessor.ProductRepository.FindByFilterIncluding(filter, includeProperties).ToList();
                list.Entities = list.Entities.ShortCircuitCollection().ToList();
            }
            else
                list.Entities = accessor.ProductRepository.FindByFilter(filter).ToList();
            filter.Take = 0;
            filter.Skip = 0;
            list.TotalQueryCount = expression != null ? accessor.ProductRepository.FindByFilter(filter).Count() : accessor.ProductRepository.GetAll().Count();
            return list;
        }

        public void Deactivate(Product Product)
        {
            Product.IsActive = false;
            this.MergeProduct(Product);
        }
    }
}
