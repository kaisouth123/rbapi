﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Helpers.Utility;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class EmailService
    {
        private IEmailEngine _emailService;

        public EmailService(IEmailEngine emailService = null)
        {
            _emailService = emailService;
        }

        public bool SendEmail(Email email)
        {
            IEmailEngine emailEngine = _emailService == null ? new ExchangeHelper() : _emailService;

            return emailEngine.SendEmail(email);
        }
    }
}
