﻿using Bridgepay.Core.Common.Helpers;
using Bridgepay.Core.Framework;
using Bridgepay.RecurringBilling.Business.Enums;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Helpers.CustomFields;
using Bridgepay.RecurringBilling.Business.Interfaces;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.Specification;
using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.DataLayer.Helpers;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;


namespace Bridgepay.RecurringBilling.Business.Services
{
    public class ContractService
    {
        Credential _credentials;
        Credential _chargeCredentials;
        IContractAccessor _accessor;
        IChargeAccessor _chargeAccessor;

        public ContractService(Credential credentials, IContractAccessor accessor = null, IChargeAccessor chargeAccessor = null)
        {
            _credentials = credentials;
            _accessor = accessor;
            _chargeAccessor = chargeAccessor;
        }

        public ContractService(Credential credentials, VendorContext context, ChargeContext ccontext)
        {
            _credentials = credentials;
            _accessor = new ContractAccessor(credentials, context);
            _chargeAccessor = new ChargeAccessor(credentials, ccontext);
        }

        public ContractService(Credential credentials, Credential chargeCredentials, IContractAccessor accessor = null, IChargeAccessor chargeAccessor = null)
        {
            _credentials = credentials;
            _chargeCredentials = chargeCredentials;
            _accessor = accessor;
            _chargeAccessor = chargeAccessor;
        }

        public WCFList<Contract> GetAllContracts(int skip = 0, int take = -1)
        {
            WCFList<Contract> list = new WCFList<Contract>();
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.ContractRepository.GetAll().OrderBy(c => c.Id).Skip(skip).ToList<Contract>() : accessor.ContractRepository.GetAll().OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Contract>();
            list.TotalQueryCount = accessor.ContractRepository.GetAll().Count();
            return list;
        }

        public WCFList<Contract> GetActiveContracts(int skip = 0, int take = -1)
        {
            WCFList<Contract> list = new WCFList<Contract>();
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            list.Entities = take == -1 ? accessor.ContractRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).ToList<Contract>() : accessor.ContractRepository.GetAll().Where(p => p.IsActive == true).OrderBy(c => c.Id).Skip(skip).Take(take).ToList<Contract>();
            list.TotalQueryCount = accessor.ContractRepository.GetAll().Count();
            return list;
        }

        public Contract GetContractById(int id)
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            return accessor.ContractRepository.Find(id);
        }

        public void DeleteById(int id)
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            accessor.ContractRepository.Delete(id);
            accessor.Save();
        }

        public void Delete(Contract Contract)
        {
            if (Contract != null)
                DeleteById(Contract.Id);
        }

        public Contract PostContract(Contract contract, bool checkGraphs = false)
        {
            Contract nullContract = null;
            contract.Status = (int)StatusCode.ACTIVE;
            if (contract != null)
            {
                CustomFieldAccessor customFieldAccessor = new CustomFieldAccessor(_credentials);
                IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
                contract.ModifiedOn = DateTime.UtcNow;
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (contract.Id == default(int))
                    contract.ObjectState = ObjectState.Added;
                else
                    contract.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (contract.Product != null)
                    {
                        if (contract.Product.Id != default(int))
                        {
                            int countIsNotModified = accessor.ProductRepository.FindByFilter(new BaseFilter<Product>(contract.Product, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Product");
                            }
                            else
                                contract.Product.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            contract.Product.ObjectState = ObjectState.Added;
                            if (contract.ObjectState != ObjectState.Added)
                                contract.ObjectState = ObjectState.Modified;
                        }
                    }

                    if (contract.Account != null)
                    {
                        if (contract.Account.Id != default(int))
                        {
                            int countIsNotModified = accessor.AccountRepository.FindByFilter(new BaseFilter<Account>(contract.Account, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Account");
                            }
                            else
                                contract.Account.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            contract.Account.ObjectState = ObjectState.Added;
                            if (contract.ObjectState != ObjectState.Added)
                                contract.ObjectState = ObjectState.Modified;
                        }
                    }

                    if (contract.BillingFrequency != null)
                    {
                        if (contract.BillingFrequency.Id != default(int))
                        {
                            int countIsNotModified = accessor.BillingFrequencyRepository.FindByFilter(new BaseFilter<BillingFrequency>(contract.BillingFrequency, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("BillingFrequency");
                            }
                            else
                                contract.BillingFrequency.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            contract.BillingFrequency.ObjectState = ObjectState.Added;
                            if (contract.ObjectState != ObjectState.Added)
                                contract.ObjectState = ObjectState.Modified;
                        }
                    }
                    if (copy)
                        contract = contract.clone<Contract>(ignoreProperties);
                }

                if (contract.NeedsEndDateOrNumberOfPayments())
                {
                    var billingFrequency = accessor.BillingFrequencyRepository.Find(contract.BillingFrequencyId);
                    ContractHelper.GetCalculatedContract(contract, billingFrequency);
                }

                // TP 15538 - Custom fields can now be required and/or have a regex for validation.
                // Validate the incoming fields against what's been activated for the merchant account.
                var org = new OrganizationMapAccessor().OrganizationMapRepository
                                                       .All
                                                       .FirstOrDefault(om => om.UnipayId == contract.MerchantId);
                if (org == null)
                    throw new InvalidOperationException($"Organization with merchant account ID {contract.MerchantId} was not found.");

                var coreAccessor = new Core.Common.DataLayer.Accessors.BridgePayAccessor();
                var activatedCustomFields = coreAccessor.CustomFieldActivationRepository.GetCustomFieldsForOrganization(org.OrgId, "Contract");
                List<CustomFieldData> validatedCustomFields = null;
                var validationResult = CustomFieldHelper.ValidateCustomFields(activatedCustomFields, contract.ContractCustomFields, contract.Id, ref validatedCustomFields);
                if (validationResult != Core.Common.Helpers.CustomFieldValidationResult.Valid)
                {
                    throw new InvalidOperationException($"Custom field validation failed: {validationResult}.");
                }

                accessor.ContractRepository.InsertOrUpdateGraph(contract);
                accessor.Save();

                if (validatedCustomFields != null)
                {
                    foreach (var customFieldData in validatedCustomFields)
                    {
                        customFieldData.EntityId = contract.Id.ToString();
                        customFieldAccessor.CustomFieldDataRepository.InsertOrUpdate(customFieldData);
                    }
                    customFieldAccessor.Save();
                }

                // Replace the incoming custom fields with the database records so the ID's are included.
                contract.ContractCustomFields = customFieldAccessor.ContractCustomFieldRepository
                                                                .All.Where(x => x.EntityId == contract.Id)
                                                                .ToList();
                return contract.ShortCircuitReference<Contract>();
            }

            return nullContract;
        }

        public Contract MergeContract(Contract contract, bool checkGraphs = false)
        {
            Contract nullContract = null;
            if (contract != null)
            {
                IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
                contract.ModifiedOn = DateTime.UtcNow;
                CustomFieldAccessor customFieldAccessor = new CustomFieldAccessor(_credentials);
                List<String> ignoreProperties = new List<string>();
                bool copy = false;

                if (contract.Id == default(int))
                    contract.ObjectState = ObjectState.Added;
                else
                    contract.ObjectState = ObjectState.Modified;

                if (checkGraphs)
                {
                    if (contract.Product != null)
                    {
                        if (contract.Product.Id != default(int))
                        {
                            int countIsNotModified = accessor.ProductRepository.FindByFilter(new BaseFilter<Product>(contract.Product, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Product");
                            }
                            else
                                contract.Product.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            contract.Product.ObjectState = ObjectState.Added;
                            if (contract.ObjectState != ObjectState.Added)
                                contract.ObjectState = ObjectState.Modified;
                        }
                    }

                    if (contract.Account != null)
                    {
                        if (contract.Account.Id != default(int))
                        {
                            int countIsNotModified = accessor.AccountRepository.FindByFilter(new BaseFilter<Account>(contract.Account, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("Account");
                            }
                            else
                                contract.Account.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            contract.Account.ObjectState = ObjectState.Added;
                            if (contract.ObjectState != ObjectState.Added)
                                contract.ObjectState = ObjectState.Modified;
                        }
                    }

                    if (contract.BillingFrequency != null)
                    {
                        if (contract.BillingFrequency.Id != default(int))
                        {
                            int countIsNotModified = accessor.BillingFrequencyRepository.FindByFilter(new BaseFilter<BillingFrequency>(contract.BillingFrequency, null, true)).ToList().Count();

                            if (countIsNotModified > 0)
                            {
                                copy = true;
                                ignoreProperties.Add("BillingFrequency");
                            }
                            else
                                contract.BillingFrequency.ObjectState = ObjectState.Modified;
                        }
                        else
                        {
                            contract.BillingFrequency.ObjectState = ObjectState.Added;
                            if (contract.ObjectState != ObjectState.Added)
                                contract.ObjectState = ObjectState.Modified;
                        }
                    }
                    if (copy)
                        contract = contract.clone<Contract>(ignoreProperties);
                }

                if (contract.NeedsEndDateOrNumberOfPayments())
                {
                    var billingFrequency = accessor.BillingFrequencyRepository.Find(contract.BillingFrequencyId);
                    ContractHelper.GetCalculatedContract(contract, billingFrequency);
                }

                // TP 15539 - Custom fields can now be required and/or have a regex for validation.
                // Validate the incoming fields against what's been activated for the merchant account.
                var org = new OrganizationMapAccessor().OrganizationMapRepository
                                                       .All
                                                       .FirstOrDefault(o => o.UnipayId == contract.MerchantId);
                if (org == null)
                    throw new InvalidOperationException($"Organization with merchant account ID {contract.MerchantId} was not found.");

                var coreAccessor = new Core.Common.DataLayer.Accessors.BridgePayAccessor();
                var activatedCustomFields = coreAccessor.CustomFieldActivationRepository.GetCustomFieldsForOrganization(org.OrgId, "Contract");
                List<CustomFieldData> validatedCustomFields = null;
                var validationResult = CustomFieldHelper.ValidateCustomFields(activatedCustomFields, contract.ContractCustomFields, contract.Id, ref validatedCustomFields);
                if (validationResult != CustomFieldValidationResult.Valid)
                {
                    throw new InvalidOperationException($"Custom field validation failed: {validationResult}.");
                }

                if (validatedCustomFields != null)
                {
                    foreach (var customFieldData in validatedCustomFields)
                    {
                        customFieldAccessor.CustomFieldDataRepository.InsertOrUpdate(customFieldData);
                    }
                    customFieldAccessor.Save();

                    // "Refresh" the contract's custom fields from the database so that the any properties the caller omitted are included.
                    contract.ContractCustomFields = customFieldAccessor.ContractCustomFieldRepository
                                                                       .All
                                                                       .Where(f => f.EntityId == contract.Id)
                                                                       .ToList();
                }

                accessor.ContractRepository.InsertOrUpdateGraph(contract);
                accessor.Save();

                return contract.ShortCircuitReference<Contract>();
            }

            return nullContract;
        }

        public List<ScheduledPayment> ForcastPayments(int contractID, int number, bool includeOneTime = false)
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            Contract contract = accessor.ContractRepository.AllIncludingSetUnchanged(c => c.ScheduledPayments).Where(c => c.Id == contractID).FirstOrDefault();
            if (contract != null)
            {
                ForecastPayment forecast = _accessor == null ? new ForecastPayment(_credentials, number, contractID) : new ForecastPayment(_credentials, number, contractID, "", accessor);
                List<ScheduledPayment> payments = forecast.ReturnPayments();
                if (includeOneTime)
                    payments.AddRange(contract.ScheduledPayments.Where(p => p.Paid == false));
                return payments;
            }
            else
                return null;
        }

        public List<ChargeTransactionsAux> GetCharges(int ContractID)
        {
            IChargeAccessor chargeAccessor = _chargeAccessor == null ? new ChargeAccessor(_credentials) : _chargeAccessor;

            if (chargeAccessor.ChargeRepository.Find(ContractID) != null)
            {
                return chargeAccessor.ChargeRepository.FindByFilter(new ChargeTransactionsAuxFilter(new ChargeTransactionsAux { ContractId = ContractID }, null, false)).ToList();
            }
            else
                return null;
        }

        public Contract GetContractGraph(int contractID)
        {
            // TP 15816 - A shared DbContext (via the ContractAccessor) caused EntityFramework to mark the cached Account object as modified,
            // and delete them on a subsequent call to Find(). This caused the Account reference on the Contract objects to become null.
            // Database contexts should be short-lived and not passed around. See https://stackoverflow.com/questions/34087243/dbcontext-caching
            IContractAccessor accessor = new ContractAccessor(_credentials);
            IChargeAccessor chargeAccessor = new ChargeAccessor(_credentials);

            if (accessor.ContractRepository.Find(contractID) != null)
            {
                Contract contract = accessor.ContractRepository.AllIncluding(p => p.ScheduledPayments,
                                                                             p => p.Account,
                                                                             p => p.Product,
                                                                             p => p.BillingFrequency,
                                                                             p => p.Account.FinancialResponsibleParties,
                                                                             p => p.Account.FinancialResponsibleParties.Select(frp => frp.Wallet),
                                                                             p => p.Account.FinancialResponsibleParties.Select(frp => frp.Wallet.PaymentMethods))
                                            .Where(p => p.Id == contractID).ToList().FirstOrDefault();
                if (contract.ScheduledPayments != null)
                    contract.ScheduledPayments.ToList().ForEach(s => { s.Contract = null; });
                contract.ChargeTransactionsAuxes = chargeAccessor.ChargeRepository.FindByFilter(new ChargeTransactionsAuxFilter(new ChargeTransactionsAux { ContractId = contractID }, null, false)).ToList();
                CustomFieldAccessor customFieldAccessor = new CustomFieldAccessor(_credentials);
                var contractCustomFields = customFieldAccessor.ContractCustomFieldRepository.All.Where(x => x.EntityId == contract.Id);
                if (contractCustomFields != null)
                {
                    contract.ContractCustomFields = contractCustomFields.ToList();
                    contract.ContractCustomFields.ToList().ForEach(s => { s.Contract = null; });
                }
                return contract.ShortCircuitReference<Contract>();
            }
            else
                return null;
        }

        public WCFList<Contract> GetContracts(BaseFilter<Contract> filter, bool includeGraph, ContractFilterModel contractFilter = null)
        {
            WCFList<Contract> list = new WCFList<Contract>();
            var queryFilter = filter;
            var take = filter.Take;
            var skip = filter.Skip;
            queryFilter.Skip = 0;
            queryFilter.Take = 0;
            IContractAccessor accessor = new ContractAccessor(_credentials);
            filter.IncludeGraph = includeGraph;
            var accountIds = new List<int>();
            if (contractFilter != null && contractFilter.ContractCustomFields!=null && contractFilter.ContractCustomFields.Count > 0)
            {
                filter.entity.ContractCustomFields = contractFilter.ContractCustomFields;
                includeGraph = true;
            }
            if (contractFilter != null && !string.IsNullOrEmpty(contractFilter.LastFour))
            {
                Bridgepay.RecurringBilling.Business.ServiceAdaptors.WalletAdaptor adaptor = new ServiceAdaptors.WalletAdaptor();
                WalletService.PaymentMethodFilter paymentMethodFilter = new WalletService.PaymentMethodFilter() { LastFour = contractFilter.LastFour };
                WalletService.PagingFilter pagingFilter = new WalletService.PagingFilter() { Skip = 0, Take = 10000 };

                // Find all the payment methods with a matching last four, then get their wallet IDs.
                var paymentMethods = adaptor.GetPaymentMethodsByFilter(paymentMethodFilter, pagingFilter).Entities;
                var walletIds = paymentMethods.Select(x => x.WalletId).ToList();

                // Get the account IDs of any financially responsible parties with matching wallet IDs.
                accountIds = accessor.FinancialResponsiblePartyRepository.GetAll()
                                                                         .Where(x => x.Guid.HasValue && walletIds.Contains(x.Guid.Value))
                                                                         .Select(x => x.AccountId)
                                                                         .ToList();

                if (accountIds.Count == 0)
                {
                    // There were no matching FRPs with these payment methods.
                    list.Entities = new List<Contract>();
                    return list;
                }
            }
            if (includeGraph == true)
            {
                Expression<Func<Contract, object>>[] includeProperties = {
                    a => a.Account,
                    a => a.BillingFrequency,
                    a => a.Product,
                    a => a.ScheduledPayments,
                    a => a.Account.FinancialResponsibleParties,
                    a => a.Account.FinancialResponsibleParties.Select(p => p.Wallet),
                    a => a.Account.FinancialResponsibleParties.Select(p => p.Wallet.PaymentMethods)
                };

                // TP: 16408 - Search for custom fields that match the filter's custom field config and value.
                var customFieldAccessor = new CustomFieldAccessor(_credentials);
                if (contractFilter?.ContractCustomFields != null && contractFilter.ContractCustomFields.Count() > 0)
                {
                    var configIdAndValueMatch = contractFilter.ContractCustomFields.ToFilterExpression();
                    if (configIdAndValueMatch == null)
                    {
                        throw new ArgumentException("Custom field contract filter is invalid.", nameof(contractFilter));
                    }

                    // Find the entity (contract) ID's with matching custom fields.
                    // Group by entity ID and ensure the number of matching fields
                    // is equal to the number of search terms to "search by AND".
                    // A subquery is used with Distinct() to avoid matching on
                    // duplicate custom fields more than once.
                    var customFieldSearchTermCount = contractFilter.ContractCustomFields.Count();
                    var distinctContractIdsAndConfigs = customFieldAccessor.ContractCustomFieldRepository
                                                                           .All
                                                                           .Where(configIdAndValueMatch)
                                                                           .Select(c => new { c.EntityId, c.CustomFieldConfigId })
                                                                           .Distinct();

                    var contractIDs = distinctContractIdsAndConfigs.GroupBy(g => g.EntityId)
                                                                   .Where(g => g.Count() == customFieldSearchTermCount)
                                                                   .Select(g => g.Key)
                                                                   .ToList();

                    if (contractIDs.Count() == 0)
                    {
                        // There were no contract custom fields with the supplied search criteria.
                        list.Entities = new List<Contract>();
                        return list;
                    }

                    // Build an expression from the filter AND limit to the matching contract IDs.
                    Expression<Func<Contract, bool>> contractIdsIn = c => contractIDs.Contains(c.Id);
                    var finalExpression = queryFilter.GetFilterExpression(filter.entity, searchFalse: filter.SearchFalse);
                    finalExpression = finalExpression.AndAlso(contractIdsIn);

                    // Replace FindByFilterIncluding with a direct query, but retain the behavior of
                    // only including the extra properties if the filter's IncludeGraph is true.
                    var contractQuery = queryFilter.IncludeGraph ? accessor.ContractRepository.AllIncluding(includeProperties) :
                                                                   accessor.ContractRepository.All;
                    list.Entities = contractQuery.Where(finalExpression)
                                                 .ToList();
                }
                else
                {
                    list.Entities = accessor.ContractRepository.FindByFilterIncluding(queryFilter, includeProperties).ToList();
                }

                if (accountIds.Count > 0)
                {
                    list.Entities = list.Entities.Where(x => accountIds.Contains(x.AccountId)).ToList();
                }
                list.Entities = list.Entities.ShortCircuitCollection().ToList();

                // The custom field data is in a different database, so
                // pull all the fields for the contracts and attach them.
                if (list.Entities.Count > 0)
                {
                    var allContractIds = list.Entities.Select(c => c.Id).ToList();
                    var lookup = customFieldAccessor.ContractCustomFieldRepository
                                                    .All
                                                    .Where(f => allContractIds.Contains(f.EntityId))
                                                    .ToLookup(f => f.EntityId);

                    foreach (var contract in list.Entities)
                    {
                        contract.ContractCustomFields = lookup[contract.Id]?.ToList();
                    }
                }
            }
            else
            {
                list.Entities = accessor.ContractRepository.FindByFilter(queryFilter).ToList();
                if (accountIds.Count > 0)
                {
                    list.Entities = list.Entities.Where(x => accountIds.Contains(x.AccountId)).ToList();
                }
            }
            list.TotalQueryCount = list.Entities.Count;
            list.Entities = list.Entities.Skip(skip).Take(take).ToList();
            return list;
        }

        public List<FinancialResponsibleParty> GetFinancialResponsibleParties(int contractId)
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            Contract contract = accessor.ContractRepository.Find(contractId);
            Expression<Func<FinancialResponsibleParty, object>>[] includeProperties = { a => a.Contact };
            BaseFilter<FinancialResponsibleParty> filter = new BaseFilter<FinancialResponsibleParty> { entity = new FinancialResponsibleParty { AccountId = contract.AccountId }, IncludeGraph = true };
            List<FinancialResponsibleParty> parties = accessor.FinancialResponsiblePartyRepository.FindByFilterIncluding(filter, includeProperties).ToList();
            parties.ForEach(c =>
            {
                if (c.Contact == null)
                {
                    c.Contact = accessor.ContactRepository.Find(c.ContactId);
                }
            });
            parties.ShortCircuitCollection<FinancialResponsibleParty>();

            return parties;
        }

        public void Deactivate(Contract Contract)
        {
            Contract.IsActive = false;
            this.MergeContract(Contract);
        }

        public WCFList<Contract> GetContractsByMerchant(int merchantId, BaseFilter<Contract> filter, bool includeGraph)
        {
            WCFList<Contract> list = new WCFList<Contract>();
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            Expression<Func<Contract, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            filter.IncludeGraph = includeGraph;
            filter.OrderBy = string.IsNullOrEmpty(filter.OrderBy) ? "Id" : filter.OrderBy;
            if (includeGraph == true)
            {
                Expression<Func<Contract, object>>[] includeProperties = { a => a.Account, a => a.BillingFrequency, a => a.Product, a => a.ScheduledPayments };
                if (filter.Take == -1)
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Where(expression).Include(c => c.Account).Include(c => c.BillingFrequency).Include(c => c.Product).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<Contract>() :
                                     (from accounts in accessor.AccountRepository.GetAll()
                                      where accounts.MerchantId == merchantId
                                      select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Include(c => c.Account).Include(c => c.BillingFrequency).Include(c => c.Product).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<Contract>();
                }
                else
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Where(expression).Include(c => c.Account).Include(c => c.BillingFrequency).Include(c => c.Product).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<Contract>() :
                                    (from accounts in accessor.AccountRepository.GetAll()
                                     where accounts.MerchantId == merchantId
                                     select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Include(c => c.Account).Include(c => c.BillingFrequency).Include(c => c.Product).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<Contract>();

                }
                list.Entities.ShortCircuitCollection();
            }
            else
            {
                if (filter.Take == -1)
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Where(expression).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<Contract>() :
                                     (from accounts in accessor.AccountRepository.GetAll()
                                      where accounts.MerchantId == merchantId
                                      select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<Contract>();
                }
                else
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Where(expression).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<Contract>() :
                                                          (from accounts in accessor.AccountRepository.GetAll()
                                                           where accounts.MerchantId == merchantId
                                                           select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<Contract>();

                }
            }
            list.TotalQueryCount = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                         where accounts.MerchantId == merchantId
                                                         select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Where(expression).Count<Contract>() :
                                   (from accounts in accessor.AccountRepository.GetAll()
                                    where accounts.MerchantId == merchantId
                                    select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Count<Contract>();

            return list;
        }

        public WCFList<BillingFrequency> GetFrequenciesByMerchant(int merchantId, BaseFilter<BillingFrequency> filter)
        {
            WCFList<BillingFrequency> list = new WCFList<BillingFrequency>();
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            Expression<Func<BillingFrequency, bool>> expression = filter.GetFilterExpression(filter.entity, filter.IgnoreProperties, filter.SearchFalse);
            Expression<Func<BillingFrequency, object>>[] includeProperties = { };
            filter.OrderBy = string.IsNullOrEmpty(filter.OrderBy) ? "Id" : filter.OrderBy;
            if (true)
            {
                if (filter.Take == -1)
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Include(c => c.BillingFrequency).Select(c => c.BillingFrequency).Where(expression).Include(b => b.Interval).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<BillingFrequency>() :
                                      (from accounts in accessor.AccountRepository.GetAll()
                                       where accounts.MerchantId == merchantId
                                       select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Include(c => c.BillingFrequency).Select(c => c.BillingFrequency).Include(b => b.Interval).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).ToList<BillingFrequency>();
                }
                else
                {
                    list.Entities = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                          where accounts.MerchantId == merchantId
                                                          select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Include(c => c.BillingFrequency).Select(c => c.BillingFrequency).Where(expression).Include(b => b.Interval).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<BillingFrequency>() :
                                    (from accounts in accessor.AccountRepository.GetAll()
                                     where accounts.MerchantId == merchantId
                                     select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Include(c => c.BillingFrequency).Select(c => c.BillingFrequency).Include(b => b.Interval).OrderByExt(filter.OrderBy, filter.SortDesc).Skip(filter.Skip).Take(filter.Take).ToList<BillingFrequency>();
                }

            }
            filter.Take = 0;
            list.TotalQueryCount = expression != null ? (from accounts in accessor.AccountRepository.GetAll()
                                                         where accounts.MerchantId == merchantId
                                                         select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Include(c => c.BillingFrequency).Select(c => c.BillingFrequency).Where(expression).ToList<BillingFrequency>().Count() :
                                    (from accounts in accessor.AccountRepository.GetAll()
                                     where accounts.MerchantId == merchantId
                                     select accounts).Include(a => a.Contracts).SelectMany(c => c.Contracts).Include(c => c.BillingFrequency).Select(c => c.BillingFrequency).ToList<BillingFrequency>().Count();

            list.Entities = list.Entities.ShortCircuitCollection().ToList();
            return list;
        }

        public bool HasPaymentsReadyForProcessing()
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            int countNull = accessor.ContractRepository.GetAll().Where(c => c.NextBillDate == null).Count();

            if (countNull > 0)
                SetPaymentDates();

            return ReturnNumberofContractsToBeProcessed() + ReturnNumberofPaymentsToBeProcessed() > 0;
        }

        public List<Contract> GetContractForProcessing(BaseFilter<Contract> filter)
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            if (HasPaymentsReadyForProcessing() == false)
                return null;
            DateTime addDay = DateTime.Today.AddDays(1).ToUniversalTime();
            List<Contract> contracts = filter.Take <= 0 ? accessor.ContractRepository.GetAll().Where(c => c.NextBillDate != null && ((DateTime)c.NextBillDate < addDay && c.IsActive == true && c.Status != (int)StatusCode.CANCELED))
                .OrderBy(c => c.Id).Skip(filter.Skip).ToList() :
                accessor.ContractRepository.GetAll().Where(c => c.NextBillDate != null && ((DateTime)c.NextBillDate < addDay && c.IsActive == true && c.Status != (int)StatusCode.CANCELED))
                .OrderBy(c => c.Id).Skip(filter.Skip).Take(filter.Take).ToList();

            return contracts;
        }

        public List<Contract> GetContractToExpire(int take, int skip, int Days)
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            if (HasPaymentsReadyForProcessing() == false)
                return null;
            DateTime TopDay = DateTime.Today.AddDays(Days + 1).ToUniversalTime();
            DateTime BottomDay = DateTime.Today.AddDays(Days).ToUniversalTime();
            return take <= 0 ? accessor.ContractRepository.GetAll().Where(c => c.EndDate != null && ((DateTime)c.EndDate <= TopDay && (DateTime)c.EndDate > BottomDay && c.IsActive == true))
                .OrderBy(c => c.Id).Skip(skip).ToList() :
                accessor.ContractRepository.GetAll().Where(c => c.EndDate != null && ((DateTime)c.EndDate <= TopDay && (DateTime)c.EndDate > BottomDay && c.IsActive == true))
                .OrderBy(c => c.Id).Skip(skip).Take(take).ToList();
        }

        public List<Bridgepay.RecurringBilling.Business.WalletService.PaymentMethod> GetCardsToExpire(int take, int skip, int days)
        {
            Bridgepay.RecurringBilling.Business.ServiceAdaptors.WalletAdaptor adaptor = new ServiceAdaptors.WalletAdaptor();

            return adaptor.GetExpiringPaymentMethods(skip, take, days);
        }

        public List<ScheduledPayment> GetPaymentsForProcessing(int skip = 0, int take = -1)
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            DateTime addDay = DateTime.Today.AddDays(1).ToUniversalTime();
            List<ScheduledPayment> scheduledPayments = accessor.ScheduledPaymentRepository.GetAll().Where(c => ((DateTime)c.BillDate) <= addDay && c.Paid == false).ToList();

            return take <= 0 ? accessor.ScheduledPaymentRepository.GetAll().Where(c => ((DateTime)c.BillDate) < addDay && c.Paid == false).OrderBy(c => c.Id).Skip(skip).ToList() :
                                accessor.ScheduledPaymentRepository.GetAll().Where(c => ((DateTime)c.BillDate) < addDay && c.Paid == false).OrderBy(c => c.Id).Skip(skip).Take(take).ToList();
        }

        public void SetPaymentDates()
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;

            //SetPaymentDates for null dates
            List<Contract> contracts = accessor.ContractRepository.GetAll().Where(c => c.NextBillDate == null).ToList();
            foreach (Contract contract in contracts)
            {
                List<ScheduledPayment> payments = ForcastPayments(contract.Id, 1);
                contract.NextBillDate = payments[0].BillDate;
                accessor.ContractRepository.InsertOrUpdate(contract);
            }
            accessor.Save();
        }

        public int ReturnNumberofContractsToBeProcessed()
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            DateTime addDays = DateTime.Today.AddDays(1).ToUniversalTime();
            return accessor.ContractRepository.GetAll().Where(c => ((DateTime)c.NextBillDate) < addDays && c.IsActive == true && c.Status != (int)StatusCode.CANCELED).Count();
        }

        public int ReturnNumberofPaymentsToBeProcessed()
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            DateTime addDays = DateTime.Today.AddDays(1).ToUniversalTime();
            List<ScheduledPayment> scheduledPayments = accessor.ScheduledPaymentRepository.GetAll().Where(c => ((DateTime)c.BillDate) < addDays && c.Paid == false).ToList();
            return scheduledPayments.Count;
        }

        public bool ProcessCardExpiringEmail(Credential merchantCredentials, Business.WalletService.PaymentMethod method, int days)
        {
            IContractAccessor accessor = _accessor == null ? new ContractAccessor(_credentials) : _accessor;
            IMerchantAccessor merchantAcessor = new MerchantAccessor(merchantCredentials);

            FinancialResponsibleParty party = accessor.FinancialResponsiblePartyRepository.GetAll().Where(f => f.Guid != null && f.Guid == method.WalletId).Include(f => f.Account).Include(f => f.Contact).Include(f => f.Contact.ContactAddress).FirstOrDefault();
            if (party == null)
                return false;
            Account account = accessor.AccountRepository.Find(party.AccountId);
            Merchant merchant = merchantAcessor.MerchantRepository.Find(account.MerchantId);
            AccountPaymentInfo info = new AccountPaymentInfo(account, party, party.Contact, merchant, method);

            String header = "";

            if (days < 1)
                header = "The following payment method has expired";
            else if (days == 1)
                header = "The following payment method is expiring in 1 day";
            else
                header = "The following payment method is expiring in " + days.ToString() + " days";

            AccountExpiringEmail email = new AccountExpiringEmail();
            email.To = party.Contact.ContactEmail;
            email.ReplyTo = email.ReplyTo = ConfigurationManager.AppSettings["ReplyToEmail"].ToString();
            email.SenderName = "BridgePay Network";
            email.Subject = header;
            email.SendEmail(info, header);

            return true;
        }
    }
}
