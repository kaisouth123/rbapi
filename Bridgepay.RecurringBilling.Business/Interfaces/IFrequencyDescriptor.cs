﻿using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Interfaces
{
    public interface IFrequencyDescriptor
    {
        BillingFrequency Frequency { get; set; }

        string GetFrequencyDescriptor(IBillingFrequencyAccessor accessor = null);
    }
}
