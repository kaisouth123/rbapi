﻿using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
namespace Bridgepay.RecurringBilling.Business
{
    public interface IFinancialResponsiblePartyProcessor
    {
        IContractAccessor _accessor { get; set; }
        Contract Contract { get; set; }
        ContractPayment ProcessPayment(FinancialResponsibleParty party, ContractPayment contractPayment);
    }
}
