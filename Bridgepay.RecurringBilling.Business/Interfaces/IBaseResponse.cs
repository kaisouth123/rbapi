﻿using System;
namespace Bridgepay.RecurringBilling.Business.Models
{
    public interface IBaseResponse
    {
        bool IsApproval { get; }
        string RequestType { get; set; }
        string ResponseCode { get; set; }
        string ResponseDescription { get; set; }
        string TransactionID { get; set; }
    }
}
