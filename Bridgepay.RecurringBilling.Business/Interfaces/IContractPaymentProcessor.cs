﻿using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
namespace Bridgepay.RecurringBilling.Business.Interfaces
{
    public interface IContractPaymentProcessor
    {
        Contract Contract { get; set; }
        IContractAccessor _accessor { get; set; }
        ContractPayment ProcessContractPayment(Contract contract);
        ContractPayment ProcessScheduledPayment(ScheduledPayment payment);
    }
}
