﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business
{
    public interface IAuthenticator
    {
        bool Authenticate(Credential credentials);
    }
}
