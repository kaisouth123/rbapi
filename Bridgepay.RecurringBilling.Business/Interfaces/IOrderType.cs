﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Interfaces
{
    public interface IOrderType<T>
    {
        int ReturnMaxOrder(int entityId);
        void SetNewOrder(T entity);
    }
}
