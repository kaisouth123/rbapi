﻿using Bridgepay.RecurringBilling.Business.Helpers.Utility;
using Bridgepay.RecurringBilling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Interfaces
{
    public interface IEmailTemplate 
    {
        string EmailID { get; set; }
        string Subject { get; set; }
        string To { get; set; }
        string CC { get; set; }
        string BCC { get; set; }
        string Body { get; set; }
        string SenderName { get; set; }
        string ReplyTo { get; set; }
        bool IsHTML { get; set; }

        void GenerateBody<T>(T entity, List<string> includeFields = null, List<string> includeCollections = null, List<string> childFields = null);
    }

}
