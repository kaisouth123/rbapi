﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Interfaces
{
    public interface IBaseRequest
    {
        string ClientIdentifier { get; set; }
        string TransactionID { get; set; }
        string RequestDateTime { get; set; }
        string User { get; set; }
        string Password { get; set; }
    }
}
