﻿using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business
{
    public interface IPaymentForecastor
    {
        int ContractID { get; set; }
        PaymentInfo PaymentsInfo { get; set; }

        List<ScheduledPayment> ForecastPayments(int number, IContractAccessor accessor = null);
        
    }
}
