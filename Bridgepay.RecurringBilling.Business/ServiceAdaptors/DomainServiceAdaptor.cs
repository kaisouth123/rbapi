﻿using Bridgepay.RecurringBilling.Business.WalletService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.DomainService;

namespace Bridgepay.RecurringBilling.Business.ServiceAdaptors
{
    public class DomainServiceAdaptor
    {
        public string GetIdustryCode(Credentials creds, int accountCode, string merchantCode)
        {
            using (ActivationServiceClient client = new ActivationServiceClient())
            {
                ActivationData data = client.GetActivationData(creds, merchantCode, accountCode).FirstOrDefault();
                return data.ClientCode;
            }
        }

    }
}
