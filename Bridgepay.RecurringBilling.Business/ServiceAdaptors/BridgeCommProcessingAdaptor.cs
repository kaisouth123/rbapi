﻿using Bridgepay.RecurringBilling.Business.Factories;
using Bridgepay.RecurringBilling.Business.Models;
using Bridgepay.RecurringBilling.Business.RequestHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.ServiceAdaptors
{
    public class BridgeCommProcessingAdaptor
    {
        public AuthorizationResponse ProcessAuthorization(AuthTransaction transaction)
        {
            string response = null;
            using (var client = new RequestHandlerClient())
            {
                response = client.ProcessRequest(transaction.ToString());
            }

            return new AuthorizationResponse(response);
        }


        public VoidRefundResponse ProcessVoid(VoidTransaction transaction)
        {
            string response = null;

            using (var client = new RequestHandlerClient())
            {
                response = client.ProcessRequest(transaction.ToString());
            }

            return new VoidRefundResponse(response);
        }


        public BaseResponse ProcessTransaction(BaseTransaction transaction)
        {
            using (var client = new RequestHandlerClient())
            {
                var response = client.ProcessRequest(transaction.ToString());
                return TransactionResponseFactory.MakeResponse(transaction, response);
            }
        }
    }
}
