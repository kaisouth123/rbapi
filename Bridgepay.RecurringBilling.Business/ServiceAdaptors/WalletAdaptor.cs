﻿using Bridgepay.RecurringBilling.Business.WalletService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Business.Helpers;

namespace Bridgepay.RecurringBilling.Business.ServiceAdaptors
{
    public class WalletAdaptor
    {
        public Wallet GetWalletById(Guid id, bool includeGraph)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                return client.GetWalletByIdAsync(id, includeGraph).Result;
            }
        }

        public List<PaymentMethod> GetPaymentMethodsById(Guid walletId)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                PaymentMethodFilter filter = new PaymentMethodFilter();
                filter.WalletId = new List<Guid> { walletId, }.ToArray();
                filter.IsActive = true;
                PagingFilter paging = new PagingFilter();
                paging.SortField = "Order";

                return client.GetPaymentMethodsByFilterAsync(filter, paging).Result.Entities.ToList();
            }
        }

        public PaymentMethod GetPaymentMethodById(Guid id, bool includeGraph = false)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                PaymentMethodFilter filter = new PaymentMethodFilter();
                filter.PaymentMethodId = id;
                PagingFilter paging = new PagingFilter();
                paging.SortField = "Order";

                return client.GetPaymentMethodsByFilter(filter, paging).Entities.FirstOrDefault();
            }
        }

        public List<PaymentMethod> GetExpiringPaymentMethods(int skip, int take, int days)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                List<PaymentMethod> methods = new List<PaymentMethod>();
                if (!DateTime.UtcNow.AddDays(days).IsLastDay())
                    return methods;

                PaymentMethodFilter filter = new PaymentMethodFilter();
                filter.ExpirationDate = DateTime.UtcNow.AddDays(days).ToString("MMyy");
                filter.IsActive = true;
                PagingFilter paging = new PagingFilter();
                paging.SortField = "Order";
                if (take > 0)
                    paging.Take = take;
                paging.Skip = skip;

                return client.GetPaymentMethodsByFilterAsync(filter, paging).Result.Entities.ToList();
            }
        }

        public PaymentMethod UpdatePaymentMethod(PaymentMethod method)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                return client.UpdatePaymentMethodAsync(method).Result;
            }
        }

        public PagedListOfPaymentMethodLTquZG_Pa GetPaymentMethodsByFilter(PaymentMethodFilter filter, PagingFilter pagingFilter)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                return client.GetPaymentMethodsByFilter(filter, pagingFilter);
            }
        }

        public void DeletePaymentMethod(Guid id)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                client.DeletePaymentMethod(id);
            }
        }

        public PaymentMethod InsertOrUpdate(PaymentMethod entity)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                if (entity.PaymentMethodId == null)
                    return client.CreatePaymentMethod(entity);
                else
                    return client.UpdatePaymentMethod(entity);
            }
        }

        public PagedListOfWalletLTquZG_Pa GetWalletsByFilter(WalletFilter filter, PagingFilter pagingFilter)
        {
            filter.SiteId = GetSiteID();
            using (WalletServiceClient client = new WalletServiceClient())
            {               
                return client.GetWalletsByFilter(filter, pagingFilter);
            }
        }

        public Wallet InsertOrUpdate(Wallet entity)
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                if (entity.WalletId == null)
                    return client.CreateWallet(entity);
                else
                    return client.UpdateWallet(entity);
            }
        }

        public Guid? GetSiteID()
        {
            using (WalletServiceClient client = new WalletServiceClient())
            {
                SiteFilter filter = new SiteFilter();
                PagingFilter pagingFilter = new PagingFilter { SortField = "SiteId" };
                filter.SiteTypeId = Bridgepay.RecurringBilling.Business.WalletService.SiteType.RecurringBilling;
                Site site = client.GetSitesByFilter(filter, pagingFilter).Entities.FirstOrDefault();
                return site == null ? null : site.SiteId;
            }
        }

        
    }
}
