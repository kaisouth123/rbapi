﻿using Bridgepay.RecurringBilling.Business.Enums;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Common.Utilities;

namespace Bridgepay.RecurringBilling.Business.Models
{
    [DataContract]
    public class ContractPayment: BaseEntity
    {
        public ContractPayment()
        {
            Exceptions = new List<Exception>();
            Responses = new List<PaymentProcessResponse>();
            Status = 0;
            ChargedAmount = 0;
        }

        public ContractPayment(Contract contract, int merchantId, int merchantCode, string merchantName = "", List<Exception> exceptions = null)
        {
            try
            {
                ContractId = contract.Id;
                Name = merchantName;
                Id = contract.Id;
                ProductId = contract.ProductId;
                Date = DateTime.UtcNow.ToLongDateString() + "UTC Time";
                ChargedAmount = contract.BillAmount + contract.TaxAmount;
                NumFailures = contract.NumFailures != null ? (int)contract.NumFailures : 0;
                EmailCustomerAtFailure = contract.EmailCustomerAtFailure;
                EmailCustomerReceipt = contract.EmailCustomerReceipt;
                EmailMerchantAtFailure = contract.EmailMerchantAtFailure;
                EmailMerchantReceipt = contract.EmailMerchantReceipt;
                Status = 0;
                Exceptions = exceptions;
                MerchantId = merchantId;
                MerchantCode = merchantCode;
                Responses = new List<PaymentProcessResponse>();
            }
            catch(Exception ex)
            {
                Logger.LogEvent(LogUtility.log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
            }
        }

        public ContractPayment(Contract contract, ScheduledPayment payment, int merchantId, int merchantCode, string merchantName = "", List<Exception> exceptions = null)
        {
            try
            {
                ContractId = contract.Id;
                Id = contract.Id;
                Name = merchantName;
                ProductId = contract.ProductId;
                Date = DateTime.UtcNow.ToLongDateString() + "UTC Time";
                ChargedAmount = payment.BillAmount;
                NumFailures = contract.NumFailures != null ? (int)contract.NumFailures : 0;
                EmailCustomerAtFailure = contract.EmailCustomerAtFailure;
                EmailCustomerReceipt = contract.EmailCustomerReceipt;
                EmailMerchantAtFailure = contract.EmailMerchantAtFailure;
                EmailMerchantReceipt = contract.EmailMerchantReceipt;
                Status = 0;
                Exceptions = exceptions;
                MerchantId = merchantId;
                MerchantCode = merchantCode;
                Responses = new List<PaymentProcessResponse>();
                ScheduledPaymentId = payment.Id;
            }
            catch (Exception ex)
            {
                Logger.LogEvent(LogUtility.log, Utility.GetCurrentMethod(), LogType.Error, null, ex);
            }
        }

        [DataMember]public bool Processed { get; set; }
        [DataMember]public int ContractId { get; set; }
        [DataMember]public int? ProductId { get; set; }
        [DataMember]public int MerchantId { get; set; }
        [DataMember]public string Name { get; set; }
        [DataMember]public int MerchantCode { get; set; }
        [DataMember]public int ChargeId { get; set; }
        [DataMember]public String Date { get; set;}
        [DataMember]public string Account {get;set;}
        [DataMember]public double ChargedAmount { get; set; }
        [DataMember]public int NumFailures { get; set; }
        [DataMember]public bool? EmailCustomerAtFailure { get; set; }
        [DataMember]public bool? EmailMerchantAtFailure { get; set; }
        [DataMember]public bool? EmailCustomerReceipt { get; set; }
        [DataMember]public bool? EmailMerchantReceipt { get; set; }
        [DataMember]public StatusCode Status { get; set; }
        [DataMember]public string Subject { get; set;}
        [DataMember]public int? ScheduledPaymentId { get; set; }
        [DataMember]public List<Exception> Exceptions { get; set; }
        [DataMember]public List<PaymentProcessResponse> Responses { get; set; }

        public List<FinancialPartyPaymentInfo> GeneratePartyInfo()
        {
            List<FinancialPartyPaymentInfo> list = new List<FinancialPartyPaymentInfo>();

            foreach (PaymentProcessResponse response in Responses)
            {
                if (list.Where(c => c.FinancialResponsiblePartyId == response.FinancialResponsiblePartyId).ToList().Count <= 0)
                {
                    FinancialPartyPaymentInfo paymentInfo = new FinancialPartyPaymentInfo();
                    paymentInfo.FinancialResponsiblePartyId = response.FinancialResponsiblePartyId;
                    paymentInfo.Responses = Responses.Where(c => c.FinancialResponsiblePartyId == paymentInfo.FinancialResponsiblePartyId).ToList();
                    paymentInfo.ChargedAmount = ChargedAmount;
                    paymentInfo.Status = Status;
                    paymentInfo.Account = Account;
                    list.Add(paymentInfo);
                }
            }

            return list;
        }

        public Contract UpdateContract(Contract contract)
        {
            if(contract.Status != (int)StatusCode.ERROR && contract.Status != (int)StatusCode.PROCESSING)
               contract.LastBillDate = DateTime.UtcNow;
            contract.Status = (int)Status;
            contract.NumFailures = NumFailures;

            if (Processed == true && Status == StatusCode.APPROVED)
            {
                contract.AmountRemaining = contract.AmountRemaining < (int)ChargedAmount ? contract.AmountRemaining = 0 : contract.AmountRemaining - (int)ChargedAmount;
                contract.NumFailures = 0;
            }
            else if (Status == StatusCode.DECLINED)
            {
                contract.NumFailures++;
            }

            contract = contract.clone<Contract>(new List<string> { "Account", "Product", "ScheduledPayments", "BillingFrequency", "ChargeTransactionsAuxes" });

            return contract;
        }
    }
}
