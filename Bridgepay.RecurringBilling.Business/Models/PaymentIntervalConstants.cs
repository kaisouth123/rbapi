﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public static class PaymentIntervalConstants
    {
        public const string Daily = "Daily";
        public const string DailyByDays = "Daily # Days";
        public const string Weekly = "Weekly # Days";
        public const string Monthly = "Monthly #  DayOfMonth";
        public const string MonthlyByDaysAndWeek = "Monthly # Days WeekOfMonth";
        public const string Yearly = "Yearly # DayOfMonth Month";
        public const string YearlyByDaysMonthAndWeek = "Yearly # Days WeekOfMonth Month";
    }
}
