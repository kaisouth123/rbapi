﻿using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class CloseBatchRequest : BaseTransaction
    {
        [RequestHeader]
        public override string RequestType
        {
            get { return PaymentConstants.RequestType.CloseBatch; }
        }

        public int MerchantCode { get; set; }
        public int MerchantAccountCode { get; set; }
    }
}
