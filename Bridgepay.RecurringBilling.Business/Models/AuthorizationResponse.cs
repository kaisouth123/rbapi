﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class AuthorizationResponse : BaseResponse
    {
        public string Token { get; set; }
        public string AuthorizationCode { get; set; }
        public string ReferenceNumber { get; set; }
        public string GatewayResult { get; set; }
        public int AuthorizedAmount { get; set; }
        public int OriginalAmount { get; set; }
        public string ExpirationDate { get; set; }
        public string AVSResult { get; set; }
        public string AVSMessage { get; set; }
        public string StreetMatchMessage { get; set; }
        public string ZipMatchMessage { get; set; }
        public string CVResult { get; set; }
        public string CVMessage { get; set; }
        public string IsCommercialCard { get; set; }
        public string GatewayTransID { get; set; }
        public string GatewayMessage { get; set; }
        public int Balance { get; set; }
        public int CashBackAmount { get; set; }
        public string CurrencyCode { get; set; }
        public string TransactionCode { get; set; }
        public string TransactionDate { get; set; }
        public string CardType { get; set; }
        public int RemainingAmount { get; set; }

        public AuthorizationResponse(string base64EncodedTransaction) : base(base64EncodedTransaction) { }
    }
}
