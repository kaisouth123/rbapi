﻿using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class MultiUseTokenResponse : BaseResponse
    {
        public MultiUseTokenResponse(string base64EncodedTransaction) : base(base64EncodedTransaction) { }
        public string Token { get; set; }
    }
}
