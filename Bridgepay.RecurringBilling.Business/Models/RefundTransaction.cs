﻿using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class RefundTransaction : VoidTransaction
    {
        public override string TransactionType
        {
            get { return PaymentConstants.TransType.Refund; }
        }
    }
}
