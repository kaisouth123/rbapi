﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class BaseResponse : IBaseResponse 
    {
        public string TransactionID { get; set; }
        public string RequestType { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseDescription { get; set; }

        public BaseResponse(string base64EncodedTransaction)
        {
            string decoded = Encoding.UTF8.GetString(Convert.FromBase64String(base64EncodedTransaction));

            Hydrate(decoded);
        }

        private void Hydrate(string decodedResponse)
        {
            XElement xel = XElement.Parse(decodedResponse);

            Hydrate(xel);
        }

        private void Hydrate(XElement xel)
        {
            if (xel.Elements().Count() == 0)
            {
                this.SetValueFromString(xel.Name.ToString(), xel.Value);
            }
            else
            {
                foreach (var child in xel.Elements())
                {
                    Hydrate(child);
                }
            }
        }

        public bool IsApproval
        {
            get
            {
                int responseCode = 0;

                if (int.TryParse(ResponseCode, out responseCode))
                {
                    return responseCode == 0;
                }

                return false;
            }
        }
    }
}
