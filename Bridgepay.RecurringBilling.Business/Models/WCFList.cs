﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    [DataContract]
    [KnownType(typeof(Account))]
    [KnownType(typeof(Address))]
    [KnownType(typeof(BaseEntity))]
    [KnownType(typeof(BillingFrequency))]
    [KnownType(typeof(ChargeTransactionsAux))]
    [KnownType(typeof(Contact))]
    [KnownType(typeof(ContactType))]
    [KnownType(typeof(Contract))]
    [KnownType(typeof(Customer))]
    [KnownType(typeof(CustomerAddress))]
    [KnownType(typeof(FinancialResponsibleParty))]
    [KnownType(typeof(Interval))]
    [KnownType(typeof(Merchant))]
    [KnownType(typeof(Product))]
    [KnownType(typeof(ScheduledPayment))]
    public class WCFList<T> where T : BaseEntity
    {
        [DataMember]public List<T> Entities { get; set; }
        [DataMember]public int TotalQueryCount { get; set; }
    }
}
