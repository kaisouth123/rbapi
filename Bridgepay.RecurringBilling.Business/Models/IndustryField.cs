﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class IndustryField
    {
        public int ID { get; set; }
        public string StaticID { get; set; }
        public int IndustryTypeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DataType { get; set; }
        public string MinData { get; set; }
        public string MaxData { get; set; }
        public bool Required { get; set; }
        public string RegEx { get; set; }
        public int SortOrder { get; set; }
        public int TransactionLevel { get; set; }
        public bool IsSubItem { get; set; }
        public virtual IndustryType IndustryType { get; set; }
    }
}
