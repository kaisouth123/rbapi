﻿using Bridgepay.RecurringBilling.Business.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    [DataContract]
    public class FinancialPartyPaymentInfo
    {
        [DataMember]public string Account { get; set; }
        [DataMember]public double ChargedAmount { get; set; }
        [DataMember]public StatusCode Status { get; set; }
        [DataMember]public int FinancialResponsiblePartyId { get; set; }
        [DataMember]public bool? EmailCustomerAtFailure { get; set; }
        [DataMember]public bool? EmailCustomerReceipt { get; set; }
        [DataMember]public string Email { get; set; }
        [DataMember]public string Subject { get; set;}
        [DataMember]public List<PaymentProcessResponse> Responses { get; set; }
    }
}
