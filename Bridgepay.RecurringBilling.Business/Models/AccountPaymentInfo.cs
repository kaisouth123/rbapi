﻿using Bridgepay.RecurringBilling.Business.Enums;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    [DataContract]
    public class AccountPaymentInfo: BaseEntity
    {
        public AccountPaymentInfo()
        { }

        public AccountPaymentInfo(Account account, FinancialResponsibleParty party, Contact customer, Merchant merchant, Business.WalletService.PaymentMethod method)
        {
            MerchantId = merchant.Id;
            MerchantName = merchant.Name;
            Account = account.AccountName;
            AccountNumber = account.AccountNumber;
            FinancialResponsbilePartyID = party.Id;
            PaymentInfo = method.PaymentMethodType.ToString().TrimEnd("s".ToCharArray()) + " " + method.LastFour;
            ExpirationDate = method.ExpirationDate;
            CustomerName = customer.ContactFirstName + " " + customer.ContactLastName;
            if (customer.ContactAddress != null)
            {
                Address = customer.ContactAddress.AddressStreet + " " + customer.ContactAddress.AddressStreetTwo;
                CityStateZip = customer.ContactAddress.AddressCity + ", " + customer.ContactAddress.AddressState + " " + customer.ContactAddress.AddressZip;
            }
            else
                Address = "Not on File";
            Email = customer.ContactEmail;
            Phone = "D: " + customer.ContactDayPhone + ", E: " + customer.ContactEveningPhone + ", M: " + customer.ContactMobilePhone;
        }

        [DataMember]public int MerchantId { get; set; }
        [DataMember]public string MerchantName { get; set;}
        [DataMember]public string Account { get; set; }
        [DataMember]public string AccountNumber { get; set; }
        [DataMember]public int FinancialResponsbilePartyID { get; set; }
        [DataMember]public string PaymentInfo { get; set; } //Type + LastFour
        [DataMember]public string ExpirationDate { get; set;}
        [DataMember]public string CustomerName { get; set; } // Title + First + Last Name
        [DataMember]public string Address { get; set; }
        [DataMember]public string CityStateZip { get; set; }
        [DataMember]public string Email { get; set; }
        [DataMember]public string Phone { get; set; } // m: mobile, w: work h: home
        [DataMember]public string Subject { get; set;}
        [DataMember]List<String> Messages {get; set;}
    }
}
