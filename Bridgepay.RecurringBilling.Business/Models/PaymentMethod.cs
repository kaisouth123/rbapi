﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class PaymentMethod
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public Dictionary<string, string> TransactionTypes { get; set; }

        public PaymentMethod()
        {
            TransactionTypes = new Dictionary<string, string>();
        }
    }
}
