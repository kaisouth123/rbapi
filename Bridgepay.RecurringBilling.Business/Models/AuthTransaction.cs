﻿using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class AuthTransaction : BaseTransaction
    {
        public override string RequestType
        {
            get { return PaymentConstants.RequestType.ProcessAuthorization; }
        }

        public int MerchantCode { get; set; }
        public int MerchantAccountCode { get; set; }

        // Auth fields
        private string paymentAccountNumber;
        public string PaymentAccountNumber
        {
            get { return string.IsNullOrEmpty(Token) ? paymentAccountNumber : null; }
            set { paymentAccountNumber = value; }
        }

        public string Token { get; set; }
        public string ExpirationDate { get; set; }
        public string SecurityCode { get; set; }
        public int Amount { get; set; }
        public string CurrencyCode { get; set; }
        public string TrackIndicator { get; set; }
        public string MsrKsn { get; set; }
        public string SecureFormat { get; set; }
        public string BDKSlot { get; set; }
        public string Track1 { get; set; }
        public string Track2 { get; set; }
        public string Track3 { get; set; }
        public string EncryptionID { get; set; }
        public string TransactionType { get; set; }
        public string TransIndustryType { get; set; }
        public string TransCatCode { get; set; }
        public string SwipeResult { get; set; }
        public string PINBlock { get; set; }
        public string PINKey { get; set; }
        public string BankAccountNum { get; set; }
        public string RoutingNum { get; set; }
        public string AcctType { get; set; }
        public string InvoiceNum { get; set; }
        public string PONum { get; set; }
        public string CustomerAccountCode { get; set; }
        public string PaymentType { get; set; }
        public string AccountHolderName { get; set; }
        public string HolderType { get; set; }
        public int? FeeAmount { get; set; }
        public int? TipAmount { get; set; }
        public string AccountStreet { get; set; }
        public string AccountZip { get; set; }
        public string AccountPhone { get; set; }
        public decimal? TaxRate { get; set; }
        public int? TaxAmount { get; set; }
        public string TaxIndicator { get; set; }
        public string ShipToName { get; set; }
        public string ShipToStreet { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToState { get; set; }
        public string ShipToZip { get; set; }
        public string ShipToCountryCode { get; set; }
        public string ShippingOriginZip { get; set; }
        public int? DiscountAmount { get; set; }
        public int? ShippingAmount { get; set; }
        public int? DutyAmount { get; set; }
        public string TaxInvoiceCode { get; set; }
        public decimal? LocalTaxAmount { get; set; }
        public string LocalTaxIndicator { get; set; }
        public decimal? NationalTaxAmount { get; set; }
        public string NationalTaxIndicator { get; set; }
        public string OrderCode { get; set; }
        public DateTime? OrderDate { get; set; }
        public string CommodityCode { get; set; }
        public string CustomerAccountTaxID { get; set; }
        public string ItemCount { get; set; }
        public Guid? WalletPaymentMethodID { get; set; }
        public List<object> Items { get; set; }

        public AuthTransaction ShallowCopy()
        {
            return (AuthTransaction)this.MemberwiseClone();
        }

    }
}
