﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class ReceiptStream
    {
        public System.IO.Stream Stream { get; set; }
        public Int64 TransactionID { get; set; }
    }
}
