﻿using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class BaseTransaction: IBaseRequest
    {
        // RequestHeader fields
        [RequestHeader]
        public string ClientIdentifier { get; set; }
        [RequestHeader]
        public string TransactionID { get; set; }
        [RequestHeader]
        public virtual string RequestType { get; private set; }
        [RequestHeader]
        public string RequestDateTime { get; set; }
        [RequestHeader]
        public string User { get; set; }
        [RequestHeader]
        public string Password { get; set; }

        /// <summary>
        /// The Gateway Transaction Id for transactions that have been processed. Otherwise, null.
        /// </summary>
        [XmlIgnore]
        public string GatewayTransId { get; set; }

        public BaseTransaction()
        {
            TransactionID = Guid.NewGuid().ToString();
            RequestDateTime = DateTime.UtcNow.ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// Returns a Base64 encoding xml representation of the transaction suitable for sending to the Payment Service.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string xml = TransactionXml.ToString();

            //example
            //xml="<requestHeader><ClientIdentifier>TOPE</ClientIdentifier><TransactionID>1234-5678-9012-3456</TransactionID><RequestType>004</RequestType><RequestDateTime>03/17/2014</RequestDateTime><Domain>unipay</Domain><User>bptest</User><Password>Kgcfei!780</Password><requestMessage><TransactionType>sale-auth</TransactionType><transindustrytype>RE</transindustrytype><PaymentAccountNumber>4111111111111111</PaymentAccountNumber><ExpirationDate>0814</ExpirationDate><SecurityCode>999</SecurityCode><AcctType>R</AcctType><InvoiceNum>12345</InvoiceNum><PONum>12345</PONum><CustomerID>12345</CustomerID><AccountholderName>JohnDoe</AccountholderName><AccountStreet>1234WAmericaStreet</AccountStreet><AccountZip>90897</AccountZip><Amount>15000</Amount><HolderType>O</HolderType><TaxRate>600</TaxRate><TaxAmount>600</TaxAmount><TaxIndicator>P</TaxIndicator><ShipToName>JohnDoe</ShipToName><ShipToStreet>1234AnywhereStreet</ShipToStreet><ShipToCity>Metropolis</ShipToCity><ShipToState>NY</ShipToState><ShipToZip>12345</ShipToZip><ShipToCountryCode>US</ShipToCountryCode><ShippingOriginZip>08978</ShippingOriginZip><DiscountAmount>400</DiscountAmount><ShippingAmount>1200</ShippingAmount><DutyAmount>000</DutyAmount><TaxInvoiceCode>1234</TaxInvoiceCode><LocalTaxAmount>000</LocalTaxAmount><LocalTaxIndicator>E</LocalTaxIndicator><NationalTaxAmount>000</NationalTaxAmount><NationalTaxIndicator>E</NationalTaxIndicator><OrderCode>14</OrderCode><OrderDate>06012013</OrderDate><CommodityCode>123</CommodityCode><CustomerAccountTaxID>12345</CustomerAccountTaxID><ItemCount>3</ItemCount><Item><Code>000000000001</Code><CommodityCode>WIDGET</CommodityCode><Description>DaubSpotter</Description><Quantity>10</Quantity><UnitCostAmount>99</UnitCostAmount><UnitMeasure>ea</UnitMeasure><TaxRate>150</TaxRate><TaxAmount>399</TaxAmount><TaxIndicator>P</TaxIndicator><TaxCode>LOCL</TaxCode><DiscountRate>150</DiscountRate><DiscountAmount>399</DiscountAmount><TotalAmount>990</TotalAmount><IsCredit>false</IsCredit></Item><Item><Code>000000000002</Code><CommodityCode>WHATSIT</CommodityCode><Description>BingoSpotter</Description><Quantity>10</Quantity><UnitCostAmount>49</UnitCostAmount><UnitMeasure>ea</UnitMeasure><TaxRate>150</TaxRate><TaxAmount>499</TaxAmount><TaxIndicator>P</TaxIndicator><TaxCode>LOCL</TaxCode><DiscountRate>150</DiscountRate><DiscountAmount>499</DiscountAmount><TotalAmount>490</TotalAmount><IsCredit>false</IsCredit></Item></requestMessage></requestHeader>";

            return Convert.ToBase64String(Encoding.UTF8.GetBytes(xml));
        }

        public string ToXml(bool maskSensitiveData = false)
        {
            if (maskSensitiveData)
            {
                // Dont affect the original element!
                var transactionCopy = new XElement(TransactionXml);
                var request = transactionCopy.Element("requestMessage");

                transactionCopy.MaskValue(PaymentConstants.Fields.Password, (s) => { return new string(PaymentConstants.Security.DefaultMaskCharacter, PaymentConstants.Security.PasswordMaskLength); });

                request.MaskValue(PaymentConstants.Fields.PaymentAccountNumber, UtilityHelper.MaskCardNumber);
                request.MaskValue(PaymentConstants.Fields.BankAccountNumber, UtilityHelper.MaskACHAccountNumber);
                request.MaskValue(PaymentConstants.Fields.SecurityCode, (s) => { return new string(PaymentConstants.Security.DefaultMaskCharacter, s.Length); });
                request.MaskValue(PaymentConstants.Fields.ExpirationDate, (s) => { return new string(PaymentConstants.Security.DefaultMaskCharacter, s.Length); });

                return transactionCopy.ToString();
            }
            else
            {
                return TransactionXml.ToString();
            }
        }

        protected XElement TransactionXml
        {
            get
            {
                XElement xel = new XElement("requestHeader");
                AddHeaderProperties(ref xel);

                XElement xelMessage = new XElement("requestMessage");
                AddRequestMessageProperties(ref xelMessage);

                xel.Add(xelMessage);

                return xel;
            }
        }

        private void AddHeaderProperties(ref XElement xel)
        {
            foreach (var pi in typeof(BaseTransaction).GetProperties()
                .Where(p => p.CustomAttributes.Any(a => a.AttributeType == typeof(RequestHeaderAttribute))))
            {
                xel.Add(new XElement(pi.Name, pi.GetValue(this)));
            }
        }

        private void AddRequestMessageProperties(ref XElement xel)
        {
            foreach (var pi in this.GetType().GetProperties(System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance)
                .Where(p => p.DeclaringType != typeof(BaseTransaction) &&
                !p.CustomAttributes.Any(a => a.AttributeType == typeof(RequestHeaderAttribute))))
            {
                string value = Convert.ToString(pi.GetValue(this));

                if (!string.IsNullOrEmpty(value))
                {
                    xel.Add(new XElement(pi.Name, value));
                }
            }

        }
    }
}
