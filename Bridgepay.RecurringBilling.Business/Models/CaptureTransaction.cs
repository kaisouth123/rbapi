﻿using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class CaptureTransaction : VoidTransaction
    {
        [RequestHeader]
        public override string RequestType
        {
            get { return PaymentConstants.RequestType.Capture; }
        }

        public override string TransactionType
        {
            get { return PaymentConstants.TransType.Capture; }
        }
    }
}
