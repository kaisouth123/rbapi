﻿using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Helpers.Utility;
using Bridgepay.RecurringBilling.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class CustomerEmail: Email
    {
        public CustomerEmail(IEmailEngine engine = null)
            : base()
        {
            if (engine == null)
                Engine = new ExchangeHelper();
            IsHTML = true;
        }

        public IEmailEngine Engine { get; set; }

        public void SendEmail(FinancialPartyPaymentInfo info, String header)
        {
            try
            {
                if (BodyTable == null)
                    BodyTable = new ObjectTable();
                BodyTable.TableHeader = header;
                BodyTable.LogoUrl = "http://www.bridgepaynetwork.com/css/white_orange/logo.png";
                GenerateBody<FinancialPartyPaymentInfo>(info, new List<string> { "Account", "ChargedAmount", "Status", "Responses" }, new List<string> { "Responses" }, new List<string> { "Status,LastFour,Message" });
                Engine.SendEmail(this);
            }
            catch (Exception ex)
            {
                //Add logging here
            }
        }
    }
}
