﻿using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class MultiUseTokenRequest : BaseTransaction
    {
        public override string RequestType
        {
            get
            {
                if (!string.IsNullOrEmpty(PaymentAccountNumber))
                    return PaymentConstants.RequestType.WalletToken; // Cards
                else
                    return PaymentConstants.RequestType.TokenizeAccount; // ACH
            }
        }

        public string PaymentAccountNumber { get; set; }
        public string ExpirationDate { get; set; }
        public string BankAccountNum { get; set; }
    }
}
