﻿using Bridgepay.RecurringBilling.DataLayer;
using Bridgepay.RecurringBilling.DataLayer.Accessors;
using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business
{
    public class PaymentInfo
    {
        public int ContractId { get; set; }
        public int BillAmount { get; set; }
        public int TaxAmount { get; set; }
        public int? AmountRemaining { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime LastBillDate { get; set; }
        public DateTime? NextBillDate { get; set; }
        public String DaysString { get; set; }
        public int? Frequency { get; set; }
        public int? DayMonth { get; set; }
        public int? WeekMonth { get; set; }
        public int? Month { get; set; }
        public Interval Interval { get; set; }

        public PaymentInfo(Credential credentials, int contractId, IContractAccessor accessor = null)
        {
            using (var context = new VendorContext())
            {
                accessor = accessor == null ? new ContractAccessor(credentials) : accessor;
                Contract contract = accessor.ContractRepository.Find(contractId);
                BillingFrequency frequency = accessor.BillingFrequencyRepository.AllIncludingSetUnchanged(f => f.Interval).ToList().Where(f => f.Id == contract.BillingFrequencyId).ToList().FirstOrDefault();
                ContractId = contractId;
                BillAmount = contract.BillAmount;
                TaxAmount = contract.TaxAmount;
                AmountRemaining = contract.AmountRemaining;
                LastBillDate = contract.LastBillDate != null ? (DateTime)contract.LastBillDate : contract.StartDate.AddDays(-1);
                DaysString = frequency.Days;
                Frequency = frequency.Frequency;
                WeekMonth = frequency.WeekMonth;
                NextBillDate = contract.NextBillDate;
                Month = frequency.Month;
                DayMonth = frequency.DayMonth;
                Interval = frequency.Interval;
                StartDate = contract.LastBillDate == null ? contract.StartDate.AddDays(-1) : contract.StartDate;
            }
        }

        public List<String> ReturnDays()
        {
            List<String> days = new List<string>();

            Char[] dayMasks = DaysString.ToCharArray();

            if (dayMasks[0].ToString() == "1")
                days.Add("Sunday");
            if (dayMasks[1].ToString() == "1")
                days.Add("Monday");
            if (dayMasks[2].ToString() == "1")
                days.Add("Tuesday");
            if (dayMasks[3].ToString() == "1")
                days.Add("Wednesday");
            if (dayMasks[4].ToString() == "1")
                days.Add("Thursday");
            if (dayMasks[5].ToString() == "1")
                days.Add("Friday");
            if (dayMasks[6].ToString() == "1")
                days.Add("Saturday");

            return days;
        }

        public List<int> ReturnDayNumbers()
        {
            List<int> days = new List<int>();

            Char[] dayMasks = DaysString.ToCharArray();
            int count = dayMasks.Count();
            for (int x = 0; x < count; x++)
            {
                if (dayMasks[x].ToString() == "1")
                    days.Add(x);
            }

            return days;
        }

        
    }
}
