﻿using Bridgepay.RecurringBilling.Business.Helpers;
using Bridgepay.RecurringBilling.Business.Helpers.Utility;
using Bridgepay.RecurringBilling.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class ContractExpiringEmail: Email
    {
        public ContractExpiringEmail(IEmailEngine engine = null)
            : base()
        {
            if (engine == null)
                Engine = new ExchangeHelper();
            IsHTML = true;
        }

        public IEmailEngine Engine { get; set; }

        public void SendEmail(ContractPaymentInfo info, String header)
        {
            if (BodyTable == null)
                BodyTable = new ObjectTable();
           BodyTable.TableHeader = header;
           BodyTable.LogoUrl = "http://www.bridgepaynetwork.com/css/white_orange/logo.png";
           GenerateBody<ContractPaymentInfo>(info, new List<string> { "MerchantName", "ContractId", "Contract", "Status", "LastBillDate", "LastPayment", "CustomerName", "Address", "CityStateZip", "Email", "Phone" }, null, null);
           Engine.SendEmail(this);
        }
    }
}
