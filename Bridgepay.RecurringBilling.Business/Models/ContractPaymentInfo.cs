﻿using Bridgepay.RecurringBilling.Business.Enums;
using Bridgepay.RecurringBilling.Models;
using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    [DataContract]
    public class ContractPaymentInfo: BaseEntity
    {
        public ContractPaymentInfo()
        { }

        public ContractPaymentInfo(Contract contract, FinancialResponsibleParty party, Contact customer, Merchant merchant)
        {
            MerchantId = merchant.Id;
            MerchantName = merchant.Name;
            ContractId = contract.Id;
            Contract = contract.ContractName;
            Status = (StatusCode)contract.Status;
            LastPayment = Convert.ToDouble(contract.BillAmount / 100);
            LastBillDate = contract.LastBillDate != null ? (DateTime)contract.LastBillDate : contract.StartDate;
            FinancialResponsbilePartyID = party.Id;
            CustomerName = customer.ContactFirstName + " " + customer.ContactLastName;
            if (customer.ContactAddress != null)
            {
                Address = customer.ContactAddress.AddressStreet + " " + customer.ContactAddress.AddressStreetTwo;
                CityStateZip = customer.ContactAddress.AddressCity + ", " + customer.ContactAddress.AddressState + " " + customer.ContactAddress.AddressZip;
            }
            else
                Address = "Not on File";
            Email = customer.ContactEmail;
            Phone = "D: " + customer.ContactDayPhone + ", E: " + customer.ContactEveningPhone + ", M: " + customer.ContactMobilePhone;
        }

        [DataMember]public int MerchantId { get; set; }
        [DataMember]public string MerchantName { get; set;}
        [DataMember]public int ContractId { get; set; }
        [DataMember]public string Contract { get; set; }
        [DataMember]public StatusCode Status { get; set; }
        [DataMember]public string Subject { get; set;}
        [DataMember]public double LastPayment { get; set; }
        [DataMember]public DateTime LastBillDate { get; set; }
        [DataMember]public int FinancialResponsbilePartyID { get; set; }
        [DataMember]public string CustomerName { get; set; } // Title + First + Last Name
        [DataMember]public string Address { get; set; }
        [DataMember]public string CityStateZip { get; set; }
        [DataMember]public string Email { get; set; }
        [DataMember]public string Phone { get; set; } // m: mobile, w: work h: home
        [DataMember]List<String> Messages {get; set;}
    }
}
