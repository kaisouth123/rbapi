﻿using Bridgepay.RecurringBilling.Business.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Bridgepay.RecurringBilling.Business.Models
{
    [DataContract]
    public class PaymentProcessResponse
    {
        public PaymentProcessResponse()
        {
            Status = 0;
            PaymentApproved = false;
        }

        [DataMember]public int FinancialResponsiblePartyId { get; set; }
        [DataMember]public String LastFour { get; set; }
        [DataMember]public String Name { get; set; }
        [DataMember]public StatusCode Status { get; set; }
        [DataMember]public int AmountPaid { get; set; }
        [DataMember]public DateTime PaymentTime { get; set; }
        [DataMember]public String Message { get; set; }
        [DataMember]public bool PaymentApproved { get; set; }

        
    }
}
