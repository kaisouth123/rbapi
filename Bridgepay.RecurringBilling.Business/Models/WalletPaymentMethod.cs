﻿using Bridgepay.RecurringBilling.Business.WalletService;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    [DataContract(IsReference = true)]
    public class WalletPaymentMethod : BaseEntity
    {
        [DataMember]public Guid? PaymentMethodId { get; set; }
        [DataMember]public PaymentMethodType PaymentMethodType { get; set; }
        [DataMember]public string CardType { get; set; }
        [DataMember]public string Description { get; set; }
        [DataMember]public string ExpirationDate { get; set; }
        [DataMember]public string LastFour { get; set; }
        [DataMember]public string Token { get; set; }
        [DataMember]public string RoutingNo { get; set; }
        [DataMember]public string AccountType { get; set; }
        [DataMember]public string AccountHolderName { get; set; }
        [DataMember]public string AccountHolderAddress { get; set; }
        [DataMember]public string AccountHolderCity { get; set; }
        [DataMember]public string AccountHolderState { get; set; }
        [DataMember]public string AccountHolderZip { get; set; }
        [DataMember]public string AccountHolderPhone { get; set; }
        [DataMember]public string AccountHolderEmail { get; set; }
        [DataMember]public bool IsActive { get; set; }
        [DataMember]public int Order { get; set; }
        [DataMember]public Guid WalletId { get; set; }
        [DataMember]public DateTime CreateDate { get; set; }
        [DataMember]public DateTime? UpdateDate { get; set; }

        // Navigation
        [DataMember]
        public Wallet Wallet { get; set; }
    }
}
