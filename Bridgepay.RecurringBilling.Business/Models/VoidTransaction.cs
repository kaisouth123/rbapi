﻿using Bridgepay.RecurringBilling.Business.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class VoidTransaction : BaseTransaction
    {
        [RequestHeader]
        public override string RequestType
        {
            get { return PaymentConstants.RequestType.VoidRefund; }
        }

        public int MerchantCode { get; set; }
        public int MerchantAccountCode { get; set; }
        public int Amount { get; set; }
        public string ReferenceNumber { get; set; }
        public string TransactionCode { get; set; }
        public virtual string TransactionType
        {
            get { return PaymentConstants.TransType.Void; }
        }
        

        public static explicit operator VoidTransaction(AuthTransaction auth)
        {
            return new VoidTransaction
            {
                ClientIdentifier = auth.ClientIdentifier,
                TransactionID = auth.TransactionID,
                RequestDateTime = auth.RequestDateTime,
                User = auth.User,
                Password = auth.Password,
                Amount = auth.Amount,
                MerchantAccountCode = auth.MerchantAccountCode,
                MerchantCode = Convert.ToInt32(auth.MerchantCode),
                ReferenceNumber = auth.GatewayTransId,
                TransactionCode = auth.TransactionID
            };
        }

    }
}
