﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class PaymentResponse: BaseEntity
    {

        public PaymentResponse()
        {
            Processed = false;
            NumFailures = 0;
            Status = 0; //Ready
            Exceptions = new List<Exception>();
        }
        
        public bool Processed { get; set; }
        public int? ChargeId { get; set; }
        public int? NumFailures { get; set; }
        public int? Status { get; set; }
        public int AmountCharged { get; set; }
        public List<Exception> Exceptions { get; set; }
    }
}
