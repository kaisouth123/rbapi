﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class IndustryType
    {
        public IndustryType()
        {
            IndustryFields = new List<IndustryField>();
        }

        public int ID { get; set; }
        public string Abbreviation { get; set; }
        public string Description { get; set; }
        public virtual List<IndustryField> IndustryFields { get; set; }
    }
}
