﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Helpers
{
    public static class PaymentConstants
    {
        public static class RequestType
        {
            public const string OneTimeToken = "000";
            public const string WalletToken = "001";
            public const string WalletSecurityCode = "002";
            public const string GetEncryptionKey = "003";
            public const string ProcessAuthorization = "004";
            public const string BinLookup = "005";
            public const string CheckPasswordExpiration = "006";
            public const string UpdatePassword = "007";
            public const string AccountLookup = "008";
            public const string TokenizeBatch = "009";
            public const string ValidateBatch = "010";
            public const string GetMerchantInfo = "011";
            public const string VoidRefund = "012";
            public const string DeleteWalletPayment = "013";
            public const string DeleteWallet = "014";
            public const string GetWalletPayment = "015";
            public const string GetWallet = "016";
            public const string SaveWalletPayment = "017";
            public const string SaveWallet = "018";
            public const string Capture = "019";
            public const string CloseBatch = "020";
            public const string TokenizeAccount = "013";
        }

        public static class Fields
        {
            public const string Password = "Password";
            public const string PaymentAccountNumber = "PaymentAccountNumber";
            public const string SecurityCode = "SecurityCode";
            public const string ExpirationDate = "ExpirationDate";
            public const string BankAccountNumber = "BankAccountNum";
        }

        public static class ClientIdentifier
        {
            public const string VT = "MyBridgepay VT";
        }

        public static class TransType
        {
            public const string Sale = "sale";
            public const string Auth = "sale-auth";
            public const string Credit = "credit";
            public const string CreditAuth = "credit-auth";
            public const string Void = "void";
            public const string Refund = "refund";
            public const string Capture = "capture";
        }

        public static class TransactionStatus
        {
            public const string Voided = "Voided";
            public const string Open = "Open";
            public const string Confirmed = "Confirmed";
        }

        public static class ApplicationIdentifiers
        {
            public const string VT = "MyBridgepay VT";
            public const string Weblink = "Weblink";
        }

        public static class Security
        {
            public const int PasswordMaskLength = 8;
            public const int UnmaskedCardLength = 4;
            public const char DefaultMaskCharacter = '*';
        }
    }
}
