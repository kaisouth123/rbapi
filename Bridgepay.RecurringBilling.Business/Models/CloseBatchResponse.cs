﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class CloseBatchResponse : BaseResponse
    {
        public string CycleCode { get; set; }
        public string GatewayTransId { get; set; }
        public string GatewayMessage { get; set; }
        public string GatewayResult { get; set; }
        public string MerchantAccountCode { get; set; }
        public string ResponseType { get; set; }

        public CloseBatchResponse(string base64EncodedTransaction) : base(base64EncodedTransaction) { }
    }
}
