﻿using Bridgepay.RecurringBilling.Business.Helpers.Utility;
using Bridgepay.RecurringBilling.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Models
{
    public class Email: IEmailTemplate
    {
        public Email()
        {
            BodyTable = new ObjectTable();
        }

        [Key]
        [DataMember]public string EmailID { get; set; }
        [DataMember]public string Subject { get; set; }
        [DataMember]public string To { get; set; }
        [DataMember]public string CC { get; set; }
        [DataMember]public string BCC { get; set; }
        [DataMember]public string Body { get; set; }
        [DataMember]public string SenderName { get; set; }
        [DataMember]public string ReplyTo { get; set; }
        [DataMember]public bool IsHTML { get; set; }
        public ObjectTable BodyTable { get; set; }

        public void GenerateBody<T>(T entity, List<string> includeFields = null, List<string> includeCollections=null, List<string> childFields= null)
        {
            Body = BodyTable.BuildTable<T>(entity, includeFields, includeCollections, childFields);
        }
    }
}
