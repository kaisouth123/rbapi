﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Business.Enums
{
    [DataContract]
    public enum StatusCode
    {
        [EnumMember]ACTIVE=0,
        [EnumMember]APPROVED=1,
        [EnumMember]ERROR=2,
        [EnumMember]DECLINED=3,
        [EnumMember]CANCELED=4,
        [EnumMember]PROCESSING=5,
        [EnumMember]DEACTIVATED=6,
        [EnumMember]COMPLETE=7,
    }
}
