﻿using System.Collections.Generic;

namespace Bridgepay.RecurringBilling.Models
{
    public class AccountListSet: BaseEntity, IEntitySet<Account>
    {
        public AccountListSet()  : base()
        {
            this.EntityList = new List<Account>();
        }

        public IEnumerable<Account> EntityList {get; set;}
        public int Count { get; set; }
      
    }
}
