﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Models
{
    public class ContractFilterModel
    {
        public string LastFour { get; set; }
        public virtual List<ContractCustomField> ContractCustomFields { get; set; }
    }
}
