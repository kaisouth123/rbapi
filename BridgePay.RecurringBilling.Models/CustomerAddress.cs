﻿using Bridgepay.RecurringBilling.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class CustomerAddress : BaseEntity, IAddress
    {
        public CustomerAddress()
        {
            this.Customers = new List<Customer>(); 
        }

        [DataMember]public String AddressStreet { get; set; }
        [DataMember]public String AddressStreetTwo { get; set; }
        [DataMember]public String AddressCity { get; set; }
        [DataMember]public String AddressState { get; set; }
        [DataMember]public String AddressCountry { get; set; }
        [DataMember]public String AddressZip { get; set; }

        //Navigation
        [DataMember]public virtual List<Customer> Customers { get; set; }
    }
}
