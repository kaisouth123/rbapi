﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = true)]
    public partial class MerchantAccount : MinBaseEntity
    {
        [Key]
        [DataMember] public int MerchantAccountID { get; set; }
        [DataMember] public int MerchantID { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public virtual UnipayMerchant Merchant { get; set; }
        //[DataMember] public Nullable<System.Guid> MerchantAccountSiteId { get; set; }

    }
}
