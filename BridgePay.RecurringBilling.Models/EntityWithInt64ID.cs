﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using Bridgepay.Core.Framework;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class EntityWithInt64ID : Bridgepay.Core.Framework.BaseEntity, IHasInt64ID
    {
        [DataMember]
        public Int64 ID { get; set; }

        public EntityWithInt64ID() { }

    }
}
