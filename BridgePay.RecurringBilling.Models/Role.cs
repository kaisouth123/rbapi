﻿using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract] 
    public class Role :  BaseEntity
    {
        public Role()
        {
            this.Users = new List<User>();
        }

        [DataMember] public int RoleId { get; set; }
        [DataMember] public string RoleName { get; set; }
        [DataMember] public Nullable<int> UserType { get; set; }
        [DataMember] public Nullable<bool> IsSystemRole { get; set; }
        [DataMember] public Nullable<int> ResellerId { get; set; }
        [IgnoreDataMember] public virtual ICollection<User> Users { get; set; }
    }
}
