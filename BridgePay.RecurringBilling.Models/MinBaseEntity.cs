﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.Framework.Interfaces;
using System.Reflection;
using Bridgepay.Core.Framework;
using System.Collections;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference=false)]
    public class MinBaseEntity : Bridgepay.Core.Framework.BaseEntity, IBaseEntity
    {
        [NotMapped]
        [DataMember]
        public int Id { get; set; }

        [NotMapped][DataMember]
        public ObjectState ObjectState { get; set; }

        public MinBaseEntity() { }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            object thisKeyValue = GetKeyValue(this);
            object thatKeyValue = GetKeyValue(obj);

            if (thisKeyValue == null && thatKeyValue == null) return true;
            if (thisKeyValue == null || thatKeyValue == null) return false;

            return thisKeyValue.Equals(thatKeyValue);

        }

        private object GetKeyValue(object obj)
        {
            var props = obj.GetType().GetProperties();
            object returnValue = new object();
            foreach (var prop in props)
            {
                var propattr = prop.GetCustomAttributes(false);

                object attr = (from row in propattr where row.GetType() == typeof(KeyAttribute) select row).FirstOrDefault();
                if (attr == null)
                    continue;

                KeyAttribute myattr = (KeyAttribute)attr;

                returnValue = prop.GetValue(obj, null);
            }

            return returnValue;
        }

        public T clone<T>(List<String> ignoreFields = null)
        {
            T newClass = (T)Activator.CreateInstance(this.GetType());

            FieldInfo[] fis = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            PropertyInfo[] props = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            int x = 0;
         

            foreach (PropertyInfo property in props)
            {
                if (ignoreFields == null || !ignoreFields.Contains(property.Name))
                {
                    object fieldValue = property.GetValue(this);
                    if (property.GetType().Namespace != this.GetType().Namespace)
                        property.SetValue(newClass, fieldValue);
                    else
                        property.SetValue(newClass, clone<T>(ignoreFields));

                    x++;
                }
            }

            return newClass;
        }

        public void CopyFieldsFrom(object entity, List<String> ignoreFields = null)
        {
            FieldInfo[] fis = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            PropertyInfo[] DestProps = this.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            PropertyInfo[] SourceProps = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            
            int x = 0;

            foreach (PropertyInfo property in SourceProps)
            {
                if (ignoreFields == null || !ignoreFields.Contains(property.Name) && DestProps.Where(c=> c.Name == property.Name).FirstOrDefault() != null)
                {
                    PropertyInfo destProp = DestProps.Where(c => c.Name == property.Name).FirstOrDefault();
                    object fieldValue = property.GetValue(entity);

                    try
                    {
                        destProp.SetValue(this, Convert.ChangeType(fieldValue, destProp.GetType()));
                    }
                    catch { }

                    x++;
                }
            }
        }

        public void SetUnchanged()
        {
            this.ObjectState = Core.Framework.ObjectState.Unchanged;
            FieldInfo[] fis = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (FieldInfo property in fis)
            {
                object objectValue = property.GetValue(this);
                if (typeof(BaseEntity).IsAssignableFrom(property.FieldType) && objectValue != null)
                {
                    IBaseEntity baseEntity = property.GetValue(this) as IBaseEntity;
                    baseEntity.ObjectState = Core.Framework.ObjectState.Unchanged;
                    baseEntity.SetUnchanged();
                }
            }

            return;
        }
        
    }
}
