﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class Charge : EntityWithInt64ID, IHasInt64ID
    {
        [DataMember]public int? ContractId { get; set; }
        [DataMember]public int? Amount { get; set; }
        [DataMember]public int? TaxAmount { get; set; }

        [DataMember]public virtual Contract Contract { get; set; }
    }
}
