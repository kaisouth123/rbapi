﻿using Bridgepay.Core.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class ScheduledPayment : BaseEntity, IObjectWithState
    {
        [DataMember]public int ContractId { get; set; }
        [DataMember]public DateTime BillDate { get; set; }
        [DataMember]public int BillAmount { get; set; }
        [DataMember]public bool Paid {get; set;}

        //Navigation
        [DataMember]public virtual Contract Contract { get; set; }
        
    }
}
