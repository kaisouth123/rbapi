﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    public class OrganizationMap : Core.Framework.BaseEntity
    {
        public int OrgId { get; set; }

        public int? ParentId { get; set; }

        public int? GrandParentId { get; set; }

        public int? GreatGrandParentId { get; set; }

        public string OrgType { get; set; }

        public string ParentType { get; set; }

        public string GrandParentType { get; set; }

        public string GreatGrandParentType { get; set; }

        public int? UnipayId { get; set; }

        public Guid? WalletSiteId { get; set; }

        public string OrganizationName { get; set; }

        public string ParentName { get; set; }

        public string GrandParentName { get; set; }

        public string GreatGrandParentName { get; set; }

        public int? ParentUnipayId { get; set; }

        public int? GrandParentUnipayId { get; set; }

        public int? GreatGrandParentUnipayId { get; set; }
    }
}
