﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Models
{
    public class ChargeRefence : BaseEntity
    {
        public int ContractId { get; private set; }
        public virtual Contract Contract { get; private set; }
    }
}
