﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class Contact : BaseEntity, IContact
    {
        public Contact()
        {
            this.FinancialResponsibleParties = new List<FinancialResponsibleParty>();
            this.Products = new List<Product>();
        }

        [DataMember]public int ContactTypeId { get; set; }
        [DataMember]public string ContactFirstName { get; set; }
        [DataMember]public string ContactLastName { get; set; }
        [DataMember]public string CompanyName { get; set; } 
        [DataMember]public int? AddressId { get; set; }
        [DataMember]public string ContactDepartment { get; set; }
        [DataMember]public string ContactTitle { get; set; }
        [DataMember]public string ContactEmail { get; set; }
        [DataMember]public string ContactDayPhone { get; set; }
        [DataMember]public string ContactEveningPhone { get; set; }
        [DataMember]public string ContactMobilePhone { get; set; }
        [DataMember]public string ContactFax { get; set; }
        [DataMember]public bool IsActive { get; set; }

        //Navigation
        [DataMember]public virtual List<Product> Products { get; set; }
        [DataMember]public virtual List<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        [DataMember]public virtual Address ContactAddress { get; set; }
        [DataMember]public virtual ContactType ContactType { get; set; }
    }
}

