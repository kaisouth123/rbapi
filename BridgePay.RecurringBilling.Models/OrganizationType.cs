﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class OrganizationType : Core.Framework.BaseEntity
    {
        [Key]
        [DataMember]
        public int Id { get; set; } // ID (Primary key)
        [DataMember]
        public string Name { get; set; } // NAME (length: 50)
        [DataMember]
        public int OrganizationLevel { get; set; } // ORGANIZATION_LEVEL
        [DataMember]
        public int SubGroupLevel { get; set; } // CAN_HAVE_CHILDREN
        [DataMember]
        public System.DateTime CreateDate { get; set; } // CREATE_DATE
        [DataMember]
        public System.DateTime? LastUpdated { get; set; } // LAST_UPDATED
        [DataMember]
        public string LastModifiedBy { get; set; } // LAST_MODIFIED_BY (length: 50)
        [DataMember] public string GatewayObjectType { get; set; }

        // Reverse navigation

        /// <summary>
        /// Child Organizations where [ORGANIZATION].[ORGANIZATION_TYPE_ID] point to this entity (FK_Organization_OrganizationType)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<Organization> Organizations { get; set; } // ORGANIZATION.FK_Organization_OrganizationType

        public OrganizationType()
        {
            Organizations = new System.Collections.Generic.List<Organization>();
        }
    }
}
