﻿using Bridgepay.RecurringBilling.Models.Interfaces;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class Product : EntityWithIntID, IHasMerchantId
    {
        public Product()
        {
            this.Contracts = new List<Contract>();
        }

        [DataMember]public string ProductName { get; set; }
        [DataMember]public int ProductDefaultAmount { get; set; }
        [DataMember]public string Description { get; set; }
        [DataMember]public int ContactId { get; set; }
        [DataMember]public int MerchantId { get; set; }
        [DataMember]public bool IsActive { get; set; }

        //Navigation
        [DataMember]public virtual List<Contract> Contracts { get; set; }
        [DataMember]public virtual Contact Contact { get; set; }
    }
}