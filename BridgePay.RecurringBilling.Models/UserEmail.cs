﻿using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class UserEmail : BaseEntity
    {
        [DataMember] public int UserEmailId { get; set; }
        [DataMember] public int UserId { get; set; }
        [DataMember] public string Email { get; set; }
        [DataMember] public string Type { get; set; }
        [DataMember] public bool Primary { get; set; }
        [IgnoreDataMember] public virtual User User { get; set; }
    }
}
