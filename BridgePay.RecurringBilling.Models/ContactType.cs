﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridgepay.RecurringBilling.Models
{   
    [DataContract]
    public class ContactType : BaseEntity
    {
        public ContactType()
        {
            this.Contacts = new List<IContact>();
        }
        
        [DataMember]public string TypeName { get; set; }

        //Navigation
        [DataMember]public virtual List<IContact> Contacts { get; set; }

    }
}
