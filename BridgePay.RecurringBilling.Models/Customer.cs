﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class Customer : BaseEntity
    {   
       
        public Customer()
        {
            this.FinancialResponsibleParties = new List<FinancialResponsibleParty>();
        }
        
        [DataMember]public int ContactTypeId { get; set; }
        [DataMember]public string ContactFirstName { get; set; }
        [DataMember]public string ContactLastName { get; set; }
        [DataMember]public string CompanyName { get; set; }      
        [DataMember]public int? AddressId { get; set; }
        [DataMember]public string ContactDepartment { get; set; }
        [DataMember]public string ContactTitle { get; set; }
        [DataMember]public string ContactEmail { get; set; }
        [DataMember]public string ContactDayPhone { get; set; }
        [DataMember]public string ContactEveningPhone { get; set; }
        [DataMember]public string ContactMobilePhone { get; set; }
        [DataMember]public string ContactFax { get; set; }
        [DataMember]public bool IsActive { get; set; }

        //Navigation
        [DataMember]public virtual List<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        [DataMember]public virtual CustomerAddress ContactAddress { get; set; }
        [DataMember]public virtual ContactType ContactType { get; set; }

    }
}
