﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class ChargeTransactionsAux : BigEntity, IBigEntity
    {
        [DataMember]public Int64 ContractId { get; set; }
    }
}
