﻿//using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class CustomField : EntityWithIntID
    {
        [Key]
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int OrganizationId { get; set; }
        [DataMember]
        public string EntityName { get; set; }
        [DataMember]
        public string FieldName { get; set; }
        [DataMember]
        public string FieldDisplayName { get; set; }
        [DataMember]
        public string FieldRegEx { get; set; }
        [DataMember]
        public string FieldDefault { get; set; }
        [DataMember]
        public int? SourceEntity { get; set; }
        [DataMember]
        public int? SourceField { get; set; }
        [DataMember]
        public int FieldTypeId { get; set; }
        [DataMember]
        public int FieldEntityId { get; set; }

        public virtual CustomFieldType CustomFieldType { get; set; }
        public virtual Organization Organization{ get; set; }
        public virtual CustomizableEntity CustomizableEntity { get; set; }
        public virtual ICollection<CustomFieldActivation> CustomFieldActivations { get; set; }
        public virtual ICollection<CustomFieldData> CustomFieldDatas { get; set; }
    }
}
