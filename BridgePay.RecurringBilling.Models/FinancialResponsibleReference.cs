﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridgepay.RecurringBilling.Models
{
    public class FinancialResponsibleReference: BaseEntity
    {
        public FinancialResponsibleReference()
        {
            
        }

        public int AccountId { get; set; }
        public int ContactId { get; set; }
        public int MerchantId { get; set; }
        public Nullable<int> Order { get; set; }
        public bool IsActive { get; set; }
        public virtual Account Account { get; set; }
        public virtual Contact Contact { get; set; }
    }
}
