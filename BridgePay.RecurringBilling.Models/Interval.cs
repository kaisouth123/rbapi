﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class Interval: BaseEntity
    {
        public Interval()
        {
            this.BillingFrequencies = new List<BillingFrequency>();
        }

        [DataMember]public string Name { get; set; }

        //Navigation
        [DataMember]public virtual List<BillingFrequency> BillingFrequencies { get; set; }

    }
}
