﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridgepay.RecurringBilling.Models
{
    public class ContactReference : BaseEntity
    {
        public ContactReference()
        {
            this.FinancialResponsibleParties = new List<FinancialResponsibleParty>();
            this.Products = new List<Product>();
        }
        
        public int ContactTypeId { get; private set; }
        public string ContactFirstName { get; private set; }
        public string ContactLastName { get; private set; }
        public string CompanyName { get; private set; }
        public int AddressId { get; private set; }
        public string ContactDepartment { get; private set; }
        public string ContactTitle { get; private set; }
        public string ContactEmail { get; private set; }
        public string ContactDayPhone { get; private set; }
        public string ContactEveningPhone { get; private set; }
        public string ContactMobilePhone { get; private set; }
        public string ContactFax { get; private set; }
        public bool IsActive { get; set; }

        //Navigation
        public virtual List<Product> Products { get; set; }
        public virtual List<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        public virtual Address ContactAddress { get; set; }
        public virtual ContactType ContactType { get; set; }
    }
}
