﻿using Bridgepay.RecurringBilling.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class Account : BaseEntity, IHasMerchantId
    {
        public Account()
        {
        }

        [DataMember]public int MerchantId { get; set; }
        [DataMember]public string AccountName { get; set; }
        [DataMember]public string AccountNumber { get; set; }
        [DataMember]public DateTime? CreatedOn {  get; private set; }
        [DataMember]public DateTime? ModifiedOn { get; set; }
        [DataMember]public bool IsActive { get; set; }

        //Navigation
        [DataMember]public virtual List<Contract> Contracts { get; set; }
        [DataMember]public virtual List<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
    }
}

