﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models.Interfaces
{
    public interface IAddress
    {
        int Id { get; set; }
        String AddressStreet { get; set; }
        String AddressStreetTwo { get; set; }
        String AddressCity { get; set; }
        String AddressState { get; set; }
        String AddressCountry { get; set; }
        String AddressZip { get; set; }

        //Navigation

        
    }
}
