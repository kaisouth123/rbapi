﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    public interface IEntitySet<T> where T : class
    {
        IEnumerable<T> EntityList { get; set; }
        int Count { get; set; }
    }
}
