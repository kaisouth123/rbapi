﻿namespace Bridgepay.RecurringBilling.Models
{
    public interface IHasIntID
    {
        int Id { get; set; }
    }
}
