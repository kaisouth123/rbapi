﻿namespace Bridgepay.RecurringBilling.Models.Interfaces
{
    /// <summary>
    /// An entity that includes a merchant ID property.
    /// </summary>
    public interface IHasMerchantId
    {
        int MerchantId { get; set; }
    }
}
