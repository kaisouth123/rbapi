﻿namespace Bridgepay.RecurringBilling.Models.Interfaces
{
    public interface IHasOrganizationScope
    {
        OrganizationScope OrganizationScope { get; set; }
    }
}
