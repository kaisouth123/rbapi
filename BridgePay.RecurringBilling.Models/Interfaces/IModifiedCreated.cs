﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    public interface IModifiedCreated
    {
        DateTime ModifiedOn { get; set; }
        DateTime CreatedOn { get; }
    }
}
