﻿using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;

namespace Bridgepay.RecurringBilling.Models
{
    public interface IContact
    {
        int? AddressId { get; set; }
        string CompanyName { get; set; }
        Address ContactAddress { get; set; }
        string ContactDayPhone { get; set; }
        string ContactDepartment { get; set; }
        string ContactEmail { get; set; }
        string ContactEveningPhone { get; set; }
        string ContactFax { get; set; }
        string ContactFirstName { get; set; }
        string ContactLastName { get; set; }
        string ContactMobilePhone { get; set; }
        string ContactTitle { get; set; }
        ContactType ContactType { get; set; }
        ObjectState ObjectState { get; set; }
        int ContactTypeId { get; set; }
        List<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
        List<Product> Products { get; set; }
        int Id { get; set; }
        bool IsActive { get; set; }
    }
}
