﻿using System;

namespace Bridgepay.RecurringBilling.Models
{
    public interface IHasInt64ID
    {
        Int64 ID { get; set; }
    }
}
