﻿using Bridgepay.Core.Framework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    public interface IBaseEntity: IObjectWithState,IHasIntID
    {
        //int Id { get; set; }
        void SetUnchanged();
    }
}
