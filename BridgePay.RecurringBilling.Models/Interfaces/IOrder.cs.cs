﻿using Bridgepay.RecurringBilling.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridgepay.RecurringBilling.Models
{
    public interface IOrder
    {
        int Order { get; set; }
    }
}
