﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.Core.Framework.Interfaces;
using Bridgepay.Core.Framework;
using System.Reflection;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class BigEntity : IBigEntity
    {
        [DataMember]
        public Int64 ID { get; set; }

        [NotMapped]
        [DataMember]
        public ObjectState ObjectState { get; set; }

        public BigEntity() { }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            object thisKeyValue = GetKeyValue(this);
            object thatKeyValue = GetKeyValue(obj);

            if (thisKeyValue == null && thatKeyValue == null) return true;
            if (thisKeyValue == null || thatKeyValue == null) return false;

            return thisKeyValue.Equals(thatKeyValue);

        }

        private object GetKeyValue(object obj)
        {
            var props = obj.GetType().GetProperties();
            object returnValue = new object();
            foreach (var prop in props)
            {
                var propattr = prop.GetCustomAttributes(false);

                object attr = (from row in propattr where row.GetType() == typeof(KeyAttribute) select row).FirstOrDefault();
                if (attr == null)
                    continue;

                KeyAttribute myattr = (KeyAttribute)attr;

                returnValue = prop.GetValue(obj, null);
            }

            return returnValue;
        }
        public void SetUnchanged()
        {
            this.ObjectState = Core.Framework.ObjectState.Unchanged;
            FieldInfo[] fis = this.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (FieldInfo property in fis)
            {
                object objectValue = property.GetValue(this);
                if (typeof(BaseEntity).IsAssignableFrom(property.FieldType) && objectValue != null)
                {
                    IBaseEntity baseEntity = property.GetValue(this) as IBaseEntity;
                    baseEntity.ObjectState = Core.Framework.ObjectState.Unchanged;
                    baseEntity.SetUnchanged();
                }
            }

            return;
        }

    }
}
