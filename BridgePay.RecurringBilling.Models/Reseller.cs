﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = true)]
    public partial class Reseller: MinBaseEntity
    {

        public Reseller()
        {
            Merchants = new List<UnipayMerchant>();
        }

        [Key]
        [DataMember] public int ResellerID { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public virtual List<UnipayMerchant> Merchants { get; set; }

    }
}
