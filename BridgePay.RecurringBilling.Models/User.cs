﻿using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class User : MinBaseEntity
    {
        public User()
        {
            this.UserEmails = new List<UserEmail>();
            this.UserNames = new List<UserName>();
            this.Roles = new List<Role>();
        }

        [DataMember] public int UserId { get; set; }
        [DataMember] public string UserName { get; set; }
        [DataMember] public bool LockedOut { get; set; }
        [DataMember] public Nullable<int> ResellerId { get; set; }
        [DataMember] public Nullable<int> UserType { get; set; }
        [DataMember] public Nullable<int> MerchantId { get; set; }
        [DataMember] public Nullable<int> MerchantAccountId { get; set; }
        [DataMember] public bool EnableMsrJavaApplet { get; set; }
        [DataMember] public bool IsApiUser { get; set; }
        //[IgnoreDataMember] public virtual Membership Membership { get; set; }
        [DataMember] public virtual ICollection<UserEmail> UserEmails { get; set; }
        [DataMember] public virtual ICollection<UserName> UserNames { get; set; }
        [DataMember] public virtual ICollection<Role> Roles { get; set; }
        [DataMember] public virtual ICollection<UserAddress> UserAddresses { get; set; }
        
        //[DataMember] public virtual ICollection<UserPhone> UserPhones { get; set; }
    }
}
