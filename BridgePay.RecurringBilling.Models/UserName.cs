﻿using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class UserName : BaseEntity
    {
        [DataMember] public int UserNameId { get; set; }
        [DataMember] public int UserId { get; set; }
        [DataMember] public string FirstName { get; set; }
        [DataMember] public string MiddleName { get; set; }
        [DataMember] public string LastName { get; set; }
        [DataMember] public bool Primary { get; set; }
        [DataMember] public virtual User User { get; set; }
    }
}
