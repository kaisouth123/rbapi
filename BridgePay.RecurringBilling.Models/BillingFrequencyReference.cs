﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridgepay.RecurringBilling.Models
{
    public class BillingFrequencyReference : BaseEntity
    {
        //constructor here to overcome the DateTime to DateTime2 error with SQL and .Net
        public BillingFrequencyReference()
        {
            StartDate = DateTime.UtcNow;
            EndDate = null;
            this.Contracts = new List<Contract>();
        }

        public string FrequencyName { get; private set; }
        public int MerchantId { get; private set; }
        public DateTime? StartDate { get; private set; }
        public DateTime? EndDate { get; private set; }
        public int IntervalId { get; private set; }
        public bool? NoEnd { get; private set; }
        public int? Frequency { get; private set; }
        public int? DayMonth { get; private set; }
        public int? WeekMonth { get; private set; }
        public int? Month { get; private set; }
        public string Days { get; private set; }
        public bool IsActive { get; private set; }

        //Navigation
        public virtual List<Contract> Contracts { get; set; }
        public virtual Interval Interval { get; set; }
     }
}
