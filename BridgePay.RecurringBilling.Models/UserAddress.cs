﻿using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class UserAddress : BaseEntity
    {
        [DataMember] public int UserAddressId { get; set; }
        [DataMember] public int AddressId { get; set; }
        [DataMember] public int UserId { get; set; }
        [DataMember] public bool Primary { get; set; }
        [IgnoreDataMember] public virtual User User { get; set; }
        [DataMember] public virtual Address Address { get; set; }
    }
}
