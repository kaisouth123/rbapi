﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    public class ContractBalance
    {
        public int Id { get; set; }
        public string ContractName { get; set; }
        public int ProductId { get; set; }
        public int BillingFrequencyId { get; set; }
        public int AccountId { get; set; }
        public int TotalAmount { get; set; }
        public int PaidAmount { get; set; }
        public int DueAmount { get; set; }
        public bool Status { get; set; }

        //Navigation
        public virtual List<ChargeTransactionsAux> ChargeTransactionsAuxes { get; set; }
        public virtual Product Product { get; set; }
        public virtual BillingFrequency BillingFrequency { get; set; }
        public virtual Account Account { get; set; }
    }
}
