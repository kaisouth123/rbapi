﻿using Bridgepay.RecurringBilling.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class FinancialResponsibleParty: BaseEntity, IBaseEntity, IHasMerchantId
    {
        public FinancialResponsibleParty()
        {

        }

        [DataMember]public int AccountId { get; set; }
        [DataMember]public int ContactId { get; set; }
        [DataMember]public int MerchantId { get; set; }
        [DataMember]public Nullable<int> Order { get; set; }
        [DataMember]public bool IsActive { get; set; }
        [DataMember]public Guid? Guid {get; set;}
        [DataMember]public virtual Account Account { get; set; }
        [DataMember]public virtual Contact Contact { get; set; }
        [DataMember]public virtual Customer Customer { get; set; }
        [ForeignKey("Guid")]
        [DataMember]public virtual Wallet Wallet { get; set; }
    }
}
