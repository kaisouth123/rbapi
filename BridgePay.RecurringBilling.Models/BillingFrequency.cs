﻿using Bridgepay.RecurringBilling.Models.Interfaces;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class BillingFrequency : BaseEntity, IHasMerchantId
    {
        //constructor here to overcome the DateTime to DateTime2 error with SQL and .Net
        public BillingFrequency()
        {
            this.Contracts = new List<Contract>();
        }

        [DataMember]public string FrequencyName { get; set; }
        [DataMember]public string Description {get; set;}
        [DataMember]public int MerchantId { get; set; }
        [DataMember]public int IntervalId { get; set; }
        [DataMember]public bool? NoEnd { get; set; }
        [DataMember]public int? Frequency { get; set; }
        [DataMember]public int? DayMonth { get; set; }
        [DataMember]public int? WeekMonth { get; set; }
        [DataMember]public int? Month { get; set; }
        [DataMember]public string Days { get; set; }
        [DataMember]public bool IsActive { get; set; }
        [DataMember]public bool Template { get; set; }

        //Navigation
        [DataMember]public virtual List<Contract> Contracts { get; set; }
        [DataMember]public virtual Interval Interval { get; set; }
     }
}
