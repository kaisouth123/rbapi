﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class PaymentMethod
    {
        [DataMember] public System.Guid PaymentMethodId { get; set; } // PaymentMethodId (Primary key)
        [DataMember] public byte PaymentMethodType { get; set; } // PaymentMethodType (Primary key)
        [DataMember] public string CardType { get; set; } // CardType (length: 20)
        [DataMember] public string Description { get; set; } // Description (length: 65)
        [DataMember] public string ExpirationDate { get; set; } // ExpirationDate (length: 4)
        [DataMember] public string LastFour { get; set; } // LastFour (length: 4)
        [DataMember] public string Token { get; set; } // Token (Primary key) (length: 30)
        [DataMember] public string RoutingNo { get; set; } // RoutingNo (length: 15)
        [DataMember] public string AccountType { get; set; } // AccountType (length: 20)
        [DataMember] public string AccountHolderName { get; set; } // AccountHolderName (length: 50)
        [DataMember] public string AccountHolderAddress { get; set; } // AccountHolderAddress (length: 50)
        [DataMember] public string AccountHolderCity { get; set; } // AccountHolderCity (length: 50)
        [DataMember] public string AccountHolderState { get; set; } // AccountHolderState (length: 2)
        [DataMember] public string AccountHolderZip { get; set; } // AccountHolderZip (length: 10)
        [DataMember] public string AccountHolderPhone { get; set; } // AccountHolderPhone (length: 20)
        [DataMember] public string AccountHolderEmail { get; set; } // AccountHolderEmail (length: 50)
        [DataMember] public bool IsActive { get; set; } // IsActive (Primary key)
        [DataMember] public int Order { get; set; } // Order (Primary key)
        [DataMember] public System.Guid WalletId { get; set; } // WalletId (Primary key)
        [DataMember] public System.DateTime CreateDate { get; set; } // CreateDate (Primary key)
        [DataMember] public System.DateTime? UpdateDate { get; set; } // UpdateDate
        [DataMember] public int FailureCount { get; set; } // FailureCount (Primary key)
        [DataMember] public string LastResult { get; set; } // LastResult (length: 200)
        [DataMember] public int MaxFailures { get; set; } // MaxFailures (Primary key)
        [DataMember] public int? DetailId { get; set; } // DetailId
        [DataMember] public string ExData { get; set; } // ExData
        [DataMember] public int? PaymentKey { get; set; } // PaymentKey
        [DataMember] public virtual Wallet Wallet { get; set; }
    }
}
