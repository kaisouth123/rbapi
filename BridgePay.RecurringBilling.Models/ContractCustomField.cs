﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class ContractCustomField: EntityWithIntID
    {
        [DataMember]
        public int OwnerId { get; set; } // OwnerId (Primary key)
        [DataMember]
        public string TableName { get; set; } // TableName
        [DataMember]
        public int CustomFieldConfigId { get; set; } 
        [DataMember]
        public int EntityId { get; set; } // EntityId 
        [DataMember]
        public string FieldName { get; set; } // FieldName
        [DataMember]
        public string DisplayName { get; set; } // DisplayName
        [DataMember]
        public string Value { get; set; } // Value 
        [DataMember]
        public virtual Contract Contract { get; set; }
    }
}
