﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;


namespace Bridgepay.RecurringBilling.Models 
{
    public class AccountReference : BaseEntity
    {
        public int Id { get; set; }
        public int MerchantId { get; private set; }
        public string AccountName { get; private set; }
        public string AccountNumber { get; private set; }
        public DateTime CreatedOn { get; private set; }
        public DateTime ModifiedOn { get; set; }
        public bool IsActive { get; set; }

        //Navigation
        public virtual List<Contract> Contracts { get; set; }
        public virtual List<FinancialResponsibleParty> FinancialResponsibleParties { get; set; }
    }
}

