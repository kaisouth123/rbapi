﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class Wallet
    {
        [DataMember] public Guid WalletId { get; set; } // WalletId (Primary key)
        [DataMember] public Guid SiteId { get; set; } // SiteId
        [DataMember] public string Name { get; set; } // Name (length: 50)
        [DataMember] public string Description { get; set; } // Description
        [DataMember] public string CustomerAccountNumber { get; set; } // CustomerAccountNumber (length: 50)
        [DataMember] public bool IsActive { get; set; } // IsActive
        [DataMember] public DateTime CreateDate { get; set; } // CreateDate
        [DataMember] public DateTime? UpdateDate { get; set; } // UpdateDate
        [DataMember] public string CustomerWalletId { get; set; } // CustomerWalletID (length: 128)
        [DataMember] public virtual List<PaymentMethod> PaymentMethods { get; set; }
    }
}
