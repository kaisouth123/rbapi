﻿//using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class CustomFieldActivation : BaseEntity
    {
        //[Key]
        //[DataMember]
        //public int Id { get; set; }
        [DataMember]    
        public int CustomFieldConfigId { get; set; }
        [DataMember]
        public int OrganizationId { get; set; }
        [DataMember]
        public int CustomFieldId { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual CustomField CustomField { get; set; }
    }
}
