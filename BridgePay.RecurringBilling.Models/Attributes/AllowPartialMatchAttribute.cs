﻿using System;

namespace Bridgepay.RecurringBilling.Models.Attributes
{
    /// <summary>
    /// Denotes a property that supports filtering by partial matches. Should only be applied to string properties.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class AllowPartialMatchAttribute : Attribute
    {
    }
}
