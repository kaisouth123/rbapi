﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class Merchant : BaseEntity, IBaseEntity
    {
        [DataMember]public string Name { get; set; }
        [DataMember]public bool IsActive { get; set; }
        [DataMember]public int MerchantCode { get; set;}

        //Navigation
        [DataMember]public virtual List<Account> Accounts { get; set; }
        [DataMember]public virtual List<Product> Products { get; set; }
        [DataMember]public virtual List<BillingFrequency> BillingFrequencies { get; set; }
    }

}


