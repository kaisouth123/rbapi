﻿using Bridgepay.RecurringBilling.Models.Attributes;
using Bridgepay.RecurringBilling.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = false)]
    public class Contract : EntityWithIntID, IHasMerchantId
    {
        public Contract()
        {
            LastBillDate = null;
            EndDate = null;
            this.ChargeTransactionsAuxes = new List<ChargeTransactionsAux>();
            this.ScheduledPayments = new List<ScheduledPayment>();
            this.Charges = new List<Charge>();
        }

        [DataMember][AllowPartialMatch]public string ContractName { get; set; }
        [DataMember]public int? ProductId { get; set; }
        [DataMember]public int MerchantId { get; set; }
        [DataMember]public int BillingFrequencyId { get; set; }
        [DataMember]public int AccountId { get; set; }
        [DataMember]public int BillAmount { get; set; }
        [DataMember]public int TaxAmount { get; set; }
        [DataMember]public int? AmountRemaining { get; set; }
        [DataMember]public DateTime? CreatedOn { get; private set;}
        [DataMember]public DateTime? ModifiedOn { get; set; }
        [DataMember]public DateTime? LastBillDate { get; set; }
        [DataMember]public DateTime? NextBillDate { get; set; }
        [DataMember]public DateTime StartDate { get; set; }
        [DataMember]public DateTime? EndDate { get; set; }
        [DataMember]public int? NumFailures { get; set; }
        [DataMember]public int? MaxFailures { get; set; }
        [DataMember]public int? RetryWaitTime { get; set; }
        [DataMember]public bool? EmailCustomerAtFailure { get; set; }
        [DataMember]public bool? EmailMerchantAtFailure { get; set; }
        [DataMember]public bool? EmailCustomerReceipt { get; set; }
        [DataMember]public bool? EmailMerchantReceipt { get; set; }
        [DataMember]public bool IsActive { get; set; }
        [DataMember]public int? Status { get; set; }

        //Navigation
        [DataMember]public virtual List<ScheduledPayment> ScheduledPayments { get; set; }
        [DataMember][NotMapped]public virtual List<ChargeTransactionsAux> ChargeTransactionsAuxes { get; set; }
        [DataMember]public virtual Product Product { get; set; }
        [DataMember] public virtual List<Charge> Charges { get; set; }
        [DataMember]public virtual BillingFrequency BillingFrequency { get; set; }
        [DataMember]public virtual Account Account { get; set; }
        [IgnoreDataMember] public virtual List<ContractCustomField> ContractCustomFields { get; set; }

        /// <summary>
        /// Gets/sets the calculated number of payments for the contract.
        /// </summary>
        /// <remarks>
        [DataMember]
        public int? NumberOfPayments { get; set; }
    }

}
