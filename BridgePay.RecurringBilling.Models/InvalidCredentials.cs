﻿using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    public static class InvalidCredentials<T> where T : BaseEntity, new()
    {
        public static T GenerateModel()
        {
            T model = new T();
            model.Messages.Add(new ValidationMessage("Invalid Credentials", "Error",""));
            return model;
        }

        public static List<T> GenerateModelList()
        {
            T model = new T();
            model.Messages.Add(new ValidationMessage("Invalid Credentials", "Error",""));
            List<T> models = new List<T>();
            models.Add(model);
            return models;
        }

    }
}
