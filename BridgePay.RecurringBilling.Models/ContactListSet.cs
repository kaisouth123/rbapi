﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Models
{
    public class ContactListSet: BaseEntity, IEntitySet<Contact>
    {
        public ContactListSet()  : base()
        {
            this.EntityList = new List<Contact>();
        }

        public IEnumerable<Contact> EntityList {get; set;}
        public int Count { get; set; }
      
    }
}
