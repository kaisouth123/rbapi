﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bridgepay.RecurringBilling.Models
{
    public class ContractReference :BaseEntity
    {
        public ContractReference()
        {
            this.ChargeTransactionsAuxes = new List<ChargeTransactionsAux>();
            this.ScheduledPayments = new List<ScheduledPayment>();
            this.IsActive = true;
        }

        public string ContractName { get; private set; }
        public int ProductId { get; private set; }
        public int BillingFrequencyId { get; private set; }
        public int AccountId { get; private set; }
        public int BillAmount { get; private set; }
        public int TaxAmount { get; private set; }
        public int MaxAmount { get; private set; }
        public DateTime CreatedOn { get; private set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime LastBillDate { get; set; }
        public bool? EmailCustomerAtFailure { get; set; }
        public bool? EmailCustomerReceipt { get; set; }
        public bool? EmailMerchantAtFailure { get; set; }
        public bool? EmailMerchantReceipt { get; set; }
        public bool IsActive { get; set; }
        public bool HasScheduledPayment { get; set; }

        //Navigation
        public virtual List<ScheduledPayment> ScheduledPayments { get; set; }
        public virtual List<ChargeTransactionsAux> ChargeTransactionsAuxes { get; set; }
        public virtual Product Product { get; set; }
        public virtual BillingFrequency BillingFrequency { get; set; }
        public virtual AccountReference Account { get; set; }

    }

}
