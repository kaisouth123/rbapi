﻿//using Bridgepay.Core.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Bridgepay.RecurringBilling.Models;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class CustomFieldData : EntityWithIntID
    {
        //[Key]
        //[DataMember]
        //public int Id { get; set; }
        [DataMember]
        public int CustomFieldConfigId { get; set; }
        [DataMember]
        public string EntityId { get; set; }
        [DataMember]
        public string Value { get; set; }
        public virtual CustomField CustomField { get; set; }
        //public Contract Contract { get; set; }
    }
}
