﻿using System.Runtime.Serialization;
using Bridgepay.Core.Framework;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference=false)]
    public class EntityWithIntID : BaseEntity, IHasIntID
    {
        //[DataMember]
        //public int Id { get; set; }


        public EntityWithIntID() { }
        
    }
}
