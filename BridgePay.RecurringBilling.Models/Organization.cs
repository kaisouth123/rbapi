﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Bridgepay.Core.Framework;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract]
    public class Organization : Core.Framework.BaseEntity
    {
        [Key]
        [DataMember] public int Id { get; set; } // ID (Primary key)
        [DataMember] public string Name { get; set; } // NAME (length: 50)
        [DataMember] public string AddressLine1 { get; set; } // ADDRESS_LINE_1 (length: 128)
        [DataMember] public string AddressLine2 { get; set; } // ADDRESS_LINE_2 (length: 128)
        [DataMember] public string AddressLine3 { get; set; } // ADDRESS_LINE_3 (length: 128)
        [DataMember] public string City { get; set; } // CITY (length: 50)
        [DataMember] public string State { get; set; } // STATE (length: 3)
        [DataMember] public string PostalCode { get; set; } // POSTAL_CODE (length: 15)
        [DataMember] public string CountryCode { get; set; } // COUNTRY_CODE (length: 2)
        [DataMember] public string EmailAddress { get; set; } // EMAIL_ADDRESS (length: 100)
        [DataMember] public string PhoneNumber { get; set; } // PHONE_NUMBER (length: 15)
        [DataMember] public int? GatewayObjectId { get; set; } // GATEWAY_OBJECT_ID
        [DataMember] public int OrganizationTypeId { get; set; } // ORGANIZATION_TYPE_ID
        [DataMember] public int? ParentId { get; set; } // PARENT_ID
        [DataMember] public System.DateTime CreateDate { get; set; } // CREATE_DATE
        [DataMember] public System.DateTime? LastUpdated { get; set; } // LAST_UPDATED
        [DataMember] public string LastModifiedBy { get; set; } // LAST_MODIFIED_BY (length: 50)
        [DataMember] public System.DateTime? DeactivateDate { get; set; } // DEACTIVATE_DATE
        [DataMember] public bool? IsActive { get; set; } // IS_ACTIVE
        [DataMember] public string Notes { get; set; } // NOTES (length: 255)
        [DataMember] public string TimeZone { get; set; } // TIME_ZONE (length: 33)


        // Reverse navigation

        /// <summary>
        /// Child MerchantFees where [MERCHANT_FEE].[FEE_RECIPIENT_CODE] point to this entity (FK_FEE_RECIPIENT)
        /// </summary>
        //[IgnoreDataMember]
        //public virtual System.Collections.Generic.ICollection<MerchantFee> ReceivedFees { get; set; } // MERCHANT_FEE.FK_FEE_RECIPIENT

        /// <summary>
        /// Child MerchantFees where [MERCHANT_FEE].[ORGANIZATION_ID] point to this entity (FK_FEE_ORGANIZATION)
        /// </summary>
        //[DataMember]
        //public virtual System.Collections.Generic.ICollection<MerchantFee> MerchantFees { get; set; } // MERCHANT_FEE.FK_FEE_ORGANIZATION

        /// <summary>
        /// Child Organizations where [ORGANIZATION].[PARENT_ID] point to this entity (FK_Organization_Organization)
        /// </summary>
        [DataMember]
        public virtual System.Collections.Generic.ICollection<Organization> Children { get; set; } // ORGANIZATION.FK_Organization_Organization
                                                                                                   /// <summary>
                                                                                                   /// Child OrganizationContacts where [ORGANIZATION_CONTACT].[ORGANIZATION_ID] point to this entity (FK_ORGANIZATION_CONTACT_ORGANIZATION)
                                                                                                   /// </summary>
        // Foreign keys

        /// <summary>
        /// Parent Organization pointed by [ORGANIZATION].([ParentId]) (FK_Organization_Organization)
        /// </summary>
        public virtual Organization Parent { get; set; } // FK_Organization_Organization

        /// <summary>
        /// Parent OrganizationType pointed by [ORGANIZATION].([OrganizationTypeId]) (FK_Organization_OrganizationType)
        /// </summary>
        [DataMember]
        public virtual OrganizationType OrganizationType { get; set; } // FK_Organization_OrganizationType

        [DataMember]
        //public virtual MerchantAux MerchantAuxRecord { get; set; } // FK_Organization_OrganizationType
        public ICollection<CustomField> CustomFields { get; set; }
        public ICollection<CustomFieldActivation> CustomFieldActivations { get; set; }
        #region Unipay Tables

        //[DataMember]
        //[NotMapped]
        //public virtual List<CardsRealTimeProviderProfile> CardsRealTimeProviderProfiles { get; set; }
        //[DataMember]
        //[NotMapped]
        //public virtual List<GiftRealTimeProviderProfile> GiftRealTimeProviderProfiles { get; set; }
        //[DataMember]
        //[NotMapped]
        //public virtual List<ACHProviderProfile> ACHProviderProfiles { get; set; }
        //[DataMember]
        //[NotMapped]
        //public virtual List<TokenizationProviderProfile> TokenizationProviderProfiles { get; set; }
        //[DataMember]
        //[NotMapped]
        //public virtual ProcessingConfig ProcessingConfig { get; set; }

        #endregion

        public Organization() 
        {
            Children = new System.Collections.Generic.List<Organization>();
            //MerchantFees = new System.Collections.Generic.List<MerchantFee>();
            //ReceivedFees = new System.Collections.Generic.List<MerchantFee>();
        }
    }
}
