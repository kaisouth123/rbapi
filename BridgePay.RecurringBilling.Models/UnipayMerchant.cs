﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Bridgepay.RecurringBilling.Models
{
    [DataContract(IsReference = true)]
    public partial class UnipayMerchant : MinBaseEntity
    {

        public UnipayMerchant()
        {
            MerchantAccounts = new List<MerchantAccount>();
        }

        [Key]
        [DataMember] public int MerchantID { get; set; }
        [DataMember] public int? ResellerID { get; set; }
        [DataMember] public string Name { get; set; }
        [DataMember] public virtual Reseller Reseller { get; set; }
        [DataMember] public virtual List<MerchantAccount> MerchantAccounts { get; set; }

    }
}
