﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Common.Utilities
{
    public static class Utility
    {
        public static bool CheckIfNullOrDefault(this object objectValue, Type fieldType, bool searchFalse = false)
        {
            bool isNull = false;

            if (objectValue == null)
            {
                return true;
            }
            else if (fieldType.Name == typeof(Int32).Name)
            {
                if ((int)objectValue == 0)
                    return true;
            }
            else if (fieldType.Name == typeof(Int64).Name)
            {
                if (Convert.ToInt64(objectValue) == 0 || Convert.ToInt64(objectValue) == null)
                    return true;
            }
            else if (fieldType.Name == typeof(string).Name)
            {
                if (String.IsNullOrEmpty((String)objectValue))
                    return true;
            }
            else if (objectValue is ICollection)
            {
                return true;
            }
            else if (fieldType.Name == typeof(DateTime).Name)
            {
                DateTime date = (DateTime)objectValue;
                if (date == default(DateTime))
                    return true;
            }
            else if (fieldType.FullName == typeof(Boolean?).FullName)
            {
                Boolean? boolNull = (Boolean?)objectValue;
                if (boolNull == default(Boolean?))
                    return true;
            }
            else if (fieldType.FullName == typeof(int?).FullName)
            {
                int? intNull = (int?)objectValue;
                if (intNull == default(int?) || intNull == 0)
                    return true;
            }
            else if (fieldType.FullName == typeof(DateTime?).FullName)
            {
                DateTime? date = (DateTime?)objectValue;
                if (date == default(DateTime?))
                    return true;
            }
            else if (fieldType.Name == typeof(bool).Name && !searchFalse)
            {
                if ((bool)objectValue == false)
                    return true;
            }
            else if (fieldType.Name == typeof(Guid?).Name)
            {
                if ((Guid)objectValue == null)
                    return true;
            }

            return isNull;
        }

        public static T Clone<T>(T original, List<String> ignoreFields = null)
        {
            T tempMyClass = (T)Activator.CreateInstance(original.GetType());

            FieldInfo[] fis = original.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (FieldInfo fi in fis)
            {
                if (ignoreFields == null || !ignoreFields.Contains(fi.Name))
                {
                    object fieldValue = fi.GetValue(original);
                    if (fi.FieldType.Namespace != original.GetType().Namespace)
                        fi.SetValue(tempMyClass, fieldValue);
                    else
                        fi.SetValue(tempMyClass, Clone(fieldValue, ignoreFields));
                }
            }

            return tempMyClass;
        }

        public static object CopyFrom(this object model, object entity, List<String> ignoreFields = null, Dictionary<string, string> FieldMatching = null, bool CheckNull = false)
        {

            PropertyInfo[] DestProps = model.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            PropertyInfo[] SourceProps = entity.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (PropertyInfo property in SourceProps)
            {
                if ((ignoreFields == null || !ignoreFields.Contains(property.Name)) && DestProps.Where(c => c.Name == property.Name).FirstOrDefault() != null  && (FieldMatching == null && FieldMatching.Count < 1))
                {
                    PropertyInfo destProp = DestProps.Where(c => c.Name == property.Name).FirstOrDefault();
                    object fieldValue = property.GetValue(entity);

                    if (CheckNull && fieldValue.CheckIfNullOrDefault(property.PropertyType, true))
                        continue;

                    try
                    {
                        destProp.SetValue(model, Convert.ChangeType(fieldValue, destProp.PropertyType));
                    }
                    catch { }

                }
                else if(ignoreFields == null || !ignoreFields.Contains(property.Name) &&  FieldMatching != null)
                {
                    if(FieldMatching.Keys.Contains(property.Name))
                    {
                        string Name = FieldMatching[property.Name];
                        PropertyInfo destProp = DestProps.Where(c => c.Name == Name).FirstOrDefault();
                        object fieldValue = property.GetValue(entity);

                        if (CheckNull && fieldValue.CheckIfNullOrDefault(property.PropertyType, true))
                            continue;
                       
                        try
                        {
                            destProp.SetValue(model, fieldValue);
                        }
                        catch { }

                    }
                    else 
                    {
                        PropertyInfo destProp = DestProps.Where(c => c.Name == property.Name).FirstOrDefault();

                        object fieldValue = property.GetValue(entity);
                        if (CheckNull && fieldValue.CheckIfNullOrDefault(property.PropertyType, true))
                            continue;

                        try
                        {
                            destProp.SetValue(model, fieldValue);
                        }
                        catch { }
                    }
                }
            }

            return model;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }
    }
}
