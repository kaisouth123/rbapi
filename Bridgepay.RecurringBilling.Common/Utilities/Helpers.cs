﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Bridgepay.RecurringBilling.Common.Utilities
{
    public static class LogUtility
    {
        public static ILog log;
        static LogUtility()
        {
            string directory = AppDomain.CurrentDomain.RelativeSearchPath ?? AppDomain.CurrentDomain.BaseDirectory;
            string logFile = directory + "\\" + @"log4net.config";
            if (File.Exists(logFile))
            {
                FileInfo info = new FileInfo(logFile);
                XmlConfigurator.ConfigureAndWatch(info);
            }
                
            log = LogManager.GetLogger(typeof(HttpApplication));
        }
    }

}