﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridgepay.RecurringBilling.Common.Utilities
{
    public static class Logger
    {
        public static void LogEvent(ILog log, string methodName, LogType logtype, String message = null, Exception ex = null)
        {
            if(logtype == LogType.Error)
            {
                if (ex != null)
                    log.Error(methodName, ex);
                else
                    log.Error(methodName + ": " + message);
            }
            else if(logtype == LogType.Info)
            {
                log.Info(methodName + ": " + message);
            }              
        }

        public static void LogLogin(ILog log, string methodName, string user, DateTime time, bool success, string system, String ip = "", String message = "")
        {
            if (success)
                log.Info("Login Success for User: " + user + " at: " + time.ToString() + ", system: " + system + ", method: " + methodName + ", from: " + ip + ", message: " + message);
            else
                log.Warn("Login Failure for User: " + user + " at: " + time.ToString() + ", system: " + system + ", method: " + methodName + ", from: " + ip + ", message: " + message);
        }

        public static void LogTransaction(ILog log, string methodName, string system, bool success, int MerchantId, string MerchantName, int Amount, string lastFour, string Type, string message)
        {
            if (success)
                log.Info("Successful " + Type + "Transaction " + "from " + system + " " + methodName + " for " + Amount.ToString() + ". Merchant: " + MerchantId.ToString() + " " + MerchantName + ", AuthResponse: " + message);
            else
                log.Warn("Unsuccessful " + Type + "Transaction " + "from " + system + " " + methodName + " for " + Amount.ToString() + ". Merchant: " + MerchantId.ToString() + " " + MerchantName + ", AuthResponse: " + message);
        }
    }

    public enum LogType
    {
        Error = 1,
        Info = 2,
    }

    public enum LogMode
    {
        Verbose = 1,
        Normal = 2,
        ErrorOnly = 3,
    }
}
